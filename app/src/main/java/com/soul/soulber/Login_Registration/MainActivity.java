package com.soul.soulber.Login_Registration;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.firebase.iid.FirebaseInstanceId;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.Dependent.DependentSearchActivity;
import com.soul.soulber.Login_Registration.Registerpages.Registeras;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;

import com.soul.soulber.databinding.ActivityMainBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class MainActivity extends AppCompatActivity {

    private MainActivity activity = this;
    private LoadingDialog loadingDialog;
    private Session session;
    private ActivityMainBinding binding;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        onClick();
        getFirebaseTokenAndDeviceID();

    }

    private void onClick() {

        binding.Signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkLoginValidation();
            }
        });

        binding.Forgtpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                startActivity(new Intent(MainActivity.this, Forgotpassword.class));
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
            }
        });

        binding.lnrRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                startActivity(new Intent(MainActivity.this, Registeras.class));
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
            }
        });
    }

    private void checkLoginValidation() {
        if (TextUtils.isEmpty(binding.etxEmail.getText().toString().trim())) {
            binding.etxEmail.setError("Enter email id");
            binding.etxEmail.requestFocus();
        } else if (TextUtils.isEmpty(binding.etxPassword.getText().toString().trim())) {
            binding.etxPassword.setError("Enter password");
            binding.etxPassword.requestFocus();
        } else if (binding.etxPassword.getText().toString().trim().length() != 6) {
            binding.etxPassword.setError("Enter valid password");
            binding.etxPassword.requestFocus();
        } else {
            hitMobileLogin(binding.etxEmail.getText().toString().trim(), binding.etxPassword.getText().toString().trim());
        }
    }

    private void hitMobileLogin(String email, String password) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.email, email);
        j.addString(P.password, password);
        j.addString(P.fcm_token, new Session(activity).getString(P.fcm_token));

        Api.newApi(activity, API.BaseUrl + "user/login").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        session.addJson(Config.userData, data);
                        session.addBool(Config.userLogin, true);
                        String usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
                        if (usertype_id.equals(Config.GUARDIAN)) {
                            jumpToDependentSearch();
                        } else {
                            jumpToMain();
                        }
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitMobileLogin");
    }


    private void jumpToDependentSearch() {
        Intent intent = new Intent(activity, DependentSearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void jumpToMain() {
        Intent intent = new Intent(activity, BaseAcivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }


    private void onBackPress() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        H.showMessage(activity, "Please click BACK again to exit");
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void getFirebaseTokenAndDeviceID() {
        String androidID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        new Session(activity).addString(P.device_id, androidID);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            new Session(activity).addString(P.fcm_token, newToken);
            Log.e("TAG", "getFirebaseToken: " + newToken);
        });
    }

    @Override
    public void onBackPressed() {
        onBackPress();
    }

}