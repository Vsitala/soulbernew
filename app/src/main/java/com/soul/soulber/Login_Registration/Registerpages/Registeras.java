package com.soul.soulber.Login_Registration.Registerpages;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.CountrySelectionAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityRegisterasBinding;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;

public class Registeras extends AppCompatActivity {

    private Registeras activity = this;
    private ActivityRegisterasBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;

    private ArrayList<Register_Modle> Register_Modle;
    private int image[] = {R.drawable.check};
    private String name[] = {"You will become better in stress-coping mechanisms",
            "You will have a better mental health",
            "You will have a better physical health",
            "You will have a better social life", "You will have more success"};

    private String userType = Config.DEPENDENT;

    private String amount = "";
    private String dependentAmount = "";
    private String guardianAmount = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_registeras);
        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        binding.viewPagerRegister.setLayoutManager(new LinearLayoutManager(Registeras.this, LinearLayoutManager.VERTICAL, false));
        Register_Modle = new ArrayList<>();
        for (int i = 0; i < name.length; i++) {
            Register_Modle register = new Register_Modle();
            register.setImage(image[0]);
            register.setName(name[i]);
            Register_Modle.add(register);

        }
        Adapter_Register_guardian_dependent object = new Adapter_Register_guardian_dependent(Registeras.this, Register_Modle);
        binding.viewPagerRegister.setAdapter(object);

        onClick();
        hitPayment();
    }

    private void onClick() {

        binding.dependent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userType = Config.DEPENDENT;
                amount = dependentAmount;
                checkAmount(amount);
                binding.viewDependent.setVisibility(View.VISIBLE);
                binding.viewGuardian.setVisibility(View.GONE);
            }
        });

        binding.guardian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userType = Config.GUARDIAN;
                amount = guardianAmount;
                checkAmount(amount);
                binding.viewDependent.setVisibility(View.GONE);
                binding.viewGuardian.setVisibility(View.VISIBLE);
            }
        });

        binding.btRegistertopay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, Register_details.class);
                intent.putExtra(Config.registerAmount, amount);
                intent.putExtra(Config.registerUserType, userType);
                ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
                startActivity(intent, options.toBundle());
            }
        });

    }

    private void hitPayment() {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "Common/subscription_prices")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    Json data = json.getJson(P.data);
                    JsonList prices = data.getJsonList(P.prices);
                    dependentAmount = prices.get(0).getString(P.subscription_amt);
                    guardianAmount = prices.get(1).getString(P.subscription_amt);
                    amount = dependentAmount;
                    checkAmount(amount);
                })
                .run("hitPayment");
    }


    private void checkAmount(String amount) {
        if (amount.equals("0")) {
            binding.btRegistertopay.setText("Register");
        } else {
            binding.btRegistertopay.setText("Register to pay $" + amount);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}