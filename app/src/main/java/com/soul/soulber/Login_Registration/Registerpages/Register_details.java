package com.soul.soulber.Login_Registration.Registerpages;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.Dependent.DependentSearchActivity;
import com.soul.soulber.JobsHouseFriends.JobsFragment;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.activity.PaymentViewActivity;
import com.soul.soulber.activity.PaymentWebViewActivity;
import com.soul.soulber.adapter.CountrySelectionAdapter;
import com.soul.soulber.adapter.SelectCityAdapter;
import com.soul.soulber.adapter.SkillsSearchAdapter;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityRegisterDetailsBinding;
import com.soul.soulber.model.JobCategoryModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;
import com.soul.soulber.util.Validation;

import java.util.ArrayList;
import java.util.List;

public class Register_details extends AppCompatActivity implements SelectCityAdapter.onClick {

    private Register_details activity = this;
    private ActivityRegisterDetailsBinding binding;
    private LoadingDialog loadingDialog;
    private Session session;
    private String registerAmount = "";
    private String registerUserType = "";
    private String countryCode = "";
    private String cityID = "";
    private List<CountryModel> countryList;
    private List<CountryModel> countryCodeList;

    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_details);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        registerAmount = getIntent().getStringExtra(Config.registerAmount);
        registerUserType = getIntent().getStringExtra(Config.registerUserType);
        binding.txtRegisterAmount.setText("$" + registerAmount);

        countryList = new ArrayList<>();
        countryCodeList = new ArrayList<>();

        String message = "I accept privacy policy & Term Conditions Click here.";
        binding.txtCheck.setText(message);

        if (binding.txtCheck.getText().toString().contains("Click here.")) {
            setClickableHighLightedText(binding.txtCheck, "Click here.", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse("https://soulber.co/privacy_policy.html"));
                    startActivity(i);
                }
            });
        }

        binding.etxCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                cityDialog();
            }
        });

        if (registerAmount.equals("0")) {
            binding.lnrProceed.setVisibility(View.GONE);
        } else {
            binding.lnrProceed.setVisibility(View.VISIBLE);
        }

        hideButton();
        binding.checkPrivacy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    visibleButton();
                } else {
                    hideButton();
                }
            }
        });

        hitAllCountries();
        hitCommonData();
        onClick();


    }


    private void hideButton() {
        binding.btnPayNow.setBackgroundResource(R.drawable.buttonnosignup);
        binding.btnPayNow.setTextColor(getResources().getColor(R.color.inactive));
    }

    private void visibleButton() {
        binding.btnPayNow.setBackgroundResource(R.drawable.buttonsignup);
        binding.btnPayNow.setTextColor(getResources().getColor(R.color.white));
    }

    private void cityDialog() {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_select_city);

        EditText etxSearch = dialog.findViewById(R.id.etxSearch);
        RecyclerView recyclerCity = dialog.findViewById(R.id.recyclerCity);
        recyclerCity.setLayoutManager(new LinearLayoutManager(activity));
        recyclerCity.setHasFixedSize(true);

        SelectCityAdapter adapter = new SelectCityAdapter(activity, countryList);
        recyclerCity.setAdapter(adapter);

        etxSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (!TextUtils.isEmpty(newText)) {
                    List<CountryModel> list = new ArrayList<CountryModel>();
                    for (CountryModel model : countryList) {
                        if (model.getCity().toLowerCase().contains(newText.toLowerCase())) {
                            list.add(model);
                        }
                    }
                    SelectCityAdapter adapter = new SelectCityAdapter(activity, list);
                    recyclerCity.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    SelectCityAdapter adapter = new SelectCityAdapter(activity, countryList);
                    recyclerCity.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void citySelection(CountryModel model) {

        cityID = model.getCity_id();
        binding.etxCity.setText(model.getCity());
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void onClick() {

        binding.spinnerCountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = countryCodeList.get(position);
                countryCode = model.getCity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//        binding.spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (countryList.size() != 0) {
//                    CountryModel model = countryList.get(position);
//                    if (model.getCity_id().equals("0")) {
//                        cityID = "";
//                    } else {
//                        cityID = model.getCity_id();
//                    }
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.lnrSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(activity, MainActivity.class));
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
            }
        });

        binding.btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkValidation();
            }
        });
    }

    private void checkValidation() {
        if (TextUtils.isEmpty(binding.etxName.getText().toString().trim())) {
            binding.etxName.setError("Enter your name");
            binding.etxName.requestFocus();
        } else if (TextUtils.isEmpty(binding.etxEmail.getText().toString().trim())) {
            binding.etxEmail.setError("Enter email id");
            binding.etxEmail.requestFocus();
        } else if (!Validation.validEmail(binding.etxEmail.getText().toString().trim())) {
            binding.etxEmail.setError("Enter valid email id");
            binding.etxEmail.requestFocus();
        } else if (TextUtils.isEmpty(binding.etxPassword.getText().toString().trim())) {
            binding.etxPassword.setError("Enter password");
            binding.etxPassword.requestFocus();
        } else if (binding.etxPassword.getText().toString().trim().length() < 6) {
            binding.etxPassword.setError("Enter valid password");
            binding.etxPassword.requestFocus();
        } else if (TextUtils.isEmpty(binding.etxNumber.getText().toString().trim())) {
            binding.etxNumber.setError("Enter phone number");
            binding.etxNumber.requestFocus();
        } else if (binding.etxNumber.getText().toString().trim().length() < 10 || binding.etxNumber.getText().toString().trim().length() > 10) {
            binding.etxNumber.setError("Enter valid phone number");
            binding.etxNumber.requestFocus();
        } else if (cityID.equals("") || cityID.equals("0")) {
            H.showMessage(activity, "Please select city");
        } else if (!binding.checkPrivacy.isChecked()) {
            H.showMessage(activity, "Please allow privacy policy and term conditions");
        } else {
            if (registerAmount.equals("0")) {
                hitFreeMobileRegister();
            } else {
                hitMobileRegister();
            }
        }
    }


    private void hitAllCountries() {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "Common/all_countries")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    Json data = json.getJson(P.data);
                    JsonList list = data.getJsonList(P.list);
                    if (list != null && list.size() != 0) {
                        for (Json jsonValue : list) {
                            String phonecode = jsonValue.getString(P.phonecode);
                            countryCodeList.add(new CountryModel("", "+" + phonecode));
                        }
                        CountrySelectionAdapter adapter = new CountrySelectionAdapter(activity, countryCodeList);
                        binding.spinnerCountryCode.setEnabled(false);
                        binding.spinnerCountryCode.setClickable(false);
                        binding.spinnerCountryCode.setAdapter(adapter);
                    }
                })
                .run("hitAllCountries");
    }

    private void hitCommonData() {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "Common/all_common_data")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

//                    CountryModel model = new CountryModel();
//                    model.setCity_id("0");
//                    model.setCity("Select City");
//                    countryList.add(model);

                    Json citiesObject = json.getJson(P.cities);
                    Json dataCity = citiesObject.getJson(P.data);
                    JsonList citylist = dataCity.getJsonList(P.citylist);
                    if (citylist != null && citylist.size() != 0) {
                        for (Json jsonValue : citylist) {
                            CountryModel modelNew = new CountryModel();
                            modelNew.setCity_id(jsonValue.getString(P.city_id));
                            modelNew.setCity(jsonValue.getString(P.city));
                            countryList.add(modelNew);
                        }
                    }

//                    CountrySelectionAdapter adapter = new CountrySelectionAdapter(activity, countryList);
//                    binding.spinnerCountry.setAdapter(adapter);


                })
                .run("hitCommonData");
    }

    private void hitMobileRegister() {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.name, binding.etxName.getText().toString().trim());
        j.addString(P.email, binding.etxEmail.getText().toString().trim());
        j.addString(P.contact, binding.etxNumber.getText().toString().trim());
        j.addString(P.city_id, cityID);
        j.addString(P.usertype_id, registerUserType);
        j.addString(P.password, binding.etxPassword.getText().toString().trim());

        Api.newApi(activity, API.BaseUrl + "user/register_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        Json jsonData = json.getJson(P.data);
                        Json order_data_json = jsonData.getJson(P.order_data);
                        String id = order_data_json.getString(P.id);
                        String payment_form_url = order_data_json.getString(P.payment_form_url);
                        String token = order_data_json.getString(P.token);

                        Intent intent = new Intent(activity, PaymentViewActivity.class);
                        intent.putExtra(P.name, binding.etxName.getText().toString().trim());
                        intent.putExtra(P.email, binding.etxEmail.getText().toString().trim());
                        intent.putExtra(P.contact, countryCode + " " + binding.etxNumber.getText().toString().trim());
                        intent.putExtra(P.order_id, id);
                        intent.putExtra(P.payment_form_url, payment_form_url);
                        intent.putExtra(P.registerAmount, registerAmount);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
                        startActivity(intent, options.toBundle());

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitMobileRegister");
    }

    private void hitFreeMobileRegister() {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.name, binding.etxName.getText().toString().trim());
        j.addString(P.email, binding.etxEmail.getText().toString().trim());
        j.addString(P.contact, binding.etxNumber.getText().toString().trim());
        j.addString(P.city_id, cityID);
        j.addString(P.usertype_id, registerUserType);
        j.addString(P.password, binding.etxPassword.getText().toString().trim());

        Api.newApi(activity, API.BaseUrl + "user/free_register").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        Json data = json.getJson(P.data);
                        session.addJson(Config.userData, data);
                        session.addBool(Config.userLogin, true);
                        String usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
                        if (usertype_id.equals(Config.GUARDIAN)) {
                            jumpToDependentSearch();
                        } else {
                            jumpToMain();
                        }
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitFreeMobileRegister");
    }


    private void jumpToDependentSearch() {
        Intent intent = new Intent(activity, DependentSearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void jumpToMain() {
        Intent intent = new Intent(activity, BaseAcivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    public void setClickableHighLightedText(TextView tv, String textToHighlight, View.OnClickListener onClickListener) {
        String tvt = tv.getText().toString();
        int ofe = tvt.indexOf(textToHighlight, 0);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                if (onClickListener != null) onClickListener.onClick(textView);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(tv.getContext().getResources().getColor(R.color.lightBlue));
                ds.setUnderlineText(true);
            }
        };
        SpannableString wordToSpan = new SpannableString(tv.getText());
        for (int ofs = 0; ofs < tvt.length() && ofe != -1; ofs = ofe + 1) {
            ofe = tvt.indexOf(textToHighlight, ofs);
            if (ofe == -1)
                break;
            else {
                wordToSpan.setSpan(clickableSpan, ofe, ofe + textToHighlight.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE);
                tv.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}