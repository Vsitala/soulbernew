package com.soul.soulber.Login_Registration.Registerpages;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.soul.soulber.R;

import java.util.ArrayList;

public class Adapter_Register_guardian_dependent extends RecyclerView.Adapter<Adapter_Register_guardian_dependent.ViewHolder> {
    private ArrayList<Register_Modle> product;
    private Context context;
    private int selectedItem;

    public Adapter_Register_guardian_dependent(Context context, ArrayList<Register_Modle> product) {
        this.product = product;
        this.context = context;
        selectedItem = 0;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.register_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapter_Register_guardian_dependent.ViewHolder viewHolder, int position) {

        viewHolder.registername.setText(product.get(position).getName());


        Glide
                .with(context)

                .load(product.get(position).getImage())
                //cropping center image
                .into(viewHolder.registerimage);
//        if (selectedItem == position) {
//            viewHolder.addtofavorate.setImageResource(R.drawable.heart);
//
//        }
//        else
//        {
//            viewHolder.addtofavorate.setBackgroundColor(Color.TRANSPARENT);
//        }
//        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int previousItem = selectedItem;
//                selectedItem = position;
//
//               notifyItemChanged(previousItem);
//               notifyItemChanged(position);
//           }
//       });


    }

    @Override
    public int getItemCount() {
        return product.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView registerimage;
        private TextView registername;

        public ViewHolder(View view) {
            super(view);
            registername = view.findViewById(R.id.registername);

            registerimage = view.findViewById(R.id.registerimage);

        }

    }
}