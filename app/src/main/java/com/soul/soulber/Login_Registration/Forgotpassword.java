package com.soul.soulber.Login_Registration;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.R;
import com.soul.soulber.WelcomeActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityForgotpasswordBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;
import com.soul.soulber.util.Validation;

public class Forgotpassword extends AppCompatActivity {

    private Forgotpassword activity = this;
    private ActivityForgotpasswordBinding binding;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgotpassword);

        iniView();
    }

    private void iniView(){

        loadingDialog = new LoadingDialog(activity);

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkValidation();
            }
        });

        binding.onbackForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackClick();
            }
        });
    }

    private void checkValidation(){
        if (TextUtils.isEmpty(binding.etxEmail.getText().toString().trim())){
            binding.etxEmail.setError("Enter email id");
            binding.etxEmail.requestFocus();
        }else if (!Validation.validEmail(binding.etxEmail.getText().toString().trim())){
            binding.etxEmail.setError("Enter valid email id");
            binding.etxEmail.requestFocus();
        }else {
            hitForgetPassLogin(binding.etxEmail.getText().toString().trim());
        }
    }

    private void hitForgetPassLogin(String email) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.email, email);

        Api.newApi(activity, API.BaseUrl + "user/forgot_password").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        H.showMessage(activity,json.getString(P.msg));
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                onBackClick();
                            }
                        },1500);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitForgetPassLogin");
    }

    private void onBackClick(){
        startActivity(new Intent(activity, MainActivity.class));
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
        finish();
    }

    @Override
    public void onBackPressed() {
        onBackClick();
    }
}
