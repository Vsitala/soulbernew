package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.ForumAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityForumBaseBinding;
import com.soul.soulber.model.ForumModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class ForumActivity extends AppCompatActivity {

    private ForumActivity activity = this;
    private ActivityForumBaseBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private ForumAdapter adapter;
    private List<ForumModel> forumModelList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forum_base);
        initView();
    }

    private void initView() {

        Config.FORUM_ACTION = false;

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        forumModelList = new ArrayList<>();
        adapter = new ForumAdapter(activity, forumModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerForum.setLayoutManager(linearLayoutManager);
        binding.recyclerForum.setHasFixedSize(true);
        binding.recyclerForum.setAdapter(adapter);

        hitForumData(pageCount, usertype_id);
        setPagination();
        onClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.FORUM_ACTION) {
            Config.FORUM_ACTION = false;
            pageCount = 1;
            hitForumData(pageCount, usertype_id);
        }
    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

    }

    private void setPagination() {
        binding.recyclerForum.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (forumModelList != null && !forumModelList.isEmpty()) {
                        if (forumModelList.size() < count) {
                            pageCount++;
                            hitForumData(pageCount, usertype_id);
                        }
                    }
                }
            }
        });
    }

    private void hitForumData(int pageCount, String usertype_id) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "forums/show_forums_list?page=" + pageCount + "&per_page=20&usertype=" + usertype_id)
                .setMethod(Api.GET)
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {
                        ProgressView.dismiss(loadingDialog);
                        H.showMessage(activity, "On error is called");
                        checkData();
                    }
                })
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        ProgressView.dismiss(loadingDialog);

                        if (json.getInt(P.status) == 1) {

                            Json data = json.getJson(P.data);
                            String forums_path_img = data.getString(P.forums_path_img);

                            int num_rows = data.getInt(P.num_rows);
                            try {
                                count = num_rows;
                            } catch (Exception e) {
                                count = 0;
                            }

                            if (pageCount == 1) {
                                forumModelList.clear();
                                adapter.notifyDataSetChanged();
                            }

                            JsonList list = data.getJsonList(P.list);

                            if (list != null && list.size() != 0) {
                                for (Json jsonValue : list) {
                                    ForumModel model = new ForumModel();
                                    model.setId(jsonValue.getString(P.id));
                                    model.setForum_text(jsonValue.getString(P.forum_text));
                                    model.setUpvotes(jsonValue.getString(P.upvotes));
                                    model.setDownvotes(jsonValue.getString(P.downvotes));
                                    model.setComments(jsonValue.getString(P.comments));
                                    model.setIs_delete(jsonValue.getString(P.is_delete));
                                    model.setAdd_date(jsonValue.getString(P.add_date));
                                    model.setIs_upvoted(jsonValue.getString(P.is_upvoted));
                                    model.setIs_downvoted(jsonValue.getString(P.is_downvoted));
                                    model.setImage(forums_path_img + "/" + jsonValue.getString(P.image));
                                    forumModelList.add(model);
                                }
                                adapter.notifyDataSetChanged();
                            }
                            checkData();
                        } else {
                            checkData();
                            H.showMessage(activity, "On error is called");
                        }

                    }
                })
                .run("hitForumData", token);

    }


    private void checkData() {
        if (forumModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

}