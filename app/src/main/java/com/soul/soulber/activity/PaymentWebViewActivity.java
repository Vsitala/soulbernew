package com.soul.soulber.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.Dependent.DependentSearchActivity;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityPaymentWebviewBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class PaymentWebViewActivity extends AppCompatActivity {

    private PaymentWebViewActivity activity = this;
    private ActivityPaymentWebviewBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;

    private String orderId;
    private String payment_form_url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_webview);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        orderId = getIntent().getStringExtra(P.order_id);
        payment_form_url = getIntent().getStringExtra(P.payment_form_url);

        binding.webView.setWebViewClient(new WebClient());
        binding.webView.getSettings().setJavaScriptEnabled(true);
        binding.webView.loadUrl(payment_form_url);

        onClick();

    }

    private void onClick() {

        binding.btnCloseSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToLogin();
            }
        });

        binding.btnCloseFail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });
    }

    private class WebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.e("TAG", "onPageFinished: " + url);
            if (url.contains("payment_success")) {
                binding.webView.setVisibility(View.GONE);
                binding.lnrFail.setVisibility(View.GONE);
                binding.lnrSuccess.setVisibility(View.VISIBLE);
                hitVerifyPayment(orderId, binding.txtSuccessMessage);
            } else if (url.contains("payment_failed")) {
                binding.webView.setVisibility(View.GONE);
                binding.lnrSuccess.setVisibility(View.GONE);
                binding.lnrFail.setVisibility(View.VISIBLE);
                hitVerifyPayment(orderId, binding.txtCancelMessage);
            }
        }
    }


    private void jumpToLogin() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void hitVerifyPayment(String orderId, TextView textView) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.order_id, orderId);

        Api.newApi(activity, API.BaseUrl + "user/verify_register_payment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
//                    if (json.getInt(P.status) == 1) {
//                        String msg = json.getString(P.msg);
//                        textView.setText(msg);
//                    } else {
//                        String error = json.getString(P.error);
//                        textView.setText(error);
//                    }

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        Json data = json.getJson(P.data);
                        session.addJson(Config.userData, data);
                        session.addBool(Config.userLogin, true);
                        String usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
                        if (usertype_id.equals(Config.GUARDIAN)) {
                            jumpToDependentSearch();
                        } else {
                            jumpToMain();
                        }
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }

                })
                .run("hitVerifyPayment");
    }

    private void jumpToDependentSearch() {
        Intent intent = new Intent(activity, DependentSearchActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void jumpToMain() {
        Intent intent = new Intent(activity, BaseAcivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        binding.webView.onPause();
        binding.webView.pauseTimers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.webView.onResume();
        binding.webView.resumeTimers();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}