package com.soul.soulber.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.SearchDependentAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentAddConnectionBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class DependentAddConnectionActivity extends AppCompatActivity implements SearchDependentAdapter.onClick {

    private ActivityDependentAddConnectionBinding binding;
    private DependentAddConnectionActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private SearchDependentAdapter adapter;
    private List<DependentModel> dependentModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dependent_add_connection);

        initView();

    }

    private void initView() {

        Config.CONNECTION_APPLY = false;
        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        dependentModelList = new ArrayList<>();
        adapter = new SearchDependentAdapter(activity, dependentModelList, userID, false);
        binding.recyclerConnection.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerConnection.setHasFixedSize(true);
        binding.recyclerConnection.setNestedScrollingEnabled(true);
        binding.recyclerConnection.setAdapter(adapter);

        onClick();

    }

    private void onClick() {

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.etxSearch.getText().toString().trim())) {
                    H.showMessage(activity, "Please enter name or email");
                } else {
                    if (binding.etxSearch.getText().toString().trim().contains("@")) {
                        hitSearchDependent(binding.etxSearch.getText().toString().trim(), "", token);
                    } else {
                        hitSearchDependent("", binding.etxSearch.getText().toString().trim(), token);
                    }
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            if (!TextUtils.isEmpty(binding.etxSearch.getText().toString().trim())) {
                if (binding.etxSearch.getText().toString().trim().contains("@")) {
                    hitSearchDependent(binding.etxSearch.getText().toString().trim(), "", token);
                } else {
                    hitSearchDependent("", binding.etxSearch.getText().toString().trim(), token);
                }
            }
        }
    }

    @Override
    public void onConnect(DependentModel model, Button button) {
        hitConnect(model.getDependent_id(), token, button);
    }

    @Override
    public void onRemove(DependentModel model, Button button) {
        hitRemove(model.getConnection_id(), token, button);
    }

    @Override
    public void onAccept(DependentModel model, Button button) {
        hitAction(model, token, button);
    }

    private void hitSearchDependent(String email, String name, String token) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.name, name);
        j.addString(P.email, email);

        Api.newApi(activity, API.BaseUrl + "userprofiles/search_depenedent").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    dependentModelList.clear();
                    adapter.notifyDataSetChanged();

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        Json dependentlist = data.getJson(P.dependentlist);
                        JsonList connectable = dependentlist.getJsonList(P.connectable);
                        JsonList newList = dependentlist.getJsonList(P.newList);

                        if (connectable != null && connectable.size() != 0) {
                            for (Json jsonData : connectable) {

                                DependentModel model = new DependentModel();
                                model.setDependent_id(jsonData.getString(P.dependent_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                dependentModelList.add(model);
                            }
                        }

                        if (newList != null && newList.size() != 0) {
                            for (Json jsonData : newList) {

                                DependentModel model = new DependentModel();
                                model.setDependent_id(jsonData.getString(P.dependent_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                dependentModelList.add(model);
                            }
                        }

                        adapter.notifyDataSetChanged();

                        if (dependentModelList.isEmpty()) {
                            binding.txtMessage.setText("Showing 0 result for " + binding.etxSearch.getText().toString());
                        } else {
                            binding.txtMessage.setText("Showing " + dependentModelList.size() + " result for " + binding.etxSearch.getText().toString());
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitSearchDependent", token);
    }

    private void checkData() {
        if (dependentModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    private void hitConnect(String id, String token, Button button) {

        ProgressView.show(activity, loadingDialog);
        String apiUrl = "";

        Json j = new Json();
        apiUrl = "dependent/connection/connect_dependent";
        j.addString(P.to_dependent_id, id);

        Api.newApi(activity, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        button.setText("Requested");
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }


    private void hitRemove(String id, String token, Button button) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        String apiUrl = "";

        apiUrl = "dependent/connection/remove_dependent_connection";
        j.addString(P.connection_id, id);

        Api.newApi(activity, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        button.setText("Connect");
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }

    private void hitAction(DependentModel model, String token, Button txtAction) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, model.getConnection_id());

        if (txtAction.getText().toString().equals("Accept")) {
            j.addString(P.action_id, "1");
        } else if (txtAction.getText().toString().equals("Reject")) {
            j.addString(P.action_id, "2");
        }

        Api.newApi(activity, API.BaseUrl + "dependent/connection/dependent_connection_action").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        if (txtAction.getText().toString().equals("Accept")) {
                            txtAction.setText("Reject");
                        } else if (txtAction.getText().toString().equals("Reject")) {
                            txtAction.setText("Accept");
                        }
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("onAction", token);
    }

}