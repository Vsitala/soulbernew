package com.soul.soulber.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.SpannedGridLayoutManagerNew;
import com.soul.soulber.adapter.UserGreedPostAdapter;
import com.soul.soulber.adapter.UserVerticalPostAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentProfileBinding;
import com.soul.soulber.model.UserPostModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class DependentProfileActivity extends AppCompatActivity implements UserGreedPostAdapter.onClickView, Player.EventListener {

    private ActivityDependentProfileBinding binding;
    private DependentProfileActivity activity = this;

    public static final int Post = 1;
    private SlideUp slideUp;
    LoadingDialog loadingDialog;
    private String token;
    private Session session;
    private String usertype_id;
    private String userID;
    RelativeLayout slideView;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";

    private TextView txtLogOut;
    private TextView txtBlockUser;
    private TextView txtReportUser;
    private TextView txtMessage;

    private List<UserPostModel> userPostModelList;
    private UserVerticalPostAdapter userPostAdapter;
    private UserGreedPostAdapter userGreedPostAdapter;

    private SpannedGridLayoutManagerNew greedLayout;
    private boolean loading1 = true;
    private boolean loading2 = true;
    int pastVisiblesItems1, visibleItemCount1, totalItemCount1;
    int pastVisiblesItems2, visibleItemCount2, totalItemCount2;
    LinearLayoutManager linearLayoutManagerNew;
    int count;
    int pageCount = 1;

    private String profile_pic = "";


    private SimpleExoPlayer exoPlayer;
    private PlayerView playerView;
    private ProgressBar pbVideoPlayer;

    String connected = "Connected";
    String connect = "Connect";
    String requested = "Requested";
    String follow = "Follow";
    String followed = "Followed";
    String block = "Block User";
    String unblock = "Unblock User";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dependent_profile);
        iniView();
    }

    private void iniView() {

        Config.IS_BLOCK_UNBLOCK = false;

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = getIntent().getStringExtra(P.id);

        txtMessage = findViewById(R.id.txtMessage);
        txtReportUser = findViewById(R.id.txtReportUser);
        txtBlockUser = findViewById(R.id.txtBlockUser);
        txtLogOut = findViewById(R.id.txtLogOut);

        txtLogOut.setVisibility(View.GONE);

        slideView = findViewById(R.id.slideViewreport);
        slideUp = new SlideUpBuilder(slideView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        binding.dimprofile.setVisibility(View.VISIBLE);
                        if (visibility == View.GONE) {
                            binding.dimprofile.setVisibility(View.GONE);
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .withSlideFromOtherView(findViewById(R.id.slideViewreport))
                .build();

        binding.dimprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.hide();
                binding.dimprofile.setVisibility(View.GONE);
            }
        });

        binding.ivUsersmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.show();
            }
        });

        binding.imgGreed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.imgGreed.setColorFilter(ContextCompat.getColor(activity, R.color.lightgreen));
                    binding.imgList.setColorFilter(ContextCompat.getColor(activity, R.color.inactive));
                } catch (Exception e) {
                }
                binding.recyclerHorizontalPost.setVisibility(View.VISIBLE);
                binding.recyclerVerticalPost.setVisibility(View.GONE);
            }
        });


        binding.imgList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.imgList.setColorFilter(ContextCompat.getColor(activity, R.color.lightgreen));
                    binding.imgGreed.setColorFilter(ContextCompat.getColor(activity, R.color.inactive));
                } catch (Exception e) {
                }
                binding.recyclerVerticalPost.setVisibility(View.VISIBLE);
                binding.recyclerHorizontalPost.setVisibility(View.GONE);
            }
        });

        if (usertype_id.equals(Config.GUARDIAN)) {
            binding.btnFollowUser.setVisibility(View.GONE);
            binding.btnChaUser.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
        }

        onClick();
        setListData();

    }

    private void hideSlide() {
        try {
            slideUp.hide();
            binding.dimprofile.performClick();
        } catch (Exception e) {
        }
    }

    private void setListData() {

        profile_pic = imageBaseUrl + session.getJson(Config.userProfileData).getString(P.profile_pic);
        userPostModelList = new ArrayList<>();
        userPostAdapter = new UserVerticalPostAdapter(activity, userPostModelList, profile_pic);
        linearLayoutManagerNew = new LinearLayoutManager(activity);
//        SnapHelper snapHelper = new PagerSnapHelper();
        binding.recyclerVerticalPost.setLayoutManager(linearLayoutManagerNew);
//        snapHelper.attachToRecyclerView(binding.recyclerVerticalPost);
        binding.recyclerVerticalPost.setHasFixedSize(true);
        binding.recyclerVerticalPost.setItemViewCacheSize(userPostModelList.size());
        binding.recyclerVerticalPost.setAdapter(userPostAdapter);

        greedLayout = new SpannedGridLayoutManagerNew(
                new SpannedGridLayoutManagerNew.GridSpanLookup() {
                    @Override
                    public SpannedGridLayoutManagerNew.SpanInfo getSpanInfo(int position) {

                        if (position % 10 == 0 || position % 10 == 7) {
                            return new SpannedGridLayoutManagerNew.SpanInfo(2, 2);
                        } else {
                            return new SpannedGridLayoutManagerNew.SpanInfo(1, 1);
                        }
                    }
                },
                4,
                1f
        );

        userGreedPostAdapter = new UserGreedPostAdapter(activity, userPostModelList);
        binding.recyclerHorizontalPost.setLayoutManager(greedLayout);
        binding.recyclerHorizontalPost.setHasFixedSize(true);
        binding.recyclerHorizontalPost.setItemViewCacheSize(userPostModelList.size());
        binding.recyclerHorizontalPost.setAdapter(userGreedPostAdapter);

        hitUserDetails(token);
        hitPostData(pageCount);
        setPagination();
    }

    private void setPagination() {
        binding.recyclerVerticalPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading1 = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount1 = linearLayoutManagerNew.getChildCount();
                totalItemCount1 = linearLayoutManagerNew.getItemCount();
                pastVisiblesItems1 = linearLayoutManagerNew.findFirstVisibleItemPosition();

                if (loading1 && (visibleItemCount1 + pastVisiblesItems1 == totalItemCount1)) {
                    loading1 = false;
                    if (userPostModelList != null && !userPostModelList.isEmpty()) {
                        if (userPostModelList.size() < count) {
                            pageCount++;
                            hitPostData(pageCount);
                        }
                    }
                }
            }
        });

        binding.recyclerHorizontalPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading2 = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount2 = greedLayout.getChildCount();
                totalItemCount2 = greedLayout.getItemCount();
                pastVisiblesItems2 = greedLayout.getFirstVisibleItemPosition();

                if (loading2 && (visibleItemCount2 + pastVisiblesItems2 == totalItemCount2)) {
                    loading2 = false;
                    if (userPostModelList != null && !userPostModelList.isEmpty()) {
                        if (userPostModelList.size() < count) {
                            pageCount++;
                            hitPostData(pageCount);
                        }
                    }
                }
            }
        });

    }

    @Override
    public void playVideoClick(String path) {
        playerVideoDialog(path);
    }

    public void playerVideoDialog(String url) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_player_view);

        pbVideoPlayer = dialog.findViewById(R.id.pbVideoPlayer);
        playerView = dialog.findViewById(R.id.playerView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        playVideo(url);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (exoPlayer != null) {
                    exoPlayer.stop(true);
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

    }


    public void playVideo(String videoPath) {
        exoPlayer = new SimpleExoPlayer.Builder(activity).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.addListener(this);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(videoPath));

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(videoPath));
        exoPlayer.play();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (exoPlayer != null) {
            exoPlayer.stop(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (exoPlayer != null) {
            exoPlayer.pause();
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if (playbackState == Player.STATE_BUFFERING) {
            pbVideoPlayer.setVisibility(View.VISIBLE);

        } else if (playbackState == Player.STATE_READY || playbackState == Player.STATE_ENDED) {
            pbVideoPlayer.setVisibility(View.INVISIBLE);
        }
    }


    private void hitPostData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        String api = "";
        Json j = new Json();
        j.addString(P.page, pageCount + "");

        j.addString(P.user_id, userID);
        api = "Posts/viewposts_by_userid";

        Api.newApi(activity, API.BaseUrl + api).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String posts_img_path = data.getString(P.posts_img_path);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        String post_video_path = data.getString(P.post_video_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        JsonList posts = data.getJsonList(P.posts);
                        if (posts != null && posts.size() != 0) {
                            for (Json jsonValue : posts) {
                                UserPostModel model = new UserPostModel();
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setName(jsonValue.getString(P.name));
                                model.setPost_id(jsonValue.getString(P.post_id));
                                model.setMessage(jsonValue.getString(P.message));
                                model.setPost_img(posts_img_path + "/" + jsonValue.getString(P.post_img));
                                model.setPost_date(jsonValue.getString(P.post_date));
                                model.setLikes(jsonValue.getString(P.likes));
                                model.setComments(jsonValue.getString(P.comments));
                                model.setIs_liked_by_me(jsonValue.getString(P.is_liked_by_me));
                                model.setProfile_pic(profile_pic_path + "/" + jsonValue.getString(P.profile_pic));
                                model.setUpload_type(jsonValue.getString(P.upload_type));
                                model.setFont_style(jsonValue.getString(P.font_style));
                                model.setFont_color(jsonValue.getString(P.font_color));
                                model.setSubtitle(jsonValue.getString(P.subtitle));
                                model.setPost_video(post_video_path + "/" + jsonValue.getString(P.post_video));
                                model.setPaying(false);
                                userPostModelList.add(model);
                            }
                            userPostAdapter.notifyDataSetChanged();
                            userGreedPostAdapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitPostData", token);
    }

    private void checkData() {
        if (userPostModelList.isEmpty()) {
            binding.txtPostError.setVisibility(View.VISIBLE);
        } else {
            binding.txtPostError.setVisibility(View.GONE);
        }
    }

    private void hitUserDetails(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.dependent_id, userID);
        Api.newApi(activity, API.BaseUrl + "userprofiles/single_dependent_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json dependent_details = data.getJson(P.dependent_details);
                        setUserData(dependent_details);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitUserDetails", token);
    }


    private void setUserData(Json json) {

        LoadImage.glideString(activity, binding.imgProfile, imageBaseUrl + json.getString(P.profile_pic), getResources().getDrawable(R.drawable.ic_baseline_person_24));
        binding.txtTitle.setText(checkString(json.getString(P.username)));
        binding.txtUserName.setText(checkString(json.getString(P.name)));
        binding.txtUserBio.setText(checkString(json.getString(P.bio)));

        String is_connected = json.getString(P.is_connected);
        String is_following = json.getString(P.is_following);
        String is_block = json.getString(P.is_blocked);

        if (is_connected.equals("1")) {
            binding.btnConnectUser.setText(connected + " ✓");
            binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
            binding.btnChaUser.setVisibility(View.VISIBLE);
            txtMessage.setVisibility(View.VISIBLE);
        } else if (is_connected.equals("2")) {
            binding.btnConnectUser.setText(requested);
            binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
            binding.btnChaUser.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
        } else if (is_connected.equals("0")) {
            binding.btnConnectUser.setText(connect);
            binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_dark_bg));
            binding.btnChaUser.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
        }

        if (is_following.equals("1")) {
            binding.btnFollowUser.setText(followed);
            binding.btnFollowUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
        } else {
            binding.btnFollowUser.setText(follow);
            binding.btnFollowUser.setBackground(getResources().getDrawable(R.drawable.button_dark_bg));
        }

        if (is_block.equals("1")) {
            txtBlockUser.setText(unblock);
            txtBlockUser.setTextColor(getResources().getColor(R.color.lightgreen));
        } else {
            txtBlockUser.setText(block);
            txtBlockUser.setTextColor(getResources().getColor(R.color.saffron));
        }

        if (json.has(P.connections)) {
            binding.txtConnection.setText(json.getString(P.connections));
        }
        if (json.has(P.followers)) {
            binding.txtFollowers.setText(json.getString(P.followers));
        }
        if (json.has(P.following)) {
            binding.txtFollowing.setText(json.getString(P.following));
        }

        if (usertype_id.equals(Config.GUARDIAN)) {
            binding.btnFollowUser.setVisibility(View.GONE);
            binding.btnChaUser.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
        }
    }

    private void hitConnectUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.to_dependent_id, userID);
        Api.newApi(activity, API.BaseUrl + "dependent/connection/connect_dependent").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        binding.btnConnectUser.setText(requested);
                        binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitConnectUser", token);
    }

    private void hitFollowUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.follow_to_id, userID);
        Api.newApi(activity, API.BaseUrl + "Follower/follow_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        binding.btnFollowUser.setText(followed);
                        binding.btnFollowUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitFollowUser", token);
    }


    private void reportDialog(String token) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_report_user);

        EditText etxDescription = dialog.findViewById(R.id.etxDescription);
        Button btnReport = dialog.findViewById(R.id.btnReport);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(etxDescription.getText().toString().trim())) {
                    H.showMessage(activity, "Please enter comment to report");
                } else if (etxDescription.getText().toString().trim().length() < 4) {
                    H.showMessage(activity, "Please enter valid comment");
                } else {
                    dialog.dismiss();
                    hitReportUser(token, etxDescription.getText().toString().trim());
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void hitReportUser(String token, String reason) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.report_user, userID);
        j.addString(P.comment, reason);
        Api.newApi(activity, API.BaseUrl + "userprofiles/report_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitReportUser", token);
    }

    private void hitBlockUnblockUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.block_user, userID);
        Api.newApi(activity, API.BaseUrl + "userprofiles/block_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        if (txtBlockUser.getText().toString().equals(block)) {
                            txtBlockUser.setText(unblock);
                            txtBlockUser.setTextColor(getResources().getColor(R.color.lightgreen));
                            H.showMessage(activity, "User blocked");
                        } else if (txtBlockUser.getText().toString().equals(unblock)) {
                            txtBlockUser.setText(block);
                            txtBlockUser.setTextColor(getResources().getColor(R.color.saffron));
                            H.showMessage(activity, "User unblocked");
                        }
                        Config.IS_BLOCK_UNBLOCK = true;
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitBlockUnblockUser", token);
    }

    private void onClick() {

        binding.btnConnectUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (binding.btnConnectUser.getText().toString().equals(connect)) {
                    hitConnectUser(token);
                }

            }
        });

        binding.btnFollowUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (binding.btnFollowUser.getText().toString().equals(follow)) {
                    hitFollowUser(token);
                }
            }
        });

        binding.btnChaUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToChat();
            }
        });

        txtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hideSlide();
                jumpToChat();
            }
        });

        txtReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hideSlide();
                reportDialog(token);
            }
        });

        txtBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hideSlide();
                hitBlockUnblockUser(token);
            }
        });

        binding.lnrConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                if (usertype_id.equals(Config.DEPENDENT)) {
//                    Intent intentDependent = new Intent(activity, DependentConnectionActivity.class);
//                    startActivity(intentDependent);
//                } else if (usertype_id.equals(Config.GUARDIAN)) {
//                    Intent intentGuardian = new Intent(activity, GuardianConnectionActivity.class);
//                    startActivity(intentGuardian);
//                }
            }
        });

        binding.lnrFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                Intent intent = new Intent(activity, FollowersActivity.class);
//                startActivity(intent);
            }
        });

        binding.lnrFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                Intent intent = new Intent(activity, FollowingActivity.class);
//                startActivity(intent);
            }
        });

        txtLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                logOutUser();
            }
        });

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void jumpToChat() {
        Intent intent = new Intent(activity, ChatDetailsActivity.class);
        intent.putExtra(P.id, userID);
        intent.putExtra(P.username, binding.txtTitle.getText().toString().trim());
        startActivity(intent);
    }

    private void logOutUser() {
        session.clear();
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


}