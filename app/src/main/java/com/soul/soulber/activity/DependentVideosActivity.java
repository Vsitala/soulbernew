package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentVideoAdapter;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentVideosBinding;
import com.soul.soulber.model.DependentVideoModel;
import com.soul.soulber.util.Click;

import java.util.ArrayList;
import java.util.List;

public class DependentVideosActivity extends AppCompatActivity {

    private DependentVideosActivity activity = this;
    private ActivityDependentVideosBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private List<DependentVideoModel> dependentVideoModelList;
    private DependentVideoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dependent_videos);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        dependentVideoModelList = new ArrayList<>();
        adapter = new DependentVideoAdapter(activity, dependentVideoModelList);
        binding.recyclerVideos.setLayoutManager(new GridLayoutManager(activity, 2));
        binding.recyclerVideos.setHasFixedSize(true);
        binding.recyclerVideos.setNestedScrollingEnabled(false);
        binding.recyclerVideos.setAdapter(adapter);

        setData();
        onClick();
    }


    private void setData() {
        DependentVideoModel model = new DependentVideoModel();
        model.setTitle("A Day in a Zoo");
        model.setDate("Date : 23-2-2021");
        model.setVideoPath("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4");
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        dependentVideoModelList.add(model);
        adapter.notifyDataSetChanged();
    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

    }

}