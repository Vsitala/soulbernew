package com.soul.soulber.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentSelectionAdapter;
import com.soul.soulber.adapter.PanicHistoryAdapter;
import com.soul.soulber.adapter.PanicHistoryChildAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityPanicHistoryBinding;
import com.soul.soulber.model.ChildExpandableModel;
import com.soul.soulber.model.DependentModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PanicHistoryActivity extends AppCompatActivity {
    private ActivityPanicHistoryBinding binding;
    private PanicHistoryActivity activity = this;
    Session session;
    String token;
    PanicHistoryChildAdapter listAdapter;
    private ArrayList<DependentModel> listDataHeader;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ChildExpandableModel> panicchildlist;
    int lastExpandedPosition = -1;
    String dependentid;
    String userid;
    LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_panic_history);
        initview();
    }

    private void initview() {
        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);
        token = session.getJson(Config.userData).getString(P.user_token);
        listDataHeader = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.expandableListView.setLayoutManager(linearLayoutManager);
        setspinnerlist();
        onClickspinner();
        onClick();
    }

    private void setspinnerlist() {
        Api.newApi(activity, API.BaseUrl + "dashboard/guardian_panic_history_list").setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {
                            String profile_pic_path = "https://www.soulber.co/dev/uploads/profile_pic/";
                            JsonList jsonList = json.getJsonList(P.dependent_list);
                            DependentModel modelnew = new DependentModel();
                            modelnew.setName("Select Dependent");
                            modelnew.setUsername("Select Dependent");
                            modelnew.setDependent_id("0");
                            listDataHeader.add(modelnew);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    DependentModel model = new DependentModel();
                                    model.setName(jvalue.getString(P.name));
                                    model.setUsername(jvalue.getString(P.username));
                                    model.setProfile_pic(jvalue.getString(P.profile_pic));
                                    model.setDependent_id(jvalue.getString(P.dependent_id));
                                    listDataHeader.add(model);
                                }

                                DependentSelectionAdapter dependentSelectionAdapter = new DependentSelectionAdapter(activity, listDataHeader);
                                binding.spinnerdependentlist.setAdapter(dependentSelectionAdapter);
                            } else {
                                H.showMessage(activity, json.getString(P.error));
                            }

                        }
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {

                    }
                })
                .run("hitdependentpanic", token);


    }

    private void onClickspinner() {
        binding.spinnerdependentlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userid = listDataHeader.get(position).getDependent_id();
                panicchildlist = new ArrayList<>();
                Api.newApi(activity, API.BaseUrl + "dashboard/guardian_panic_history_list?dependent=" + userid)
                        .setMethod(Api.GET)
                        .onSuccess(new Api.OnSuccessListener() {
                            @Override
                            public void onSuccess(Json json) {
                                if (json.getInt(P.status) == 1) {
                                    String profile_pic_path = "https://www.soulber.co/dev/uploads/profile_pic/";
                                    Json data = json.getJson(P.data);
                                    JsonList jsonList = data.getJsonList(P.panics);
                                    if (jsonList != null && jsonList.size() != 0) {

                                        for (Json jvalue : jsonList) {
                                            binding.expandableListView.setVisibility(View.VISIBLE);
                                            ChildExpandableModel modelnew = new ChildExpandableModel();
                                            modelnew.setName(jvalue.getString(P.dependent_name));
                                            modelnew.setProfile_pic(jvalue.getString(P.profile_pic));
                                            modelnew.setAdd_date(jvalue.getString(P.panic_date));
                                            modelnew.setRemark(jvalue.getString(P.remark));
                                            panicchildlist.add(modelnew);
                                        }
                                        listAdapter = new PanicHistoryChildAdapter(activity, panicchildlist);
                                        binding.expandableListView.setAdapter(listAdapter);
                                        listAdapter.notifyDataSetChanged();
                                    }
                                    if (jsonList.isEmpty()) {
                                        binding.expandableListView.setVisibility(View.GONE);
                                        binding.tvEmplylist.setVisibility(View.VISIBLE);
                                    }

                                }
                            }
                        })
                        .onError(new Api.OnErrorListener() {
                            @Override
                            public void onError() {

                            }
                        })
                        .run("hitpanicdetails", token);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void onClick() {
        binding.onbackPanichistoy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
