package com.soul.soulber.activity;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityJobDetailsBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.FileUtils;
import com.soul.soulber.util.ProgressView;
import com.soul.soulber.util.RemoveHtml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;

public class JobDetailsActivity extends AppCompatActivity {

    private JobDetailsActivity activity = this;
    private ActivityJobDetailsBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;
    private String jobID;
    private String shareMessage = "";
    private int fromBookmarkAdd = 1;
    private int fromBookmarkRemove = 2;

    final public int REQUEST_CODE_READ_WRITE = 110;
    final public int REQUEST_DOC = 9;

    private String is_apply_allowed = "";
    private String uploadedFileName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_job_details);
        initView();
    }

    private void initView() {

        getAccess();

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
        jobID = getIntent().getStringExtra(Config.JOB_ID);

        hitJobDetailsData(jobID);
        onClick();
    }


    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.lnrUploadResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPermission();
            }
        });

    }


    private void hitJobDetailsData(String job_id) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.job_id, job_id);

        Api.newApi(activity, API.BaseUrl + "job/job_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        setData(data);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitJobDetailsData", token);
    }


    private void hitBookmark(String job_id, ImageView imgAdd, ImageView imgRemove, int from) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.job_id, job_id);

        Api.newApi(activity, API.BaseUrl + "job/bookmark_job").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Config.UPDATED_BOOKMARK = true;
                        if (from == fromBookmarkAdd) {
                            imgAdd.setVisibility(View.GONE);
                            imgRemove.setVisibility(View.VISIBLE);
                        } else if (from == fromBookmarkRemove) {
                            imgAdd.setVisibility(View.VISIBLE);
                            imgRemove.setVisibility(View.GONE);
                        }
                        H.showMessage(activity, json.getString(P.msg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitBookmark", token);
    }

    private void setData(Json json) {
        binding.txtSkills.setText(checkString(json.getString(P.skills), binding.txtSkills));
        binding.txtWebsite.setText(checkString(json.getString(P.website_link), binding.txtWebsite));
        binding.txtLocation.setText(checkString(json.getString(P.job_location), binding.txtLocation));
        binding.txtJobType.setText(checkString(json.getString(P.category), binding.txtJobType));
        binding.btnApplyNow.setText(checkString(json.getString(P.button_name)));

        String application_status = checkString(json.getString(P.application_status));
        is_apply_allowed = checkString(json.getString(P.apply_allowed));

        if (is_apply_allowed.equals("1")) {
            binding.lnrUploadResume.setVisibility(View.VISIBLE);
        } else {
            binding.lnrUploadResume.setVisibility(View.GONE);
        }

        String experience_from = json.getString(P.experience_from);
        String experience_to = json.getString(P.experience_to);

        if (!TextUtils.isEmpty(checkString(experience_to))) {
            binding.txtExperience.setText(checkString(checkString(experience_from) + checkString(" - " + experience_to + " year"), binding.txtExperience));
        } else {
            binding.txtExperience.setText(checkString(checkString(experience_from + " year"), binding.txtExperience));
        }


        String salary_from = json.getString(P.salary_from);
        String salary_to = json.getString(P.salary_to);
        if (!TextUtils.isEmpty(checkString(salary_to))) {
            binding.txtSalary.setText(checkString(checkString("₹ " + salary_from) + checkString(" - " + salary_to + " a month"), binding.txtSalary));
        } else {
            binding.txtSalary.setText(checkString("₹ " + checkString(salary_from + " a month"), binding.txtSalary));
        }

        binding.txtAboutCompany.setText(RemoveHtml.html2text(json.getString(P.about_company)).trim());
        binding.txtAboutJob.setText(RemoveHtml.html2text(json.getString(P.about_job)).trim());
        binding.txtSkillsRequired.setText(RemoveHtml.html2text(json.getString(P.skills)).intern());
        binding.txtWhoApply.setText(RemoveHtml.html2text(json.getString(P.who_can_apply)).trim());

        shareMessage = "Job Description - \n" +
                "Skills : " + json.getString(P.skills) + "\n" +
                "Website : " + json.getString(P.website_link) + "\n" +
                "Location : " + json.getString(P.job_location) + " " + json.getString(P.category) + "\n" +
                "Experience : " + binding.txtExperience.getText().toString() + "\n" +
                "Salary : " + binding.txtSalary.getText().toString();

        if (json.getString(P.is_bookmarked).equals("1")) {
            binding.imgBookmarkRemove.setVisibility(View.VISIBLE);
            binding.imgBookmarkAdd.setVisibility(View.GONE);
        } else {
            binding.imgBookmarkRemove.setVisibility(View.GONE);
            binding.imgBookmarkAdd.setVisibility(View.VISIBLE);
        }

        binding.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });

        binding.imgBookmarkAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitBookmark(json.getString(P.id), binding.imgBookmarkAdd, binding.imgBookmarkRemove, fromBookmarkAdd);
            }
        });

        binding.imgBookmarkRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitBookmark(json.getString(P.id), binding.imgBookmarkAdd, binding.imgBookmarkRemove, fromBookmarkRemove);
            }
        });

        binding.btnApplyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_apply_allowed.equals("1")) {
                    if (uploadedFileName.equals("")) {
                        H.showMessage(activity, "Please upload resume file");
                    } else {
                        hitJobApply(jobID);
                    }
                }

            }
        });

    }

    private void hitJobApply(String job_id) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.job_id, job_id);
        j.addString(P.resume, uploadedFileName);

        Api.newApi(activity, API.BaseUrl + "job/job_apply").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        finish();
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitJobApply", token);
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private void getAccess() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
        }
    }

    public void getPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_READ_WRITE);
    }

    public void jumpToSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void openDocument() {
        Intent galleryIntent = new Intent();
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
//        galleryIntent.setType("application/pdf");
        galleryIntent.setType("application/*");
//        galleryIntent.setType("file/*");
        startActivityForResult(galleryIntent, REQUEST_DOC);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_READ_WRITE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openDocument();
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    jumpToSetting();
                } else {
                    getPermission();
                }
                return;
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    try {
                        Uri selectedDoc = data.getData();

//                        String base64 = Base64.encodeToString(getBytesFromUri(selectedDoc, activity), Base64.NO_WRAP);
//                        String filePath = getRealPathFromURI(activity,selectedDoc);
//                        String filePath = getPath(selectedDoc);
                        String filePath = FileUtils.getPath(activity, selectedDoc);
//                        upload(selectedDoc, filePath);
                        uploadResumeFile(getFileName(selectedDoc), getMimeType(selectedDoc), filePath);
                    } catch (Exception e) {
                        Log.e("TAG", "onActivityResult21212: " + e.getMessage());
                        H.showMessage(activity, "Unable to get document, try again.");
                    }
                }
                break;
        }
    }


    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getMimeType(Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }


    public static byte[] getBytesFromUri(Uri uri, Context context) throws IOException {
        InputStream iStream = context.getContentResolver().openInputStream(uri);
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = iStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private void upload(Uri uri, String filePath) {

        Log.e("TAG", "uploadTOKE: " + token);
        Log.e("TAG", "uploadURI: " + uri);
        Log.e("TAG", "uploadFILE: " + filePath);

        try {
            OkHttpClient client = new OkHttpClient();
            ContentResolver contentResolver = getContentResolver();
            final String contentType = contentResolver.getType(uri);
            final AssetFileDescriptor fd = contentResolver.openAssetFileDescriptor(uri, "r");
            if (fd == null) {
                throw new FileNotFoundException("could not open file descriptor");
            }

            RequestBody videoFile = new RequestBody() {
                @Override
                public long contentLength() {
                    return fd.getDeclaredLength();
                }

                @Override
                public MediaType contentType() {
                    return MediaType.parse(contentType);
                }

                @Override
                public void writeTo(BufferedSink sink) throws IOException {
                    try (InputStream is = fd.createInputStream()) {
                        sink.writeAll(Okio.buffer(Okio.source(is)));
                    }
                }
            };

//            RequestBody requestBody = new MultipartBody.Builder()
//                    .setType(MultipartBody.FORM)
//                    .addFormDataPart("files", "fname", videoFile)
//                    .build();

            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("files", "fileName" + ".pdf",
                            RequestBody.create(MediaType.parse("application/octet-stream"),
                                    new File(filePath)))
                    .build();
            Request request = new Request.Builder()
                    .url(API.BaseUrl + "Common/upload_file")
                    .post(body)
//                    .method("POST", requestBody)
                    .addHeader("x-api-key", "123456")
                    .addHeader("X-Authorization", token)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    try {
                        fd.close();
                    } catch (IOException ex) {
                        e.addSuppressed(ex);
                    }
                    Log.e("TAG", "upload1: " + e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.e("TAG", "upload2: " + response.body().string());
                    fd.close();
                }
            });
        } catch (Exception e) {
            Log.e("TAG", "upload3: " + e.getMessage());
        }

    }

    private void uploadResumeFile(String fileName, String fileExt, String filePath) {

        Log.e("TAG", "uploadFILE_NAME: " + fileName);
        Log.e("TAG", "uploadFILE_EXT: " + fileExt);
        Log.e("TAG", "uploadFILE_PATH: " + filePath);

        ProgressView.show(activity, loadingDialog);

        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("files", fileName + "." + fileExt,
                            RequestBody.create(MediaType.parse("application/octet-stream"),
                                    new File(filePath)))
                    .build();
            Request request = new Request.Builder()
                    .url(API.BaseUrl + "Common/upload_file")
                    .post(body)
//                    .method("POST", requestBody)
                    .addHeader("x-api-key", "123456")
                    .addHeader("X-Authorization", "Bearer " + token)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "Unable to upload file, try again");
                    Log.e("TAG", "uploadFailure: " + e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    ProgressView.dismiss(loadingDialog);

                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);
                    String responseData = response.body().string();
                    try {
                        JSONObject jsonObject = new JSONObject(responseData);
                        int status = jsonObject.getInt(P.status);
                        String msg = jsonObject.getString(P.msg);
                        if (status == 1) {
                            JSONObject jsonObjectData = jsonObject.getJSONObject(P.data);
                            JSONArray filesArray = jsonObjectData.getJSONArray(P.files);
                            uploadedFileName = filesArray.getString(0);
                            Log.e("TAG", "uploadResponseJSON: " + uploadedFileName);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    binding.txtResumeText.setText("Resume Uploaded - " + fileName + "." + fileExt);
                                    binding.txtResumeText.setTextColor(getResources().getColor(R.color.bg_screen1));
                                }
                            });
                        } else {
                            H.showMessage(activity, msg);
                        }

                    } catch (JSONException e) {
                        Log.e("TAG", "uploadResponseEXP: " + e.getMessage());
                    }

                }
            });

        } catch (Exception e) {
            ProgressView.dismiss(loadingDialog);
            H.showMessage(activity, "Unable to upload file, try again");
            Log.e("TAG", "uploadException: " + e.getMessage());
        }

    }


}
