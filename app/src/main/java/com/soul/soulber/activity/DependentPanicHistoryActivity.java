package com.soul.soulber.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.view.MotionEvent;
import android.view.View;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentPanicHistoryAdapter;
import com.soul.soulber.adapter.DependentSelectionAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentPanicHistoryBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.PanicHistoryModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;

public class DependentPanicHistoryActivity extends AppCompatActivity {
    private DependentPanicHistoryActivity activity = this;
    private ActivityDependentPanicHistoryBinding binding;
    Session session;
    String token;
    long down, up;
    private ArrayList<PanicHistoryModel> paniclist;
    private LoadingDialog loadingDialog;
    private LinearLayoutManager linearLayoutManager;
    private DependentPanicHistoryAdapter adapter;

    final public int REQUEST_CALL = 99;
    private boolean isCall = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_dependent_panic_history);
        initView();
        onClick();
        hitPanicHistoryapi();
    }

    private void initView() {
        isCall = false;
        getAccess();
        session = new Session(activity);
        token = session.getJson(Config.userData).getString(P.user_token);
        loadingDialog = new LoadingDialog(activity);

        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvPanicHistory.setLayoutManager(linearLayoutManager);
    }

    private void onClick() {
        binding.llPanicbutton.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        down = System.currentTimeMillis();
                        break;
                    case MotionEvent.ACTION_UP:
                        up = System.currentTimeMillis();
                        if (up - down > 5000) {
                            getCallPermission();
                        } else {
                            H.showMessage(activity, "Please press and hold for 5 second.");
                        }
                        return true;
                }
                return false;
            }
        });

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void getAccess() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
        }
    }

    public void getCallPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CALL_PHONE},
                REQUEST_CALL);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    hitPanicAction(token);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    settingAlert();
                } else {
                    getCallPermission();
                }
                return;
            }
        }
    }


    private void settingAlert() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(activity, R.style.MyDatePickerStyle);
        builder1.setMessage("Please allow call permission from setting.");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        jumpToSetting();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public void jumpToSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    private void callUser() {
        try {
            if (ActivityCompat.checkSelfPermission(activity,
                    Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                isCall = true;
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + "8667085835"));
                startActivity(intent);
            } else {
                H.showMessage(activity, "You don't assign permission.");
            }
        } catch (Exception e) {
            H.showMessage(activity, "Unable to open dial number,try again");
        }

    }


    private void hitPanicAction(String token) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
//        j.addString("", "");

        Api.newApi(activity, API.BaseUrl + "Dashboard/hit_panic")
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        callUser();
                        H.showMessage(activity, json.getString(P.msg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitPanicAction", token);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isCall) {
            isCall = false;
            hitPanicHistoryapi();
        }
    }

    private void hitPanicHistoryapi() {
        paniclist = new ArrayList<>();
        Api.newApi(activity, API.BaseUrl + "dashboard/dependent_panic_history_list")
                .setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {

                            Json data = json.getJson(P.data);
                            JsonList jsonList = data.getJsonList(P.panics);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    PanicHistoryModel model = new PanicHistoryModel();
                                    model.setDependent_username(jvalue.getString(P.dependent_username));
                                    model.setPanic_date(jvalue.getString(P.panic_date));
                                    model.setProfile_pic(jvalue.getString(P.profile_pic));
                                    paniclist.add(model);
                                }
                                adapter = new DependentPanicHistoryAdapter(activity, paniclist);
                                binding.rvPanicHistory.setAdapter(adapter);
                            }
                        } else {
                            H.showMessage(activity, json.getString(P.error));
                        }
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {

                    }
                })
                .run("paniclistapi", token);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}