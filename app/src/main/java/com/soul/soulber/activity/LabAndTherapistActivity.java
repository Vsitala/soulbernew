package com.soul.soulber.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.adoisstudio.helper.H;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.tabs.TabLayout;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.databinding.ActivityLabTherapistBinding;
import com.soul.soulber.fragment.LabsFragment;
import com.soul.soulber.fragment.TherapistsFragment;
import com.soul.soulber.util.Downloader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

public class LabAndTherapistActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ActivityLabTherapistBinding binding;
    private LabAndTherapistActivity activity = this;

    private LabsFragment labsFragment = LabsFragment.newInstance();
    private TherapistsFragment therapistsFragment = TherapistsFragment.newInstance();

    private String lab = "Labs";
    private String therapist = "Therapists";

    final int AUTOCOMPLETE_REQUEST_CODE = 1000;
    int fromLAB = 1;
    int fromTHERAPIST = 2;
    int fromWHERE = 0;
    TextView txtLocation;

    private static final int READ_WRITE = 20;
    String pdf_url;
    String pdf_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lab_therapist);

        initializeKey();
        getAccess();
        initView();

    }

    private void initView() {

        setupViewPager(binding.viewPager);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.addOnPageChangeListener(activity);
        setupTabIcons();
        onClick();
        loadFromNotification();

    }

    private void loadFromNotification() {
        try {
            if (Config.FOR_THERAPIST) {
                binding.viewPager.setCurrentItem(1);
            }
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Config.FOR_THERAPIST = false;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(labsFragment, "");
        adapter.addFragment(therapistsFragment, "");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {

        View view1 = LayoutInflater.from(this).inflate(R.layout.activity_customt_ab, null);
        ((TextView) view1.findViewById(R.id.text)).setText(lab);
        ((TextView) view1.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));

        View view2 = LayoutInflater.from(this).inflate(R.layout.activity_customt_ab, null);
        ((TextView) view2.findViewById(R.id.text)).setText(therapist);
        ((TextView) view2.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));


        binding.tabLayout.getTabAt(0).setCustomView(view1);
        binding.tabLayout.getTabAt(1).setCustomView(view2);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void onClick() {
        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initializeKey() {
        if (!Places.isInitialized()) {
            Places.initialize(activity, Config.API_KEY);
        }
        PlacesClient placesClient = Places.createClient(activity);
    }

    public void onGoogleSearchCalled(int fromCame, TextView textView) {
        fromWHERE = fromCame;
        txtLocation = textView;
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(
//                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("NG") //NIGERIA
//                .build(this);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry(Config.COUNTRY) //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    String id = place.getId();
                    String address = place.getAddress();
                    String name = place.getName();
                    LatLng latlong = place.getLatLng();
                    double latitude = latlong.latitude;
                    double longitude = latlong.longitude;

                    if (fromWHERE == fromLAB) {
                        Config.LAB_LAT = latitude + "";
                        Config.LAB_LONG = longitude + "";
                        Config.LAB_ADD = address;
                    } else if (fromWHERE == fromTHERAPIST) {
                        Config.THERAPIST_LAT = latitude + "";
                        Config.THERAPIST_LOGN = longitude + "";
                        Config.THERAPIST_ADD = address;
                    }

                    txtLocation.setText(address);

                    Log.e(TAG, "latitude: " + latitude + "");
                    Log.e(TAG, "longitude: " + longitude + "");
                    Log.e(TAG, "address: " + address.trim() + "");

                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
//                    H.showMessage(activity, "Unable to get address, try again.");
                    H.showMessage(activity, status + "");
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;
        }
    }

    public void onDownload(String url, String title) {
        pdf_url = url;
        pdf_title = title;
        if (TextUtils.isEmpty(pdf_url) || pdf_url.equals("null")) {
            H.showMessage(activity, "No pdf path found.");
        } else {
            getPermission();
        }
    }

    private void getAccess() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
        }
    }

    private void getPermission() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_WRITE);
    }

    public void jumpToSetting() {
        H.showMessage(activity, "Allow permission from setting");
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRITE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Downloader.download(activity, pdf_url, pdf_title, Config.OPEN);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    jumpToSetting();
                } else {
                    getPermission();
                }
                return;
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}