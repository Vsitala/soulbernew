package com.soul.soulber.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityCreateResumeBinding;
import com.soul.soulber.util.Click;

public class CreateResumeActivity extends AppCompatActivity {

    private CreateResumeActivity activity = this;
    private ActivityCreateResumeBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_resume);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        onClick();
    }


    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

        binding.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(activity, EditResumeActivity.class);
                startActivity(intent);
            }
        });

        binding.lnFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.imgArrowDown.getVisibility() == View.VISIBLE) {
                    binding.imgArrowDown.setVisibility(View.GONE);
                    binding.imgArrowUp.setVisibility(View.VISIBLE);
                } else if (binding.imgArrowUp.getVisibility() == View.VISIBLE) {
                    binding.imgArrowUp.setVisibility(View.GONE);
                    binding.imgArrowDown.setVisibility(View.VISIBLE);
                }

            }
        });

        binding.btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);

            }
        });


    }

}