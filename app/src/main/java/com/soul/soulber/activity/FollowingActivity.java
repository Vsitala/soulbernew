package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.FollowingAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityFollowingBinding;
import com.soul.soulber.model.FollowingModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class FollowingActivity extends AppCompatActivity implements FollowingAdapter.onClick {

    private ActivityFollowingBinding binding;
    private FollowingActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private List<FollowingModel> followingModelList;
    private FollowingAdapter followingAdapter;

    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_following);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        followingModelList = new ArrayList<>();
        followingAdapter = new FollowingAdapter(activity, followingModelList);
        binding.recycelerFollowing.setLayoutManager(new LinearLayoutManager(activity));
        binding.recycelerFollowing.setHasFixedSize(true);
        binding.recycelerFollowing.setAdapter(followingAdapter);

        hitFollowingData(token, userID);
        onClick();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            followingModelList.clear();
            followingAdapter.notifyDataSetChanged();
            hitFollowingData(token, userID);
        }
    }

    private void onClick() {
        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onFollow(FollowingModel model, TextView txtAction) {
        hitFollow(token, model.getUser_id(), txtAction);
    }

    @Override
    public void onUnFollow(FollowingModel model, TextView txtAction) {
        hitUnFollow(token, model.getUser_id(), txtAction);
    }

    private void hitFollowingData(String token, String userId) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.user_id, userId);

        Api.newApi(activity, API.BaseUrl + "Follower/show_user_following").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        String profile_pic_path = json.getString(P.profile_pic_path);
                        Json data = json.getJson(P.data);
                        JsonList list = data.getJsonList(P.list);

                        if (list != null && list.size() != 0) {
                            for (Json jsonValue : list) {
                                FollowingModel model = new FollowingModel();
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setProfile_pic(profile_pic_path + jsonValue.getString(P.profile_pic));
                                model.setUser_type_id(jsonValue.getString(P.user_type_id));
                                followingModelList.add(model);
                            }
                            followingAdapter.notifyDataSetChanged();
                        }

                        if (followingModelList.isEmpty() && followingModelList.isEmpty()) {
                            binding.txtError.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtError.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.msg));
                    }
                })
                .run("hitFollowingData", token);
    }

    private void hitFollow(String token, String id, TextView textView) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.follow_to_id, id);

        Api.newApi(activity, API.BaseUrl + "Follower/follow_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        textView.setText(Config.Unfollow);
                        H.showMessage(activity, json.getString(P.msg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitFollow", token);
    }

    private void hitUnFollow(String token, String id, TextView textView) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.unfollow_to_id, id);

        Api.newApi(activity, API.BaseUrl + "Follower/unfollow_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        textView.setText(Config.Follow);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitUnFollow", token);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}