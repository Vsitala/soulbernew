package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityEditResumeBinding;
import com.soul.soulber.util.Click;

public class EditResumeActivity extends AppCompatActivity {

    private EditResumeActivity activity = this;
    private ActivityEditResumeBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_resume);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        onClick();
    }


    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

        binding.btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
            }
        });
    }

}