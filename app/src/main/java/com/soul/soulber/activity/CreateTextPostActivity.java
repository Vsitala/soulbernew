package com.soul.soulber.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.android.volley.VolleyError;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityCreateTextPostBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class CreateTextPostActivity extends AppCompatActivity {

    private ActivityCreateTextPostBinding binding;
    private CreateTextPostActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private int colorClick = 1;
    private int fontClick = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_text_post);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);
        String userName = session.getJson(Config.userProfileData).getString(P.username);

        binding.txtTitle.setText(userName);

        binding.etxPost.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.etxPost) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        onClick();
        checkColor(1);
        checkFont(1);
    }

    private void onClick(){

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });

        binding.imgText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fontClick<5){
                    fontClick = fontClick + 1;
                }else {
                    fontClick = 1;
                }
                checkFont(fontClick);
            }
        });

        binding.imgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (colorClick<5){
                    colorClick = colorClick + 1;
                }else {
                    colorClick = 1;
                }
                checkColor(colorClick);
            }
        });

        binding.btnSavePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                String message = binding.etxPost.getText().toString().trim();
                if (TextUtils.isEmpty(message)){
                    binding.etxPost.requestFocus();
                    binding.etxPost.setError("Enter caption");
                }else {
                    hitCreatePost(message);
                }
            }
        });
    }

    private void checkColor(int value){
        try {
            if (value==1){
                binding.lnrBackground.setBackgroundColor(getResources().getColor(R.color.bg1));
            }else if (value==2){
                binding.lnrBackground.setBackgroundColor(getResources().getColor(R.color.bg2));
            }else if (value==3){
                binding.lnrBackground.setBackgroundColor(getResources().getColor(R.color.bg3));
            }else if (value==4){
                binding.lnrBackground.setBackgroundColor(getResources().getColor(R.color.bg4));
            }else if (value==5){
                binding.lnrBackground.setBackgroundColor(getResources().getColor(R.color.bg5));
            }
        }catch (Exception e){
        }
    }

    private void checkFont(int value){
        try {
            if (value==1){
                Typeface face = ResourcesCompat.getFont(activity, R.font.font_1);
                binding.etxPost.setTypeface(face);
            }else if (value==2){
                Typeface face = ResourcesCompat.getFont(activity, R.font.font_2);
                binding.etxPost.setTypeface(face);
            }else if (value==3){
                Typeface face = ResourcesCompat.getFont(activity, R.font.font_3);
                binding.etxPost.setTypeface(face);
            }else if (value==4){
                Typeface face = ResourcesCompat.getFont(activity, R.font.font_4);
                binding.etxPost.setTypeface(face);
            }else if (value==5){
                Typeface face = ResourcesCompat.getFont(activity, R.font.font_5);
                binding.etxPost.setTypeface(face);
            }
        }catch (Exception e){
        }
    }

    private void hitCreatePost(String message) {
        ProgressView.show(activity,loadingDialog);

        Json j = new Json();
        j.addString(P.post_msg,message);
        j.addString(P.image,"");
        j.addString(P.font_color,colorClick + "");
        j.addString(P.font_style,fontClick + "");
        j.addString(P.subtitle,"");
        j.addString(P.video,"");
        j.addString(P.upload_type,Config.UPLOAD_TYPE_1);


        Api.newApi(activity, API.BaseUrl+ "Posts/create_post").addJson(j)
                .setMethod(Api.POST)
                .onError(() -> {
                    VolleyError volleyError = new VolleyError();
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity,""+volleyError);
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if(json.getInt(P.status)==1)
                    {
                        Json data = json.getJson(P.data);
                        String post_id = data.getString(P.post_id);
                        Config.FOR_MY_POST = true;
                        Intent intent = new Intent(activity,UserPostActivity.class);
                        startActivity(intent);
                        finish();

                    }

                })
                .run("hitCreatePost",token);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}