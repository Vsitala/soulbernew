package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityCommingSoonBinding;

public class ComingSoonActivity extends AppCompatActivity {

    private ComingSoonActivity activity = this;
    private ActivityCommingSoonBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_comming_soon);
        initView();
    }

    private void initView() {

        onClick();
    }

    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
