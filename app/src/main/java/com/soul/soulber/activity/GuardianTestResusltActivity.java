package com.soul.soulber.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentSelectionAdapter;
import com.soul.soulber.adapter.GuardianReportlistAdapter;
import com.soul.soulber.adapter.GuardianTestResultAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianTestResusltBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.GuardianReportModel;
import com.soul.soulber.model.GuardianTestResultModel;
import com.soul.soulber.util.Downloader;

import java.util.ArrayList;

public class GuardianTestResusltActivity extends AppCompatActivity implements GuardianReportlistAdapter.onClick {

    private GuardianTestResusltActivity activity = this;
    private ActivityGuardianTestResusltBinding binding;
    private Session session;
    private LoadingDialog loadingDialog;
    String token;
    ArrayList<DependentModel> dependentModels;
    ArrayList<GuardianTestResultModel> testResultModels;
    String userid;
    LinearLayoutManager linearLayoutManager;
    private GuardianTestResultAdapter adapter;

    private static final int READ_WRITE = 20;
    String pdf_url;
    String pdf_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_guardian_test_resuslt);
        getAccess();
        initViw();
        setSpinnerdata();
        onClickspinner();
        onClick();
    }


    private void initViw() {

        session = new Session(activity);
        loadingDialog = new LoadingDialog(activity);
        token = session.getJson(Config.userData).getString(P.user_token);
        dependentModels = new ArrayList<>();
        adapter = new GuardianTestResultAdapter(activity, testResultModels, dependentModels);
        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvGuardtestresult.setLayoutManager(linearLayoutManager);

    }

    private void setSpinnerdata() {
        Api.newApi(activity, API.BaseUrl + "lab/guardian_lab_list")
                .setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {
                            String dependid = json.getString(P.dependent_id);
                            JsonList jsonList = json.getJsonList(P.dependent_list);
                            DependentModel modelnew = new DependentModel();
                            modelnew.setName("Select Dependent");
                            modelnew.setUsername("Select Dependent");
                            modelnew.setDependent_id("0");
                            dependentModels.add(modelnew);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    DependentModel model = new DependentModel();
                                    model.setName(jvalue.getString(P.name));
                                    model.setUsername(jvalue.getString(P.username));
                                    model.setProfile_pic(jvalue.getString(P.profile_pic));
                                    model.setDependent_id(jvalue.getString(P.dependent_id));
                                    dependentModels.add(model);
                                }
                                DependentSelectionAdapter dependentSelectionAdapter = new DependentSelectionAdapter(activity, dependentModels);
                                binding.spinnerdependentlist.setAdapter(dependentSelectionAdapter);
                            }
                        } else {
                            H.showMessage(activity, json.getString(P.error));
                        }
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {

                    }
                })
                .run("guardlablist", token);


    }

    private void onClickspinner() {
        binding.spinnerdependentlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userid = dependentModels.get(position).getDependent_id();
                testResultModels = new ArrayList<>();
                Api.newApi(activity, API.BaseUrl + "lab/guardian_lab_list?dependent=" + userid)
                        .setMethod(Api.GET)
                        .onSuccess(new Api.OnSuccessListener() {
                            @Override
                            public void onSuccess(Json json) {
                                if (json.getInt(P.status) == 1) {
                                    String lab_image_path = "https://www.soulber.co/dev/uploads/lab/";
                                    Json data = json.getJson(P.data);
                                    JsonList jsonList = data.getJsonList(P.labs);
                                    if (jsonList != null && jsonList.size() != 0) {

                                        for (Json jvalue : jsonList) {
                                            binding.rvGuardtestresult.setVisibility(View.VISIBLE);
                                            GuardianTestResultModel modelnew = new GuardianTestResultModel();
                                            modelnew.setLab_name(jvalue.getString(P.lab_name));
                                            modelnew.setProfile_pic(jvalue.getString(P.profile_pic));
                                            modelnew.setImage(lab_image_path + jvalue.getString(P.image));
                                            modelnew.setUsername(jvalue.getString(P.dependent_username));
                                            modelnew.setRemark(jvalue.getString(P.remark));
                                            modelnew.setAddress(jvalue.getString(P.address));
                                            modelnew.setAppointment_date(jvalue.getString(P.appointment_date));
                                            testResultModels.add(modelnew);
                                        }
                                        adapter = new GuardianTestResultAdapter(activity, testResultModels, dependentModels);
                                        binding.rvGuardtestresult.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (jsonList.isEmpty()) {
                                        binding.rvGuardtestresult.setVisibility(View.GONE);
                                        binding.tvEmplylist.setVisibility(View.VISIBLE);
                                    }

                                }
                            }
                        })
                        .onError(new Api.OnErrorListener() {
                            @Override
                            public void onError() {

                            }
                        })
                        .run("hitpanicdetails", token);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void onClick() {
        binding.onbackTestresult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onDownload(GuardianReportModel model) {
        String reportUrl = "https://www.soulber.co/dev/uploads/lab/reports/";
        String docFile = reportUrl + model.getReport_file();
        onDownload(docFile, model.getReport_file());
    }


    public void onDownload(String url, String title) {
        pdf_url = url;
        pdf_title = title;
        if (TextUtils.isEmpty(pdf_url) || pdf_url.equals("null")) {
            H.showMessage(activity, "No pdf path found.");
        } else {
            getPermission();
        }
    }

    private void getAccess() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
        }
    }

    private void getPermission() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                READ_WRITE);
    }

    public void jumpToSetting() {
        H.showMessage(activity, "Allow permission from setting");
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRITE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Downloader.download(activity, pdf_url, pdf_title, Config.OPEN);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    jumpToSetting();
                } else {
                    getPermission();
                }
                return;
            }

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}