package com.soul.soulber.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentSelectionAdapter;
import com.soul.soulber.adapter.GuardianMeetingAdapter;
import com.soul.soulber.adapter.GuardianTestResultAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianMeetingBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.GuardianMeetingModel;
import com.soul.soulber.model.GuardianTestResultModel;

import java.util.ArrayList;

public class GuardianMeetingActivity extends AppCompatActivity {
    private GuardianMeetingActivity activity = this;
    private ActivityGuardianMeetingBinding binding;
    private GuardianMeetingAdapter adapter;
    ArrayList<DependentModel> dependentModels;
    Session session;
    LoadingDialog loadingDialog;
    String token;
    LinearLayoutManager linearLayoutManager;
    String userid;
    ArrayList<GuardianMeetingModel> meetlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_guardian_meeting);
        initView();
        setSpinnerdata();
        spinneronclick();
        onClick();

    }

    private void initView() {
        session = new Session(activity);
        loadingDialog = new LoadingDialog(activity);
        token = session.getJson(Config.userData).getString(P.user_token);
        dependentModels = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvGuardmeeting.setLayoutManager(linearLayoutManager);
    }

    private void setSpinnerdata() {
        Api.newApi(activity, API.BaseUrl + "meeting/guardian_meeting_attend_list")
                .setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {
                            DependentModel modelnew = new DependentModel();
                            modelnew.setName("Select Dependent");
                            modelnew.setUsername("Select Dependent");
                            modelnew.setDependent_id("0");
                            dependentModels.add(modelnew);
                            JsonList jsonList = json.getJsonList(P.dependent_list);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    DependentModel model = new DependentModel();
                                    model.setName(jvalue.getString(P.name));
                                    model.setUsername(jvalue.getString(P.username));
                                    model.setProfile_pic(jvalue.getString(P.profile_pic));
                                    model.setDependent_id(jvalue.getString(P.dependent_id));
                                    dependentModels.add(model);
                                }
                                DependentSelectionAdapter dependentSelectionAdapter = new DependentSelectionAdapter(activity, dependentModels);
                                binding.spinnerdependentlist.setAdapter(dependentSelectionAdapter);
                            }

                        } else {
                            H.showMessage(activity, json.getString(P.error));
                        }

                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {

                    }
                })
                .run("dependentdata", token);


    }

    private void spinneronclick() {
        binding.spinnerdependentlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userid = dependentModels.get(position).getDependent_id();
                meetlist = new ArrayList<>();
                Api.newApi(activity, API.BaseUrl + "meeting/guardian_meeting_attend_list?dependent=" + userid)
                        .setMethod(Api.GET)
                        .onSuccess(new Api.OnSuccessListener() {
                            @Override
                            public void onSuccess(Json json) {
                                if (json.getInt(P.status) == 1) {
                                    String meet_image_path = "https://www.soulber.co/dev/uploads/meetings/";
                                    Json data = json.getJson(P.data);
                                    JsonList jsonList = data.getJsonList(P.meetings);
                                    if (jsonList != null && jsonList.size() != 0) {

                                        for (Json jvalue : jsonList) {
                                            binding.rvGuardmeeting.setVisibility(View.VISIBLE);
                                            GuardianMeetingModel modelnew = new GuardianMeetingModel();
                                            modelnew.setCity(jvalue.getString(P.city));
                                            modelnew.setDate(jvalue.getString(P.date));
                                            modelnew.setProfile_pic(jvalue.getString(P.profile_pic));
                                            modelnew.setPrimary_image(meet_image_path + jvalue.getString(P.primary_image));
                                            modelnew.setUsername(jvalue.getString(P.dependent_username));
                                            modelnew.setRemark(jvalue.getString(P.remark));
                                            modelnew.setVenue(jvalue.getString(P.venue));
                                            modelnew.setTime(jvalue.getString(P.time));
                                            modelnew.setTopic(jvalue.getString(P.topic));
                                            meetlist.add(modelnew);
                                        }
                                        adapter = new GuardianMeetingAdapter(activity, meetlist);
                                        binding.rvGuardmeeting.setAdapter(adapter);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (jsonList.isEmpty()) {
                                        binding.rvGuardmeeting.setVisibility(View.GONE);
                                        binding.tvEmplylist.setVisibility(View.VISIBLE);
                                    }

                                }
                            }
                        })
                        .onError(new Api.OnErrorListener() {
                            @Override
                            public void onError() {

                            }
                        })
                        .run("hitmeetinglist", token);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    private void onClick() {
        binding.onbackMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}