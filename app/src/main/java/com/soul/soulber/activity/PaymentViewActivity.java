package com.soul.soulber.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.LoadingDialog;
import com.soul.soulber.R;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityPaymentViewBinding;
import com.soul.soulber.util.Click;

public class PaymentViewActivity extends AppCompatActivity {

    private ActivityPaymentViewBinding binding;
    private PaymentViewActivity activity = this;

    private LoadingDialog loadingDialog;

    private String name;
    private String email;
    private String contact;
    private String orderId;
    private String payment_form_url;
    private String registerAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_view);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);

        name = getIntent().getStringExtra(P.name);
        email = getIntent().getStringExtra(P.email);
        contact = getIntent().getStringExtra(P.contact);
        orderId = getIntent().getStringExtra(P.order_id);
        payment_form_url = getIntent().getStringExtra(P.payment_form_url);
        registerAmount = getIntent().getStringExtra(P.registerAmount);


        binding.txtName.setText(name);
        binding.txtEmail.setText(email);
        binding.txtNumber.setText(contact);
        binding.txtPrice.setText("$" + registerAmount);
        onClick();

    }

    private void onClick() {
        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });

        binding.txtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });

        binding.btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(activity, PaymentWebViewActivity.class);
                intent.putExtra(P.order_id, orderId);
                intent.putExtra(P.payment_form_url, payment_form_url);
                intent.putExtra(P.registerAmount, registerAmount);
                ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
                startActivity(intent, options.toBundle());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}