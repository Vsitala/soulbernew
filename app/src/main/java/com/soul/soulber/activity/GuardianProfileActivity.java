package com.soul.soulber.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.adapter.GuardianProfileAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianProfileBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;

public class GuardianProfileActivity extends AppCompatActivity {

    private ActivityGuardianProfileBinding binding;
    private GuardianProfileActivity activity = this;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";
    Session session;
    String userID;
    private TextView txtMessage, txtReportUser, txtBlockUser, txtLogOut;
    RelativeLayout slideView;
    private SlideUp slideUp;
    String token;
    String usertype_id;

    private LoadingDialog loadingDialog;

    ArrayList<DependentModel> dependentlist;
    private GuardianProfileAdapter adapter;

    String connected = "Connected";
    String connect = "Connect";
    String requested = "Requested";
    String follow = "Follow";
    String followed = "Followed";
    String block = "Block this user";
    String unblock = "Unblock this user";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_guardian_profile);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);

        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);

        userID = getIntent().getStringExtra(P.id);

        txtMessage = findViewById(R.id.txtMessage);
        txtReportUser = findViewById(R.id.txtReportUser);
        txtBlockUser = findViewById(R.id.txtBlockUser);
        txtLogOut = findViewById(R.id.txtLogOut);

        txtLogOut.setVisibility(View.GONE);

        dependentlist = new ArrayList<>();
        adapter = new GuardianProfileAdapter(activity, dependentlist);
        binding.recyclerDependent.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerDependent.setHasFixedSize(true);
        binding.recyclerDependent.setNestedScrollingEnabled(false);
        binding.recyclerDependent.setAdapter(adapter);


        slideView = findViewById(R.id.slideViewreport);
        slideUp = new SlideUpBuilder(slideView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        binding.dimprofile.setVisibility(View.VISIBLE);
                        if (visibility == View.GONE) {
                            binding.dimprofile.setVisibility(View.GONE);
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .withSlideFromOtherView(findViewById(R.id.slideViewreport))
                .build();

        binding.dimprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.hide();
                binding.dimprofile.setVisibility(View.GONE);
            }
        });

        onClick();
        hitUserDetails(token);
        hitDependentList(token);
    }


    public void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;

            dependentlist.clear();
            adapter.notifyDataSetChanged();
            hitDependentList(token);
        }
    }

    private void hideSlide() {
        try {
            slideUp.hide();
            binding.dimprofile.performClick();
        } catch (Exception e) {

        }
    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });


        binding.ivUsersmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.show();
            }
        });

        binding.btnConnectUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (binding.btnConnectUser.getText().toString().equals(connect)) {
                    hitConnectUser(token);
                }

            }
        });

        binding.btnFollowUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (binding.btnFollowUser.getText().toString().equals(follow)) {
                    hitFollowUser(token);
                }
            }
        });

        binding.btnChaUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToChat();
            }
        });

        txtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hideSlide();
                jumpToChat();
            }
        });

        txtReportUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hideSlide();
                hitReportUser(token);
            }
        });

        txtBlockUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hideSlide();
                hitBlockUnblockUser(token);
            }
        });

        binding.lnrConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                if (usertype_id.equals(Config.DEPENDENT)) {
//                    Intent intentDependent = new Intent(activity, DependentConnectionActivity.class);
//                    startActivity(intentDependent);
//                } else if (usertype_id.equals(Config.GUARDIAN)) {
//                    Intent intentGuardian = new Intent(activity, GuardianConnectionActivity.class);
//                    startActivity(intentGuardian);
//                }
            }
        });

        binding.lnrFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                Intent intent = new Intent(activity, FollowersActivity.class);
//                startActivity(intent);
            }
        });

        binding.lnrFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                Intent intent = new Intent(activity, FollowingActivity.class);
//                startActivity(intent);
            }
        });

        txtLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                logOutUser();
            }
        });
    }

    private void jumpToChat() {
        Intent intent = new Intent(activity, ChatDetailsActivity.class);
        intent.putExtra(P.id, userID);
        intent.putExtra(P.username, binding.txtTitle.getText().toString().trim());
        startActivity(intent);
    }

    private void logOutUser() {
        session.clear();
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private void hitDependentList(String token) {
        ProgressView.show(activity, loadingDialog);
        Api.newApi(activity, API.BaseUrl + "guardian/connection/my_connections")
                .setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {

                            Json data = json.getJson(P.data);

                            String profile_pic_path = data.getString(P.profile_pic_path);

                            JsonList jsonList = data.getJsonList(P.dependent_connection);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jsonValue : jsonList) {

                                    DependentModel model = new DependentModel();
                                    model.setName(jsonValue.getString(P.dependent_name));
                                    model.setCity(jsonValue.getString(P.city));
                                    model.setAge(jsonValue.getString(P.Age));
                                    model.setDependent_id(jsonValue.getString(P.dependent_id));
                                    model.setProfile_pic(profile_pic_path + jsonValue.getString(P.profile_pic));
                                    model.setIs_accepted(jsonValue.getString(P.is_accepted));
                                    dependentlist.add(model);

                                }

                                adapter.notifyDataSetChanged();

                            }

                        }
                        ProgressView.dismiss(loadingDialog);
                        checkData();
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {
                        H.showMessage(activity, "On error is called");
                        ProgressView.dismiss(loadingDialog);
                        checkData();
                    }
                })
                .run("hitDependentList", token);

    }

    private void checkData() {
        if (dependentlist.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }


    private void hitUserDetails(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.guardian_id, userID);
        Api.newApi(activity, API.BaseUrl + "userprofiles/single_guardian_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json guardian_details = data.getJson(P.guardian_details);
                        setUserData(guardian_details);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitUserDetails", token);
    }

    private void setUserData(Json json) {

        LoadImage.glideString(activity, binding.imgProfile, imageBaseUrl + json.getString(P.profile_pic), getResources().getDrawable(R.drawable.ic_baseline_person_24));
        binding.txtTitle.setText(checkString(json.getString(P.username)));
        binding.txtUserName.setText(checkString(json.getString(P.name)));
        binding.txtUserBio.setText(checkString(json.getString(P.bio)));

        String is_connected = json.getString(P.is_connected);
        String is_following = json.getString(P.is_following);
        String is_block = json.getString(P.is_blocked);

        if (is_connected.equals("1")) {
            binding.btnConnectUser.setText(connected + " ✓");
            binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
            binding.btnChaUser.setVisibility(View.VISIBLE);
            txtMessage.setVisibility(View.VISIBLE);
        } else if (is_connected.equals("2")) {
            binding.btnConnectUser.setText(requested);
            binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
            binding.btnChaUser.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
        } else if (is_connected.equals("0")) {
            binding.btnConnectUser.setText(connect);
            binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_dark_bg));
            binding.btnChaUser.setVisibility(View.GONE);
            txtMessage.setVisibility(View.GONE);
        }

        if (is_following.equals("1")) {
            binding.btnFollowUser.setText(followed);
            binding.btnFollowUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
        } else {
            binding.btnFollowUser.setText(follow);
            binding.btnFollowUser.setBackground(getResources().getDrawable(R.drawable.button_dark_bg));
        }

        if (is_block.equals("1")) {
            txtBlockUser.setText(unblock);
            txtBlockUser.setTextColor(getResources().getColor(R.color.lightgreen));
        } else {
            txtBlockUser.setText(block);
            txtBlockUser.setTextColor(getResources().getColor(R.color.saffron));
        }

        if (json.has(P.connections)) {
            binding.txtConnection.setText(json.getString(P.connections));
        }
        if (json.has(P.followers)) {
            binding.txtFollowers.setText(json.getString(P.followers));
        }
        if (json.has(P.following)) {
            binding.txtFollowing.setText(json.getString(P.following));
        }

    }

    private void hitConnectUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.guardian_id, userID);
        Api.newApi(activity, API.BaseUrl + "guardian/connection/connect_guardian").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        binding.btnConnectUser.setText(requested);
                        binding.btnConnectUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitConnectUser", token);
    }

    private void hitFollowUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.follow_to_id, userID);
        Api.newApi(activity, API.BaseUrl + "Follower/follow_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        binding.btnFollowUser.setText(followed);
                        binding.btnFollowUser.setBackground(getResources().getDrawable(R.drawable.button_light_bg));
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitFollowUser", token);
    }

    private void hitReportUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.report_user, userID);
        Api.newApi(activity, API.BaseUrl + "userprofiles/report_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, "User report sent successfully");
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitReportUser", token);
    }

    private void hitBlockUnblockUser(String token) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.block_user, userID);
        Api.newApi(activity, API.BaseUrl + "userprofiles/block_user").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        if (txtBlockUser.getText().toString().equals(block)) {
                            txtBlockUser.setText(unblock);
                            txtBlockUser.setTextColor(getResources().getColor(R.color.lightgreen));
                            H.showMessage(activity, "User blocked");
                        } else if (txtBlockUser.getText().toString().equals(unblock)) {
                            txtBlockUser.setText(block);
                            txtBlockUser.setTextColor(getResources().getColor(R.color.saffron));
                            H.showMessage(activity, "User unblocked");
                        }
                        Config.IS_BLOCK_UNBLOCK = true;
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitBlockUnblockUser", token);
    }

}