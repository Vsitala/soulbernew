package com.soul.soulber.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.github.chrisbanes.photoview.PhotoView;
import com.soul.soulber.R;
import com.soul.soulber.adapter.ForumCommentAdapter;
import com.soul.soulber.adapter.ForumReplyAdapter;
import com.soul.soulber.adapter.UserReplyAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityForumDetailsBinding;
import com.soul.soulber.model.ForumCommentModel;
import com.soul.soulber.model.ForumModel;
import com.soul.soulber.model.ForumReplyModel;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.model.UserReplyModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class ForumDetailsActivity extends AppCompatActivity implements ForumCommentAdapter.onClick {

    private ForumDetailsActivity activity = this;
    private ActivityForumDetailsBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private String forumID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    int countReply = 0;
    int pageCountReply = 1;
    String commentID = "";

    private String postImage = "";

    private List<ForumCommentModel> forumCommentModelList;
    private ForumCommentAdapter adapter;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forum_details);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        forumID = getIntent().getStringExtra(P.id);

        forumCommentModelList = new ArrayList<>();
        adapter = new ForumCommentAdapter(activity, forumCommentModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerComment.setLayoutManager(linearLayoutManager);
        binding.recyclerComment.setHasFixedSize(true);
        binding.recyclerComment.setNestedScrollingEnabled(false);
        binding.recyclerComment.setAdapter(adapter);

        hitForumDetailsData();
        hitForumCommentData(pageCount);
        setPagination();
        onClick();

    }


    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

        binding.imgImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewImageDialog(postImage);
            }
        });

        binding.txtUpVotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitUpvote();
            }
        });

        binding.txtDownVotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitDownvote();
            }
        });

        binding.btnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(binding.etxMessage.getText().toString().trim())) {
                    H.showMessage(activity, "Please enter comment");
                } else {
                    hitPostCommentData();
                }
            }
        });

    }

    private void hitForumDetailsData() {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.forum_id, forumID);
        Api.newApi(activity, API.BaseUrl + "forums/single_forum_detail").addJson(j)
                .setMethod(Api.POST)
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {
                        ProgressView.dismiss(loadingDialog);
                        H.showMessage(activity, "On error is called");

                    }
                })
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        ProgressView.dismiss(loadingDialog);

                        if (json.getInt(P.status) == 1) {

                            Json data = json.getJson(P.data);

                            String forums_path_img = data.getString(P.forums_path_img);
                            String id = data.getString(P.id);
                            String forum_text = data.getString(P.forum_text);
                            String upvotes = data.getString(P.upvotes);
                            String downvotes = data.getString(P.downvotes);
                            String comments = data.getString(P.comments);
                            String is_delete = data.getString(P.is_delete);
                            String add_date = data.getString(P.add_date);
                            postImage = forums_path_img + "/" + data.getString(P.image);

                            LoadImage.glideString(activity, binding.imgImage, postImage, getResources().getDrawable(R.drawable.ic_no_image));
                            binding.txtTitle.setText(checkString(forum_text));
                            binding.txtUpVotes.setText(checkStringCount(upvotes) + " upvotes");
                            binding.txtDownVotes.setText(checkStringCount(downvotes) + " downvotes");
                            binding.txtComment.setText(checkStringCount(comments) + " comments");

                        } else {
                            H.showMessage(activity, "On error is called");
                        }

                    }
                })
                .run("hitForumDetailsData", token);

    }


    private void setPagination() {
        binding.recyclerComment.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (forumCommentModelList != null && !forumCommentModelList.isEmpty()) {
                        if (forumCommentModelList.size() < count) {
                            pageCount++;
                            hitForumCommentData(pageCount);
                        }
                    }
                }
            }
        });
    }


    private void hitForumCommentData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "forums/show_comments?page=" + pageCount + "&per_page=20&forum_id=" + forumID)
                .setMethod(Api.GET)
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {
                        ProgressView.dismiss(loadingDialog);
                        H.showMessage(activity, "On error is called");
                        checkData();
                    }
                })
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        ProgressView.dismiss(loadingDialog);

                        if (json.getInt(P.status) == 1) {

                            Json data = json.getJson(P.data);

                            int num_rows = data.getInt(P.num_rows);
                            try {
                                count = num_rows;
                            } catch (Exception e) {
                                count = 0;
                            }

                            if (pageCount == 1) {
                                forumCommentModelList.clear();
                                adapter.notifyDataSetChanged();
                            }

                            JsonList list = data.getJsonList(P.list);

                            if (list != null && list.size() != 0) {
                                for (Json jsonValue : list) {
                                    ForumCommentModel model = new ForumCommentModel();
                                    model.setId(jsonValue.getString(P.id));
                                    model.setForum_id(jsonValue.getString(P.forum_id));
                                    model.setComment(jsonValue.getString(P.comment));
                                    model.setParent_comment_id(jsonValue.getString(P.parent_comment_id));
                                    model.setUser_id(jsonValue.getString(P.user_id));
                                    model.setLikes(jsonValue.getString(P.likes));
                                    model.setAdd_date(jsonValue.getString(P.add_date));
                                    model.setIs_delete(jsonValue.getString(P.is_delete));
                                    model.setName(jsonValue.getString(P.name));
                                    model.setIs_liked(jsonValue.getString(P.is_liked));
                                    model.setReplies_count(jsonValue.getString(P.replies_count));
                                    model.setComment_time(jsonValue.getString(P.comment_time));
                                    model.setUsername(jsonValue.getString(P.username));
                                    forumCommentModelList.add(model);

                                }
                                adapter.notifyDataSetChanged();
                            }
                            checkData();
                        } else {
                            checkData();
                            H.showMessage(activity, "On error is called");
                        }

                    }
                })
                .run("hitForumCommentData", token);

    }


    private void hitPostCommentData() {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.forum_id, forumID);
        j.addString(P.comment, binding.etxMessage.getText().toString().trim());

        Api.newApi(activity, API.BaseUrl + "forums/add_comment").addJson(j)
                .setMethod(Api.POST)
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {
                        ProgressView.dismiss(loadingDialog);
                        H.showMessage(activity, "On error is called");
                    }
                })
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        ProgressView.dismiss(loadingDialog);

                        if (json.getInt(P.status) == 1) {
                            Config.FORUM_ACTION = true;
                            binding.etxMessage.setText("");
                            hitForumDetailsData();
                            pageCount = 1;
                            hitForumCommentData(pageCount);
                        } else {
                            H.showMessage(activity, "On error is called");
                        }

                    }
                })
                .run("hitForumDetailsData", token);

    }


    private void hitUpvote() {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.forum_id, forumID);

        Api.newApi(activity, API.BaseUrl + "forums/forum_upvote").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Config.FORUM_ACTION = true;
                        hitForumDetailsData();
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitUpvote", token);
    }

    private void hitDownvote() {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.forum_id, forumID);

        Api.newApi(activity, API.BaseUrl + "forums/forum_downvote").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Config.FORUM_ACTION = true;
                        hitForumDetailsData();
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitDownvote", token);
    }


    private void checkData() {
        if (forumCommentModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    private void viewImageDialog(String imagePath) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_image_view);

        PhotoView imageView = dialog.findViewById(R.id.imageView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        LoadImage.glideString(activity, imageView, imagePath, activity.getResources().getDrawable(R.drawable.ic_no_image));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    @Override
    public void onReply(ForumCommentModel model, TextView txtReplyCountView) {
        replyDialog(model, txtReplyCountView);
    }


    private void replyDialog(ForumCommentModel model, TextView txtReplyCountView) {

        Json jsonProfile = session.getJson(Config.userProfileData);
        String profilePic = imageBaseUrl + jsonProfile.getString(P.profile_pic);

        pageCountReply = 1;
        countReply = 0;
        commentID = model.getId();

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_reply_view);

        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        ImageView imgReplyPerson = dialog.findViewById(R.id.imgReplyPerson);
        EditText etxReply = dialog.findViewById(R.id.etxReply);
        TextView txtPostReply = dialog.findViewById(R.id.txtPostReply);
        TextView txtComment = dialog.findViewById(R.id.txtComment);
        TextView txtViewMoreReply = dialog.findViewById(R.id.txtViewMoreReply);
        RecyclerView recyclerReply = dialog.findViewById(R.id.recyclerReply);

        LoadImage.glideString(activity, imgReplyPerson, profilePic, getResources().getDrawable(R.drawable.ic_baseline_person_24));

        recyclerReply.setLayoutManager(new LinearLayoutManager(activity));
        recyclerReply.setHasFixedSize(true);

        List<ForumReplyModel> forumReplyModelList = new ArrayList<>();
        ForumReplyAdapter adapter = new ForumReplyAdapter(activity, forumReplyModelList);
        recyclerReply.setAdapter(adapter);

        String comments = "Comment: " + model.getComment();
        comments = comments.replace("Comment:", "<font color='#5D816C'>Comment:</font>");
        txtComment.setText(Html.fromHtml(comments));

        txtPostReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(etxReply.getText().toString().trim())) {
                    H.showMessage(activity, "Please enter reply");
                } else {
                    hitPostReply(model, etxReply, forumReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);
                }
            }
        });

        txtViewMoreReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (forumReplyModelList.size() < countReply) {
                    pageCountReply = pageCountReply + 1;
                    hitReplyData(pageCountReply, forumReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        hitReplyData(pageCountReply, forumReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    private void hitPostReply(ForumCommentModel model, EditText editText, List<ForumReplyModel> forumReplyModelList, ForumReplyAdapter adapter, TextView txtViewMoreReply, RecyclerView recyclerReply, TextView txtReplyCountView) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, model.getId());
        j.addString(P.comment, editText.getText().toString().trim());

        Api.newApi(activity, API.BaseUrl + "forums/reply_to_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        editText.setText("");
                        pageCountReply = 1;
                        hitReplyData(pageCountReply, forumReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitPostReply", token);
    }


    private void hitReplyData(int pageCount, List<ForumReplyModel> forumReplyModelList, ForumReplyAdapter adapter, TextView txtViewMoreReply, RecyclerView recyclerReply, TextView txtReplyCountView) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "forums/show_comments_replies?page=" + pageCount + "&per_page=20&comment_id=" + commentID)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        int num_rows = data.getInt(P.num_rows);

                        try {
                            countReply = num_rows;
                        } catch (Exception e) {
                            countReply = 0;
                        }

                        JsonList list = data.getJsonList(P.list);
                        if (list != null && list.size() != 0) {

                            if (pageCount == 1) {
                                forumReplyModelList.clear();
                            }

                            for (Json jsonValue : list) {
                                ForumReplyModel model = new ForumReplyModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setForum_id(jsonValue.getString(P.forum_id));
                                model.setComment(jsonValue.getString(P.comment));
                                model.setParent_comment_id(jsonValue.getString(P.parent_comment_id));
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setLikes(jsonValue.getString(P.likes));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setIs_delete(jsonValue.getString(P.is_delete));
                                model.setReplies_count(jsonValue.getString(P.replies_count));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setComment_time(jsonValue.getString(P.comment_time));
                                model.setProfile_pic(profile_pic_path + "/" + jsonValue.getString(P.profile_pic));

                                forumReplyModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                            try {
                                recyclerReply.smoothScrollToPosition(0);
                            } catch (Exception e) {
                            }

                            if (list.size() < num_rows) {
                                txtViewMoreReply.setVisibility(View.VISIBLE);
                            } else {
                                txtViewMoreReply.setVisibility(View.GONE);
                            }

                            txtReplyCountView.setText("View " + list.size() + " replies");
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitReplyData", token);
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkStringCount(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "0";
        } else {
            value = string;
        }
        return value;
    }
}