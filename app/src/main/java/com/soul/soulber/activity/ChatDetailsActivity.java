package com.soul.soulber.activity;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.ChatDetailsAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityChatDetailsBinding;
import com.soul.soulber.model.ChatDetailsModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import io.github.rockerhieu.emojiconize.Emojiconize;


public class ChatDetailsActivity extends AppCompatActivity {

    private ChatDetailsActivity activity = this;
    private ActivityChatDetailsBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private String id = "";
    private String username = "";

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private String fromFirstTime = "1";
    private String fromPagination = "2";
    private String fromSendMessage = "3";

    private String message = "";

    private List<ChatDetailsModel> chatDetailsModelList;
    private ChatDetailsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Emojiconize.activity(this).go();
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat_details);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        id = getIntent().getStringExtra(P.id);
        username = getIntent().getStringExtra(P.username);

        binding.txtName.setText(username);

        chatDetailsModelList = new ArrayList<>();
        adapter = new ChatDetailsAdapter(activity, chatDetailsModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        binding.recyclerMessage.setLayoutManager(linearLayoutManager);
        binding.recyclerMessage.setHasFixedSize(true);
        binding.recyclerMessage.setAdapter(adapter);

        binding.etxMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence newText, int start, int before, int count) {
                message = newText.toString();
                if (!TextUtils.isEmpty(newText)) {
                    binding.lnrPost.setVisibility(View.VISIBLE);
                } else {
                    binding.lnrPost.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.imgFace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.etxMessage.requestFocus();
                    InputMethodManager imgr = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imgr.showSoftInput(binding.etxMessage, 0);
                    imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                } catch (Exception e) {
                }
            }
        });

        hitMessageListData(pageCount, fromFirstTime);
        setPagination();
        onClick();

    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

        binding.lnrPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(binding.etxMessage.getText().toString().trim())) {
                    H.showMessage(activity, "Please enter message");
                } else {
                    hitSendMessage(token);
                }
            }
        });

    }


    private void setPagination() {
        binding.recyclerMessage.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (chatDetailsModelList != null && !chatDetailsModelList.isEmpty()) {
                        if (chatDetailsModelList.size() < count) {
                            pageCount++;
                            hitMessageListData(pageCount, fromPagination);
                        }
                    }
                }
            }
        });
    }

    private void hitMessageListData(int pageCount, String from) {

        if (from.equals(fromFirstTime) || from.equals(fromPagination)) {
            ProgressView.show(activity, loadingDialog);
        } else {
            clearView();
        }

        Json j = new Json();
        j.addString(P.to_user_id, id);
        j.addString(P.page, pageCount + "");
        j.addString(P.per_page, "100");

        Api.newApi(activity, API.BaseUrl + "Userprofiles/get_all_messages").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    if (from.equals(fromFirstTime) || from.equals(fromPagination)) {
                        ProgressView.dismiss(loadingDialog);
                    }
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    if (from.equals(fromFirstTime) || from.equals(fromPagination)) {
                        ProgressView.dismiss(loadingDialog);
                    }

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            chatDetailsModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList messages = data.getJsonList(P.messages);
                        if (messages != null && messages.size() != 0) {
                            for (Json jsonValue : messages) {

                                ChatDetailsModel model = new ChatDetailsModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setMessage_thread_id(jsonValue.getString(P.message_thread_id));
                                model.setFrom_user_id(jsonValue.getString(P.from_user_id));
                                model.setTo_user_id(jsonValue.getString(P.to_user_id));
                                model.setMessage(jsonValue.getString(P.message));
                                model.setMessage_date(jsonValue.getString(P.message_date));
                                model.setFormatted_date(jsonValue.getString(P.formatted_date));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setProfile_pic(jsonValue.getString(P.profile_pic));

                                chatDetailsModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                            if (from.equals(fromFirstTime)) {
                                binding.recyclerMessage.scrollToPosition(0);
                            } else if (from.equals(fromPagination)) {
//                                binding.recyclerMessage.scrollToPosition(chatDetailsModelList.size() - 1);
                            } else if (from.equals(fromSendMessage)) {
                                binding.recyclerMessage.scrollToPosition(0);
                            }
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitMessageListData", token);
    }

    private void hitSendMessage(String token) {

//        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.to_user_id, id);
        j.addString(P.message, encodeEmoji(message));
        Api.newApi(activity, API.BaseUrl + "Userprofiles/send_message").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
//                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
//                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        pageCount = 1;
                        hitMessageListData(pageCount, fromSendMessage);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitSendMessage", token);
    }

    private void clearView() {
        binding.etxMessage.setText("");
        hidekey();
    }

    private void hidekey() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
        }
    }

    public static String encodeEmoji(String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }


    public static String decodeEmoji(String message) {
        String myString = null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

    public static InputFilter EMOJI_FILTER = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            boolean keepOriginal = true;
            StringBuilder sb = new StringBuilder(end - start);
            for (int index = start; index < end; index++) {
                int type = Character.getType(source.charAt(index));
                if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                    return "";
                }
                char c = source.charAt(index);
                if (isCharAllowed(c))
                    sb.append(c);
                else
                    keepOriginal = false;
            }
            if (keepOriginal)
                return null;
            else {
                if (source instanceof Spanned) {
                    SpannableString sp = new SpannableString(sb);
                    TextUtils.copySpansFrom((Spanned) source, start, sb.length(), null, sp, 0);
                    return sp;
                } else {
                    return sb;
                }
            }
        }
    };

    private static boolean isCharAllowed(char c) {
        return Character.isLetterOrDigit(c) || Character.isSpaceChar(c);
    }

}