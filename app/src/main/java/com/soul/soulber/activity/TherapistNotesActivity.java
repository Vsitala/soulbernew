package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.TestRecordsAdapter;
import com.soul.soulber.adapter.TheropistNotesAdapter;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityTestRecordsBinding;
import com.soul.soulber.databinding.ActivityTherapistNotesBinding;
import com.soul.soulber.model.TestRecordsModel;
import com.soul.soulber.model.TheropistHistoryModel;
import com.soul.soulber.model.TheropistNotesModel;
import com.soul.soulber.util.Click;

import java.util.ArrayList;
import java.util.List;

public class TherapistNotesActivity extends AppCompatActivity {

    private TherapistNotesActivity activity = this;
    private ActivityTherapistNotesBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private List<TheropistNotesModel> theropistNotesModelList;
    private TheropistNotesAdapter theropistNotesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_therapist_notes);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        theropistNotesModelList = new ArrayList<>();
        theropistNotesAdapter = new TheropistNotesAdapter(activity, theropistNotesModelList);
        binding.recyclerTherapistNotes.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerTherapistNotes.setHasFixedSize(true);
        binding.recyclerTherapistNotes.setNestedScrollingEnabled(false);
        binding.recyclerTherapistNotes.setAdapter(theropistNotesAdapter);

        onClick();
    }


    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                finish();
            }
        });

    }

}