package com.soul.soulber.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.android.material.tabs.TabLayout;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianConnectionBinding;
import com.soul.soulber.fragment.DependentConnectionFragment;
import com.soul.soulber.fragment.GuardianConnectionFragment;
import com.soul.soulber.util.Click;

import java.util.ArrayList;
import java.util.List;

public class GuardianConnectionActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private ActivityGuardianConnectionBinding binding;
    private GuardianConnectionActivity activity = this;

    private GuardianConnectionFragment guardianConnectionFragment = GuardianConnectionFragment.newInstance();
    private DependentConnectionFragment dependentConnectionFragment = DependentConnectionFragment.newInstance();

    private String guardian = "Guardian";
    private String dependent = "Dependent";

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_guardian_connection);

        initView();

    }

    private void initView() {

        Config.CONNECTION_APPLY = false;

        session = new Session(activity);
        loadingDialog = new LoadingDialog(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        setupViewPager(binding.viewPager);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.addOnPageChangeListener(activity);
        setupTabIcons();
        onClick();

    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(guardianConnectionFragment, "");
        adapter.addFragment(dependentConnectionFragment, "");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {

        View view1 = LayoutInflater.from(this).inflate(R.layout.activity_customt_ab, null);
        ((TextView) view1.findViewById(R.id.text)).setText(guardian);
        ((TextView) view1.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));

        View view2 = LayoutInflater.from(this).inflate(R.layout.activity_customt_ab, null);
        ((TextView) view2.findViewById(R.id.text)).setText(dependent);
        ((TextView) view2.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));


        binding.tabLayout.getTabAt(0).setCustomView(view1);
        binding.tabLayout.getTabAt(1).setCustomView(view2);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void onClick() {
        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.txtAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(activity, GuardianAddConnectionActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.CONNECTION_APPLY) {
            Config.CONNECTION_APPLY = false;
            guardianConnectionFragment.hitMyConnection(token);
            dependentConnectionFragment.hitMyConnection(token);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}