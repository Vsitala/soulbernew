package com.soul.soulber.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentSelectionAdapter;
import com.soul.soulber.adapter.GuardianTheraphyAdapter;
import com.soul.soulber.adapter.PanicHistoryChildAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianTherapyBinding;
import com.soul.soulber.model.ChildExpandableModel;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.GuardianTheraphyModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;

public class GuardianTherapyActivity extends AppCompatActivity {
    private ActivityGuardianTherapyBinding binding;
    private GuardianTherapyActivity activity = this;
    Session session;
    String token;
    GuardianTheraphyAdapter listAdapter;
    private ArrayList<DependentModel> listDataHeader;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<GuardianTheraphyModel> panicchildlist;
    int lastExpandedPosition = -1;
    String dependentid;
    String userid;
    LoadingDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_guardian_therapy);
        initview();
    }

    public void initview() {
        session = new Session(activity);
        loadingDialog = new LoadingDialog(activity);
        token = session.getJson(Config.userData).getString(P.user_token);
        listDataHeader = new ArrayList<>();
        linearLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        binding.rvTheraphy.setLayoutManager(linearLayoutManager);
        setspinnerlist();
        onClickspinner();
        onClick();
    }

    private void setspinnerlist() {
        ProgressView.show(activity, loadingDialog);
        Api.newApi(activity, API.BaseUrl + "therapist/guardian_therapist_list").setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {
                            ProgressView.dismiss(loadingDialog);
                            String profile_pic_path = "https://www.soulber.co/dev/uploads/profile_pic/";
                            JsonList jsonList = json.getJsonList(P.dependent_list);
                            DependentModel modelnew = new DependentModel();
                            modelnew.setName("Select Dependent");
                            modelnew.setUsername("Select Dependent");
                            modelnew.setDependent_id("0");
                            listDataHeader.add(modelnew);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    DependentModel model = new DependentModel();
                                    model.setName(jvalue.getString(P.name));
                                    model.setUsername(jvalue.getString(P.username));
                                    model.setProfile_pic(jvalue.getString(P.profile_pic));
                                    model.setDependent_id(jvalue.getString(P.dependent_id));
                                    listDataHeader.add(model);
                                }

                                DependentSelectionAdapter dependentSelectionAdapter = new DependentSelectionAdapter(activity, listDataHeader);
//                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                                    binding.spinnerdependentlist.setPopupBackgroundResource(R.drawable.spinnerbg);
//                                    binding.spinnerdependentlist.setBackgroundResource(R.drawable.spinner);
//                                }
//                                binding.spinnerdependentlist.setBackgroundResource(R.drawable.spinnerlollipop);
                                binding.spinnerdependentlist.setAdapter(dependentSelectionAdapter);
                            } else {
                                H.showMessage(activity, json.getString(P.error));
                            }

                        }
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {

                    }
                })
                .run("hitdependentpanic", token);

    }

    private void onClickspinner() {
        binding.spinnerdependentlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                userid = listDataHeader.get(position).getDependent_id();
                panicchildlist = new ArrayList<>();
                Api.newApi(activity, API.BaseUrl + "therapist/guardian_therapist_list?dependent=" + userid)
                        .setMethod(Api.GET)
                        .onSuccess(new Api.OnSuccessListener() {
                            @Override
                            public void onSuccess(Json json) {
                                if (json.getInt(P.status) == 1) {
                                    String profile_pic_path = "https://www.soulber.co/dev/uploads/profile_pic/";
                                    String therapist_images_path = "https://www.soulber.co/dev/uploads/therapist/";
                                    Json data = json.getJson(P.data);
                                    JsonList jsonList = data.getJsonList(P.therapists);
                                    if (jsonList != null && jsonList.size() != 0) {

                                        for (Json jvalue : jsonList) {
                                            binding.rvTheraphy.setVisibility(View.VISIBLE);
                                            GuardianTheraphyModel modelnew = new GuardianTheraphyModel();
                                            modelnew.setUsername(jvalue.getString(P.dependent_username));
                                            modelnew.setAppointment_date(jvalue.getString(P.appointment_date));
                                            modelnew.setTherpist_name(jvalue.getString(P.therpist_name));
                                            modelnew.setImage(therapist_images_path + jvalue.getString(P.image));
                                            modelnew.setExperience(jvalue.getString(P.experience) + " years of experience");
                                            modelnew.setAppointment_date(jvalue.getString(P.appointment_date));
                                            modelnew.setProfilepic(jvalue.getString(P.profile_pic));
                                            modelnew.setRemark(jvalue.getString(P.remark));
                                            panicchildlist.add(modelnew);
                                        }
                                        listAdapter = new GuardianTheraphyAdapter(activity, panicchildlist);
                                        binding.rvTheraphy.setAdapter(listAdapter);
                                        listAdapter.notifyDataSetChanged();
                                    }
                                    if (jsonList.isEmpty()) {
                                        binding.rvTheraphy.setVisibility(View.GONE);
                                        binding.tvEmplylist.setVisibility(View.VISIBLE);
                                    }

                                }
                            }
                        })
                        .onError(new Api.OnErrorListener() {
                            @Override
                            public void onError() {

                            }
                        })
                        .run("hitpanicdetails", token);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    private void onClick() {
        binding.onbackTheraphy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}