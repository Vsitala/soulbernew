package com.soul.soulber.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityPaymentVerifyBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class PaymentVerifyActivity extends AppCompatActivity {

    private PaymentVerifyActivity activity = this;
    private ActivityPaymentVerifyBinding binding;

    private LoadingDialog loadingDialog;

    private String orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment_verify);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);

        orderId = getIntent().getStringExtra(P.order_id);
        hitVerifyPayment(orderId);

        onClick();
    }

    private void onClick() {

        binding.btnClose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToLogin();
            }
        });

        binding.btnClose2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToLogin();
            }
        });
    }

    private void hitVerifyPayment(String orderId) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.order_id, orderId);

        Api.newApi(activity, API.BaseUrl + "user/verify_register_payment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        binding.lnrSuccess.setVisibility(View.VISIBLE);
                        binding.lnrFail.setVisibility(View.GONE);
                        String msg = json.getString(P.msg);
                        binding.txtSuccessMessage.setText(msg);
                    } else {
                        binding.lnrFail.setVisibility(View.VISIBLE);
                        binding.lnrSuccess.setVisibility(View.GONE);
                        String error = json.getString(P.error);
                        binding.txtCancelMessage.setText(error);
                    }
                })
                .run("hitVerifyPayment");
    }

    private void jumpToLogin() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        jumpToLogin();
    }
}