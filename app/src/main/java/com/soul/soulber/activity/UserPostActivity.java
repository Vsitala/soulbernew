package com.soul.soulber.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.android.exoplayer2.ui.PlayerView;
import com.soul.soulber.R;
import com.soul.soulber.adapter.UserCommentAdapter;
import com.soul.soulber.adapter.UserPostAdapter;
import com.soul.soulber.adapter.UserReplyAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityUserPostBinding;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.model.UserPostModel;
import com.soul.soulber.model.UserReplyModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UserPostActivity extends AppCompatActivity implements UserPostAdapter.onClick, UserCommentAdapter.onClick {

    private ActivityUserPostBinding binding;
    private UserPostActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private List<UserPostModel> userPostModelList;
    private UserPostAdapter userPostAdapter;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;
    final public int REQUEST_CODE_READ_WRITE = 1212;
    private String post_image_url = "";
    private String post_by = "";
    private String post_message = "";
    private String profile_pic = "";

    int countReply = 0;
    int pageCountReply = 1;
    String commentID = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_post);

        initView();

    }

    private void initView() {

        getAccess();

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
        profile_pic = imageBaseUrl + session.getJson(Config.userProfileData).getString(P.profile_pic);

        userPostModelList = new ArrayList<>();
        userPostAdapter = new UserPostAdapter(activity, userPostModelList, profile_pic);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerUserPost.setLayoutManager(linearLayoutManager);
//        SnapHelper snapHelper = new PagerSnapHelper();
//        binding.recyclerUserPost.setLayoutManager(linearLayoutManager);
//        snapHelper.attachToRecyclerView(binding.recyclerUserPost);
        binding.recyclerUserPost.setHasFixedSize(true);
        binding.recyclerUserPost.setAdapter(userPostAdapter);
//        binding.recyclerUserPost.setItemViewCacheSize(userPostModelList.size());


        hitPostData(pageCount);
        setPagination();
        onClick();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            pageCount = 1;
            hitPostData(pageCount);
        }
    }

    private void setPagination() {
        binding.recyclerUserPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (userPostModelList != null && !userPostModelList.isEmpty()) {
                        if (userPostModelList.size() < count) {
                            pageCount++;
                            hitPostData(pageCount);
                        }
                    }
                }
            }
        });
    }


    private void hitPostData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        String api = "";
        Json j = new Json();
        j.addString(P.page, pageCount + "");

        if (Config.FOR_MY_POST) {
            j.addString(P.user_id, userID);
            api = "Posts/viewposts_by_userid";
        } else if (Config.IS_NOTIFICATION_POST) {
            j.addString(P.user_id, Config.NOTIFICATION_POST_ID);
            api = "Posts/viewposts_by_userid";
        } else {
            if (usertype_id.equals(Config.GUARDIAN)) {
                api = "posts/show_my_dependents_posts";
            } else if (usertype_id.equals(Config.DEPENDENT)) {
                api = "posts/show_my_followers_posts";
            }
        }

        Api.newApi(activity, API.BaseUrl + api).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String posts_img_path = data.getString(P.posts_img_path);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        String post_video_path = data.getString(P.post_video_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            userPostModelList.clear();
                            userPostAdapter.notifyDataSetChanged();
                        }

                        JsonList posts = data.getJsonList(P.posts);
                        if (posts != null && posts.size() != 0) {
                            for (Json jsonValue : posts) {
                                UserPostModel model = new UserPostModel();
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setPost_id(jsonValue.getString(P.post_id));
                                model.setMessage(jsonValue.getString(P.message));
                                model.setPost_img(posts_img_path + "/" + jsonValue.getString(P.post_img));
                                model.setPost_date(jsonValue.getString(P.post_date));
                                model.setLikes(jsonValue.getString(P.likes));
                                model.setComments(jsonValue.getString(P.comments));
                                model.setIs_liked_by_me(jsonValue.getString(P.is_liked_by_me));
                                model.setProfile_pic(profile_pic_path + "/" + jsonValue.getString(P.profile_pic));
                                model.setUpload_type(jsonValue.getString(P.upload_type));
                                model.setFont_style(jsonValue.getString(P.font_style));
                                model.setFont_color(jsonValue.getString(P.font_color));
                                model.setSubtitle(jsonValue.getString(P.subtitle));
                                model.setPost_video(post_video_path + "/" + jsonValue.getString(P.post_video));
                                model.setPaying(false);
                                userPostModelList.add(model);
                            }
                            userPostAdapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitPostData", token);
    }

    private void checkData() {
        if (userPostModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onShare(UserPostModel model) {

        if (model.getUpload_type().equals("1")) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
            share.putExtra(android.content.Intent.EXTRA_STREAM, Uri.parse(model.getPost_video()));
            share.putExtra(android.content.Intent.EXTRA_TEXT, "@Soulber\nPosted by - " + model.getName() + "\n\n" + model.getMessage());
            startActivity(Intent.createChooser(share, "Share using"));
        } else if (model.getUpload_type().equals("2")) {
            post_image_url = model.getPost_img();
            post_message = model.getMessage();
            post_by = model.getName();
            if (TextUtils.isEmpty(post_image_url) || post_image_url.equals("null")) {
                H.showMessage(activity, "No image found for share");
            } else {
                getPermission();
            }
        } else if (model.getUpload_type().equals("3")) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(android.content.Intent.EXTRA_TEXT, model.getSubtitle() + "\n" + model.getPost_video() + "\n\n@Soulber\nPosted by - " + model.getName());
            startActivity(Intent.createChooser(share, "Share using"));
        }

    }


    @Override
    public void onReply(UserCommentModel model, TextView txtReplyCountView) {
        replyDialog(model, txtReplyCountView);
    }

    private void onClick() {
        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getAccess() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
        }
    }

    public void getPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                REQUEST_CODE_READ_WRITE);
    }

    public void jumpToSetting() {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_CODE_READ_WRITE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            shareProduct();
                        }
                    }, 500);
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    jumpToSetting();
                } else {
                    getPermission();
                }
                return;
            }
        }
    }


    private void shareProduct() {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        try {

            BitmapDrawable drawable = (BitmapDrawable) drawableFromUrl(post_image_url);
            Bitmap b = drawable.getBitmap();
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/jpeg");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                    b, "product_image_" + randomString(), null);
            Uri imageUri = Uri.parse(path);
            share.putExtra(Intent.EXTRA_TEXT, post_message + "\n\n@Soulber\nPosted by - " + post_by);
            share.putExtra(Intent.EXTRA_STREAM, imageUri);
            startActivity(Intent.createChooser(share, "Share using"));
        } catch (android.content.ActivityNotFoundException ex) {
            H.showMessage(this, "Something went wrong to share image, try after some time");
        } catch (IOException e) {
            e.printStackTrace();
            H.showMessage(this, "Something went wrong to share image, try after some time");
        }

    }

    public static String randomString() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(10);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public Drawable drawableFromUrl(String url) throws IOException {
        Bitmap x;

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.connect();
        InputStream input = connection.getInputStream();

        x = BitmapFactory.decodeStream(input);
        return new BitmapDrawable(Resources.getSystem(), x);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void replyDialog(UserCommentModel model, TextView txtReplyCountView) {

        pageCountReply = 1;
        countReply = 0;
        commentID = model.getComment_id();

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_reply_view);

        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        ImageView imgReplyPerson = dialog.findViewById(R.id.imgReplyPerson);
        EditText etxReply = dialog.findViewById(R.id.etxReply);
        TextView txtPostReply = dialog.findViewById(R.id.txtPostReply);
        TextView txtComment = dialog.findViewById(R.id.txtComment);
        TextView txtViewMoreReply = dialog.findViewById(R.id.txtViewMoreReply);
        RecyclerView recyclerReply = dialog.findViewById(R.id.recyclerReply);

        LoadImage.glideString(activity, imgReplyPerson, profile_pic, getResources().getDrawable(R.drawable.ic_baseline_person_24));

        recyclerReply.setLayoutManager(new LinearLayoutManager(activity));
        recyclerReply.setHasFixedSize(true);

        List<UserReplyModel> userReplyModelList = new ArrayList<>();
        UserReplyAdapter adapter = new UserReplyAdapter(activity, userReplyModelList);
        recyclerReply.setAdapter(adapter);

        String comments = "Comment: " + model.getComment();
        comments = comments.replace("Comment:", "<font color='#5D816C'>Comment:</font>");
        txtComment.setText(Html.fromHtml(comments));

        txtPostReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(etxReply.getText().toString().trim())) {
                    H.showMessage(activity, "Please enter reply");
                } else {
                    hitPostReply(model, etxReply, userReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);
                }
            }
        });

        txtViewMoreReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (userReplyModelList.size() < countReply) {
                    pageCountReply = pageCountReply + 1;
                    hitReplyData(pageCountReply, userReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        hitReplyData(pageCountReply, userReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

    }

    private void hitPostReply(UserCommentModel model, EditText editText, List<UserReplyModel> userReplyModelList, UserReplyAdapter adapter, TextView txtViewMoreReply, RecyclerView recyclerReply, TextView txtReplyCountView) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, model.getComment_id());
        j.addString(P.reply_msg, editText.getText().toString().trim());

        Api.newApi(activity, API.BaseUrl + "posts/reply_to_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        editText.setText("");
                        pageCountReply = 1;
                        hitReplyData(pageCountReply, userReplyModelList, adapter, txtViewMoreReply, recyclerReply, txtReplyCountView);
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitPostReply", token);
    }


    private void hitReplyData(int pageCount, List<UserReplyModel> userReplyModelList, UserReplyAdapter adapter, TextView txtViewMoreReply, RecyclerView recyclerReply, TextView txtReplyCountView) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, commentID);
        j.addString(P.page, pageCount + "");

        Api.newApi(activity, API.BaseUrl + "Posts/show_comments_replies").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        int num_rows = data.getInt(P.num_rows);

                        try {
                            countReply = num_rows;
                        } catch (Exception e) {
                            countReply = 0;
                        }

                        JsonList comments_list = data.getJsonList(P.comments_list);
                        if (comments_list != null && comments_list.size() != 0) {

                            if (pageCount == 1) {
                                userReplyModelList.clear();
                            }

                            for (Json jsonValue : comments_list) {
                                UserReplyModel model = new UserReplyModel();
                                model.setComment_id(jsonValue.getString(P.comment_id));
                                model.setComment(jsonValue.getString(P.comment));
                                model.setComment_by(jsonValue.getString(P.comment_by));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setProfile_pic(profile_pic_path + "/" + jsonValue.getString(P.profile_pic));
                                model.setOn_date(jsonValue.getString(P.on_date));
                                model.setComment_time(jsonValue.getString(P.comment_time));
                                userReplyModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                            try {
                                recyclerReply.smoothScrollToPosition(0);
                            } catch (Exception e) {
                            }

                            if (userReplyModelList.size() < num_rows) {
                                txtViewMoreReply.setVisibility(View.VISIBLE);
                            } else {
                                txtViewMoreReply.setVisibility(View.GONE);
                            }

                            txtReplyCountView.setText("View " + userReplyModelList.size() + " replies");
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitReplyData", token);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Config.FOR_MY_POST = false;
        Config.IS_NOTIFICATION_POST = false;
    }

}