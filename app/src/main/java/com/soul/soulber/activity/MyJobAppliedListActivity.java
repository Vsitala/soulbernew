package com.soul.soulber.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.MyAppliedJobAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMyAppliedJobBinding;
import com.soul.soulber.model.MyAppliedJobModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class MyJobAppliedListActivity extends AppCompatActivity {

    private MyJobAppliedListActivity activity = this;
    private ActivityMyAppliedJobBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private List<MyAppliedJobModel> appliedJobModelList;
    private MyAppliedJobAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ourpartners);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_applied_job);
        initView();
    }

    private void initView() {

        Config.UPDATED_BOOKMARK = false;

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        appliedJobModelList = new ArrayList<>();
        adapter = new MyAppliedJobAdapter(activity, appliedJobModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerAppliedJob.setLayoutManager(linearLayoutManager);
        binding.recyclerAppliedJob.setItemViewCacheSize(appliedJobModelList.size());
        binding.recyclerAppliedJob.setHasFixedSize(true);
        binding.recyclerAppliedJob.setAdapter(adapter);


        hitAppliedJobData(pageCount);
        setPagination();
        onClick();
    }


    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.UPDATED_BOOKMARK) {
            Config.UPDATED_BOOKMARK = false;
            pageCount = 1;
            hitAppliedJobData(pageCount);
        }
    }

    private void setPagination() {
        binding.recyclerAppliedJob.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (appliedJobModelList != null && !appliedJobModelList.isEmpty()) {
                        if (appliedJobModelList.size() < count) {
                            pageCount++;
                            hitAppliedJobData(pageCount);
                        }
                    }
                }
            }
        });
    }


    private void hitAppliedJobData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        String api = "job/applied_job_list?page=" + pageCount + "&per_page=20";

        Api.newApi(activity, API.BaseUrl + api)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String partner_logo_path = data.getString(P.partner_logo_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            appliedJobModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList jobs = data.getJsonList(P.jobs);
                        if (jobs != null && jobs.size() != 0) {
                            for (Json jsonValue : jobs) {

                                MyAppliedJobModel model = new MyAppliedJobModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setCategory_id(jsonValue.getString(P.category_id));
                                model.setPartner_id(jsonValue.getString(P.partner_id));
                                model.setTitle(jsonValue.getString(P.title));
                                model.setAbout_company(jsonValue.getString(P.about_company));
                                model.setAbout_job(jsonValue.getString(P.about_job));
                                model.setWebsite_link(jsonValue.getString(P.website_link));
                                model.setEmployment(jsonValue.getString(P.employment));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setAddress(jsonValue.getString(P.address));
                                model.setCity(jsonValue.getString(P.city));
                                model.setSalary_from(jsonValue.getString(P.salary_from));
                                model.setSalary_to(jsonValue.getString(P.salary_to));
                                model.setExperience_from(jsonValue.getString(P.experience_from));
                                model.setExperience_to(jsonValue.getString(P.experience_to));
                                model.setWho_can_apply(jsonValue.getString(P.who_can_apply));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setUpdate_date(jsonValue.getString(P.update_date));
                                model.setDelete_date(jsonValue.getString(P.delete_date));
                                model.setStatus(jsonValue.getString(P.status));
                                model.setDeleteflag(jsonValue.getString(P.deleteflag));
                                model.setJob_location(jsonValue.getString(P.job_location));
                                model.setRequest_date(jsonValue.getString(P.request_date));
                                model.setApplication_status(jsonValue.getString(P.application_status));
                                model.setSkills(jsonValue.getString(P.skills));
                                model.setIs_bookmarked(jsonValue.getString(P.is_bookmarked));
                                model.setPartner(jsonValue.getString(P.partner));
                                model.setPartner_location(jsonValue.getString(P.partner_location));
                                model.setPartner_logo(jsonValue.getString(P.partner_logo));
                                model.setCategory(jsonValue.getString(P.category));
                                model.setApplied(jsonValue.getString(P.applied));

                                appliedJobModelList.add(model);

                            }
                            adapter.notifyDataSetChanged();

                        }

                        checkData();

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                        checkData();
                    }
                })
                .run("hitAppliedJobData", token);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Config.FROM_APPLIED_JOB = false;
    }

    private void checkData() {
        if (appliedJobModelList.isEmpty()) {
            binding.txtJobListError.setVisibility(View.VISIBLE);
        } else {
            binding.txtJobListError.setVisibility(View.GONE);
        }
    }

}