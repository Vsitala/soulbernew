package com.soul.soulber.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.android.volley.VolleyError;
import com.github.chrisbanes.photoview.PhotoView;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityCreateImagePostBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class CreateImagePostActivity extends AppCompatActivity {

    private ActivityCreateImagePostBinding binding;
    private CreateImagePostActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_image_post);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);
        String base_64_string = Config.POST_BASE_64;
        Bitmap base_64_bitmap = Config.POST_BITMAP;

        binding.imgPost.setImageBitmap(base_64_bitmap);

        onClick(base_64_string,base_64_bitmap);
    }

    private void onClick(String base_64_string,Bitmap base_64_bitmap){

        binding.imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewImageDialog(base_64_bitmap);
            }
        });

        binding.btnSavePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(binding.etxPost.getText().toString())){
                    binding.etxPost.setError("Enter message");
                    binding.etxPost.requestFocus();
                }else {
                    hitCreatePost(binding.etxPost.getText().toString().trim(),base_64_string);
                }
            }
        });
    }

    private void hitCreatePost(String message, String base64) {
        ProgressView.show(activity,loadingDialog);

        Json j = new Json();
        j.addString(P.post_msg,message);
        j.addString(P.image,"data:image/jpeg;base64," + base64);
        j.addString(P.font_color,"");
        j.addString(P.font_style,"");
        j.addString(P.subtitle,"");
        j.addString(P.video,"");
        j.addString(P.upload_type,Config.UPLOAD_TYPE_2);

        Api.newApi(activity, API.BaseUrl+ "Posts/create_post").addJson(j)
                .setMethod(Api.POST)
                .onError(() -> {
                    VolleyError volleyError = new VolleyError();
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity,""+volleyError);
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if(json.getInt(P.status)==1)
                    {
                        Json data = json.getJson(P.data);
                        String post_id = data.getString(P.post_id);
                        Config.FOR_MY_POST = true;
                        Intent intent = new Intent(activity,UserPostActivity.class);
                        startActivity(intent);
                        finish();

                    }

                })
                .run("hitCreatePost",token);
    }

    private void viewImageDialog(Bitmap imagePath) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_image_view);

        PhotoView imageView = dialog.findViewById(R.id.imageView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        imageView.setImageBitmap(imagePath);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}