package com.soul.soulber.activity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.soul.soulber.R;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityVideoFullScreenBinding;
import com.soul.soulber.util.Click;

public class VideoFullScreenActivity extends AppCompatActivity implements Player.EventListener {

    private VideoFullScreenActivity activity = this;
    private ActivityVideoFullScreenBinding binding;
    SimpleExoPlayer exoPlayer;
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } catch (Exception e) {
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_video_full_screen);
        initView();
    }

    private void initView() {

        String videoPath = getIntent().getStringExtra(P.video);

        playVideo(videoPath);
        onClick();

    }

    private void playVideo(String url) {

        Uri uri = Uri.parse(url);

        exoPlayer = new SimpleExoPlayer.Builder(this).build();
        binding.playerView.setPlayer(exoPlayer);
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.addListener(this);

        imgBack = binding.playerView.findViewById(R.id.imgBack);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(uri);

        exoPlayer.setMediaItem(MediaItem.fromUri(uri));
        exoPlayer.prepare(videoSource);

//        if (CrashCourseDetailFragment.lastVideoPosition != C.TIME_UNSET) {
//            exoPlayer.seekTo(CrashCourseDetailFragment.lastVideoPosition );
//        }
        exoPlayer.play();

    }

    private void onClick() {

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });


    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if (playbackState == Player.STATE_BUFFERING) {
            binding.pbVideoPlayer.setVisibility(View.VISIBLE);

        } else if (playbackState == Player.STATE_READY || playbackState == Player.STATE_ENDED) {
            binding.pbVideoPlayer.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
            finish();
        }

    }
}