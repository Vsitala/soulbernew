package com.soul.soulber.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.LoadingDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.soul.soulber.JobsHouseFriends.GetAddressIntentService;
import com.soul.soulber.JobsHouseFriends.JobsFragment;
import com.soul.soulber.R;
import com.soul.soulber.adapter.LocationAdapter;
import com.soul.soulber.api.Config;
import com.soul.soulber.databinding.ActivitySearchLocationBinding;
import com.soul.soulber.model.LocationModel;
import com.soul.soulber.util.ProgressView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;


public class SearchLocationActivity extends AppCompatActivity implements LocationAdapter.ClickInterface {

    //Google Places Autocomplete Android Example
    private SearchLocationActivity activity = this;
    private ActivitySearchLocationBinding binding;
    private List<LocationModel> locationModelList;
    private MaterialSearchView searchView;
    private LocationAdapter locationAdapter;
    private Geocoder geocoder;

    private FusedLocationProviderClient fusedLocationClient;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 2;
    private LocationAddressResultReceiver addressResultReceiver;
    private Location currentLocation;
    private LocationCallback locationCallback;

    String newText = "";
    final Handler mHandler = new Handler();

    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_search_location);
        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initView();
        initSearchView();
    }

    private void initView() {
        loadingDialog = new LoadingDialog(activity);
        geocoder = new Geocoder(this, Locale.getDefault());
        binding.txtTitle.setText("Get Current Location");
        binding.txtAddress.setText("Using GPS");
        locationModelList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        binding.recyelrAddress.setLayoutManager(linearLayoutManager);
        binding.recyelrAddress.setHasFixedSize(true);
        binding.recyelrAddress.setNestedScrollingEnabled(false);
        locationAdapter = new LocationAdapter(activity, locationModelList);
        binding.recyelrAddress.setAdapter(locationAdapter);

        binding.lnrCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addressResultReceiver = new LocationAddressResultReceiver(new Handler());
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
                locationCallback = new LocationCallback() {
                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        currentLocation = locationResult.getLocations().get(0);
                        getAddress();
                    }
                };
                startLocationUpdates();

//                finishView();
            }
        });
    }


    private void initSearchView() {
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setHint("Search");
        searchView.setTextColor(getResources().getColor(R.color.grayLight));
        searchView.setHintTextColor(getResources().getColor(R.color.grayLight));


        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {
                //Do some magic
                return false;
            }

            @Override
            public boolean onQueryTextChange(String value) {
                newText = value;
                mHandler.removeCallbacksAndMessages(null);
                mHandler.postDelayed(userStoppedTyping, 1000);


//                if (!TextUtils.isEmpty(newText)) {
////                    Geocoder geocoder = new Geocoder(activity);
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            List<Address> addressList = null;
//                            try {
//                                addressList = geocoder.getFromLocationName(newText, 1000);
//                                for (int i = 0; i < addressList.size(); i++) {
//                                    Address address = addressList.get(0);
//                                    addAddress(address, newText);
//                                }
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }, 1000);
//                } else {
//                    locationModelList.clear();
//                    locationAdapter = new LocationAdapter(activity, locationModelList);
//                    binding.recyelrAddress.setAdapter(locationAdapter);
//                }
//
                return false;
            }

            Runnable userStoppedTyping = new Runnable() {
                @Override
                public void run() {

                    if (!TextUtils.isEmpty(newText)) {
                        try {
                            List<Address> addressList = geocoder.getFromLocationName(newText, 100);
                            for (int i = 0; i < addressList.size(); i++) {
                                Address address = addressList.get(0);
                                addAddress(address, newText);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            H.showMessage(activity, "Unable to get address, try again.");
                        }
                    } else {
                        locationModelList.clear();
                        locationAdapter = new LocationAdapter(activity, locationModelList);
                        binding.recyelrAddress.setAdapter(locationAdapter);
                    }
                }
            };

        });

    }


    private void checkData() {
        if (locationModelList.isEmpty()) {
            binding.txtSearchView.setVisibility(View.GONE);
        } else {
            binding.txtSearchView.setVisibility(View.VISIBLE);
        }
    }

    private void addAddress(Address address, String title) {
        LocationModel model = new LocationModel();
        model.setTitle(title);
        model.setLatitute(address.getLatitude());
        model.setLognitute(address.getLongitude());
        model.setAddress(getAddress(model, address.getLatitude(), address.getLongitude()));
        locationModelList.add(model);
        locationAdapter.notifyDataSetChanged();
    }

    private String getAddress(LocationModel model, double currentLat, double currentLong) {
        String addressData = "";
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(currentLat, currentLong, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            addressData = address + "";

            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            String showAdd = "";
            if (!city.equals("null")) {
                showAdd = showAdd + city;
            }

            if (!state.equals("null")) {
                showAdd = showAdd + "," + state;
            }

            if (!country.equals("null")) {
                showAdd = showAdd + "," + country;
            }

            model.setShowAddress(showAdd);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return addressData;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.product, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        searchView.showSearch();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
            super.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void getLocation(LocationModel model) {
        Config.IS_SEARCH = true;
        Config.SEARCH_LAT = model.getLatitute() + "";
        Config.SEARCH_LOGN = model.getLognitute() + "";
        Config.SEARCH_ADD = model.getAddress();
        finishView();
    }

    private void finishView() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        }
        finish();
    }

    @SuppressWarnings("MissingPermission")
    private void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new
                            String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            LocationRequest locationRequest = new LocationRequest();
//            locationRequest.setInterval(2000);
//            locationRequest.setFastestInterval(1000);
//            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getAddress() {
        if (!Geocoder.isPresent()) {
            H.showMessage(activity, "Can't find current address");
            return;
        } else {

//            H.showMessage(activity, "getting location....");
            Intent intent = new Intent(activity, GetAddressIntentService.class);
            intent.putExtra("add_receiver", addressResultReceiver);
            intent.putExtra("add_location", currentLocation);
            startService(intent);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull
            int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates();
            } else {
                H.showMessage(activity, "Location permission not granted, " + "restart the app if you want thefeature");
            }
        }
    }

    private class LocationAddressResultReceiver extends ResultReceiver {
        LocationAddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            if (resultCode == 0) {
                Log.d("Address", "Location null retrying");
                getAddress();
            }
            if (resultCode == 1) {
                H.showMessage(activity, "Address not found");
            }
            String currentAdd = resultData.getString("address_result");
            String address_lat = resultData.getString("address_lat");
            String address_logn = resultData.getString("address_logn");
            showResults(currentAdd, address_lat, address_logn);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void showResults(String currentAdd, String lat, String logn) {
//        Log.e("TAG", "showResults: " + currentAdd + " " + lat + " " + logn);
        Config.IS_SEARCH = true;
        Config.SEARCH_LAT = lat;
        Config.SEARCH_LOGN = logn;
        Config.SEARCH_ADD = currentAdd;
        finishView();
    }
}