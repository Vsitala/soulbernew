package com.soul.soulber.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.android.volley.VolleyError;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityCreateVideoPostBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.FileUtils;
import com.soul.soulber.util.ProgressView;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreateVideoPostActivity extends AppCompatActivity implements Player.EventListener {

    private ActivityCreateVideoPostBinding binding;
    private CreateVideoPostActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private SimpleExoPlayer exoPlayer;
    private PlayerView playerView;
    private ProgressBar pbVideoPlayer;

    private Uri videoUri;
    private String videoBase64;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_video_post);

        initView();

    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);
        String userName = session.getJson(Config.userProfileData).getString(P.username);

        videoUri = Config.POST_VIDEO_URI;
        videoBase64 = Config.POST_VIDEO_BASE64;

        pbVideoPlayer = findViewById(R.id.pbVideoPlayer);
        playerView = findViewById(R.id.playerView);

        playVideo(videoUri);
        onClick();

    }

    private void onClick() {

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });


        binding.btnSavePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                String message = binding.etxPost.getText().toString().trim();
                if (TextUtils.isEmpty(message)) {
                    binding.etxPost.setError("Enter caption");
                    binding.etxPost.requestFocus();
                } else {
                    exoPlayer.pause();
                    hitCreatePost(message, videoBase64);

//                    String filePath = FileUtils.getPath(activity, videoUri);
//                    uploadVideoFile(getFileName(videoUri), getMimeType(videoUri), filePath);

                }
            }
        });

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if (playbackState == Player.STATE_BUFFERING) {
            pbVideoPlayer.setVisibility(View.VISIBLE);

        } else if (playbackState == Player.STATE_READY || playbackState == Player.STATE_ENDED) {
            pbVideoPlayer.setVisibility(View.INVISIBLE);
        }
    }


    private void playVideo(Uri videoPath) {
        Log.e("TAG", "playVideo: " + videoPath);
        exoPlayer = new SimpleExoPlayer.Builder(activity).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.addListener(this);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(videoPath);

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(videoPath));

        exoPlayer.pause();

    }

    private void stopPlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopPlayer();
    }


    private void hitCreatePost(String message, String base64) {
        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.post_msg, "test");
        j.addString(P.image, "");
        j.addString(P.font_color, "");
        j.addString(P.font_style, "");
        j.addString(P.subtitle, message);
        j.addString(P.upload_type, Config.UPLOAD_TYPE_3);
        j.addString(P.video, "data:video/mp4;base64," + base64);

        Api.newApi(activity, API.BaseUrl + "Posts/create_post").addJson(j)
                .setMethod(Api.POST)
                .onError(() -> {
                    VolleyError volleyError = new VolleyError();
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "" + volleyError);
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        stopPlayer();
                        Json data = json.getJson(P.data);
                        String post_id = data.getString(P.post_id);
                        Config.FOR_MY_POST = true;
                        Intent intent = new Intent(activity, UserPostActivity.class);
                        startActivity(intent);
                        finish();
                    }

                })
                .run("hitCreatePost", token);
    }

    public String getMimeType(Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    private void uploadVideoFile(String fileName, String fileExt, String filePath) {

        Log.e("TAG", "uploadVideoFileNAME: " + fileName);
        Log.e("TAG", "uploadVideoFileEXT: " + fileExt);
        Log.e("TAG", "uploadVideoFilePATH: " + filePath);

        ProgressView.show(activity, loadingDialog);

        try {

            OkHttpClient client = new OkHttpClient().newBuilder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();

            MediaType mediaType = MediaType.parse("text/plain");
            RequestBody body = new MultipartBody.Builder().setType(MultipartBody.FORM)
                    .addFormDataPart("files", fileName,
                            RequestBody.create(MediaType.parse("application/octet-stream"),
                                    new File(filePath)))
                    .build();
            Request request = new Request.Builder()
                    .url(API.BaseUrl + "Common/upload_file")
                    .post(body)
//                    .method("POST", requestBody)
                    .addHeader("x-api-key", "123456")
                    .addHeader("X-Authorization", "Bearer " + token)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ProgressView.dismiss(loadingDialog);
                            H.showMessage(activity, "Unable to upload file, try again");
                        }
                    });
                    Log.e("TAG", "uploadFailure: " + e.getMessage());
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ProgressView.dismiss(loadingDialog);
                        }
                    });
                    Log.e("TAG", "uploadResponse: " + response.body().string());
                }
            });

        } catch (Exception e) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "Unable to upload file, try again");
                }
            });
            Log.e("TAG", "uploadException: " + e.getMessage());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}