package com.soul.soulber.Therapist;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.TherapistAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityTherapistresultBinding;
import com.soul.soulber.model.TherapistModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class TherapistResultActivity extends AppCompatActivity {

    private TherapistResultActivity activity = this;
    private ActivityTherapistresultBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private String searchLat = "";
    private String searchLogn = "";
    private String searchAdd = "";

    private List<TherapistModel> therapistModelList;
    private TherapistAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_therapistresult);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        searchLat = Config.THERAPIST_LAT;
        searchLogn = Config.THERAPIST_LOGN;
        searchAdd = Config.THERAPIST_ADD;

        therapistModelList = new ArrayList<>();
        adapter = new TherapistAdapter(activity, therapistModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerTherapist.setLayoutManager(linearLayoutManager);
        binding.recyclerTherapist.setItemViewCacheSize(therapistModelList.size());
        binding.recyclerTherapist.setHasFixedSize(true);
        binding.recyclerTherapist.setAdapter(adapter);

        hitTherapistListData(pageCount);
        setPagination();
        onClick();
    }


    private void setPagination() {
        binding.recyclerTherapist.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (therapistModelList != null && !therapistModelList.isEmpty()) {
                        if (therapistModelList.size() < count) {
                            pageCount++;
                            hitTherapistListData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitTherapistListData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "therapist/therapist_list?lt=" + searchLat + "&lng=" + searchLogn)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);

                        String therapist_images_path = data.getString(P.therapist_images_path);

                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            therapistModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList therapists = data.getJsonList(P.therapists);
                        if (therapists != null && therapists.size() != 0) {
                            for (Json jsonValue : therapists) {
                                TherapistModel meetingModel = new TherapistModel();
                                meetingModel.setId(jsonValue.getString(P.id));
                                meetingModel.setName(jsonValue.getString(P.name));
                                meetingModel.setImage(therapist_images_path + jsonValue.getString(P.image));
                                meetingModel.setExperience(jsonValue.getString(P.experience));
                                meetingModel.setBio(jsonValue.getString(P.bio));
                                meetingModel.setClinic_name(jsonValue.getString(P.clinic_name));
                                meetingModel.setCity(jsonValue.getString(P.city));
                                meetingModel.setLatitude(jsonValue.getString(P.latitude));
                                meetingModel.setLongitude(jsonValue.getString(P.longitude));
                                meetingModel.setConsultation_fees(jsonValue.getString(P.consultation_fees));
                                meetingModel.setAdd_date(jsonValue.getString(P.add_date));
                                meetingModel.setUpdate_date(jsonValue.getString(P.update_date));
                                meetingModel.setDelete_date(jsonValue.getString(P.delete_date));
                                meetingModel.setStatus(jsonValue.getString(P.status));
                                meetingModel.setDeleted(jsonValue.getString(P.deleted));
                                meetingModel.setTherapist_location(jsonValue.getString(P.therapist_location));
                                meetingModel.setIs_connected(jsonValue.getString(P.is_connected));
                                meetingModel.setIs_appointed(jsonValue.getString(P.is_appointed));
                                meetingModel.setDistance(jsonValue.getString(P.distance));
                                meetingModel.setBooking_allowed(jsonValue.getString(P.booking_allowed));
                                meetingModel.setButton_name(jsonValue.getString(P.button_name));

                                therapistModelList.add(meetingModel);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitTherapistListData", token);
    }

    void checkData() {
        if (therapistModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}