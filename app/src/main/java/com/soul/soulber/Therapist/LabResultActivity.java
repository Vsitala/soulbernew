package com.soul.soulber.Therapist;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.LabAdapter;
import com.soul.soulber.adapter.TherapistAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityLabResultBinding;
import com.soul.soulber.model.LabModel;
import com.soul.soulber.model.TherapistModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class LabResultActivity extends AppCompatActivity {

    private LabResultActivity activity = this;
    private ActivityLabResultBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private String searchLat = "";
    private String searchLogn = "";
    private String searchAdd = "";

    private List<LabModel> labModelList;
    private LabAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_lab_result);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        searchLat = Config.LAB_LAT;
        searchLogn = Config.LAB_LONG;
        searchAdd = Config.LAB_ADD;


        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerLab.setLayoutManager(linearLayoutManager);
        binding.recyclerLab.setHasFixedSize(true);


        labModelList = new ArrayList<>();
        adapter = new LabAdapter(activity, labModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerLab.setLayoutManager(linearLayoutManager);
        binding.recyclerLab.setItemViewCacheSize(labModelList.size());
        binding.recyclerLab.setHasFixedSize(true);
        binding.recyclerLab.setAdapter(adapter);

        hitLabListData(pageCount);
        setPagination();
        onClick();
    }


    private void setPagination() {
        binding.recyclerLab.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (labModelList != null && !labModelList.isEmpty()) {
                        if (labModelList.size() < count) {
                            pageCount++;
                            hitLabListData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitLabListData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "lab/lab_list?lt=" + searchLat + "&lng=" + searchLogn)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);

                        String lab_images_path = data.getString(P.lab_images_path);

                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            labModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList labs = data.getJsonList(P.labs);
                        if (labs != null && labs.size() != 0) {
                            for (Json jsonValue : labs) {
                                LabModel meetingModel = new LabModel();
                                meetingModel.setId(jsonValue.getString(P.id));
                                meetingModel.setLab_name(jsonValue.getString(P.lab_name));
                                meetingModel.setImage(lab_images_path + jsonValue.getString(P.image));
                                meetingModel.setBio(jsonValue.getString(P.bio));
                                meetingModel.setCity(jsonValue.getString(P.city));
                                meetingModel.setLatitude(jsonValue.getString(P.latitude));
                                meetingModel.setLongitude(jsonValue.getString(P.longitude));
                                meetingModel.setAddress(jsonValue.getString(P.address));
                                meetingModel.setAdd_date(jsonValue.getString(P.add_date));
                                meetingModel.setUpdate_date(jsonValue.getString(P.update_date));
                                meetingModel.setUpdate_date(jsonValue.getString(P.update_date));
                                meetingModel.setDelete_date(jsonValue.getString(P.delete_date));
                                meetingModel.setStatus(jsonValue.getString(P.status));
                                meetingModel.setDeleted(jsonValue.getString(P.deleted));
                                meetingModel.setLab_location(jsonValue.getString(P.lab_location));
                                meetingModel.setAppointment_id(jsonValue.getString(P.appointment_id));
                                meetingModel.setAppointment_status(jsonValue.getString(P.appointment_status));
                                meetingModel.setAppointment_date(jsonValue.getString(P.appointment_date));
                                meetingModel.setBooking_allowed(jsonValue.getString(P.booking_allowed));
                                meetingModel.setButton_name(jsonValue.getString(P.button_name));

                                labModelList.add(meetingModel);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitLabListData", token);
    }

    void checkData() {
        if (labModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}