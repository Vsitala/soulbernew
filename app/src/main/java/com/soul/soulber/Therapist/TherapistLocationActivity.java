package com.soul.soulber.Therapist;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityTherapistBaseBinding;

import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

public class TherapistLocationActivity extends AppCompatActivity {

    private TherapistLocationActivity activity = this;
    private ActivityTherapistBaseBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    final int AUTOCOMPLETE_REQUEST_CODE = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_therapist_base);
        initView();
    }

    private void initView() {

        initializeKey();
        clearSearch();

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        onClick();

    }

    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGoogleSearchCalled();
            }
        });

        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.txtLocation.getText().toString().equals("")) {
                    H.showMessage(activity, "Please select location");
                } else {
                    Intent intent = new Intent(activity, TherapistResultActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void clearSearch() {
        Config.IS_SEARCH = false;
        Config.SEARCH_ADD = "";
        Config.SEARCH_LOGN = "";
        Config.SEARCH_LAT = "";
    }

    @Override
    protected void onResume() {
        super.onResume();
//        setSearch();
    }

    public void setSearch() {
        if (Config.IS_SEARCH) {
            Config.IS_SEARCH = false;
            Config.THERAPIST_LAT = Config.SEARCH_LAT;
            Config.THERAPIST_LOGN = Config.SEARCH_LOGN;
            Config.THERAPIST_ADD = Config.SEARCH_ADD;
            binding.txtLocation.setText(Config.SEARCH_ADD);
        }
    }

    private void initializeKey() {
        if (!Places.isInitialized()) {
            Places.initialize(activity, Config.API_KEY);
        }
        PlacesClient placesClient = Places.createClient(activity);
    }

    public void onGoogleSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(
//                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("NG") //NIGERIA
//                .build(this);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry(Config.COUNTRY) //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    String id = place.getId();
                    String address = place.getAddress();
                    String name = place.getName();
                    LatLng latlong = place.getLatLng();
                    double latitude = latlong.latitude;
                    double longitude = latlong.longitude;

                    Config.THERAPIST_LAT = latitude + "";
                    Config.THERAPIST_LOGN = longitude + "";
                    Config.THERAPIST_ADD = address;
                    binding.txtLocation.setText(address.trim());

                    Log.e(TAG, "latitude: " + latitude + "");
                    Log.e(TAG, "longitude: " + longitude + "");
                    Log.e(TAG, "address: " + address + "");

                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
//                    H.showMessage(activity, "Unable to get address, try again.");
                    H.showMessage(activity, status + "");
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;
        }
    }
}