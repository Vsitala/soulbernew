package com.soul.soulber.Meeting;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.soul.soulber.R;
import java.util.ArrayList;


    public class MeetingResultadapter extends RecyclerView.Adapter<MeetingResultadapter.ViewHolder> {
        private ArrayList<MeetingResultModleclass> meetinglist;
        private Context context;
        private int selectedItem;
        public MeetingResultadapter(Context context,ArrayList<MeetingResultModleclass> meetinglist) {
            this.meetinglist = meetinglist;
            this.context = context;
            selectedItem=0;
        }

        @Override
        public MeetingResultadapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_meetingresults, viewGroup, false);
            return new MeetingResultadapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MeetingResultadapter.ViewHolder viewHolder, int position) {

            viewHolder.tv_meetingname.setText(meetinglist.get(position).getMeetingname());
            viewHolder.tv_location.setText(meetinglist.get(position).getLocation());
            viewHolder.tv_meetiming.setText(meetinglist.get(position).getMeetiming());
            Glide
                    .with(context)

                    .load(meetinglist.get(position).getMeetingimage())
                    //cropping center image
                    .into(viewHolder.iv_meetingimg);
            viewHolder.ll_meetinglayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(context, MeetingDetailsActivity.class);

                    Bundle bundle = new Bundle();

                    bundle.putString("meetname",meetinglist.get(position).getMeetingname());
                    bundle.putString("meetdate",meetinglist.get(position).getMeetiming());
                    bundle.putString("meetlocation",meetinglist.get(position).getLocation());
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
                    context.startActivity(intent);
                }
            });

        }


        @Override
        public int getItemCount() {
            return meetinglist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

         private TextView tv_meetiming,tv_location,tv_meetingname;
         private ImageView iv_meetingimg;
         private LinearLayout ll_meetinglayout;

            public ViewHolder(View view) {
                super(view);
                tv_meetiming=view.findViewById(R.id.tv_meettiming);
               tv_location=view.findViewById(R.id.tv_location);
                tv_meetingname=view.findViewById(R.id.tv_meetingname);
                iv_meetingimg=view.findViewById(R.id.iv_meetingimg);
                ll_meetinglayout=view.findViewById(R.id.ll_meetinglayout);

            }

        }

    }



