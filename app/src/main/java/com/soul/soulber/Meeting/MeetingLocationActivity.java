package com.soul.soulber.Meeting;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.tabs.TabLayout;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMeetingLocationBinding;
import com.soul.soulber.fragment.MeetingPastFragment;
import com.soul.soulber.fragment.MeetingUnConfirmFragment;
import com.soul.soulber.fragment.MeetingUpcomingFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;


public class MeetingLocationActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private MeetingLocationActivity activity = this;
    private ActivityMeetingLocationBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    final int AUTOCOMPLETE_REQUEST_CODE = 1000;

    private MeetingUpcomingFragment meetingUpcomingFragment = MeetingUpcomingFragment.newInstance();
    private MeetingPastFragment meetingPastFragment = MeetingPastFragment.newInstance();
    private MeetingUnConfirmFragment meetingUnConfirmFragment = MeetingUnConfirmFragment.newInstance();

    private String upcoming = "Upcoming";
    private String past = "Past";
    private String unConfirm = "Un-Confirm";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_meeting_location);
        initView();

    }

    private void initView() {

        initializeKey();
        clearSearch();

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        setupViewPager(binding.viewPager);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.addOnPageChangeListener((ViewPager.OnPageChangeListener) activity);
        setupTabIcons();

        onClick();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(meetingUpcomingFragment, "");
        adapter.addFragment(meetingPastFragment, "");
        adapter.addFragment(meetingUnConfirmFragment, "");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {

        View view1 = LayoutInflater.from(activity).inflate(R.layout.activity_customt_ab_small, null);
        ((TextView) view1.findViewById(R.id.text)).setText(upcoming);
        ((TextView) view1.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));

        View view2 = LayoutInflater.from(activity).inflate(R.layout.activity_customt_ab_small, null);
        ((TextView) view2.findViewById(R.id.text)).setText(past);
        ((TextView) view2.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));

        View view3 = LayoutInflater.from(activity).inflate(R.layout.activity_customt_ab_small, null);
        ((TextView) view3.findViewById(R.id.text)).setText(unConfirm);
        ((TextView) view3.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));


        binding.tabLayout.getTabAt(0).setCustomView(view1);
        binding.tabLayout.getTabAt(1).setCustomView(view2);
        binding.tabLayout.getTabAt(2).setCustomView(view3);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        binding.txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(activity, SearchLocationActivity.class);
//                startActivity(intent);
                onGoogleSearchCalled();
            }
        });

        binding.btnSearchMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.txtLocation.getText().toString().equals("")) {
                    H.showMessage(activity, "Please select location");
                } else {
                    Intent intent = new Intent(activity, MeetingListActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void clearSearch() {
        Config.IS_SEARCH = false;
        Config.SEARCH_ADD = "";
        Config.SEARCH_LOGN = "";
        Config.SEARCH_LAT = "";
    }

    @Override
    protected void onResume() {
        super.onResume();
//        setSearch();
    }

    public void setSearch() {
        if (Config.IS_SEARCH) {
            Config.IS_SEARCH = false;
            Config.MEETING_LAT = Config.SEARCH_LAT;
            Config.MEETING_LOGN = Config.SEARCH_LOGN;
            Config.MEETING_ADD = Config.SEARCH_ADD;
            binding.txtLocation.setText(Config.SEARCH_ADD);
        }
    }

    private void initializeKey() {
        if (!Places.isInitialized()) {
            Places.initialize(activity, Config.API_KEY);
        }
        PlacesClient placesClient = Places.createClient(activity);
    }

    public void onGoogleSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(
//                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("NG") //NIGERIA
//                .build(this);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry(Config.COUNTRY) //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    String id = place.getId();
                    String address = place.getAddress();
                    String name = place.getName();
                    LatLng latlong = place.getLatLng();
                    double latitude = latlong.latitude;
                    double longitude = latlong.longitude;

                    Config.MEETING_LAT = latitude + "";
                    Config.MEETING_LOGN = longitude + "";
                    Config.MEETING_ADD = address;
                    binding.txtLocation.setText(address.trim());

                    Log.e(TAG, "latitude: " + latitude + "");
                    Log.e(TAG, "longitude: " + longitude + "");
                    Log.e(TAG, "address: " + address + "");

                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
//                    H.showMessage(activity, "Unable to get address, try again.");
                    H.showMessage(activity, status + "");
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}