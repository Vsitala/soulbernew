package com.soul.soulber.Meeting;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.MeetingAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMeetingListBinding;
import com.soul.soulber.model.MeetingModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class MeetingListActivity extends AppCompatActivity {

    private MeetingListActivity activity = this;
    private ActivityMeetingListBinding binding;
    MeetingResultadapter meetingResultadapter;
    MeetingFilterActivity add;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private String filterId = "";

    private String searchLat = "";
    private String searchLogn = "";
    private String searchAdd = "";

    private List<MeetingModel> meetingModelList;
    private MeetingAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_meeting_list);
        initView();

    }

    private void initView() {

        Config.IS_MEETING_FILTER = false;
        Config.MEETING_FILTER_ID = "";

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        searchLat = Config.MEETING_LAT;
        searchLogn = Config.MEETING_LOGN;
        searchAdd = Config.MEETING_ADD;

        meetingModelList = new ArrayList<>();
        adapter = new MeetingAdapter(activity, meetingModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerMeeting.setLayoutManager(linearLayoutManager);
        binding.recyclerMeeting.setItemViewCacheSize(meetingModelList.size());
        binding.recyclerMeeting.setHasFixedSize(true);
        binding.recyclerMeeting.setAdapter(adapter);

        onClick();
        hitMeetingListData(pageCount, filterId);
        setPagination();
    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MeetingFilterActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (Config.IS_MEETING_FILTER) {
            Config.IS_MEETING_FILTER = false;
            filterId = Config.MEETING_FILTER_ID;
            pageCount = 1;
            hitMeetingListData(pageCount, filterId);
        }
    }

    private void setPagination() {
        binding.recyclerMeeting.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (meetingModelList != null && !meetingModelList.isEmpty()) {
                        if (meetingModelList.size() < count) {
                            pageCount++;
                            hitMeetingListData(pageCount, filterId);
                        }
                    }
                }
            }
        });
    }

    private void hitMeetingListData(int pageCount, String attendee_type) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "meeting/meeting_list?page=" + pageCount + "&per_page=20&lt=" + searchLat + "&lng=" + searchLogn + "&attendee_type=" + attendee_type)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);

                        String meeting_image_path = data.getString(P.meeting_image_path);

                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            meetingModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList meetings = data.getJsonList(P.meetings);
                        if (meetings != null && meetings.size() != 0) {
                            for (Json jsonValue : meetings) {
                                MeetingModel meetingModel = new MeetingModel();
                                meetingModel.setId(jsonValue.getString(P.id));
                                meetingModel.setTopic(jsonValue.getString(P.topic));
                                meetingModel.setTopic_detail(jsonValue.getString(P.topic_detail));
                                meetingModel.setDate(jsonValue.getString(P.date));
                                meetingModel.setTime(jsonValue.getString(P.time));
                                meetingModel.setVenue(jsonValue.getString(P.venue));
                                meetingModel.setCity(jsonValue.getString(P.city));
                                meetingModel.setLatitude(jsonValue.getString(P.latitude));
                                meetingModel.setLongitude(jsonValue.getString(P.longitude));
                                meetingModel.setAttendee_type(jsonValue.getString(P.attendee_type));
                                meetingModel.setPrimary_image(jsonValue.getString(P.primary_image));
                                meetingModel.setAttendee_count(jsonValue.getString(P.attendee_count));
                                meetingModel.setStatus(jsonValue.getString(P.status));
                                meetingModel.setDeleted(jsonValue.getString(P.deleted));
                                meetingModel.setAdd_date(jsonValue.getString(P.add_date));
                                meetingModel.setUpdate_date(jsonValue.getString(P.update_date));
                                meetingModel.setMeeting_location(jsonValue.getString(P.meeting_location));
                                meetingModel.setImagePath(meeting_image_path);
                                meetingModel.setOther_images(jsonValue.getJsonArray(P.other_images));

                                meetingModelList.add(meetingModel);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitMeetingListData", token);
    }

    void checkData() {
        if (meetingModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
            binding.txtMeetingCount.setText("0 meetings are conducted near you");
        } else {
            binding.txtError.setVisibility(View.GONE);
            binding.txtMeetingCount.setText(meetingModelList.size() + " meetings are conducted near you");
        }
    }
}