package com.soul.soulber.Meeting;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.H;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.databinding.ActivityMeetingFilterBinding;

public class MeetingFilterActivity extends AppCompatActivity {

    private MeetingFilterActivity activity = this;
    private ActivityMeetingFilterBinding binding;

    private String selectedValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_meeting_filter);
        initView();
    }


    private void initView() {

        Config.IS_MEETING_FILTER = false;

        if (!Config.MEETING_FILTER_ID.equals("")){
            if (Config.MEETING_FILTER_ID.equals("1")){
                binding.radioMale.setChecked(true);
            }else if (Config.MEETING_FILTER_ID.equals("2")){
                binding.radioFemale.setChecked(true);
            }else if (Config.MEETING_FILTER_ID.equals("3")){
                binding.radioOther.setChecked(true);
            }
        }

        binding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioMale:
                        selectedValue = "1";
                        break;
                    case R.id.radioFemale:
                        selectedValue = "2";
                        break;
                    case R.id.radioOther:
                        selectedValue = "3";
                        break;
                }
            }
        });

        onClick();
    }

    private void onClick() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedValue.equals("")) {
                    H.showMessage(activity, "Please select filter value");
                } else {
                    Config.IS_MEETING_FILTER = true;
                    Config.MEETING_FILTER_ID = selectedValue;
                    finish();
                }
            }
        });

    }
}