package com.soul.soulber.Meeting;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.HousingImageAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMeetingDetailsBinding;
import com.soul.soulber.model.ImageModel;
import com.soul.soulber.util.ProgressView;
import com.soul.soulber.util.RemoveHtml;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MeetingDetailsActivity extends AppCompatActivity {

    private MeetingDetailsActivity activity = this;
    private ActivityMeetingDetailsBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    String meetingId = "";
    String is_attended = "";

    private HousingImageAdapter imageAdapter;
    private List<ImageModel> imageModels;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_meeting_details);

        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        meetingId = Config.MEETING_ID;

        imageModels = new ArrayList<>();
        imageAdapter = new HousingImageAdapter(activity, imageModels, 1);
        binding.recyclerMeetingImages.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
        binding.recyclerMeetingImages.setHasFixedSize(true);
        binding.recyclerMeetingImages.setItemViewCacheSize(imageModels.size());
        binding.recyclerMeetingImages.setAdapter(imageAdapter);

        onClick();
        hitMeetingDetails(meetingId);

    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnAttend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_attended.equals("1")) {
                    hitMeetingStatus(meetingId);
                }
            }
        });

    }

    private void hitMeetingStatus(String id) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.meeting_id, id);
        Api.newApi(activity, API.BaseUrl + "meeting/attend_meeting").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        binding.btnAttend.setText("Requested");
                        is_attended = "0";

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitMeetingStatus", token);
    }

    private void hitMeetingDetails(String id) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.meeting_id, id);
        Api.newApi(activity, API.BaseUrl + "meeting/meeting_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        binding.txtTopic.setText(checkString(data.getString(P.topic)));
                        binding.txtLocation.setText(checkString(data.getString(P.meeting_location)));
                        binding.txtDiscussion.setText(RemoveHtml.html2text(checkString(data.getString(P.topic_detail))));
                        binding.txtDate.setText(getFormatDate(checkString(data.getString(P.date))));
                        binding.txtTime.setText(getFormatTime(checkString(data.getString(P.time))));
                        binding.btnAttend.setText(checkString(data.getString(P.button_name)));

                        is_attended = data.getString(P.booking_allowed);
                        String meeting_images_path = data.getString(P.meeting_images_path);

                        JSONArray jsonArray = data.getJsonArray(P.other_images);
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                String image = meeting_images_path + jsonArray.getString(i);
                                ImageModel imageModel = new ImageModel();
                                imageModel.setImage(image);
                                imageModels.add(imageModel);
                            }
                            imageAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            binding.recyclerMeetingImages.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitMeetingDetails", token);
    }

    public String getFormatTime(String dt) {
        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("HH:mm:ss");
            Date date = inFormat.parse(dt);
            SimpleDateFormat outFormat = new SimpleDateFormat("hh:mm a");
            String goal = outFormat.format(date);
            return goal;
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM, yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

}