package com.soul.soulber.Meeting;

public class MeetingResultModleclass {
    int meetingimage;

    public MeetingResultModleclass(int meetingimage, String meetiming, String location, String meetingname) {
        this.meetingimage = meetingimage;
        this.meetiming = meetiming;
        this.location = location;
        this.meetingname = meetingname;
    }

    public MeetingResultModleclass() {

    }

    public int getMeetingimage() {
        return meetingimage;
    }

    public void setMeetingimage(int meetingimage) {
        this.meetingimage = meetingimage;
    }

    public String getMeetiming() {
        return meetiming;
    }

    public void setMeetiming(String meetiming) {
        this.meetiming = meetiming;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMeetingname() {
        return meetingname;
    }

    public void setMeetingname(String meetingname) {
        this.meetingname = meetingname;
    }

    String meetiming;
    String location;
    String meetingname;
}
