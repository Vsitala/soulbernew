package com.soul.soulber.model;

public class GuardianTheraphyModel {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTherpist_name() {
        return therpist_name;
    }

    public void setTherpist_name(String therpist_name) {
        this.therpist_name = therpist_name;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getDependent_name() {
        return dependent_name;
    }

    public void setDependent_name(String dependent_name) {
        this.dependent_name = dependent_name;
    }

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    String id;
    String therpist_name;
    String experience;
    String appointment_date;
    String dependent_name;
    String profilepic;
    String image;
    String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    String remark;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
