package com.soul.soulber.model;

public class Userdetailsmodel {
    public Userdetailsmodel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getContact1() {
        return contact1;
    }

    public void setContact1(String contact1) {
        this.contact1 = contact1;
    }

    public String getUser_type_name() {
        return user_type_name;
    }

    public void setUser_type_name(String user_type_name) {
        this.user_type_name = user_type_name;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getContact2() {
        return contact2;
    }

    public void setContact2(String contact2) {
        this.contact2 = contact2;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public Userdetailsmodel(String id, String name, String username, String email, String phone, String email1, String contact1, String user_type_name, String bio, String email2, String contact2, String add_date,String profile_pic) {
        this.id = id;
        this.name = name;
        this.profile_pic=profile_pic;
        this.username = username;
        this.email = email;
        this.phone = phone;
        this.email1 = email1;
        this.contact1 = contact1;
        this.user_type_name = user_type_name;
        this.bio = bio;
        this.email2 = email2;
        this.contact2 = contact2;
        this.add_date = add_date;
    }

    String id;
    String name;
    String username;
    String email;
    String phone;
    String email1;
    String contact1;
    String user_type_name;
    String bio;
    String email2;
    String contact2;

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    String profile_pic;

    public Userdetailsmodel(String is_pic_private) {
        this.is_pic_private = is_pic_private;
    }

    String add_date;

    public String getIs_pic_private() {
        return is_pic_private;
    }

    public void setIs_pic_private(String is_pic_private) {
        this.is_pic_private = is_pic_private;
    }

    String is_pic_private;









}
