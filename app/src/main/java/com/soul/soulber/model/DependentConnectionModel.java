package com.soul.soulber.model;

public class DependentConnectionModel {

    String connection_id;
    String from_dependent_id;
    String from_dependent_name;
    String to_dependent_id;
    String to_dependent_name;
    String is_accepted;
    String profile_pic;
    String add_date;
    String city;
    String Age;
    String connection_time;
    String from_username;
    String to_dependent_username;

    public String getConnection_id() {
        return connection_id;
    }

    public void setConnection_id(String connection_id) {
        this.connection_id = connection_id;
    }

    public String getFrom_dependent_id() {
        return from_dependent_id;
    }

    public void setFrom_dependent_id(String from_dependent_id) {
        this.from_dependent_id = from_dependent_id;
    }

    public String getFrom_dependent_name() {
        return from_dependent_name;
    }

    public void setFrom_dependent_name(String from_dependent_name) {
        this.from_dependent_name = from_dependent_name;
    }

    public String getTo_dependent_id() {
        return to_dependent_id;
    }

    public void setTo_dependent_id(String to_dependent_id) {
        this.to_dependent_id = to_dependent_id;
    }

    public String getTo_dependent_name() {
        return to_dependent_name;
    }

    public void setTo_dependent_name(String to_dependent_name) {
        this.to_dependent_name = to_dependent_name;
    }

    public String getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(String is_accepted) {
        this.is_accepted = is_accepted;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(String connection_time) {
        this.connection_time = connection_time;
    }

    public String getFrom_username() {
        return from_username;
    }

    public void setFrom_username(String from_username) {
        this.from_username = from_username;
    }

    public String getTo_dependent_username() {
        return to_dependent_username;
    }

    public void setTo_dependent_username(String to_dependent_username) {
        this.to_dependent_username = to_dependent_username;
    }
}
