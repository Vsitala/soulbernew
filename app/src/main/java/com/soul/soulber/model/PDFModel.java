package com.soul.soulber.model;

public class PDFModel {

    String report_title;
    String report_file;

    public String getReport_title() {
        return report_title;
    }

    public void setReport_title(String report_title) {
        this.report_title = report_title;
    }

    public String getReport_file() {
        return report_file;
    }

    public void setReport_file(String report_file) {
        this.report_file = report_file;
    }
}
