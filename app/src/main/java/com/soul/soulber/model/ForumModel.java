package com.soul.soulber.model;

public class ForumModel {
    String id;
    String forum_text;
    String image;
    String upvotes;
    String downvotes;
    String comments;
    String is_delete;
    String add_date;
    String is_upvoted;
    String is_downvoted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getForum_text() {
        return forum_text;
    }

    public void setForum_text(String forum_text) {
        this.forum_text = forum_text;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(String upvotes) {
        this.upvotes = upvotes;
    }

    public String getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(String downvotes) {
        this.downvotes = downvotes;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public String getIs_upvoted() {
        return is_upvoted;
    }

    public void setIs_upvoted(String is_upvoted) {
        this.is_upvoted = is_upvoted;
    }

    public String getIs_downvoted() {
        return is_downvoted;
    }

    public void setIs_downvoted(String is_downvoted) {
        this.is_downvoted = is_downvoted;
    }
}
