package com.soul.soulber.model;

public class CountryModel {

    String city_id;
    String city;


    public CountryModel(String city_id, String city) {
        this.city_id = city_id;
        this.city = city;
    }

    public CountryModel() {

    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
