package com.soul.soulber.model;

public class ImageModel {

    String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
