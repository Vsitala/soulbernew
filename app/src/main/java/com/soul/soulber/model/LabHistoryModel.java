package com.soul.soulber.model;

import com.adoisstudio.helper.JsonList;

public class LabHistoryModel {

    String id;
    String lab_id;
    String user_id;
    String request_date;
    String approve_date;
    String appointment_date;
    String remark;
    String appointment_status;
    String appointment_id;
    String lab_name;
    String image;
    String bio;
    String address;
    String latitude;
    String longitude;
    String lab_location;
    JsonList reports;
    String lab_reports_images_path;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLab_id() {
        return lab_id;
    }

    public void setLab_id(String lab_id) {
        this.lab_id = lab_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getApprove_date() {
        return approve_date;
    }

    public void setApprove_date(String approve_date) {
        this.approve_date = approve_date;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppointment_status() {
        return appointment_status;
    }

    public void setAppointment_status(String appointment_status) {
        this.appointment_status = appointment_status;
    }

    public String getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(String appointment_id) {
        this.appointment_id = appointment_id;
    }

    public String getLab_name() {
        return lab_name;
    }

    public void setLab_name(String lab_name) {
        this.lab_name = lab_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLab_location() {
        return lab_location;
    }

    public void setLab_location(String lab_location) {
        this.lab_location = lab_location;
    }

    public JsonList getReports() {
        return reports;
    }

    public void setReports(JsonList reports) {
        this.reports = reports;
    }

    public String getLab_reports_images_path() {
        return lab_reports_images_path;
    }

    public void setLab_reports_images_path(String lab_reports_images_path) {
        this.lab_reports_images_path = lab_reports_images_path;
    }
}

