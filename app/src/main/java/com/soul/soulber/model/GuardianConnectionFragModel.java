package com.soul.soulber.model;

public class GuardianConnectionFragModel {

    String connection_id;
    String from_guardian_id;
    String from_guardian_name;
    String to_guardian_id;
    String to_guardian_name;
    String is_accepted;
    String add_date;
    String connection_time;
    String profile_pic;

    public String getConnection_id() {
        return connection_id;
    }

    public void setConnection_id(String connection_id) {
        this.connection_id = connection_id;
    }

    public String getFrom_guardian_id() {
        return from_guardian_id;
    }

    public void setFrom_guardian_id(String from_guardian_id) {
        this.from_guardian_id = from_guardian_id;
    }

    public String getFrom_guardian_name() {
        return from_guardian_name;
    }

    public void setFrom_guardian_name(String from_guardian_name) {
        this.from_guardian_name = from_guardian_name;
    }

    public String getTo_guardian_id() {
        return to_guardian_id;
    }

    public void setTo_guardian_id(String to_guardian_id) {
        this.to_guardian_id = to_guardian_id;
    }

    public String getTo_guardian_name() {
        return to_guardian_name;
    }

    public void setTo_guardian_name(String to_guardian_name) {
        this.to_guardian_name = to_guardian_name;
    }

    public String getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(String is_accepted) {
        this.is_accepted = is_accepted;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public String getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(String connection_time) {
        this.connection_time = connection_time;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
