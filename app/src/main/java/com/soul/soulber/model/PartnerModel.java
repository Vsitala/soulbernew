package com.soul.soulber.model;

public class PartnerModel {

    String id;
    String partner;
    String logo;
    String about_partner;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAbout_partner() {
        return about_partner;
    }

    public void setAbout_partner(String about_partner) {
        this.about_partner = about_partner;
    }
}
