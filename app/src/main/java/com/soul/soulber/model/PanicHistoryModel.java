package com.soul.soulber.model;

public class PanicHistoryModel {
    public String getPanic_date() {
        return panic_date;
    }

    public void setPanic_date(String panic_date) {
        this.panic_date = panic_date;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDependent_username() {
        return dependent_username;
    }

    public void setDependent_username(String dependent_username) {
        this.dependent_username = dependent_username;
    }

    String panic_date;
    String profile_pic;
    String dependent_username;
}
