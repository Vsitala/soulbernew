package com.soul.soulber.model;

public class AmenitiesModel {

    String amenities;

    public String getAmenities() {
        return amenities;
    }

    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }
}
