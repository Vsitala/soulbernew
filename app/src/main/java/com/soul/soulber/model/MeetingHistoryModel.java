package com.soul.soulber.model;

import org.json.JSONArray;

public class MeetingHistoryModel {

    String id;
    String user_id;
    String meeting_id;
    String request_date;
    String approve_date;
    String remark;
    String attendee_status;
    String is_latest;
    String topic;
    String primary_image;
    String venue;
    String city;
    String latitude;
    String longitude;
    String date;
    String time;
    String attendee_count;
    String imagePath;
    JSONArray other_images;
    JSONArray profile_pic;

    public JSONArray getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(JSONArray profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public JSONArray getOther_images() {
        return other_images;
    }

    public void setOther_images(JSONArray other_images) {
        this.other_images = other_images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getApprove_date() {
        return approve_date;
    }

    public void setApprove_date(String approve_date) {
        this.approve_date = approve_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAttendee_status() {
        return attendee_status;
    }

    public void setAttendee_status(String attendee_status) {
        this.attendee_status = attendee_status;
    }

    public String getIs_latest() {
        return is_latest;
    }

    public void setIs_latest(String is_latest) {
        this.is_latest = is_latest;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getPrimary_image() {
        return primary_image;
    }

    public void setPrimary_image(String primary_image) {
        this.primary_image = primary_image;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAttendee_count() {
        return attendee_count;
    }

    public void setAttendee_count(String attendee_count) {
        this.attendee_count = attendee_count;
    }
}
