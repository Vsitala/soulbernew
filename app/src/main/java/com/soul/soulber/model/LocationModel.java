package com.soul.soulber.model;

public class LocationModel {

    String title;
    String address;
    String showAddress;
    double latitute;
    double lognitute;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitute() {
        return latitute;
    }

    public void setLatitute(double latitute) {
        this.latitute = latitute;
    }

    public double getLognitute() {
        return lognitute;
    }

    public void setLognitute(double lognitute) {
        this.lognitute = lognitute;
    }

    public String getShowAddress() {
        return showAddress;
    }

    public void setShowAddress(String showAddress) {
        this.showAddress = showAddress;
    }
}
