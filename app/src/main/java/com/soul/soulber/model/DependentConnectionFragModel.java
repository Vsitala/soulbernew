package com.soul.soulber.model;

public class DependentConnectionFragModel {

    String connection_id;
    String dependent_id;
    String dependent_name;
    String is_accepted;
    String profile_pic;
    String add_date;
    String connection_time;
    String dependent_username;

    public String getDependent_username() {
        return dependent_username;
    }

    public void setDependent_username(String dependent_username) {
        this.dependent_username = dependent_username;
    }

    public String getConnection_id() {
        return connection_id;
    }

    public void setConnection_id(String connection_id) {
        this.connection_id = connection_id;
    }

    public String getDependent_id() {
        return dependent_id;
    }

    public void setDependent_id(String dependent_id) {
        this.dependent_id = dependent_id;
    }

    public String getDependent_name() {
        return dependent_name;
    }

    public void setDependent_name(String dependent_name) {
        this.dependent_name = dependent_name;
    }

    public String getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(String is_accepted) {
        this.is_accepted = is_accepted;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public String getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(String connection_time) {
        this.connection_time = connection_time;
    }
}
