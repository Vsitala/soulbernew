package com.soul.soulber.model;

public class ChatListModel {

    String id;
    String last_message;
    String from_user_id;
    String to_user_id;
    String last_updated;
    String name;
    String username;
    String profile_pic;
    String last_activated;
    String session_user_type_id;
    String opponent_user_type_id;

    public String getSession_user_type_id() {
        return session_user_type_id;
    }

    public void setSession_user_type_id(String session_user_type_id) {
        this.session_user_type_id = session_user_type_id;
    }

    public String getOpponent_user_type_id() {
        return opponent_user_type_id;
    }

    public void setOpponent_user_type_id(String opponent_user_type_id) {
        this.opponent_user_type_id = opponent_user_type_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }

    public String getFrom_user_id() {
        return from_user_id;
    }

    public void setFrom_user_id(String from_user_id) {
        this.from_user_id = from_user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getLast_updated() {
        return last_updated;
    }

    public void setLast_updated(String last_updated) {
        this.last_updated = last_updated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getLast_activated() {
        return last_activated;
    }

    public void setLast_activated(String last_activated) {
        this.last_activated = last_activated;
    }
}
