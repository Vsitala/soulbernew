package com.soul.soulber.model;

public class JobCategoryModel {

    String id;
    String category;
    String cat_img;

    public JobCategoryModel(String id, String category, String cat_img) {
        this.id = id;
        this.category = category;
        this.cat_img = cat_img;
    }

    public JobCategoryModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCat_img() {
        return cat_img;
    }

    public void setCat_img(String cat_img) {
        this.cat_img = cat_img;
    }
}
