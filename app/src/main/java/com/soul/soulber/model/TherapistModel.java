package com.soul.soulber.model;

public class TherapistModel {

    String id;
    String name;
    String image;
    String experience;
    String bio;
    String clinic_name;
    String city;
    String latitude;
    String longitude;
    String consultation_fees;
    String add_date;
    String update_date;
    String delete_date;
    String status;
    String deleted;
    String therapist_location;
    String is_connected;
    String is_appointed;
    String distance;
    String booking_allowed;
    String button_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getConsultation_fees() {
        return consultation_fees;
    }

    public void setConsultation_fees(String consultation_fees) {
        this.consultation_fees = consultation_fees;
    }

    public String getAdd_date() {
        return add_date;
    }

    public void setAdd_date(String add_date) {
        this.add_date = add_date;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    public String getDelete_date() {
        return delete_date;
    }

    public void setDelete_date(String delete_date) {
        this.delete_date = delete_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getTherapist_location() {
        return therapist_location;
    }

    public void setTherapist_location(String therapist_location) {
        this.therapist_location = therapist_location;
    }

    public String getIs_connected() {
        return is_connected;
    }

    public void setIs_connected(String is_connected) {
        this.is_connected = is_connected;
    }

    public String getIs_appointed() {
        return is_appointed;
    }

    public void setIs_appointed(String is_appointed) {
        this.is_appointed = is_appointed;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBooking_allowed() {
        return booking_allowed;
    }

    public void setBooking_allowed(String booking_allowed) {
        this.booking_allowed = booking_allowed;
    }

    public String getButton_name() {
        return button_name;
    }

    public void setButton_name(String button_name) {
        this.button_name = button_name;
    }
}
