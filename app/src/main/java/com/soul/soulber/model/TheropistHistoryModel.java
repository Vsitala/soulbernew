package com.soul.soulber.model;

public class TheropistHistoryModel {

    String id;
    String therapist_id;
    String user_id;
    String request_date;
    String approve_date;
    String appointment_date;
    String remark;
    String appointment_status;
    String therpist_name;
    String image;
    String experience;
    String bio;
    String clinic_name;
    String latitude;
    String longitude;
    String consultation_fees;
    String therapist_location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTherapist_id() {
        return therapist_id;
    }

    public void setTherapist_id(String therapist_id) {
        this.therapist_id = therapist_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getApprove_date() {
        return approve_date;
    }

    public void setApprove_date(String approve_date) {
        this.approve_date = approve_date;
    }

    public String getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(String appointment_date) {
        this.appointment_date = appointment_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAppointment_status() {
        return appointment_status;
    }

    public void setAppointment_status(String appointment_status) {
        this.appointment_status = appointment_status;
    }

    public String getTherpist_name() {
        return therpist_name;
    }

    public void setTherpist_name(String therpist_name) {
        this.therpist_name = therpist_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getConsultation_fees() {
        return consultation_fees;
    }

    public void setConsultation_fees(String consultation_fees) {
        this.consultation_fees = consultation_fees;
    }

    public String getTherapist_location() {
        return therapist_location;
    }

    public void setTherapist_location(String therapist_location) {
        this.therapist_location = therapist_location;
    }
}
