package com.soul.soulber.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentVideosActivity;
import com.soul.soulber.activity.TestRecordsActivity;
import com.soul.soulber.activity.TherapistNotesActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentGuardianTwoDashboardBinding;
import com.soul.soulber.util.ProgressView;

public class GuardianTwoDashboardFragment extends Fragment {
    private FragmentGuardianTwoDashboardBinding binding;
    private Context context;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guardian_two_dashboard, container, false);
            context = inflater.getContext();
            initView();
        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(getActivity());
        session = new Session(getActivity());

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);

        onClick();

    }

    private void onClick() {

        binding.lnrTestRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TestRecordsActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DependentVideosActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrWeekleyAss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.lnrTherapistNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TherapistNotesActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrAftercarePlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.lnrDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.lnrProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.lnrMedications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.lnrMeetings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public static GuardianTwoDashboardFragment newInstance() {
        GuardianTwoDashboardFragment fragment = new GuardianTwoDashboardFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
//        hitDashboard(token);
    }

    private void hitDashboard(String token) {

        ProgressView.show(getActivity(), loadingDialog);

        Api.newApi(getActivity(), API.BaseUrl + "Dashboard/dashboard")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json my_details = data.getJson(P.my_details);
                        binding.txtUserName.setText("Welcome " + my_details.getString(P.name));

                    } else {
                        H.showMessage(getActivity(), json.getString(P.msg));
                    }
                })
                .run("hitDashboard", token);
    }
}