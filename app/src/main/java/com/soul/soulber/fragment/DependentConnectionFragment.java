package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.DependentConnectionFragAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentDependentConnectionBinding;
import com.soul.soulber.model.DependentConnectionFragModel;
import com.soul.soulber.model.DependentConnectionModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class DependentConnectionFragment extends Fragment implements DependentConnectionFragAdapter.onClick {

    private Context context;
    private FragmentDependentConnectionBinding binding;

    private DependentConnectionFragAdapter connectionAdapter;
    private List<DependentConnectionFragModel> connectionModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    public static DependentConnectionFragment newInstance() {
        DependentConnectionFragment fragment = new DependentConnectionFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dependent_connection, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        session = new Session(context);
        loadingDialog = new LoadingDialog(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        connectionModelList = new ArrayList<>();
        connectionAdapter = new DependentConnectionFragAdapter(context, connectionModelList, DependentConnectionFragment.this);
        binding.recyclerConnection.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerConnection.setHasFixedSize(true);
        binding.recyclerConnection.setAdapter(connectionAdapter);


        hitMyConnection(token);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            hitMyConnection(token);
        }
    }

    @Override
    public void onConnect(DependentConnectionFragModel model, TextView txtAction) {
        hitConnect(model, token, txtAction);
    }

    @Override
    public void onRemove(DependentConnectionFragModel model, TextView txtAction) {
        hitRemove(model, token, txtAction);
    }

    public void hitMyConnection(String token) {

        ProgressView.show(context, loadingDialog);
        Api.newApi(context, API.BaseUrl + "guardian/connection/my_connections")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    connectionModelList.clear();
                    connectionAdapter.notifyDataSetChanged();

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        JsonList guardian_connection = data.getJsonList(P.guardian_connection);
                        JsonList dependent_connection = data.getJsonList(P.dependent_connection);

                        if (dependent_connection != null && dependent_connection.size() != 0) {
                            for (Json jsonValue : dependent_connection) {
                                DependentConnectionFragModel model = new DependentConnectionFragModel();
                                model.setConnection_id(jsonValue.getString(P.connection_id));
                                model.setDependent_id(jsonValue.getString(P.dependent_id));
                                model.setDependent_name(jsonValue.getString(P.dependent_name));
                                model.setDependent_username(jsonValue.getString(P.dependent_username));
                                model.setIs_accepted(jsonValue.getString(P.is_accepted));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setConnection_time(jsonValue.getString(P.connection_time));
                                try {
                                    model.setProfile_pic(profile_pic_path + jsonValue.getString(P.profile_pic));
                                }catch (Exception e){
                                    model.setProfile_pic(profile_pic_path);
                                }
                                connectionModelList.add(model);
                            }
                            connectionAdapter.notifyDataSetChanged();
                        }

                        if (connectionModelList.isEmpty() && connectionModelList.isEmpty()) {
                            binding.txtError.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtError.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitMyConnection", token);
    }

    private void hitConnect(DependentConnectionFragModel model, String token, TextView txtConnect) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.dependent_id, model.getDependent_id());

        Api.newApi(context, API.BaseUrl + "guardian/connection/connect_dependent").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        txtConnect.setText("Requested");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }

    private void hitRemove(DependentConnectionFragModel model, String token, TextView textView) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, model.getConnection_id());

        Api.newApi(context, API.BaseUrl + "guardian/connection/remove_dependent_connection").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        textView.setText("Remove Connection Requested");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }

}
