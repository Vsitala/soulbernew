package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.MeetingHistoryAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentMeetingAppointmentBinding;
import com.soul.soulber.model.MeetingHistoryModel;
import com.soul.soulber.util.ProgressView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class MeetingUpcomingFragment extends Fragment {

    private Context context;
    private FragmentMeetingAppointmentBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private List<MeetingHistoryModel> meetingHistoryModelList;
    private MeetingHistoryAdapter adapter;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_meeting_appointment, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        meetingHistoryModelList = new ArrayList<>();
        adapter = new MeetingHistoryAdapter(getActivity(), meetingHistoryModelList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerList.setLayoutManager(linearLayoutManager);
        binding.recyclerList.setHasFixedSize(true);
        binding.recyclerList.setAdapter(adapter);

        hitMeetingHistoryData(pageCount);
        setPagination();

    }

    private void setPagination() {
        binding.recyclerList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (meetingHistoryModelList != null && !meetingHistoryModelList.isEmpty()) {
                        if (meetingHistoryModelList.size() < count) {
                            pageCount++;
                            hitMeetingHistoryData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitMeetingHistoryData(int pageCount) {

        ProgressView.show(context, loadingDialog);
        Api.newApi(context, API.BaseUrl + "meeting/dependent_meeting_attend_list?page=" + pageCount + "&per_page=20")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    Json upcoming = json.getJson(P.upcoming);

                    if (upcoming.getInt(P.status) == 1) {

                        Json data = upcoming.getJson(P.data);
                        String meeting_images_path = data.getString(P.meeting_images_path);

                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            meetingHistoryModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList upcomingList = data.getJsonList(P.upcoming);

                        if (upcomingList != null && upcomingList.size() != 0) {
                            for (Json jsonValue : upcomingList) {
                                MeetingHistoryModel model = new MeetingHistoryModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setMeeting_id(jsonValue.getString(P.meeting_id));
                                model.setRequest_date(jsonValue.getString(P.request_date));
                                model.setApprove_date(jsonValue.getString(P.approve_date));
                                model.setRemark(jsonValue.getString(P.remark));
                                model.setAttendee_status(jsonValue.getString(P.attendee_status));
                                model.setIs_latest(jsonValue.getString(P.is_latest));
                                model.setTopic(jsonValue.getString(P.topic));
                                model.setPrimary_image(jsonValue.getString(P.primary_image));
                                model.setVenue(jsonValue.getString(P.venue));
                                model.setCity(jsonValue.getString(P.city));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setDate(jsonValue.getString(P.date));
                                model.setTime(jsonValue.getString(P.time));
                                model.setImagePath(meeting_images_path);
                                model.setAttendee_count(jsonValue.getString(P.attendee_count));
                                model.setOther_images(jsonValue.getJsonArray(P.other_images));
                                model.setProfile_pic(jsonValue.getJsonArray(P.profile_pic));

                                meetingHistoryModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(context, json.getString(P.msg));
                    }
                })
                .run("hitUpcomingMeetingHistoryData", token);
    }

    private void checkData() {
        if (meetingHistoryModelList.isEmpty() && meetingHistoryModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
        binding.txtCount.setText(meetingHistoryModelList.size() + " meetings are conducted near you");
    }

    public static MeetingUpcomingFragment newInstance() {
        MeetingUpcomingFragment fragment = new MeetingUpcomingFragment();
        return fragment;
    }

}
