package com.soul.soulber.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.soul.soulber.BaseActivity.Basefragment.profile.ProfileEditActivity;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.FollowersActivity;
import com.soul.soulber.activity.FollowingActivity;
import com.soul.soulber.activity.GuardianConnectionActivity;
import com.soul.soulber.adapter.GuardianProfileAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentGuardianProfileBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;

public class GuardianProfileFragment extends Fragment {
    private FragmentGuardianProfileBinding binding;
    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";
    Session session;
    String userID;
    private TextView txtMessage, txtReportUser, txtBlockUser, txtLogOut;
    RelativeLayout slideView;
    View root;
    private SlideUp slideUp;
    String token;
    String usertype_id;
    ArrayList<DependentModel> dependentlist;
    LinearLayoutManager linearLayoutManager;
    private GuardianProfileAdapter adapter;

    private LoadingDialog loadingDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guardian_profile, container, false);
        root = binding.getRoot();
        initview();
        onClick();
        hitdependentlist();

        return root;
    }


    private void initview() {

        loadingDialog = new LoadingDialog(getActivity());

        dependentlist = new ArrayList<>();
        session = new Session(getContext());
        userID = session.getJson(Config.userProfileData).getString(P.id);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        txtMessage = root.findViewById(R.id.txtMessage);
        txtReportUser = root.findViewById(R.id.txtReportUser);
        txtBlockUser = root.findViewById(R.id.txtBlockUser);
        txtLogOut = root.findViewById(R.id.txtLogOut);

        txtMessage.setVisibility(View.GONE);
        txtReportUser.setVisibility(View.GONE);
        txtBlockUser.setVisibility(View.GONE);

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        binding.rvDependentlist.setLayoutManager(linearLayoutManager);
        adapter = new GuardianProfileAdapter(getActivity(), dependentlist);
        binding.rvDependentlist.setAdapter(adapter);

        slideView = root.findViewById(R.id.slideViewreport);
        slideUp = new SlideUpBuilder(slideView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        binding.dimprofile.setVisibility(View.VISIBLE);
                        binding.dimprofile.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                slideUp.hide();
                            }
                        });
                        if (visibility == View.GONE) {
                            binding.dimprofile.setVisibility(View.GONE);
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .withSlideFromOtherView(root.findViewById(R.id.slideViewreport))
                .build();
    }

    private void onClick() {
        binding.ivUsersmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.show();
            }
        });
        binding.tvEditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileEditActivity.class);
                startActivity(intent);
            }
        });
        binding.lnrConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (usertype_id.equals(Config.DEPENDENT)) {
                    Intent intentDependent = new Intent(getContext(), DependentConnectionActivity.class);
                    startActivity(intentDependent);
                } else if (usertype_id.equals(Config.GUARDIAN)) {
                    Intent intentGuardian = new Intent(getContext(), GuardianConnectionActivity.class);
                    startActivity(intentGuardian);
                }
            }
        });

        binding.lnrFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getActivity(), FollowersActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getActivity(), FollowingActivity.class);
                startActivity(intent);
            }
        });

        txtLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                logOutUser();
            }
        });
    }

    private void logOutUser() {
        session.clear();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private void hitdependentlist() {
        ProgressView.show(getActivity(), loadingDialog);
        Api.newApi(getActivity(), API.BaseUrl + "guardian/connection/my_connections").setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {
                            Json data = json.getJson(P.data);
                            String profile_pic_path = data.getString(P.profile_pic_path);
                            JsonList jsonList = data.getJsonList(P.dependent_connection);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    DependentModel model = new DependentModel();
                                    model.setName(jvalue.getString(P.dependent_name));
                                    model.setUsername(jvalue.getString(P.dependent_username));
                                    model.setCity(jvalue.getString(P.city));
                                    model.setAge(jvalue.getString(P.Age));
                                    model.setDependent_id(jvalue.getString(P.dependent_id));
                                    model.setProfile_pic(profile_pic_path + jvalue.getString(P.profile_pic));
                                    model.setIs_accepted(jvalue.getString(P.is_accepted));
                                    dependentlist.add(model);
                                }
                                adapter.notifyDataSetChanged();

                            }

                        }
                        ProgressView.dismiss(loadingDialog);
                        checkData();
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {
                        H.showMessage(getActivity(), "On error is called");
                        ProgressView.dismiss(loadingDialog);
                        checkData();
                    }
                })
                .run("hitdependentlist", token);

    }

    private void checkData(){
        if (dependentlist.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }
    public void onResume() {
        super.onResume();
        hitUserDetails(token);

        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;

            dependentlist.clear();
            adapter.notifyDataSetChanged();
            hitdependentlist();
        }
    }

    private void hitUserDetails(String token) {

        ProgressView.show(getActivity(), loadingDialog);

        Json j = new Json();
        j.addString(P.guardian_id, userID);
        Api.newApi(getActivity(), API.BaseUrl + "userprofiles/single_guardian_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json guardian_details = data.getJson(P.guardian_details);
                        setUserData(guardian_details);
                    } else {
                        H.showMessage(getActivity(), json.getString(P.error));
                    }
                })
                .run("hitUserDetails", token);
    }

    private void setUserData(Json json) {

        LoadImage.glideString(getActivity(), binding.imgProfile, imageBaseUrl + json.getString(P.profile_pic), getResources().getDrawable(R.drawable.ic_baseline_person_24));
        binding.txtTitle.setText(checkString(json.getString(P.username)));
        binding.txtUserName.setText(checkString(json.getString(P.name)));
        binding.txtUserBio.setText(checkString(json.getString(P.bio)));

        if (json.has(P.connections)) {
            binding.txtConnection.setText(json.getString(P.connections));
        }
        if (json.has(P.followers)) {
            binding.txtFollowers.setText(json.getString(P.followers));
        }
        if (json.has(P.following)) {
            binding.txtFollowing.setText(json.getString(P.following));
        }

    }
}