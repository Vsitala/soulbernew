package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.SearchDependentFragAdapter;
import com.soul.soulber.adapter.SearchGuardianFragAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentGuardianAddConnectionBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.GuardianConnectionFragModel;
import com.soul.soulber.model.GuardianModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class GuardianAddConnectionFragment extends Fragment implements SearchGuardianFragAdapter.onClick {

    private Context context;
    private FragmentGuardianAddConnectionBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private SearchGuardianFragAdapter adapter;
    private List<GuardianModel> guardianModelList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guardian_add_connection, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);


        guardianModelList = new ArrayList<>();
        adapter = new SearchGuardianFragAdapter(getActivity(), guardianModelList, userID, GuardianAddConnectionFragment.this);
        binding.recyclerConnection.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerConnection.setHasFixedSize(true);
        binding.recyclerConnection.setNestedScrollingEnabled(true);
        binding.recyclerConnection.setAdapter(adapter);

        onClick();
    }

    private void onClick() {

        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.etxSearch.getText().toString().trim())) {
                    H.showMessage(getActivity(), "Please enter name or email");
                } else {
                    if (binding.etxSearch.getText().toString().trim().contains("@")) {
                        hitSearchGuardian(binding.etxSearch.getText().toString().trim(), "", token);
                    } else {
                        hitSearchGuardian("", binding.etxSearch.getText().toString().trim(), token);
                    }
                }
            }
        });
    }


    public void hitSearchGuardian(String email, String name, String token) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.name, name);
        j.addString(P.email, email);

        Api.newApi(context, API.BaseUrl + "userprofiles/search_guardian").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    guardianModelList.clear();
                    adapter.notifyDataSetChanged();

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        Json guardlist = data.getJson(P.guardlist);
                        JsonList connectable = guardlist.getJsonList(P.connectable);
                        JsonList newList = guardlist.getJsonList(P.newList);

                        if (connectable != null && connectable.size() != 0) {
                            for (Json jsonData : connectable) {

                                GuardianModel model = new GuardianModel();
                                model.setGuardian_id(jsonData.getString(P.guardian_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));


                                guardianModelList.add(model);
                            }
                        }


                        if (newList != null && newList.size() != 0) {
                            for (Json jsonData : newList) {

                                GuardianModel model = new GuardianModel();
                                model.setGuardian_id(jsonData.getString(P.guardian_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                guardianModelList.add(model);
                            }
                        }

                        adapter.notifyDataSetChanged();

                        if (guardianModelList.isEmpty()) {
                            binding.txtMessage.setText("Showing 0 result for " + binding.etxSearch.getText().toString());
                        } else {
                            binding.txtMessage.setText("Showing " + guardianModelList.size() + " result for " + binding.etxSearch.getText().toString());
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitSearchDependent", token);
    }


    private void checkData() {
        if (guardianModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnect(GuardianModel model, Button button) {
        hitConnect(model.getGuardian_id(), token, button);
    }

    @Override
    public void onRemove(GuardianModel model, Button button) {
        hitRemove(model.getConnection_id(), token, button);
    }

    @Override
    public void onAccept(GuardianModel model, Button button) {
        hitAccept(model.getConnection_id(), token, button);
    }


    private void hitConnect(String id, String token, Button button) {

        ProgressView.show(context, loadingDialog);
        String apiUrl = "";

        Json j = new Json();
        apiUrl = "guardian/connection/connect_guardian";
        j.addString(P.guardian_id, id);

        Api.newApi(context, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        button.setText("Requested");
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }


    private void hitRemove(String id, String token, Button button) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        String apiUrl = "";

        apiUrl = "guardian/connection/remove_guardian_connection";
        j.addString(P.connection_id, id);
        j.addString(P.reason, "");

        Api.newApi(context, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        button.setText("Connect");
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }

    private void hitAccept(String connectionID, String token, Button txtIgnore) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, connectionID);

        if (txtIgnore.getText().toString().equals("Reject")) {
            j.addString(P.action_id, "2");
        } else if (txtIgnore.getText().toString().equals("Accept")) {
            j.addString(P.action_id, "1");
        }

        Api.newApi(context, API.BaseUrl + "guardian/connection/guardidan_connection_action").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        if (txtIgnore.getText().toString().equals("Ignore")) {
                            txtIgnore.setText("Accept");
                        } else if (txtIgnore.getText().toString().equals("Accept")) {
                            txtIgnore.setText("Reject");
                        }
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitAccept", token);
    }


    public static GuardianAddConnectionFragment newInstance() {
        GuardianAddConnectionFragment fragment = new GuardianAddConnectionFragment();
        return fragment;
    }

}
