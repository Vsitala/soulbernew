package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.GuardianConnectionFragAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentGuardianConnectionBinding;
import com.soul.soulber.model.GuardianConnectionFragModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class GuardianConnectionFragment extends Fragment implements GuardianConnectionFragAdapter.onClick {

    private Context context;
    private FragmentGuardianConnectionBinding binding;

    private GuardianConnectionFragAdapter connectionAdapter;
    private List<GuardianConnectionFragModel> connectionModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guardian_connection, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        connectionModelList = new ArrayList<>();
        connectionAdapter = new GuardianConnectionFragAdapter(context, connectionModelList, userID, GuardianConnectionFragment.this);
        binding.recyclerConnection.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerConnection.setHasFixedSize(true);
        binding.recyclerConnection.setAdapter(connectionAdapter);

        hitMyConnection(token);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            hitMyConnection(token);
        }
    }

    @Override
    public void onConnect(GuardianConnectionFragModel model, TextView txtConnect) {
        hitConnect(model, token, txtConnect);
    }

    @Override
    public void onIgnore(GuardianConnectionFragModel model, TextView txtIgnore) {
        hitIgnore(model, token, txtIgnore);
    }

    @Override
    public void onRemove(GuardianConnectionFragModel model, TextView button) {
        hitRemove(model, token, button);
    }

    public void hitMyConnection(String token) {

        ProgressView.show(context, loadingDialog);
        Api.newApi(context, API.BaseUrl + "guardian/connection/my_connections")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    connectionModelList.clear();
                    connectionAdapter.notifyDataSetChanged();

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        JsonList guardian_connection = data.getJsonList(P.guardian_connection);
                        JsonList dependent_connection = data.getJsonList(P.dependent_connection);

                        if (guardian_connection != null && guardian_connection.size() != 0) {
                            Log.e("TAG", "hitMyConnectionG: " + guardian_connection.toString());
                            for (Json jsonValue : guardian_connection) {
                                GuardianConnectionFragModel model = new GuardianConnectionFragModel();
                                model.setConnection_id(jsonValue.getString(P.connection_id));
                                model.setFrom_guardian_id(jsonValue.getString(P.from_guardian_id));
                                model.setFrom_guardian_name(jsonValue.getString(P.from_guardian_name));
                                model.setTo_guardian_id(jsonValue.getString(P.to_guardian_id));
                                model.setTo_guardian_name(jsonValue.getString(P.to_guardian_name));
                                model.setIs_accepted(jsonValue.getString(P.is_accepted));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setConnection_time(jsonValue.getString(P.connection_time));
                                try {
                                    model.setProfile_pic(profile_pic_path + jsonValue.getString(P.profile_pic));
                                } catch (Exception e) {
                                    model.setProfile_pic(profile_pic_path);
                                }
                                connectionModelList.add(model);
                            }
                            connectionAdapter.notifyDataSetChanged();

                        }

                        if (connectionModelList.isEmpty() && connectionModelList.isEmpty()) {
                            binding.txtError.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtError.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitMyConnection", token);
    }

    private void hitConnect(GuardianConnectionFragModel model, String token, TextView txtConnect) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.guardian_id, model.getTo_guardian_id());

        Api.newApi(context, API.BaseUrl + "guardian/connection/connect_guardian").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        txtConnect.setText("Requested");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }

    private void hitIgnore(GuardianConnectionFragModel model, String token, TextView txtIgnore) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, model.getConnection_id());

        if (txtIgnore.getText().toString().equals("Ignore")) {
            j.addString(P.action_id, "2");
        } else if (txtIgnore.getText().toString().equals("Accept")) {
            j.addString(P.action_id, "1");
        }

        Api.newApi(context, API.BaseUrl + "guardian/connection/guardidan_connection_action").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        if (txtIgnore.getText().toString().equals("Ignore")) {
                            txtIgnore.setText("Accept");
                        } else if (txtIgnore.getText().toString().equals("Accept")) {
                            txtIgnore.setText("Ignore");
                        }
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitIgnore", token);
    }

    private void hitRemove(GuardianConnectionFragModel model, String token, TextView textView) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, model.getConnection_id());

        Api.newApi(context, API.BaseUrl + "guardian/connection/remove_guardian_connection").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        textView.setText("Remove Connection Requested");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }

    public static GuardianConnectionFragment newInstance() {
        GuardianConnectionFragment fragment = new GuardianConnectionFragment();
        return fragment;
    }
}
