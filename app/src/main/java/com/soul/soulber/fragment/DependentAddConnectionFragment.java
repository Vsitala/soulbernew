package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.SearchDependentFragAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentDependentAddConnectionBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class DependentAddConnectionFragment extends Fragment implements SearchDependentFragAdapter.onClick {

    private Context context;
    private FragmentDependentAddConnectionBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private SearchDependentFragAdapter adapter;
    private List<DependentModel> dependentModelList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dependent_add_connection, container, false);
            context = inflater.getContext();
            initView();
        }
        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        dependentModelList = new ArrayList<>();
        adapter = new SearchDependentFragAdapter(getActivity(), dependentModelList, userID, DependentAddConnectionFragment.this);
        binding.recyclerConnection.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerConnection.setHasFixedSize(true);
        binding.recyclerConnection.setNestedScrollingEnabled(true);
        binding.recyclerConnection.setAdapter(adapter);

        onClick();
    }


    private void onClick() {

        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.etxSearch.getText().toString().trim())) {
                    H.showMessage(getActivity(), "Please enter name or email");
                } else {
                    if (binding.etxSearch.getText().toString().trim().contains("@")) {
                        hitSearchDependent(binding.etxSearch.getText().toString().trim(), "", token);
                    } else {
                        hitSearchDependent("", binding.etxSearch.getText().toString().trim(), token);
                    }
                }
            }
        });
    }

    public void hitSearchDependent(String email, String name, String token) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.name, name);
        j.addString(P.email, email);

        Api.newApi(context, API.BaseUrl + "userprofiles/search_depenedent").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    dependentModelList.clear();
                    adapter.notifyDataSetChanged();

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        Json dependentlist = data.getJson(P.dependentlist);
                        JsonList connectable = dependentlist.getJsonList(P.connectable);
                        JsonList newList = dependentlist.getJsonList(P.newList);

                        if (connectable != null && connectable.size() != 0) {
                            for (Json jsonData : connectable) {

                                DependentModel model = new DependentModel();
                                model.setDependent_id(jsonData.getString(P.dependent_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                dependentModelList.add(model);
                            }
                        }


                        if (newList != null && newList.size() != 0) {
                            for (Json jsonData : newList) {

                                DependentModel model = new DependentModel();
                                model.setDependent_id(jsonData.getString(P.dependent_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                dependentModelList.add(model);
                            }
                        }

                        adapter.notifyDataSetChanged();

                        if (dependentModelList.isEmpty()) {
                            binding.txtMessage.setText("Showing 0 result for " + binding.etxSearch.getText().toString());
                        } else {
                            binding.txtMessage.setText("Showing " + dependentModelList.size() + " result for " + binding.etxSearch.getText().toString());
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitSearchDependent", token);
    }


    private void checkData() {
        if (dependentModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onConnect(DependentModel model, Button button) {
        hitConnect(model.getDependent_id(), token, button);
    }

    @Override
    public void onRemove(DependentModel model, Button button) {
        hitRemove(model.getConnection_id(), token, button);
    }

    private void hitConnect(String id, String token, Button button) {

        ProgressView.show(context, loadingDialog);
        String apiUrl = "";

        Json j = new Json();
        apiUrl = "guardian/connection/connect_dependent";
        j.addString(P.dependent_id, id);

        Api.newApi(context, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        button.setText("Requested");
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }


    private void hitRemove(String id, String token, Button button) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        String apiUrl = "";

        apiUrl = "guardian/connection/remove_dependent_connection";
        j.addString(P.connection_id, id);

        Api.newApi(context, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        button.setText("Connect");
                        Config.CONNECTION_APPLY = true;
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }

    public static DependentAddConnectionFragment newInstance() {
        DependentAddConnectionFragment fragment = new DependentAddConnectionFragment();
        return fragment;
    }
}
