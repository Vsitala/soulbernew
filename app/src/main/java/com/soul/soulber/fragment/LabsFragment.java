package com.soul.soulber.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.google.android.material.tabs.TabLayout;
import com.soul.soulber.R;
import com.soul.soulber.Therapist.LabResultActivity;
import com.soul.soulber.activity.LabAndTherapistActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentLabsBinding;

import java.util.ArrayList;
import java.util.List;

public class LabsFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private Context context;
    private FragmentLabsBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private LabUpcomingFragment labUpcomingFragment = LabUpcomingFragment.newInstance();
    private LabPastFragment labPastFragment = LabPastFragment.newInstance();
    private LabUnConformFragment labUnConformFragment = LabUnConformFragment.newInstance();

    private String upcoming = "Upcoming";
    private String past = "Past";
    private String unConfirm = "Un-Confirm";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_labs, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        setupViewPager(binding.viewPager);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        binding.viewPager.addOnPageChangeListener((ViewPager.OnPageChangeListener) context);
        setupTabIcons();

        onClick();

    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(labUpcomingFragment, "");
        adapter.addFragment(labPastFragment, "");
        adapter.addFragment(labUnConformFragment, "");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {

        View view1 = LayoutInflater.from(getActivity()).inflate(R.layout.activity_customt_ab_small, null);
        ((TextView) view1.findViewById(R.id.text)).setText(upcoming);
        ((TextView) view1.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));

        View view2 = LayoutInflater.from(getActivity()).inflate(R.layout.activity_customt_ab_small, null);
        ((TextView) view2.findViewById(R.id.text)).setText(past);
        ((TextView) view2.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));

        View view3 = LayoutInflater.from(getActivity()).inflate(R.layout.activity_customt_ab_small, null);
        ((TextView) view3.findViewById(R.id.text)).setText(unConfirm);
        ((TextView) view3.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));


        binding.tabLayout.getTabAt(0).setCustomView(view1);
        binding.tabLayout.getTabAt(1).setCustomView(view2);
        binding.tabLayout.getTabAt(2).setCustomView(view3);

        binding.tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.inactive));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.text)).setTextColor(getResources().getColor(R.color.gray));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void onClick() {

        binding.txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LabAndTherapistActivity) getActivity()).onGoogleSearchCalled(1, binding.txtLocation);
            }
        });

        binding.btnSearchHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.txtLocation.getText().toString().equals("")) {
                    H.showMessage(getActivity(), "Please select location");
                } else {
                    Intent intent = new Intent(getActivity(), LabResultActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public static LabsFragment newInstance() {
        LabsFragment fragment = new LabsFragment();
        return fragment;
    }

}
