package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.LabAndTherapistActivity;
import com.soul.soulber.adapter.LabHistoryAdapter;
import com.soul.soulber.adapter.PDFAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentLabsPastBinding;
import com.soul.soulber.model.LabHistoryModel;
import com.soul.soulber.model.PDFModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class LabPastFragment extends Fragment implements PDFAdapter.onClick {

    private Context context;
    private FragmentLabsPastBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private List<LabHistoryModel> labHistoryModelList;
    private LabHistoryAdapter adapter;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private String dummyPDF = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_labs_past, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);


        labHistoryModelList = new ArrayList<>();
        adapter = new LabHistoryAdapter(getActivity(), labHistoryModelList, LabPastFragment.this);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerList.setLayoutManager(linearLayoutManager);
        binding.recyclerList.setHasFixedSize(true);
        binding.recyclerList.setAdapter(adapter);

        hitLabHistoryData(pageCount);
        setPagination();
    }

    private void setPagination() {
        binding.recyclerList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (labHistoryModelList != null && !labHistoryModelList.isEmpty()) {
                        if (labHistoryModelList.size() < count) {
                            pageCount++;
                            hitLabHistoryData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitLabHistoryData(int pageCount) {

        ProgressView.show(context, loadingDialog);
        Api.newApi(context, API.BaseUrl + "lab/dependent_lab_booking_list?page=" + pageCount + "&per_page=20")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    Json past = json.getJson(P.past);

                    if (past.getInt(P.status) == 1) {

                        Json data = past.getJson(P.data);
                        String lab_images_path = data.getString(P.lab_images_path);
                        String lab_reports_images_path = data.getString(P.lab_reports_images_path);

                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            labHistoryModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList pastList = data.getJsonList(P.past);

                        if (pastList != null && pastList.size() != 0) {
                            for (Json jsonValue : pastList) {
                                LabHistoryModel model = new LabHistoryModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setLab_id(jsonValue.getString(P.lab_id));
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setRequest_date(jsonValue.getString(P.request_date));
                                model.setApprove_date(jsonValue.getString(P.approve_date));
                                model.setAppointment_date(jsonValue.getString(P.appointment_date));
                                model.setRemark(jsonValue.getString(P.remark));
                                model.setAppointment_status(jsonValue.getString(P.appointment_status));
                                model.setAppointment_id(jsonValue.getString(P.appointment_id));
                                model.setLab_name(jsonValue.getString(P.lab_name));
                                model.setImage(lab_images_path + jsonValue.getString(P.image));
                                model.setBio(jsonValue.getString(P.bio));
                                model.setAddress(jsonValue.getString(P.address));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setLab_location(jsonValue.getString(P.lab_location));
                                model.setReports(jsonValue.getJsonList(P.reports));
                                model.setLab_reports_images_path(lab_reports_images_path);

                                labHistoryModelList.add(model);

                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(context, json.getString(P.msg));
                    }
                })
                .run("hitLabHistoryData", token);
    }

    private void checkData() {
        if (labHistoryModelList.isEmpty() && labHistoryModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDownload(PDFModel model) {
        ((LabAndTherapistActivity) getActivity()).onDownload(model.getReport_file(), model.getReport_title());
    }

    public static LabPastFragment newInstance() {
        LabPastFragment fragment = new LabPastFragment();
        return fragment;
    }

}
