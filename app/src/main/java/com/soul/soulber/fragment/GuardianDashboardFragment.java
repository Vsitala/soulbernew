package com.soul.soulber.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.ComingSoonActivity;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.ForumActivity;
import com.soul.soulber.activity.GuardianConnectionActivity;
import com.soul.soulber.activity.GuardianMeetingActivity;
import com.soul.soulber.activity.GuardianTestResusltActivity;
import com.soul.soulber.activity.GuardianTherapyActivity;
import com.soul.soulber.activity.PanicHistoryActivity;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentGuardianDashboardBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class GuardianDashboardFragment extends Fragment {
    private FragmentGuardianDashboardBinding binding;
    private Context context;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_guardian_dashboard, container, false);
            context = inflater.getContext();
            initView();
        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(getActivity());
        session = new Session(getActivity());

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);

        onClick();

    }

    private void onClick() {

        binding.lnrWall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Config.FOR_MY_POST = false;
                Intent intent = new Intent(getContext(), UserPostActivity.class);
                startActivity(intent);
            }
        });
        binding.lnrPanicHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Click.preventTwoClick(view);
                Intent intent = new Intent(getContext(), PanicHistoryActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (usertype_id.equals(Config.DEPENDENT)) {
                    Intent intentDependent = new Intent(getContext(), DependentConnectionActivity.class);
                    startActivity(intentDependent);
                } else if (usertype_id.equals(Config.GUARDIAN)) {
                    Intent intentGuardian = new Intent(getContext(), GuardianConnectionActivity.class);
                    startActivity(intentGuardian);
                }
            }
        });


        binding.lnrForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getContext(), ForumActivity.class);
                startActivity(intent);
            }
        });


        binding.lnrTherapySession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getContext(), GuardianTherapyActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrRewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getContext(), ComingSoonActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrTestResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intenttestresult = new Intent(getActivity(), GuardianTestResusltActivity.class);
                startActivity(intenttestresult);
            }
        });


        binding.lnrMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intentmeeting = new Intent(getActivity(), GuardianMeetingActivity.class);
                startActivity(intentmeeting);
            }
        });

    }

    public static GuardianDashboardFragment newInstance() {
        GuardianDashboardFragment fragment = new GuardianDashboardFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        hitDashboard(token);
    }

    private void hitDashboard(String token) {

        ProgressView.show(getActivity(), loadingDialog);

        Api.newApi(getActivity(), API.BaseUrl + "Dashboard/dashboard")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json my_details = data.getJson(P.my_details);
                        String meetings_attended_count = data.getString(P.meetings_attended_count);
                        String therapist_attended_count = data.getString(P.therapist_attended_count);
                        binding.txtUserName.setText("Welcome " + my_details.getString(P.name));
                        binding.txtConnection.setText(my_details.getString(P.connections));
                        binding.txtMeetingCount.setText(meetings_attended_count);
                        binding.txtTheropyCount.setText(therapist_attended_count);
                    } else {
                        H.showMessage(getActivity(), json.getString(P.msg));
                    }
                })
                .run("hitDashboard", token);
    }
}