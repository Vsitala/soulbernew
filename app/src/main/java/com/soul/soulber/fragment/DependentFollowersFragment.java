package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.FollowersAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentDependentFollowerBinding;
import com.soul.soulber.model.FollowersModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class DependentFollowersFragment extends Fragment {

    private Context context;
    private FragmentDependentFollowerBinding binding;

    private List<FollowersModel> followersModelList;
    private FollowersAdapter followersAdapter;


    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private String userID = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dependent_follower, container, false);
            context = inflater.getContext();
            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        followersModelList = new ArrayList<>();
        followersAdapter = new FollowersAdapter(context,followersModelList,Config.DEPENDENT);
        binding.recyclerFollower.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerFollower.setHasFixedSize(true);
        binding.recyclerFollower.setAdapter(followersAdapter);

        hitFollowersData(token,userID);

    }


    @Override
    public void onResume() {
        super.onResume();
        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            followersModelList.clear();
            followersAdapter.notifyDataSetChanged();
            hitFollowersData(token,userID);
        }
    }

    private void hitFollowersData(String token, String userId) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.user_id,userId);

        Api.newApi(context, API.BaseUrl + "Follower/show_user_followers").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        String profile_pic_path = json.getString(P.profile_pic_path);
                        Json data = json.getJson(P.data);
                        JsonList guardianlist = data.getJsonList(P.guardianlist);
                        JsonList dependentlist = data.getJsonList(P.dependentlist);

                        if (dependentlist != null && dependentlist.size() != 0) {
                            for (Json jsonValue : dependentlist) {
                                FollowersModel model = new FollowersModel();
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setProfile_pic(profile_pic_path +  jsonValue.getString(P.profile_pic));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setConnection_time(jsonValue.getString(P.connection_time));
                                model.setIs_i_following(jsonValue.getString(P.is_i_following));
                                model.setUser_type_id(jsonValue.getString(P.user_type_id));
                                followersModelList.add(model);
                            }
                            followersAdapter.notifyDataSetChanged();
                        }

                        if (followersModelList.isEmpty() && followersModelList.isEmpty()) {
                            binding.txtError.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtError.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(context, json.getString(P.msg));
                    }
                })
                .run("hitFollowersData1", token);
    }


    public static DependentFollowersFragment newInstance() {
        DependentFollowersFragment fragment = new DependentFollowersFragment();
        return fragment;
    }
}
