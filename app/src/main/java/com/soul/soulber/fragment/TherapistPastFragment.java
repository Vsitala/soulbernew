package com.soul.soulber.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.TheropistHistoryAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentTheraistUpcomingBinding;
import com.soul.soulber.model.TheropistHistoryModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class TherapistPastFragment extends Fragment {

    private Context context;
    private FragmentTheraistUpcomingBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private List<TheropistHistoryModel> theropistHistoryModelList;
    private TheropistHistoryAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_theraist_upcoming, container, false);
            context = inflater.getContext();

            initView();

        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        theropistHistoryModelList = new ArrayList<>();
        adapter = new TheropistHistoryAdapter(getActivity(), theropistHistoryModelList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerList.setLayoutManager(linearLayoutManager);
        binding.recyclerList.setHasFixedSize(true);
        binding.recyclerList.setNestedScrollingEnabled(false);
        binding.recyclerList.setAdapter(adapter);

        hitTherapistHistoryData(pageCount);
        setPagination();
    }

    private void setPagination() {
        binding.recyclerList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (theropistHistoryModelList != null && !theropistHistoryModelList.isEmpty()) {
                        if (theropistHistoryModelList.size() < count) {
                            pageCount++;
                            hitTherapistHistoryData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitTherapistHistoryData(int pageCount) {

        ProgressView.show(context, loadingDialog);
        Api.newApi(context, API.BaseUrl + "therapist/dependent_therapist_booking_list?page=" + pageCount + "&per_page=20")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    Json past = json.getJson(P.past);

                    if (past.getInt(P.status) == 1) {

                        Json data = past.getJson(P.data);
                        String therapist_images_path = data.getString(P.therapist_images_path);

                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }


                        if (pageCount == 1) {
                            theropistHistoryModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList pastList = data.getJsonList(P.past);

                        if (pastList != null && pastList.size() != 0) {
                            for (Json jsonValue : pastList) {
                                TheropistHistoryModel model = new TheropistHistoryModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setTherapist_id(jsonValue.getString(P.therapist_id));
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setRequest_date(jsonValue.getString(P.request_date));
                                model.setApprove_date(jsonValue.getString(P.approve_date));
                                model.setAppointment_date(jsonValue.getString(P.appointment_date));
                                model.setRemark(jsonValue.getString(P.remark));
                                model.setAppointment_status(jsonValue.getString(P.appointment_status));
                                model.setTherpist_name(jsonValue.getString(P.therpist_name));
                                model.setImage(therapist_images_path + jsonValue.getString(P.image));
                                model.setExperience(jsonValue.getString(P.experience));
                                model.setBio(jsonValue.getString(P.bio));
                                model.setClinic_name(jsonValue.getString(P.clinic_name));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setConsultation_fees(jsonValue.getString(P.consultation_fees));
                                model.setTherapist_location(jsonValue.getString(P.therapist_location));

                                theropistHistoryModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(context, json.getString(P.msg));
                    }
                })
                .run("hitTherapistHistoryData", token);
    }

    private void checkData() {
        if (theropistHistoryModelList.isEmpty() && theropistHistoryModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }


    public static TherapistPastFragment newInstance() {
        TherapistPastFragment fragment = new TherapistPastFragment();
        return fragment;
    }
}
