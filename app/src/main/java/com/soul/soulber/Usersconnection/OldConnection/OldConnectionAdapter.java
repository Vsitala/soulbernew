package com.soul.soulber.Usersconnection.OldConnection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.soul.soulber.R;
import com.soul.soulber.Usersconnection.RecentConnection.ResentConnectionmodleclass;

import java.util.ArrayList;


    public class OldConnectionAdapter extends RecyclerView.Adapter<OldConnectionAdapter.ViewHolder> {
        private ArrayList<OldConnectionmodleclass> olduserlist;
        private Context context;
        private int selectedItem;
        public OldConnectionAdapter(Context context,ArrayList<OldConnectionmodleclass> olduserlist) {
            this.olduserlist = olduserlist;
            this.context = context;
            selectedItem=0;
        }

        @Override
        public OldConnectionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_oldconnection, viewGroup, false);
            return new OldConnectionAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(OldConnectionAdapter.ViewHolder viewHolder, int position) {

            viewHolder.tv_usersnameold.setText(olduserlist.get(position).getOldusersnmae());
            viewHolder.tv_timingold.setText(olduserlist.get(position).getOlduserstime());
//            viewHolder.tv_usersnamerec.setText(requestlist.get(position).getUsersname());
//            viewHolder.tv_connectuser.setText(requestlist.get(position).getConnect());
//            viewHolder.tv_ignoreuser.setText(requestlist.get(position).getIgnore());
            viewHolder.tv_removeuserold.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int newPosition = viewHolder.getAdapterPosition();
                    olduserlist.remove(newPosition);
                    notifyItemRemoved(newPosition);
                    notifyDataSetChanged();
                    notifyItemRangeChanged(newPosition, olduserlist.size());
                }
            });


            Glide
                    .with(context)

                    .load(olduserlist.get(position).getOlduserimage())
                    .into(viewHolder.iv_usersimageold);


        }

        @Override
        public int getItemCount() {
            return olduserlist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            private final ImageView iv_usersimageold;
            private final TextView tv_usersnameold;
            private final TextView tv_removeuserold;
            private final TextView tv_timingold;

            public ViewHolder(View view) {
                super(view);
                tv_usersnameold=view.findViewById(R.id.tv_usersnameold);
                tv_timingold=view.findViewById(R.id.tv_timingold);
                tv_removeuserold=view.findViewById(R.id.tv_removeuserold);
                iv_usersimageold =  view.findViewById(R.id.iv_usersimageold);

            }

        }

    }
