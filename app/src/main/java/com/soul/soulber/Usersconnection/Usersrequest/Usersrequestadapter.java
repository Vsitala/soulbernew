package com.soul.soulber.Usersconnection.Usersrequest;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.soul.soulber.R;

import java.util.ArrayList;

    public class Usersrequestadapter extends RecyclerView.Adapter<Usersrequestadapter.ViewHolder> {
        private ArrayList<Usersrequestmodleclass> requestlist;
        private Context context;
        private int selectedItem;
        public Usersrequestadapter(Context context,ArrayList<Usersrequestmodleclass> requestlist) {
            this.requestlist = requestlist;
            this.context = context;
            selectedItem=0;
        }

        @Override
        public Usersrequestadapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_usersrequest, viewGroup, false);
            return new Usersrequestadapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(Usersrequestadapter.ViewHolder viewHolder, int position) {

            viewHolder.tv_usersname.setText(requestlist.get(position).getUsersname());
//            viewHolder.tv_connectuser.setText(requestlist.get(position).getConnect());
//            viewHolder.tv_ignoreuser.setText(requestlist.get(position).getIgnore());


            Glide
                    .with(context)

                    .load(requestlist.get(position).getUsersimage())
                    //cropping center image
                    .into(viewHolder.iv_usersimage);


        }

        @Override
        public int getItemCount() {
            return requestlist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            private ImageView iv_usersimage;
            private TextView tv_usersname,tv_connectuser,tv_ignoreuser;

            public ViewHolder(View view) {
                super(view);
                tv_usersname=view.findViewById(R.id.tv_usersname);
                tv_connectuser=view.findViewById(R.id.tv_connectuser);
                tv_ignoreuser=view.findViewById(R.id.tv_ignoreuser);
                iv_usersimage =  view.findViewById(R.id.iv_usersimage);

            }

        }

    }






