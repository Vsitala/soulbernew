package com.soul.soulber.Usersconnection.OldConnection;

public class OldConnectionmodleclass {
    public OldConnectionmodleclass() {

    }

    public int getOlduserimage() {
        return olduserimage;
    }

    public void setOlduserimage(int olduserimage) {
        this.olduserimage = olduserimage;
    }

    public String getOldusersnmae() {
        return oldusersnmae;
    }

    public void setOldusersnmae(String oldusersnmae) {
        this.oldusersnmae = oldusersnmae;
    }

    public String getOlduserstime() {
        return olduserstime;
    }

    public void setOlduserstime(String olduserstime) {
        this.olduserstime = olduserstime;
    }

    public OldConnectionmodleclass(int olduserimage, String oldusersnmae, String olduserstime) {
        this.olduserimage = olduserimage;
        this.oldusersnmae = oldusersnmae;
        this.olduserstime = olduserstime;
    }

    int olduserimage;
    String oldusersnmae;
    String olduserstime;
}
