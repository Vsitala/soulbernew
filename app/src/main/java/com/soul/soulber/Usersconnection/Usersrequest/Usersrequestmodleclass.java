package com.soul.soulber.Usersconnection.Usersrequest;

public class Usersrequestmodleclass {
    public int getUsersimage() {
        return usersimage;
    }


    public void setUsersimage(int usersimage) {
        this.usersimage = usersimage;
    }

    public String getUsersname() {
        return usersname;
    }

    public void setUsersname(String usersname) {
        this.usersname = usersname;
    }

    public String getConnect() {
        return connect;
    }

    public void setConnect(String connect) {
        this.connect = connect;
    }

    public String getIgnore() {
        return ignore;
    }

    public void setIgnore(String ignore) {
        this.ignore = ignore;
    }

    public Usersrequestmodleclass(int usersimage, String usersname, String connect, String ignore) {
        this.usersimage = usersimage;
        this.usersname = usersname;
        this.connect = connect;
        this.ignore = ignore;
    }
    public Usersrequestmodleclass() {
    }

    int usersimage;
    String usersname;
    String connect;
    String ignore;

}
