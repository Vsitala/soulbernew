package com.soul.soulber.Usersconnection.RecentConnection;

public class ResentConnectionmodleclass {
    public int getRecentusersimage() {
        return recentusersimage;
    }

    public void setRecentusersimage(int recentusersimage) {
        this.recentusersimage = recentusersimage;
    }

    public String getRecentusersname() {
        return recentusersname;
    }

    public void setRecentusersname(String recentusersname) {
        this.recentusersname = recentusersname;
    }

    public String getRecentusertime() {
        return recentusertime;
    }

    public void setRecentusertime(String recentusertime) {
        this.recentusertime = recentusertime;
    }

    public ResentConnectionmodleclass(int recentusersimage, String recentusersname, String recentusertime) {
        this.recentusersimage = recentusersimage;
        this.recentusersname = recentusersname;
        this.recentusertime = recentusertime;
    }
    public ResentConnectionmodleclass() {

    }

    int recentusersimage;
    String recentusersname;
    String recentusertime;
}
