package com.soul.soulber.Usersconnection.RecentConnection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.soul.soulber.R;
import com.soul.soulber.Usersconnection.Usersrequest.Usersrequestadapter;
import com.soul.soulber.Usersconnection.Usersrequest.Usersrequestmodleclass;

import java.util.ArrayList;

public class ResentConnectionAdapter extends RecyclerView.Adapter<ResentConnectionAdapter.ViewHolder> {
    private ArrayList<ResentConnectionmodleclass> requestlist;
    private Context context;
    private int selectedItem;

    public ResentConnectionAdapter(Context context, ArrayList<ResentConnectionmodleclass> requestlist) {
        this.requestlist = requestlist;
        this.context = context;
        selectedItem = 0;
    }

    @Override
    public ResentConnectionAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recentconnection, viewGroup, false);
        return new ResentConnectionAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ResentConnectionAdapter.ViewHolder viewHolder, int position) {

        viewHolder.tv_usersnamerec.setText(requestlist.get(position).getRecentusersname());
        viewHolder.tv_timingrec.setText(requestlist.get(position).getRecentusertime());
//            viewHolder.tv_usersnamerec.setText(requestlist.get(position).getUsersname());
//            viewHolder.tv_connectuser.setText(requestlist.get(position).getConnect());
//            viewHolder.tv_ignoreuser.setText(requestlist.get(position).getIgnore());
        viewHolder.tv_removeuserrec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int newPosition = viewHolder.getAdapterPosition();
                requestlist.remove(newPosition);
                notifyItemRemoved(newPosition);
                notifyDataSetChanged();
                notifyItemRangeChanged(newPosition, requestlist.size());
            }
        });

        Glide
                .with(context)

                .load(requestlist.get(position).getRecentusersimage())
                .into(viewHolder.iv_usersimagerec);


    }

    @Override
    public int getItemCount() {
        return requestlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_usersimagerec;
        private TextView tv_usersnamerec, tv_removeuserrec, tv_ignoreuser, tv_timingrec;

        public ViewHolder(View view) {
            super(view);
            tv_timingrec = view.findViewById(R.id.tv_timingrec);
            tv_usersnamerec = view.findViewById(R.id.tv_usersnamerec);
            tv_removeuserrec = view.findViewById(R.id.tv_removeuserrec);
            iv_usersimagerec = view.findViewById(R.id.iv_usersimagerec);

        }

    }

}
