package com.soul.soulber.Usersconnection;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.R;
import com.soul.soulber.Usersconnection.OldConnection.OldConnectionAdapter;
import com.soul.soulber.Usersconnection.OldConnection.OldConnectionmodleclass;
import com.soul.soulber.Usersconnection.RecentConnection.ResentConnectionAdapter;
import com.soul.soulber.Usersconnection.RecentConnection.ResentConnectionmodleclass;
import com.soul.soulber.Usersconnection.Usersrequest.Usersrequestadapter;
import com.soul.soulber.Usersconnection.Usersrequest.Usersrequestmodleclass;
import com.soul.soulber.databinding.ActivityUsersConectionBinding;


import java.util.ArrayList;

public class UsersConection extends AppCompatActivity {

    ActivityUsersConectionBinding activityUsersConectionBinding;
    Usersrequestadapter usersrequestadapter;
    ArrayList<Usersrequestmodleclass> requestlist;
    ArrayList<ResentConnectionmodleclass> resentlist;
    ArrayList<OldConnectionmodleclass> oldlist;
    int userimage[]={R.drawable.profileimage};
    String username[]={"Johnathan Doe"};
    int recentuserimage[]={R.drawable.profileimage,R.drawable.profilepic2,R.drawable.profilepic3,R.drawable.profileimage,R.drawable.profilepic2,R.drawable.profilepic3};
    String[] recentusername ={"Johnathan Doe","Williams Brown","Emiley Johnson","Johnathan Doe","Williams Brown","Emiley Johnson"};
    String[] recentusertime ={"Connected 2 mins ago","Connected 5 min ago","Connected 1 hr ago","Connected 2 mins ago","Connected 5 min ago","Connected 1 hr ago"};
    int olduserimage[]={R.drawable.profileimage,R.drawable.profilepic2,R.drawable.profilepic3,R.drawable.profileimage,R.drawable.profilepic2,R.drawable.profilepic3};
    String[] oldusername ={"Johnathan Doe","Williams Brown","Emiley Johnson","Johnathan Doe","Williams Brown","Emiley Johnson"};
    String[] oldusertime ={"Connected 1 days ago","Connected 2 days ago","Connected 5 day ago","Connected 1 days ago","Connected 2 days ago","Connected 5 day ago"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityUsersConectionBinding= DataBindingUtil.setContentView(this,R.layout.activity_users_conection);
        requestlist= new ArrayList<>();
        resentlist=new ArrayList<>();
        oldlist = new ArrayList<>();
        LinearLayoutManager linearLayoutManager= new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        activityUsersConectionBinding.rvRequestlist.setNestedScrollingEnabled(false);
        activityUsersConectionBinding.rvRequestlist.setLayoutManager(linearLayoutManager);
        LinearLayoutManager linearLayoutManagerold= new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        activityUsersConectionBinding.rvOldconnectionlist.setNestedScrollingEnabled(false);
        activityUsersConectionBinding.rvOldconnectionlist.setLayoutManager(linearLayoutManagerold);
        LinearLayoutManager linearLayoutManagerrecent= new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        activityUsersConectionBinding.rvRecentconnectionlist.setNestedScrollingEnabled(false);
        activityUsersConectionBinding.rvRecentconnectionlist.setLayoutManager(linearLayoutManagerrecent);
        for(int i=0; i<username.length;i++)
        {

            Usersrequestmodleclass usersrequestmodleclass = new Usersrequestmodleclass();
            usersrequestmodleclass.setUsersname(username[i]);
            usersrequestmodleclass.setUsersimage(userimage[i]);
            requestlist.add(usersrequestmodleclass);
        }

        for(int i=0; i<recentuserimage.length;i++)
        {
            ResentConnectionmodleclass resentConnectionmodleclass = new ResentConnectionmodleclass();
            resentConnectionmodleclass.setRecentusersname(recentusername[i]);
            resentConnectionmodleclass.setRecentusersimage(recentuserimage[i]);
            resentConnectionmodleclass.setRecentusertime(recentusertime[i]);
            resentlist.add(resentConnectionmodleclass);
        }
        for(int i=0; i<olduserimage.length;i++)
        {

            OldConnectionmodleclass oldConnectionmodleclass = new OldConnectionmodleclass();
            oldConnectionmodleclass.setOlduserimage(olduserimage[i]);
            oldConnectionmodleclass.setOldusersnmae(oldusername[i]);
            oldConnectionmodleclass.setOlduserstime(oldusertime[i]);
            oldlist.add(oldConnectionmodleclass);
        }
        ResentConnectionAdapter resentConnectionAdapter = new ResentConnectionAdapter(getApplicationContext(),resentlist);
        activityUsersConectionBinding.rvRecentconnectionlist.setAdapter(resentConnectionAdapter);
        resentConnectionAdapter.notifyDataSetChanged();

        OldConnectionAdapter oldConnectionAdapter = new OldConnectionAdapter(getApplicationContext(),oldlist);
        activityUsersConectionBinding.rvOldconnectionlist.setAdapter(oldConnectionAdapter);
        oldConnectionAdapter.notifyDataSetChanged();

        Usersrequestadapter usersrequestadapter = new Usersrequestadapter(getApplicationContext(),requestlist);
        activityUsersConectionBinding.rvRequestlist.setAdapter(usersrequestadapter);
        usersrequestadapter.notifyDataSetChanged();
        activityUsersConectionBinding.onbackUsersConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    @Override
    public void onBackPressed() {
       Intent intent = new Intent(UsersConection.this, BaseAcivity.class);
       startActivity(intent);
       finish();
    }
}