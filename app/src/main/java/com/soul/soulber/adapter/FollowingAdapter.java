package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.activity.FollowingActivity;
import com.soul.soulber.activity.GuardianProfileActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityFollowingListBinding;
import com.soul.soulber.model.FollowingModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class FollowingAdapter extends RecyclerView.Adapter<FollowingAdapter.viewHolder> {

    private Context context;
    private List<FollowingModel> followersModelList;

    private Session session;
    private String usertype_id;

    public interface onClick {
        void onFollow(FollowingModel model, TextView txtAction);

        void onUnFollow(FollowingModel model, TextView txtAction);
    }

    public FollowingAdapter(Context context, List<FollowingModel> followersModelList) {
        this.context = context;
        this.followersModelList = followersModelList;
        session = new Session(context);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityFollowingListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_following_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        FollowingModel model = followersModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        String userName = "";
        if (model.getUser_type_id().equals(Config.DEPENDENT)) {
            userName = model.getUsername();
        } else {
            userName = model.getName();
        }

        holder.binding.txtUserName.setText(checkString(userName, holder.binding.txtUserName));

        holder.binding.txtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (holder.binding.txtAction.getText().toString().equals(Config.Unfollow)) {
                    ((FollowingActivity) context).onUnFollow(model, holder.binding.txtAction);
                } else if (holder.binding.txtAction.getText().toString().equals(Config.Follow)) {
                    ((FollowingActivity) context).onFollow(model, holder.binding.txtAction);
                }
            }
        });


        holder.binding.txtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToProfileView(model);
            }
        });

    }

    private void jumpToProfileView(FollowingModel model) {
        if (model.getUser_type_id().equals(Config.GUARDIAN)) {
            jumpToGuardianProfile(model.getUser_id());
        } else if (model.getUser_type_id().equals(Config.DEPENDENT)) {
            jumpToDependentProfile(model.getUser_id());
        }
    }

    private void jumpToGuardianProfile(String id) {
        if (usertype_id.equals(Config.GUARDIAN)) {
            Intent intent = new Intent(context, GuardianProfileActivity.class);
            intent.putExtra(P.id, id);
            context.startActivity(intent);
        }
    }

    private void jumpToDependentProfile(String id) {
        Intent intent = new Intent(context, DependentProfileActivity.class);
        intent.putExtra(P.id, id);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return followersModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityFollowingListBinding binding;

        public viewHolder(@NonNull ActivityFollowingListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
