package com.soul.soulber.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ChilditempanichistoryBinding;
import com.soul.soulber.databinding.ItemHistorypanicBinding;
import com.soul.soulber.model.ChildExpandableModel;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.CLIPBOARD_SERVICE;

public class PanicHistoryChildAdapter extends RecyclerView.Adapter<PanicHistoryChildAdapter.viewHolder> {
    private Context context;
    //    private ArrayList<DependentModel> dependentModelList;
    private ArrayList<ChildExpandableModel> panicchildlist;

    public PanicHistoryChildAdapter(Context context, ArrayList<ChildExpandableModel> panicchildlist) {
        this.context = context;
        this.panicchildlist = panicchildlist;
    }

    @NonNull
    @Override
    public PanicHistoryChildAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ChilditempanichistoryBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.childitempanichistory, parent, false);
        return new PanicHistoryChildAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PanicHistoryChildAdapter.viewHolder holder, int position) {
        ChildExpandableModel model = panicchildlist.get(position);
        LoadImage.glideString(context, holder.binding.ivUserimagechild, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.listTitlechild.setText(checkString(model.getName(), holder.binding.listTitlechild));
        holder.binding.timechild.setText(checkString(getFormatDate(model.getAdd_date())));
        String panicremark = model.getRemark();
        if (model.getRemark().equals("") || model.getRemark() == null) {
            holder.binding.ivPanicremark.setVisibility(View.GONE);
        } else {
            holder.binding.ivPanicremark.setVisibility(View.VISIBLE);
        }
        holder.binding.ivPanicremark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                String details = "Panic Name : " + model.getName() +
                        "\nPanic Date : " + checkString(getFormatDate(model.getAdd_date())) +
                        "\nRemark : " + model.getRemark();

                copyText(details);
            }
        });

    }

    @Override
    public int getItemCount() {
        return panicchildlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ChilditempanichistoryBinding binding;

        public viewHolder(@NonNull ChilditempanichistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }


    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    public void copyText(String text) {
        H.showMessage(context, "Details Copied");
        Object clipboardService = context.getSystemService(CLIPBOARD_SERVICE);
        final ClipboardManager clipboardManager = (ClipboardManager) clipboardService;
        ClipData clipData = ClipData.newPlainText("Details Copied", text);
        clipboardManager.setPrimaryClip(clipData);
    }

}
