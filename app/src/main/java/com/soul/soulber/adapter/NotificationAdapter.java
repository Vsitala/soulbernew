package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Session;
import com.soul.soulber.JobsHouseFriends.Housings.HousingDetailsActivity;
import com.soul.soulber.JobsHouseFriends.Maindashjobhouse;
import com.soul.soulber.Meeting.MeetingDetailsActivity;
import com.soul.soulber.Meeting.MeetingLocationActivity;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.FollowersActivity;
import com.soul.soulber.activity.ForumActivity;
import com.soul.soulber.activity.ForumDetailsActivity;
import com.soul.soulber.activity.GuardianConnectionActivity;
import com.soul.soulber.activity.JobDetailsActivity;
import com.soul.soulber.activity.LabAndTherapistActivity;
import com.soul.soulber.activity.MyJobAppliedListActivity;
import com.soul.soulber.activity.PanicHistoryActivity;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityNotificationListBinding;
import com.soul.soulber.model.NotificationModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private Context context;
    private List<NotificationModel> notificationModelList;

    private Session session;
    private String usertype_id;

    public NotificationAdapter(Context context, List<NotificationModel> notificationModelList) {
        this.context = context;
        this.notificationModelList = notificationModelList;
        session = new Session(context);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityNotificationListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_notification_list, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        NotificationModel model = notificationModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgNotify, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_notifications_none_24));

        holder.binding.txtTitle.setText(checkString(model.getTitle(), holder.binding.txtTitle));
        holder.binding.txtDescription.setText(checkString(model.getDescription(), holder.binding.txtDescription));
        holder.binding.txtDate.setText(checkString(getFormatDate(model.getAdd_date()), holder.binding.txtDate));

        holder.binding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_CONNECTION_REQUEST)) {
                    //connection activity
                    loadConnectionView();
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_REQUEST_APPROVED)) {
                    //connection activity
                    loadConnectionView();
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_REQUEST_ACCEPTED)) {
                    //connection activity
                    loadConnectionView();
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_NEW_FOLLOWER)) {
                    // followers activity
                    loadFollowersView();
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_NEW_POST)) {
                    // user post activity
                    Config.IS_NOTIFICATION_POST = true;
                    Config.NOTIFICATION_POST_ID = model.getAction_data();
                    loadPostView();
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_PANIC)) {
                    // user panic data
                    if (usertype_id.equals(Config.GUARDIAN)) {
                        loadPanicHistory();
                    }
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_NEW_FORUM)) {
                    // forum post activity
                    loadForumView(model.getAction_data());
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_JOB_APPLICATION)) {
                    // applied job activity
                    loadAppliedJob(model.getAction_data());
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_MEETING_ACTION)) {
                    // meeting appointment
                    loadMeeting(model.getAction_data());
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_HOUSE_BOOKING_ACTION)) {
                    // housing booking
                    loadHousingBooking(model.getAction_data());
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_THERAPIST_APPOINTMENT)) {
                    // therapist appointment activity
                    loadTherapist(model.getAction_data());
                } else if (model.getAction().equalsIgnoreCase(Config.NOTIFICATION_LAB_APPOINTMENT)) {
                    // lab appointment activity
                    loadLab(model.getAction_data());
                }
            }
        });
    }

    private void loadConnectionView() {
        if (usertype_id.equals(Config.DEPENDENT)) {
            Intent intentDependent = new Intent(context, DependentConnectionActivity.class);
            context.startActivity(intentDependent);
        } else if (usertype_id.equals(Config.GUARDIAN)) {
            Intent intentGuardian = new Intent(context, GuardianConnectionActivity.class);
            context.startActivity(intentGuardian);
        }
    }

    private void loadFollowersView() {
        Intent intent = new Intent(context, FollowersActivity.class);
        context.startActivity(intent);
    }

    private void loadPostView() {
        Intent intent = new Intent(context, UserPostActivity.class);
        context.startActivity(intent);
    }

    private void loadForumView(String id) {
        Intent intent = new Intent(context, ForumDetailsActivity.class);
        intent.putExtra(P.id, id);
        context.startActivity(intent);
    }

    private void loadPanicHistory() {
        Intent intent = new Intent(context, PanicHistoryActivity.class);
        context.startActivity(intent);
    }

    private void loadAppliedJob(String id) {
        Intent intent = new Intent(context, JobDetailsActivity.class);
        intent.putExtra(Config.JOB_ID, id);
        context.startActivity(intent);
    }

    private void loadHousingBooking(String id) {
        Config.HOUSING_ID = id;
        Intent intent = new Intent(context, HousingDetailsActivity.class);
        context.startActivity(intent);
    }

    private void loadMeeting(String id) {
        Config.MEETING_ID = id;
        Intent intent = new Intent(context, MeetingDetailsActivity.class);
        context.startActivity(intent);
    }

    private void loadTherapist(String id) {
        Config.FOR_THERAPIST = true;
        Intent intent = new Intent(context, LabAndTherapistActivity.class);
        context.startActivity(intent);
    }

    private void loadLab(String id) {
        Config.FOR_THERAPIST = false;
        Intent intent = new Intent(context, LabAndTherapistActivity.class);
        context.startActivity(intent);
    }


    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ActivityNotificationListBinding binding;

        public ViewHolder(@NonNull ActivityNotificationListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM, yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

}
