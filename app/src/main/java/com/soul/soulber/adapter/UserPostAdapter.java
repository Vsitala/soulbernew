package com.soul.soulber.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityUserPostListBinding;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.model.UserPostModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class UserPostAdapter extends RecyclerView.Adapter<UserPostAdapter.viewHolder> implements Player.EventListener {

    private Context context;
    private List<UserPostModel> userPostModelList;
    private String profile_pic;
    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private int lastCommentPosition = -1;

    public interface onClick {
        void onShare(UserPostModel model);
    }

    public UserPostAdapter(Context context, List<UserPostModel> userPostModelList, String profile_pic) {
        this.context = context;
        this.userPostModelList = userPostModelList;
        this.profile_pic = profile_pic;
        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityUserPostListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_user_post_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        UserPostModel model = userPostModelList.get(position);

        if (model.getUpload_type().equals("1")) {
            holder.binding.imgPost.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.GONE);
            holder.binding.lnrPostMessage.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);
            holder.binding.videoView.setVisibility(View.GONE);

            holder.binding.txtTextMessage.setText(checkString(model.getMessage()));

            if (!TextUtils.isEmpty(checkString(model.getFont_color()))) {
                try {
                    checkColor(Integer.parseInt(model.getFont_color()), holder.binding.lnrTextBg);
                } catch (Exception e) {
                    checkColor(1, holder.binding.lnrTextBg);
                }
            } else {
                checkColor(1, holder.binding.lnrTextBg);
            }

            if (!TextUtils.isEmpty(checkString(model.getFont_style()))) {
                try {
                    checkFont(Integer.parseInt(model.getFont_style()), holder.binding.txtTextMessage);
                } catch (Exception e) {
                    checkFont(1, holder.binding.txtTextMessage);
                }
            } else {
                checkFont(1, holder.binding.txtTextMessage);
            }

        } else if (model.getUpload_type().equals("2")) {
            holder.binding.lnrTextBg.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);
            holder.binding.videoView.setVisibility(View.GONE);

            LoadImage.glideString(context, holder.binding.imgPost, model.getPost_img(), context.getResources().getDrawable(R.drawable.ic_no_image));
            holder.binding.txtPostedMessage.setText(checkString(model.getUsername() + ", " + model.getMessage(), holder.binding.lnrPostMessage));

        } else if (model.getUpload_type().equals("3")) {
            holder.binding.imgPost.setVisibility(View.GONE);
            holder.binding.lnrTextBg.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.VISIBLE);
            holder.binding.videoView.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);

            playVideo(model, holder.binding.playerView, holder.binding.pbVideoPlayer, holder);
//            playVideoView(model.getPost_video(),holder.binding.videoView);
//            playWebVideo(holder.binding.weView,model.getPost_video());

            holder.binding.txtPostedMessage.setText(checkString(model.getUsername() + ", " + checkString(model.getSubtitle()), holder.binding.lnrPostMessage));

        } else {
            holder.binding.imgPost.setVisibility(View.GONE);
            holder.binding.lnrTextBg.setVisibility(View.GONE);
            holder.binding.lnrPostMessage.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);
            holder.binding.videoView.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.VISIBLE);
        }

        model.setFromPost(false);
        model.setVisibleComment(false);
        model.setPageCount(1);

        List<UserCommentModel> commentModelList = new ArrayList<>();
        UserCommentAdapter adapter = new UserCommentAdapter(context, commentModelList);
        holder.binding.recyclerComment.setLayoutManager(new LinearLayoutManager(context));
        holder.binding.recyclerComment.setHasFixedSize(true);
        holder.binding.recyclerComment.setAdapter(adapter);

        LoadImage.glideString(context, holder.binding.imgUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        LoadImage.glideString(context, holder.binding.imgCommentPerson, profile_pic, context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtUserName.setText(checkString(model.getUsername()));
        holder.binding.txtLikeCount.setText(checkString(model.getLikes() + " likes"));
        holder.binding.txtCommentCount.setText(checkString(model.getComments() + " comments"));

        if (model.getIs_liked_by_me().equals("1")) {
            holder.binding.imgLike.setVisibility(View.VISIBLE);
            holder.binding.imgDisLike.setVisibility(View.GONE);
        } else {
            holder.binding.imgLike.setVisibility(View.GONE);
            holder.binding.imgDisLike.setVisibility(View.VISIBLE);
        }

//        if (userID.equals(model.getUser_id())) {
//            holder.binding.imgMore.setVisibility(View.VISIBLE);
//        } else {
//            holder.binding.imgMore.setVisibility(View.GONE);
//        }

        holder.binding.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewPopupMenu(model, userPostModelList, position, holder.binding.imgMore);
            }
        });

        holder.binding.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitPostDisLike(model, holder.binding.imgLike, holder.binding.imgDisLike);
            }
        });

        holder.binding.imgDisLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitPostLike(model, holder.binding.imgLike, holder.binding.imgDisLike);
            }
        });

        holder.binding.imgComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (holder.binding.lnrComment.getVisibility() == View.VISIBLE) {
                    holder.binding.lnrComment.setVisibility(View.GONE);
                } else if (holder.binding.lnrComment.getVisibility() == View.GONE) {
                    holder.binding.lnrComment.setVisibility(View.VISIBLE);
                    model.setFromPost(false);
                    if (!model.isVisibleComment()) {
                        model.setVisibleComment(true);
                        hitCommentData(model, commentModelList, adapter, holder.binding.txtViewMoreComment);
                    }
                }
            }
        });

        holder.binding.txtViewMoreComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (commentModelList.size() < model.getNum_rows()) {
                    model.setFromPost(false);
                    int count = model.getPageCount();
                    model.setPageCount(count + 1);
                    hitCommentData(model, commentModelList, adapter, holder.binding.txtViewMoreComment);
                }
            }
        });

        holder.binding.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((UserPostActivity) context).onShare(model);
            }
        });

        holder.binding.txtPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(holder.binding.etxComment.getText().toString().trim())) {
                    H.showMessage(context, "Please enter comment");
                } else {
                    model.setPageCount(1);
                    hitPostComment(model, holder.binding.etxComment, commentModelList, adapter, holder.binding.txtViewMoreComment);
                }
            }
        });

        holder.binding.imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewImageDialog(model.getPost_img());
            }
        });

        holder.binding.lnrTextBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewTextDialog(model.getMessage(), model.getFont_color(), model.getFont_style());
            }
        });

        holder.binding.txtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (!Config.FOR_MY_POST) {
                    Intent intent = new Intent(context, DependentProfileActivity.class);
                    intent.putExtra(P.id, model.getUser_id());
                    context.startActivity(intent);
                }
            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userPostModelList.size();
    }

    @Override
    public void onViewDetachedFromWindow(viewHolder holder) {
        super.onViewDetachedFromWindow(holder);

//        holder.binding.weView.onPause();
//        holder.binding.weView.pauseTimers();


    }

    @Override
    public void onViewAttachedToWindow(viewHolder holder) {
        super.onViewAttachedToWindow(holder);

//        holder.binding.weView.resumeTimers();
//        holder.binding.weView.onResume();

    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityUserPostListBinding binding;

        public viewHolder(@NonNull ActivityUserPostListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkCount(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private void hitCommentData(UserPostModel modelData, List<UserCommentModel> commentModelList, UserCommentAdapter adapter, TextView txtMoreComment) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, modelData.getPost_id());
        j.addString(P.page, modelData.getPageCount() + "");

        Api.newApi(context, API.BaseUrl + "Posts/show_comments").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String profile_pic_path = data.getString(P.profile_pic_path);

                        int num_rows = 0;
                        try {
                            num_rows = data.getInt(P.num_rows);
                        } catch (Exception e) {
                            num_rows = 0;
                        }

                        modelData.setNum_rows(num_rows);

                        JsonList comments_list = data.getJsonList(P.comments_list);
                        if (comments_list != null && comments_list.size() != 0) {

                            if (modelData.isFromPost()) {
                                modelData.setFromPost(false);
                                commentModelList.clear();
                            }

                            for (Json jsonValue : comments_list) {
                                UserCommentModel model = new UserCommentModel();
                                model.setComment_id(jsonValue.getString(P.comment_id));
                                model.setComment(jsonValue.getString(P.comment));
                                model.setComment_by(jsonValue.getString(P.comment_by));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setProfile_pic(profile_pic_path + "/" + jsonValue.getString(P.profile_pic));
                                model.setOn_date(jsonValue.getString(P.on_date));
                                model.setLikes(jsonValue.getString(P.likes));
                                model.setReplies(jsonValue.getString(P.replies));
                                model.setIs_liked_by_me(jsonValue.getString(P.is_liked_by_me));
                                model.setComment_time(jsonValue.getString(P.comment_time));
                                commentModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                        }

                        if (commentModelList.size() < num_rows) {
                            txtMoreComment.setVisibility(View.VISIBLE);
                        } else {
                            txtMoreComment.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitCommentData", token);
    }


    private void hitPostComment(UserPostModel modelData, EditText etxComment, List<UserCommentModel> commentModelList, UserCommentAdapter adapter, TextView txtMoreComment) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, modelData.getPost_id());
        j.addString(P.comment, etxComment.getText().toString().trim());

        Api.newApi(context, API.BaseUrl + "Posts/add_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        etxComment.setText("");
                        modelData.setFromPost(true);
                        hitCommentData(modelData, commentModelList, adapter, txtMoreComment);
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitPostComment", token);
    }


    private void hitPostLike(UserPostModel modelData, ImageView imageLike, ImageView imgDisLike) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, modelData.getPost_id());

        Api.newApi(context, API.BaseUrl + "Posts/like_post").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        imageLike.setVisibility(View.VISIBLE);
                        imgDisLike.setVisibility(View.GONE);
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitPostLike", token);
    }

    private void hitPostDisLike(UserPostModel modelData, ImageView imageLike, ImageView imgDisLike) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, modelData.getPost_id());

        Api.newApi(context, API.BaseUrl + "Posts/unlike_post").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        imgDisLike.setVisibility(View.VISIBLE);
                        imageLike.setVisibility(View.GONE);
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitPostDisLike", token);
    }

    private void hitPostRemove(UserPostModel modelData, List<UserPostModel> userPostModelList, int position) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, modelData.getPost_id());

        Api.newApi(context, API.BaseUrl + "Posts/remove_post").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        userPostModelList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, userPostModelList.size());
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitPostRemove", token);
    }


    private void viewPopupMenu(UserPostModel model, List<UserPostModel> userPostModelList, int position, ImageView imgMore) {
        PopupMenu popup = new PopupMenu(context, imgMore);
        popup.inflate(R.menu.more_options_menu);

        Menu popupMenu = popup.getMenu();
        popupMenu.findItem(R.id.action_remove).setVisible(false);

//        if (userID.equals(model.getUser_id())) {
//            popupMenu.findItem(R.id.action_hide).setVisible(true);
//            popupMenu.findItem(R.id.action_report).setVisible(false);
//        } else {
//            popupMenu.findItem(R.id.action_hide).setVisible(false);
//            popupMenu.findItem(R.id.action_report).setVisible(true);
//        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_remove:
                        hitPostRemove(model, userPostModelList, position);
                        return true;
                    case R.id.action_hide:
                        showHideDialog(model, userPostModelList, position);
                        return true;
                    case R.id.action_report:
                        reportDialog(model);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void viewImageDialog(String imagePath) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_image_view);

        PhotoView imageView = dialog.findViewById(R.id.imageView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        LoadImage.glideString(context, imageView, imagePath, context.getResources().getDrawable(R.drawable.ic_no_image));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void viewTextDialog(String textMessage, String colorCode, String fontCode) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_text_view);

        RelativeLayout lnrBg = dialog.findViewById(R.id.lnrBg);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        TextView txtScroll = dialog.findViewById(R.id.txtScroll);

        txtScroll.setText(textMessage);

        if (!TextUtils.isEmpty(checkString(colorCode))) {
            checkColor(Integer.parseInt(colorCode), lnrBg);
        } else {
            checkColor(1, lnrBg);
        }

        if (!TextUtils.isEmpty(checkString(fontCode))) {
            checkFont(Integer.parseInt(fontCode), txtScroll);
        } else {
            checkFont(1, txtScroll);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void checkColor(int value, LinearLayout lnrBackground) {
        try {
            if (value == 1) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg1));
            } else if (value == 2) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg2));
            } else if (value == 3) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg3));
            } else if (value == 4) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg4));
            } else if (value == 5) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg5));
            }
        } catch (Exception e) {
        }
    }

    private void checkColor(int value, RelativeLayout lnrBackground) {
        try {
            if (value == 1) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg1));
            } else if (value == 2) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg2));
            } else if (value == 3) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg3));
            } else if (value == 4) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg4));
            } else if (value == 5) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg5));
            }
        } catch (Exception e) {
        }
    }

    private void checkFont(int value, TextView etxPost) {
        try {
            if (value == 1) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_1);
                etxPost.setTypeface(face);
            } else if (value == 2) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_2);
                etxPost.setTypeface(face);
            } else if (value == 3) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_3);
                etxPost.setTypeface(face);
            } else if (value == 4) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_4);
                etxPost.setTypeface(face);
            } else if (value == 5) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_5);
                etxPost.setTypeface(face);
            }
        } catch (Exception e) {
        }
    }

    private void playVideo(UserPostModel model, PlayerView playerView, ProgressBar pbVideoPlayer, viewHolder holder) {

        SimpleExoPlayer exoPlayer = new SimpleExoPlayer.Builder(context).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context.getApplicationContext(), Util.getUserAgent(context.getApplicationContext(), context.getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(model.getPost_video()));

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(model.getPost_video()));

        if (model.isPaying()) {
            play(exoPlayer);
        } else {
            pause(exoPlayer);
        }

        exoPlayer.addListener(new ExoPlayer.Listener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == Player.STATE_BUFFERING) {
                    pbVideoPlayer.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                } else if (playbackState == Player.STATE_ENDED) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                if (isPlaying) {
                    if (!model.isPaying()) {
                        for (int i = 0; i < userPostModelList.size(); i++) {
                            if (i == holder.getAdapterPosition()) {
                                userPostModelList.get(i).setPaying(true);
                            } else {
                                userPostModelList.get(i).setPaying(false);
                            }
                        }
                        notifyDataSetChanged();
                    }
                } else {

                }
            }

        });

        playerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
//                pause(exoPlayer);
                playerView.getPlayer().stop();
            }
        });


    }

    private void pause(SimpleExoPlayer playerView) {
        playerView.pause();
    }

    private void play(SimpleExoPlayer playerView) {
        playerView.play();
    }

    private void playWebVideo(WebView webView, String videoPath) {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setUserAgentString("Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        webSettings.setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        webView.loadUrl(videoPath);

//        toggleWebViewState(webView,true);
    }

    private void toggleWebViewState(WebView webView, boolean pause) {
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod(pause
                            ? "onPause"
                            : "onResume", (Class[]) null)
                    .invoke(webView, (Object[]) null);
        } catch (Exception e) {
        }
    }


    private void playVideoView(String videoPath, VideoView videoView) {
        Uri uri = Uri.parse(videoPath);
        videoView.setVideoURI(uri);
        MediaController mediaController = new MediaController(context);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
    }

    private void showHideDialog(UserPostModel model, List<UserPostModel> userPostModelList, int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder1.setTitle("Hide Post Alert..!");
        builder1.setMessage("Do you want to hide this post ?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Hide",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        hitPostHide(model, userPostModelList, position);
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void hitPostHide(UserPostModel modelData, List<UserPostModel> userPostModelList, int position) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, modelData.getPost_id());

        Api.newApi(context, API.BaseUrl + "Posts/hide_show_posts").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        userPostModelList.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position, userPostModelList.size());
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitPostHide", token);
    }


    private void reportDialog(UserPostModel model) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_report_user);

        EditText etxDescription = dialog.findViewById(R.id.etxDescription);
        Button btnReport = dialog.findViewById(R.id.btnReport);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(etxDescription.getText().toString().trim())) {
                    H.showMessage(context, "Please enter comment to report");
                } else if (etxDescription.getText().toString().trim().length() < 4) {
                    H.showMessage(context, "Please enter valid comment");
                } else {
                    dialog.dismiss();
                    hitReportSpam(model, token, etxDescription.getText().toString().trim());
                }
            }
        });
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

    private void hitReportSpam(UserPostModel model, String token, String reason) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.post_id, model.getPost_id());
        j.addString(P.comment, reason);
        Api.newApi(context, API.BaseUrl + "Posts/report_as_spam").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitReportSpam", token);
    }


}
