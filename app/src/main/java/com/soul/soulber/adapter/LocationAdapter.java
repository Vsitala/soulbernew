package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.R;
import com.soul.soulber.activity.SearchLocationActivity;
import com.soul.soulber.databinding.ActivityLocationListBinding;
import com.soul.soulber.model.LocationModel;

import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private Context context;
    private List<LocationModel> locationModelList;
    public interface ClickInterface{
        void getLocation(LocationModel model);
    }

    public LocationAdapter(Context context, List<LocationModel> locationModelList) {
        this.context = context;
        this.locationModelList = locationModelList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityLocationListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_location_list, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        LocationModel model = locationModelList.get(position);
//        holder.binding.txtTitle.setText(model.getTitle());
//        holder.binding.txtAddress.setText(model.getAddress());
        holder.binding.txtTitle.setText(model.getAddress());
        holder.binding.txtAddress.setVisibility(View.GONE);
        holder.binding.lnrSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchLocationActivity)context).getLocation(model);
            }
        });
    }

    @Override
    public int getItemCount() {
        return locationModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ActivityLocationListBinding binding;
        public ViewHolder(@NonNull ActivityLocationListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
