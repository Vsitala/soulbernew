package com.soul.soulber.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityHistoryItemBinding;
import com.soul.soulber.databinding.ActivityTheraphyItemsBinding;
import com.soul.soulber.model.ChildExpandableModel;
import com.soul.soulber.model.GuardianTheraphyModel;
import com.soul.soulber.model.PanicHistoryModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DependentPanicHistoryAdapter extends RecyclerView.Adapter<DependentPanicHistoryAdapter.viewHolder> {
    private Context context;
    private ArrayList<PanicHistoryModel> panicchildlist;

    public DependentPanicHistoryAdapter(Context context, ArrayList<PanicHistoryModel> panicchildlist) {
        this.context = context;
        this.panicchildlist = panicchildlist;
    }

    @NonNull
    @Override
    public DependentPanicHistoryAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityHistoryItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_history_item, parent, false);
        return new DependentPanicHistoryAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DependentPanicHistoryAdapter.viewHolder holder, int position) {
        PanicHistoryModel model = panicchildlist.get(position);
        LoadImage.glideString(context, holder.binding.ivUserimagechild, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.tvDate.setText("Date: "+checkString(getFormatDate(model.getPanic_date()), holder.binding.tvDate));
        holder.binding.tvTime.setText("Time: "+checkString(getFormatTime(model.getPanic_date()),holder.binding.tvTime));
        holder.binding.tvDependentname.setText(checkString(model.getDependent_username(),holder.binding.tvDependentname));
    }

    @Override
    public int getItemCount() {
        return panicchildlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityHistoryItemBinding binding;

        public viewHolder(@NonNull ActivityHistoryItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }
    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }
    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }


}
