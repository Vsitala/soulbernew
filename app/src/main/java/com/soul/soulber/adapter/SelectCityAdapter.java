package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.Login_Registration.Registerpages.Register_details;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityCitySelectionListBinding;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.util.Click;

import java.util.List;

public class SelectCityAdapter extends RecyclerView.Adapter<SelectCityAdapter.viewHolder> {

    private Context context;
    private List<CountryModel> countryModelList;

    public interface onClick{
        void citySelection(CountryModel model);
    }

    public SelectCityAdapter(Context context, List<CountryModel> countryModelList) {
        this.context = context;
        this.countryModelList = countryModelList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityCitySelectionListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_city_selection_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        CountryModel model = countryModelList.get(position);
        holder.binding.txtCity.setText(model.getCity());

        holder.binding.txtCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((Register_details)context).citySelection(model);
            }
        });

    }

    @Override
    public int getItemCount() {
        return countryModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityCitySelectionListBinding binding;

        public viewHolder(@NonNull ActivityCitySelectionListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

}
