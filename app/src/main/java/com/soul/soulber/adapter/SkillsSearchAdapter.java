package com.soul.soulber.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.soul.soulber.JobsHouseFriends.JobsFragment;
import com.soul.soulber.R;
import com.soul.soulber.model.CountryModel;

import java.util.ArrayList;
import java.util.List;

public class SkillsSearchAdapter extends ArrayAdapter<CountryModel> {


    private Context context;
    private List<CountryModel> list;
    private List<CountryModel> listAll;
    private int mLayoutResourceId;
    private JobsFragment jobsFragment;

    public SkillsSearchAdapter(Context context, int resource, List<CountryModel> list, JobsFragment jobsFragment) {
        super(context, resource, list);
        this.context = context;
        this.mLayoutResourceId = resource;
        this.list = new ArrayList<>(list);
        this.listAll = new ArrayList<>(list);
        this.jobsFragment = jobsFragment;
    }

    public int getCount() {
        return list.size();
    }

    public CountryModel getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((JobsFragment) jobsFragment).getLayoutInflater();
                convertView = inflater.inflate(mLayoutResourceId, parent, false);
            }
            CountryModel model = getItem(position);
            TextView name = (TextView) convertView.findViewById(R.id.txtName);
            name.setText(model.getCity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                return ((CountryModel) resultValue).getCity();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<CountryModel> departmentsSuggestion = new ArrayList<>();
                if (constraint != null) {
                    for (CountryModel department : listAll) {
                        if (department.getCity().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                            departmentsSuggestion.add(department);
                        }
                    }
                    filterResults.values = departmentsSuggestion;
                    filterResults.count = departmentsSuggestion.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                list.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mDepartments.addAll((ArrayList<Department>) results.values);
                    for (Object object : (List<?>) results.values) {
                        if (object instanceof CountryModel) {
                            list.add((CountryModel) object);
                        }
                    }
                    notifyDataSetChanged();
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    list.addAll(listAll);
                    notifyDataSetInvalidated();
                }
            }
        };
    }

}
