package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Session;
import com.soul.soulber.Dependent.DependentResultActivity;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentAddConnectionActivity;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivitySearchDependentListBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class SearchDependentAdapter extends RecyclerView.Adapter<SearchDependentAdapter.viewHolder> {

    private Context context;
    private List<DependentModel> dependentModelList;
    private String userID;
    private boolean fromSearch = false;


    public interface onClick {
        void onConnect(DependentModel model, Button button);

        void onRemove(DependentModel model, Button button);

        void onAccept(DependentModel model, Button button);
    }

    public SearchDependentAdapter(Context context, List<DependentModel> dependentModelList, String userID, boolean vaue) {
        this.context = context;
        this.dependentModelList = dependentModelList;
        this.userID = userID;
        fromSearch = vaue;
    }


    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivitySearchDependentListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_search_dependent_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        DependentModel model = dependentModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgProfile, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString(model.getUsername(), holder.binding.txtName));
        holder.binding.txtAddress.setText(checkString("", holder.binding.lnrAddress));
        holder.binding.txtAge.setText(checkString(model.getAge(), holder.binding.lnrAge));
        holder.binding.txtGender.setText(checkString("", holder.binding.lnrGender));
        holder.binding.txtCity.setText(checkString(model.getCity(), holder.binding.lnrCity));

        if (model.getIs_accepted().equals("0")) {
            holder.binding.btnConnect.setText("Requested");
        } else if (model.getIs_accepted().equals("1")) {
            holder.binding.btnConnect.setText("Remove");
        } else if (model.getIs_accepted().equals("3")) {
            holder.binding.btnConnect.setText("Accept");
        } else {
            holder.binding.btnConnect.setText("Connect");
        }

        holder.binding.btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);

                if (userID.equals(model.getDependent_id())){
                    H.showMessage(context,"You can't connect yourself");
                }else {

                    if (holder.binding.btnConnect.getText().equals("Connect")) {
                        if (fromSearch) {
                            ((DependentResultActivity) context).onConnect(model, holder.binding.btnConnect);
                        } else {
                            ((DependentAddConnectionActivity) context).onConnect(model, holder.binding.btnConnect);
                        }
                    } else if (holder.binding.btnConnect.getText().equals("Remove")) {
                        if (fromSearch) {
                            ((DependentResultActivity) context).onRemove(model, holder.binding.btnConnect);
                        } else {
                            ((DependentAddConnectionActivity) context).onRemove(model, holder.binding.btnConnect);
                        }
                    } else if (holder.binding.btnConnect.getText().equals("Accept") || holder.binding.btnConnect.getText().equals("Reject")) {
                        if (fromSearch) {
//                        ((DependentResultActivity) context).onAccept(model, holder.binding.btnConnect);
                        } else {
                            ((DependentAddConnectionActivity) context).onAccept(model, holder.binding.btnConnect);
                        }
                    } else if (holder.binding.btnConnect.getText().equals("Requested")) {
                        H.showMessage(context, "Connection request is already sent");
                    }

                }

            }
        });

        holder.binding.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (userID.equals(model.getDependent_id())){
                    H.showMessage(context,"You can't connect yourself");
                }else {
                    Intent intent = new Intent(context, DependentProfileActivity.class);
                    intent.putExtra(P.id, model.getDependent_id());
                    context.startActivity(intent);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return dependentModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivitySearchDependentListBinding binding;

        public viewHolder(@NonNull ActivitySearchDependentListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
