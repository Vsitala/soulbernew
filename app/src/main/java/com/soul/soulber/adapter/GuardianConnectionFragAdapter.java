package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.GuardianProfileActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentConnectionListBinding;
import com.soul.soulber.fragment.GuardianConnectionFragment;
import com.soul.soulber.model.GuardianConnectionFragModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class GuardianConnectionFragAdapter extends RecyclerView.Adapter<GuardianConnectionFragAdapter.viewHolder> {

    private Context context;
    private List<GuardianConnectionFragModel> guardianConnectionModelList;
    private String userID;
    private GuardianConnectionFragment fragment;

    private Session session;
    private String usertype_id;

    public interface onClick {
        void onConnect(GuardianConnectionFragModel model, TextView txtConnect);

        void onIgnore(GuardianConnectionFragModel model, TextView txtIgnore);

        void onRemove(GuardianConnectionFragModel model, TextView button);
    }

    public GuardianConnectionFragAdapter(Context context, List<GuardianConnectionFragModel> guardianConnectionModelList, String userID, GuardianConnectionFragment fragment) {
        this.context = context;
        this.guardianConnectionModelList = guardianConnectionModelList;
        this.userID = userID;
        this.fragment = fragment;

        session = new Session(context);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityDependentConnectionListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_dependent_connection_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        GuardianConnectionFragModel model = guardianConnectionModelList.get(position);

        String user_name = "";
        if (model.getFrom_guardian_id().equals(userID)) {
            user_name = model.getTo_guardian_name();
        } else {
            user_name = model.getFrom_guardian_name();
        }

        LoadImage.glideString(context, holder.binding.imgNewUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        LoadImage.glideString(context, holder.binding.imgOldUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtOldUserName.setText(checkString(user_name, holder.binding.txtOldUserName));
        holder.binding.txtConnectionStatusOne.setText("Requested " + checkString(model.getConnection_time(), holder.binding.txtConnectionStatusOne));
        holder.binding.txtConnectionStatusTwo.setText("Connected " + checkString(model.getConnection_time(), holder.binding.txtConnectionStatusTwo));

        if (model.getIs_accepted().equals("0") && model.getFrom_guardian_id().equals(userID)) {

            holder.binding.txtNewUserName.setText(checkString(model.getTo_guardian_name(), holder.binding.txtNewUserName));
            holder.binding.txtConnection.setText("Requested");
            holder.binding.txtConnectionStatusOne.setVisibility(View.VISIBLE);
            //
            holder.binding.txtIgnore.setVisibility(View.GONE);

        } else {
            holder.binding.txtConnectionStatusOne.setVisibility(View.GONE);
        }

        if (model.getIs_accepted().equals("0") && model.getTo_guardian_id().equals(userID)) {
            holder.binding.txtNewUserName.setText(checkString(model.getFrom_guardian_name(), holder.binding.txtNewUserName));
            holder.binding.txtIgnore.setText("Accept");
            //
            holder.binding.txtConnection.setVisibility(View.GONE);
        } else {
            holder.binding.txtIgnore.setText("Ignore");
        }

//        holder.binding.txtConnection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Click.preventTwoClick(v);
//                if (holder.binding.txtConnection.getText().toString().equals("Connect")) {
//                    ((GuardianConnectionFragment) fragment).onConnect(model, holder.binding.txtConnection);
//                } else {
//                    H.showMessage(context, "Connection request is already sent");
//                }
//            }
//        });

        holder.binding.txtIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((GuardianConnectionFragment) fragment).onIgnore(model, holder.binding.txtIgnore);
            }
        });

        holder.binding.txtRemoveConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (holder.binding.txtRemoveConnection.getText().toString().equals("Remove connection")) {
                    ((GuardianConnectionFragment) fragment).onRemove(model, holder.binding.txtRemoveConnection);
                }
            }
        });

        holder.binding.txtNewUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToProfile(model);
            }
        });

        holder.binding.txtOldUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToProfile(model);
            }
        });

        if (model.getIs_accepted().equals("1")) {
            holder.binding.lnrNewConnection.setVisibility(View.GONE);
            holder.binding.lnrOldConnection.setVisibility(View.VISIBLE);
        } else if (model.getIs_accepted().equals("0")) {
            holder.binding.lnrNewConnection.setVisibility(View.VISIBLE);
            holder.binding.lnrOldConnection.setVisibility(View.GONE);
        } else {
            holder.binding.lnrNewConnection.setVisibility(View.GONE);
            holder.binding.lnrOldConnection.setVisibility(View.GONE);
        }
    }

    private void jumpToProfile(GuardianConnectionFragModel model) {
        if (model.getFrom_guardian_id().equals(userID)) {
            jumpToGuardianProfile(model.getTo_guardian_id());
        } else {
            jumpToGuardianProfile(model.getFrom_guardian_id());
        }
    }

    private void jumpToGuardianProfile(String id) {
        if (usertype_id.equals(Config.GUARDIAN)) {
            Intent intent = new Intent(context, GuardianProfileActivity.class);
            intent.putExtra(P.id, id);
            context.startActivity(intent);
        }
    }


    @Override
    public int getItemCount() {
        return guardianConnectionModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityDependentConnectionListBinding binding;

        public viewHolder(@NonNull ActivityDependentConnectionListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
