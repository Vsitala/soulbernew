package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.JobsHouseFriends.FriendsFragment;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentConnectionListBinding;
import com.soul.soulber.model.DependentConnectionModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class DependentConnectionAdapter extends RecyclerView.Adapter<DependentConnectionAdapter.viewHolder> {

    private Context context;
    private List<DependentConnectionModel> dependentConnectionModelList;
    private String userID;
    private int form = 0;

    private FriendsFragment friendsFragment;

    private boolean fromFragment = false;

    public interface onClick {
        void onConnect(DependentConnectionModel model, TextView txtConnect);

        void onIgnore(DependentConnectionModel model, TextView txtIgnore);

        void onRemove(DependentConnectionModel model, TextView button);
    }

    public DependentConnectionAdapter(Context context, List<DependentConnectionModel> dependentConnectionModelList, String userID) {
        this.context = context;
        this.dependentConnectionModelList = dependentConnectionModelList;
        this.userID = userID;
        fromFragment = false;
    }

    public DependentConnectionAdapter(Context context, List<DependentConnectionModel> dependentConnectionModelList, String userID,FriendsFragment friendsFragment) {
        this.context = context;
        this.dependentConnectionModelList = dependentConnectionModelList;
        this.userID = userID;
        this.friendsFragment = friendsFragment;
        fromFragment = true;
    }



    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityDependentConnectionListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_dependent_connection_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        DependentConnectionModel model = dependentConnectionModelList.get(position);

        String user_name = "";
        if (model.getFrom_dependent_id().equals(userID)) {
            user_name = model.getTo_dependent_username();
        }else {
            user_name = model.getFrom_username();
        }
        LoadImage.glideString(context, holder.binding.imgNewUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        LoadImage.glideString(context, holder.binding.imgOldUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtOldUserName.setText(checkString(user_name, holder.binding.txtOldUserName));
        holder.binding.txtConnectionStatusOne.setText("Requested " + checkString(model.getConnection_time(), holder.binding.txtConnectionStatusOne));
        holder.binding.txtConnectionStatusTwo.setText("Connected " + checkString(model.getConnection_time(), holder.binding.txtConnectionStatusTwo));

        if (model.getIs_accepted().equals("0") && model.getFrom_dependent_id().equals(userID)) {

            holder.binding.txtNewUserName.setText(checkString(model.getTo_dependent_username(), holder.binding.txtNewUserName));
            holder.binding.txtConnection.setText("Requested");
            holder.binding.txtConnectionStatusOne.setVisibility(View.VISIBLE);
            //
            holder.binding.txtIgnore.setVisibility(View.GONE);

        } else {
            holder.binding.txtConnectionStatusOne.setVisibility(View.GONE);
        }

        if (model.getIs_accepted().equals("0") && model.getTo_dependent_id().equals(userID)) {
            holder.binding.txtNewUserName.setText(checkString(model.getFrom_username(), holder.binding.txtNewUserName));
            holder.binding.txtIgnore.setText("Accept");
            //
            holder.binding.txtConnection.setVisibility(View.GONE);

        } else {
            holder.binding.txtIgnore.setText("Ignore");
        }

//        holder.binding.txtConnection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Click.preventTwoClick(v);
//                if (holder.binding.txtConnection.getText().toString().equals("Connect")) {
//                    ((DependentConnectionActivity) context).onConnect(model, holder.binding.txtConnection);
//                } else {
//                    H.showMessage(context, "Connection request is already sent");
//                }
//            }
//        });

        holder.binding.txtIgnore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (fromFragment){
                    ((FriendsFragment) friendsFragment).onIgnore(model, holder.binding.txtIgnore);
                }else {
                    ((DependentConnectionActivity) context).onIgnore(model, holder.binding.txtIgnore);
                }
            }
        });

        holder.binding.txtRemoveConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (fromFragment){
                    if (holder.binding.txtRemoveConnection.getText().toString().equals("Remove connection")) {
                        ((FriendsFragment) friendsFragment).onRemove(model, holder.binding.txtRemoveConnection);
                    }
                }else {
                    if (holder.binding.txtRemoveConnection.getText().toString().equals("Remove connection")) {
                        ((DependentConnectionActivity) context).onRemove(model, holder.binding.txtRemoveConnection);
                    }
                }

            }
        });

        holder.binding.txtNewUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToDependent(model);
            }
        });

        holder.binding.txtOldUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToDependent(model);
            }
        });

        if (model.getIs_accepted().equals("1")) {
            holder.binding.lnrNewConnection.setVisibility(View.GONE);
            holder.binding.lnrOldConnection.setVisibility(View.VISIBLE);
        } else if (model.getIs_accepted().equals("0")) {
            holder.binding.lnrNewConnection.setVisibility(View.VISIBLE);
            holder.binding.lnrOldConnection.setVisibility(View.GONE);
        } else {
            holder.binding.lnrNewConnection.setVisibility(View.GONE);
            holder.binding.lnrOldConnection.setVisibility(View.GONE);
        }
    }

    private void jumpToDependent(DependentConnectionModel model) {
        if (model.getFrom_dependent_id().equals(userID)) {
            Intent intent = new Intent(context, DependentProfileActivity.class);
            intent.putExtra(P.id, model.getTo_dependent_id());
            context.startActivity(intent);
        } else {
            Intent intent = new Intent(context, DependentProfileActivity.class);
            intent.putExtra(P.id, model.getFrom_dependent_id());
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        return dependentConnectionModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityDependentConnectionListBinding binding;

        public viewHolder(@NonNull ActivityDependentConnectionListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
