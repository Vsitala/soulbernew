package com.soul.soulber.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityUserVerticalPostListBinding;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.model.UserPostModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.List;

public class UserVerticalPostAdapter extends RecyclerView.Adapter<UserVerticalPostAdapter.viewHolder> implements Player.EventListener {

    private Context context;
    private List<UserPostModel> userPostModelList;
    private String profile_pic;
    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private int lastCommentPosition = -1;

    public interface onClick {
        void onShare(UserPostModel model);
    }

    public UserVerticalPostAdapter(Context context, List<UserPostModel> userPostModelList, String profile_pic) {
        this.context = context;
        this.userPostModelList = userPostModelList;
        this.profile_pic = profile_pic;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityUserVerticalPostListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_user_vertical_post_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        UserPostModel model = userPostModelList.get(position);

        if (model.getUpload_type().equals("1")) {
            holder.binding.imgPost.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);
            holder.binding.videoView.setVisibility(View.GONE);

            holder.binding.txtTextMessage.setText(checkString(model.getMessage()));

            if (!TextUtils.isEmpty(checkString(model.getFont_color()))) {
                try {
                    checkColor(Integer.parseInt(model.getFont_color()), holder.binding.lnrTextBg);
                } catch (Exception e) {
                    checkColor(1, holder.binding.lnrTextBg);
                }
            } else {
                checkColor(1, holder.binding.lnrTextBg);
            }

            if (!TextUtils.isEmpty(checkString(model.getFont_style()))) {
                try {
                    checkFont(Integer.parseInt(model.getFont_style()), holder.binding.txtTextMessage);
                } catch (Exception e) {
                    checkFont(1, holder.binding.txtTextMessage);
                }
            } else {
                checkFont(1, holder.binding.txtTextMessage);
            }

        } else if (model.getUpload_type().equals("2")) {
            holder.binding.lnrTextBg.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);
            holder.binding.videoView.setVisibility(View.GONE);

            LoadImage.glideString(context, holder.binding.imgPost, model.getPost_img(), context.getResources().getDrawable(R.drawable.ic_no_image));

        } else if (model.getUpload_type().equals("3")) {
            holder.binding.imgPost.setVisibility(View.GONE);
            holder.binding.lnrTextBg.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.VISIBLE);
            holder.binding.videoView.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);

            playVideo(model, holder.binding.playerView, holder.binding.pbVideoPlayer, holder);
//            playVideoView(model.getPost_video(),holder.binding.videoView);
//            playWebVideo(holder.binding.weView,model.getPost_video());

        } else {
            holder.binding.imgPost.setVisibility(View.GONE);
            holder.binding.lnrTextBg.setVisibility(View.GONE);
            holder.binding.cardPlayVideo.setVisibility(View.GONE);
            holder.binding.weView.setVisibility(View.GONE);
            holder.binding.videoView.setVisibility(View.GONE);
            holder.binding.lntNoPost.setVisibility(View.VISIBLE);
        }

        model.setFromPost(false);
        model.setVisibleComment(false);
        model.setPageCount(1);

        holder.binding.imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewImageDialog(model.getPost_img());
            }
        });

        holder.binding.lnrTextBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewTextDialog(model.getMessage(), model.getFont_color(), model.getFont_style());
            }
        });

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return userPostModelList.size();
    }

    @Override
    public void onViewDetachedFromWindow(viewHolder holder) {
        super.onViewDetachedFromWindow(holder);

//        holder.binding.weView.onPause();
//        holder.binding.weView.pauseTimers();


    }

    @Override
    public void onViewAttachedToWindow(viewHolder holder) {
        super.onViewAttachedToWindow(holder);

//        holder.binding.weView.resumeTimers();
//        holder.binding.weView.onResume();

    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityUserVerticalPostListBinding binding;

        public viewHolder(@NonNull ActivityUserVerticalPostListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkCount(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private void viewImageDialog(String imagePath) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_image_view);

        PhotoView imageView = dialog.findViewById(R.id.imageView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        LoadImage.glideString(context, imageView, imagePath, context.getResources().getDrawable(R.drawable.ic_no_image));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void viewTextDialog(String textMessage, String colorCode, String fontCode) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_text_view);

        RelativeLayout lnrBg = dialog.findViewById(R.id.lnrBg);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        TextView txtScroll = dialog.findViewById(R.id.txtScroll);

        txtScroll.setText(textMessage);

        if (!TextUtils.isEmpty(checkString(colorCode))) {
            checkColor(Integer.parseInt(colorCode), lnrBg);
        } else {
            checkColor(1, lnrBg);
        }

        if (!TextUtils.isEmpty(checkString(fontCode))) {
            checkFont(Integer.parseInt(fontCode), txtScroll);
        } else {
            checkFont(1, txtScroll);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void checkColor(int value, LinearLayout lnrBackground) {
        try {
            if (value == 1) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg1));
            } else if (value == 2) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg2));
            } else if (value == 3) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg3));
            } else if (value == 4) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg4));
            } else if (value == 5) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg5));
            }
        } catch (Exception e) {
        }
    }

    private void checkColor(int value, RelativeLayout lnrBackground) {
        try {
            if (value == 1) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg1));
            } else if (value == 2) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg2));
            } else if (value == 3) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg3));
            } else if (value == 4) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg4));
            } else if (value == 5) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg5));
            }
        } catch (Exception e) {
        }
    }

    private void checkFont(int value, TextView etxPost) {
        try {
            if (value == 1) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_1);
                etxPost.setTypeface(face);
            } else if (value == 2) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_2);
                etxPost.setTypeface(face);
            } else if (value == 3) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_3);
                etxPost.setTypeface(face);
            } else if (value == 4) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_4);
                etxPost.setTypeface(face);
            } else if (value == 5) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_5);
                etxPost.setTypeface(face);
            }
        } catch (Exception e) {
        }
    }

    private void playVideo(UserPostModel model, PlayerView playerView, ProgressBar pbVideoPlayer, viewHolder holder) {

        SimpleExoPlayer exoPlayer = new SimpleExoPlayer.Builder(context).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context.getApplicationContext(), Util.getUserAgent(context.getApplicationContext(), context.getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(model.getPost_video()));

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(model.getPost_video()));

        if (model.isPaying()) {
            play(exoPlayer);
        } else {
            pause(exoPlayer);
        }

        exoPlayer.addListener(new ExoPlayer.Listener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == Player.STATE_BUFFERING) {
                    pbVideoPlayer.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                } else if (playbackState == Player.STATE_ENDED) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                if (isPlaying) {
                    if (!model.isPaying()) {
                        for (int i = 0; i < userPostModelList.size(); i++) {
                            if (i == holder.getAdapterPosition()) {
                                userPostModelList.get(i).setPaying(true);
                            } else {
                                userPostModelList.get(i).setPaying(false);
                            }
                        }
                        notifyDataSetChanged();
                    }
                } else {

                }
            }

        });

        playerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
//                pause(exoPlayer);
                playerView.getPlayer().stop();
            }
        });


    }

    private void pause(SimpleExoPlayer playerView) {
        playerView.pause();
    }

    private void play(SimpleExoPlayer playerView) {
        playerView.play();
    }

    private void playWebVideo(WebView webView, String videoPath) {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setUserAgentString("Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        webSettings.setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        webView.loadUrl(videoPath);

//        toggleWebViewState(webView,true);
    }

    private void toggleWebViewState(WebView webView, boolean pause) {
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod(pause
                            ? "onPause"
                            : "onResume", (Class[]) null)
                    .invoke(webView, (Object[]) null);
        } catch (Exception e) {
        }
    }


    private void playVideoView(String videoPath, VideoView videoView) {
        Uri uri = Uri.parse(videoPath);
        videoView.setVideoURI(uri);
        MediaController mediaController = new MediaController(context);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
    }
}
