package com.soul.soulber.adapter;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.JobsHouseFriends.Housings.HousingDetailsActivity;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.databinding.ActivityHousingListBinding;
import com.soul.soulber.model.HousingModel;
import com.soul.soulber.model.ImageModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class HousingAdapter extends RecyclerView.Adapter<HousingAdapter.viewHolder> {

    private Context context;
    private List<HousingModel> housingModelList;

    public HousingAdapter(Context context, List<HousingModel> housingModelList) {
        this.context = context;
        this.housingModelList = housingModelList;

    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityHousingListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_housing_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        HousingModel model = housingModelList.get(position);

        holder.binding.txtType.setText(checkString(model.getBedrooms() + "BHK"));
        holder.binding.txtPrice.setText(checkString(model.getPrice_range()));
        holder.binding.txtPhotoCount.setText(checkString(model.getTotal_images() + " photos"));
        holder.binding.txtAddress.setText(checkString(model.getAddress()));
        holder.binding.txtAgentName.setText(checkString(model.getAgent_name()));

        try {
            List<ImageModel> imageModels = new ArrayList<>();
            JSONArray jsonArray = model.getImages();
            for (int i = 0; i < jsonArray.length(); i++) {
                String image = model.getImagePath() + jsonArray.getString(i);
                ImageModel imageModel = new ImageModel();
                imageModel.setImage(image);
                imageModels.add(imageModel);
            }

            HousingImageAdapter adapter = new HousingImageAdapter(context, imageModels,1);
            holder.binding.recyclerImage.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
            holder.binding.recyclerImage.setHasFixedSize(true);
            holder.binding.recyclerImage.setItemViewCacheSize(imageModels.size());
            holder.binding.recyclerImage.setAdapter(adapter);

        } catch (Exception e) {
            holder.binding.recyclerImage.setVisibility(View.GONE);
        }

        holder.binding.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (TextUtils.isEmpty(checkString(model.getAgent_contact()))) {
                        H.showMessage(context, "No contact available, try after some time");
                    } else {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + model.getAgent_contact() + ""));
                        context.startActivity(intent);
                    }
                } catch (Exception e) {
                    H.showMessage(context, "Something went wrong, try after some time");
                }
            }
        });

        holder.binding.imgWatsappCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (TextUtils.isEmpty(checkString(model.getAgent_contact()))) {
                        H.showMessage(context, "No contact available, try after some time");
                    } else {

//                        Uri uri = Uri.parse("smsto:" + model.getAgent_contact());
//                        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
//                        intent.setPackage("com.whatsapp");
//                        context.startActivity(Intent.createChooser(intent, ""));

                        openWhatsAppConversation(model.getAgent_contact(), "");
                    }
                } catch (Exception e) {
                    H.showMessage(context, "Something went wrong, try after some time");
                }
            }
        });

        holder.binding.lnrMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.HOUSING_ID = model.getId();
                Intent intent = new Intent(context, HousingDetailsActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return housingModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityHousingListBinding binding;

        public viewHolder(@NonNull ActivityHousingListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    public void openWhatsAppConversation(String number, String message) {

        try {
            number = number.replace(" ", "").replace("+", "");

            Intent sendIntent = new Intent("android.intent.action.MAIN");

            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");

            context.startActivity(sendIntent);
        } catch (Exception e) {
            H.showMessage(context, "Unable to open whatsapp conversation");
        }

    }
}
