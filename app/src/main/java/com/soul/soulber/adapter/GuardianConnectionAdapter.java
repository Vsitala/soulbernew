package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Session;
import com.soul.soulber.JobsHouseFriends.FriendsFragment;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.GuardianProfileActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianConnectionListBinding;
import com.soul.soulber.model.GuardianConnectionModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class GuardianConnectionAdapter extends RecyclerView.Adapter<GuardianConnectionAdapter.viewHolder> {

    private Context context;
    private List<GuardianConnectionModel> guardianConnectionModelList;

    private Session session;
    private String usertype_id;

    private FriendsFragment friendsFragment;

    private boolean fromFragment = false;

    public interface onClick {
        void onAction(GuardianConnectionModel model, TextView txtAction);
    }

    public GuardianConnectionAdapter(Context context, List<GuardianConnectionModel> guardianConnectionModelList) {
        this.context = context;
        this.guardianConnectionModelList = guardianConnectionModelList;
        session = new Session(context);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        fromFragment = false;
    }

    public GuardianConnectionAdapter(Context context, List<GuardianConnectionModel> guardianConnectionModelList, FriendsFragment friendsFragment) {
        this.context = context;
        this.guardianConnectionModelList = guardianConnectionModelList;
        this.friendsFragment = friendsFragment;
        session = new Session(context);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        fromFragment = true;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityGuardianConnectionListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_guardian_connection_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        GuardianConnectionModel model = guardianConnectionModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtUserName.setText(checkString(model.getGuardian_name(), holder.binding.txtUserName));

        if (model.getIs_accepted().equals("1")) {
            holder.binding.txtAction.setText("Reject");
        } else {
            holder.binding.txtAction.setText("Accept");
        }

        holder.binding.txtAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (fromFragment) {
                    ((FriendsFragment) friendsFragment).onAction(model, holder.binding.txtAction);
                } else {
                    ((DependentConnectionActivity) context).onAction(model, holder.binding.txtAction);
                }
            }
        });

        holder.binding.txtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToGuardianProfile(model.getGuardian_id());
            }
        });

    }

    private void jumpToGuardianProfile(String id) {
        if (usertype_id.equals(Config.GUARDIAN)) {
            Intent intent = new Intent(context, GuardianProfileActivity.class);
            intent.putExtra(P.id, id);
            context.startActivity(intent);
        }
    }

    @Override
    public int getItemCount() {
        return guardianConnectionModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityGuardianConnectionListBinding binding;

        public viewHolder(@NonNull ActivityGuardianConnectionListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
