package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.JobsHouseFriends.Housings.HousingDetailsActivity;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.databinding.ActivityHousingRequestedListBinding;
import com.soul.soulber.model.HousingRequestedModel;
import com.soul.soulber.model.ImageModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class HousingRequestedAdapter extends RecyclerView.Adapter<HousingRequestedAdapter.viewHolder> {

    private Context context;
    private List<HousingRequestedModel> housingModelList;

    public HousingRequestedAdapter(Context context, List<HousingRequestedModel> housingModelList) {
        this.context = context;
        this.housingModelList = housingModelList;

    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityHousingRequestedListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_housing__requested_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        HousingRequestedModel model = housingModelList.get(position);

        holder.binding.txtType.setText(checkString(model.getBedrooms() + "BHK"));
        holder.binding.txtPrice.setText(checkString(model.getPrice_range()));
        holder.binding.txtPhotoCount.setText(checkString(model.getTotal_images() + " photos"));
        holder.binding.txtAddress.setText(checkString(model.getAddress()));
        holder.binding.txtAppliedTime.setText(checkString(model.getApplied()));
        holder.binding.txtStatus.setText(checkString(model.getBooking_status()));
        holder.binding.txtStatusAccepted.setText(checkString(model.getBooking_status()));

        if (checkString(model.getBooking_status()).equalsIgnoreCase("Requested") || checkString(model.getBooking_status()).equalsIgnoreCase("Applied")) {
            holder.binding.txtStatus.setTextColor(context.getResources().getColor(R.color.colorLightBlue));
            holder.binding.txtStatusAccepted.setTextColor(context.getResources().getColor(R.color.colorLightBlue));
            holder.binding.txtStatus.setVisibility(View.VISIBLE);
            holder.binding.txtStatusAccepted.setVisibility(View.GONE);
        } else if (checkString(model.getBooking_status()).equalsIgnoreCase("Accepted")) {
            holder.binding.txtStatus.setTextColor(context.getResources().getColor(R.color.colorDarkGreen));
            holder.binding.txtStatusAccepted.setTextColor(context.getResources().getColor(R.color.colorDarkGreen));
            holder.binding.txtStatus.setVisibility(View.GONE);
            holder.binding.txtStatusAccepted.setVisibility(View.VISIBLE);
        } else if (checkString(model.getBooking_status()).equalsIgnoreCase("Rejected") || checkString(model.getBooking_status()).equalsIgnoreCase("Not Selected")) {
            holder.binding.txtStatus.setTextColor(context.getResources().getColor(R.color.grayLight));
            holder.binding.txtStatusAccepted.setTextColor(context.getResources().getColor(R.color.grayLight));
            holder.binding.txtStatus.setVisibility(View.VISIBLE);
            holder.binding.txtStatusAccepted.setVisibility(View.GONE);
        }

        try {
            List<ImageModel> imageModels = new ArrayList<>();
            JSONArray jsonArray = model.getImages();
            for (int i = 0; i < jsonArray.length(); i++) {
                String image = model.getImagePath() + jsonArray.getString(i);
                ImageModel imageModel = new ImageModel();
                imageModel.setImage(image);
                imageModels.add(imageModel);
            }

            HousingImageAdapter adapter = new HousingImageAdapter(context, imageModels, 3);
            holder.binding.recyclerImage.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
            holder.binding.recyclerImage.setHasFixedSize(true);
            holder.binding.recyclerImage.setItemViewCacheSize(imageModels.size());
            holder.binding.recyclerImage.setAdapter(adapter);

        } catch (Exception e) {
            holder.binding.recyclerImage.setVisibility(View.GONE);
        }

        holder.binding.lnrMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.HOUSING_ID = model.getHouse_id();
                Config.HOUSING_IMAGE_PATH = model.getImagePath();
                Config.HOUSING_IMAGE_COUNT = model.getTotal_images();
                Intent intent = new Intent(context, HousingDetailsActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return housingModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityHousingRequestedListBinding binding;

        public viewHolder(@NonNull ActivityHousingRequestedListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
