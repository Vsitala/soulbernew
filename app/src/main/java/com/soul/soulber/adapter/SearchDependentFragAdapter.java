package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.Dependent.DependentResultActivity;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivitySearchDependentListBinding;
import com.soul.soulber.fragment.DependentAddConnectionFragment;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class SearchDependentFragAdapter extends RecyclerView.Adapter<SearchDependentFragAdapter.viewHolder> {

    private Context context;
    private List<DependentModel> dependentModelList;
    private String userID;
    private DependentAddConnectionFragment fragment;

    public interface onClick {
        void onConnect(DependentModel model, Button button);

        void onRemove(DependentModel model, Button button);
    }

    public SearchDependentFragAdapter(Context context, List<DependentModel> dependentModelList, String userID, DependentAddConnectionFragment fragmentV) {
        this.context = context;
        this.dependentModelList = dependentModelList;
        this.userID = userID;
        fragment = fragmentV;
    }


    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivitySearchDependentListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_search_dependent_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        DependentModel model = dependentModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgProfile, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString(model.getUsername(), holder.binding.txtName));
        holder.binding.txtAddress.setText(checkString("", holder.binding.lnrAddress));
        holder.binding.txtAge.setText(checkString(model.getAge(), holder.binding.lnrAge));
        holder.binding.txtGender.setText(checkString("", holder.binding.lnrGender));
        holder.binding.txtCity.setText(checkString(model.getCity(), holder.binding.lnrCity));

        if (model.getIs_accepted().equals("0")) {
            holder.binding.btnConnect.setText("Requested");
        } else if (model.getIs_accepted().equals("1")) {
            holder.binding.btnConnect.setText("Remove");
        } else {
            holder.binding.btnConnect.setText("Connect");
        }

        holder.binding.btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (holder.binding.btnConnect.getText().equals("Connect")) {
                    ((DependentAddConnectionFragment) fragment).onConnect(model, holder.binding.btnConnect);
                } else if (holder.binding.btnConnect.getText().equals("Remove")) {
                    ((DependentAddConnectionFragment) fragment).onRemove(model, holder.binding.btnConnect);
                } else if (holder.binding.btnConnect.getText().equals("Requested")) {
                    H.showMessage(context, "Connection request is already sent");
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return dependentModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivitySearchDependentListBinding binding;

        public viewHolder(@NonNull ActivitySearchDependentListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
