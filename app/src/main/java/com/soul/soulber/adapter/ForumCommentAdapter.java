package com.soul.soulber.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.ForumDetailsActivity;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityForumCommentListBinding;
import com.soul.soulber.model.ForumCommentModel;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

import java.util.List;

public class ForumCommentAdapter extends RecyclerView.Adapter<ForumCommentAdapter.viewHolder> {

    private Context context;
    private List<ForumCommentModel> forumCommentModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    public interface onClick {
        void onReply(ForumCommentModel model, TextView txtReplyCountView);
    }

    public ForumCommentAdapter(Context context, List<ForumCommentModel> forumCommentModelList) {
        this.context = context;
        this.forumCommentModelList = forumCommentModelList;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityForumCommentListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_forum_comment_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ForumCommentModel model = forumCommentModelList.get(position);

        String status = checkString(model.getUsername()) + ", " + checkString(model.getComment());
        status = status.replace(model.getUsername(), "<font color='#000'>" + "<b>" + model.getUsername() + "</b>" + "</font>");
        holder.binding.txtComment.setText(Html.fromHtml(status));

        holder.binding.txtTime.setText(checkString(model.getComment_time(), holder.binding.txtTime));

        if (!TextUtils.isEmpty(model.getLikes())) {
            holder.binding.txtLikeCount.setText(checkString(model.getLikes() + " likes"));
        } else {
            holder.binding.txtLikeCount.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(model.getReplies_count())) {
            holder.binding.txtReplyCountView.setText(checkString("View " + model.getReplies_count() + " replies"));
        } else {
            holder.binding.txtReplyCountView.setVisibility(View.GONE);
        }

        if (model.getIs_liked().equals("1")) {
            holder.binding.imgLike.setVisibility(View.VISIBLE);
            holder.binding.imgDisLike.setVisibility(View.GONE);
        } else {
            holder.binding.imgLike.setVisibility(View.GONE);
            holder.binding.imgDisLike.setVisibility(View.VISIBLE);
        }

        holder.binding.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitCommentDisLike(model, holder.binding.imgLike, holder.binding.imgDisLike);
            }
        });

        holder.binding.imgDisLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitCommentLike(model, holder.binding.imgLike, holder.binding.imgDisLike);
            }
        });

        holder.binding.txtReplyCountView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((ForumDetailsActivity) context).onReply(model, holder.binding.txtReplyCountView);
            }
        });

        holder.binding.txtReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((ForumDetailsActivity) context).onReply(model, holder.binding.txtReplyCountView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return forumCommentModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityForumCommentListBinding binding;

        public viewHolder(@NonNull ActivityForumCommentListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private void hitRemoveComment(ForumCommentModel model, int position, DialogInterface dialogInterface) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, model.getId());

        Api.newApi(context, API.BaseUrl + "forums/remove_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        dialogInterface.dismiss();
                        forumCommentModelList.remove(position);
                        notifyDataSetChanged();
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemoveComment", token);
    }

    private void hitCommentLike(ForumCommentModel modelData, ImageView imageLike, ImageView imgDisLike) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, modelData.getId());

        Api.newApi(context, API.BaseUrl + "forums/like_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        imageLike.setVisibility(View.VISIBLE);
                        imgDisLike.setVisibility(View.GONE);

                        try {
                            if (!TextUtils.isEmpty(checkString(modelData.getLikes()))) {
                                int count = Integer.parseInt(modelData.getLikes());
                                count = count + 1;
                                modelData.setLikes(count + "");
                                modelData.setIs_liked("1");
                                notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                        }

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitCommentLike", token);
    }

    private void hitCommentDisLike(ForumCommentModel modelData, ImageView imageLike, ImageView imgDisLike) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, modelData.getId());

        Api.newApi(context, API.BaseUrl + "forums/like_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        imgDisLike.setVisibility(View.VISIBLE);
                        imageLike.setVisibility(View.GONE);

                        try {
                            if (!TextUtils.isEmpty(checkString(modelData.getLikes()))) {
                                int count = Integer.parseInt(modelData.getLikes());
                                count = count - 1;
                                modelData.setLikes(count + "");
                                modelData.setIs_liked("0");
                                notifyDataSetChanged();
                            }
                        } catch (Exception e) {
                        }

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitCommentDisLike", token);
    }

}
