package com.soul.soulber.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityForumReplyListBinding;
import com.soul.soulber.model.ForumReplyModel;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class ForumReplyAdapter extends RecyclerView.Adapter<ForumReplyAdapter.viewHolder> {

    private Context context;
    private List<ForumReplyModel> forumReplyModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    public interface onClick {
        void onReply(UserCommentModel model);
    }

    public ForumReplyAdapter(Context context, List<ForumReplyModel> forumReplyModelList) {
        this.context = context;
        this.forumReplyModelList = forumReplyModelList;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityForumReplyListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_forum_reply_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ForumReplyModel model = forumReplyModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        String status = checkString(model.getUsername()) + ", " + checkString(model.getComment());
        status = status.replace(model.getUsername(), "<font color='#000'>" + model.getUsername() + "</font>");
        holder.binding.txtReply.setText(Html.fromHtml(status));
        holder.binding.txtTime.setText(checkString(model.getComment_time()));

    }

    @Override
    public int getItemCount() {
        return forumReplyModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityForumReplyListBinding binding;

        public viewHolder(@NonNull ActivityForumReplyListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }
}
