package com.soul.soulber.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityTheropistHistoryListBinding;
import com.soul.soulber.model.TheropistHistoryModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class TheropistHistoryAdapter extends RecyclerView.Adapter<TheropistHistoryAdapter.viewHolder> {

    private Context context;
    private List<TheropistHistoryModel> theropistHistoryModelList;

    public TheropistHistoryAdapter(Context context, List<TheropistHistoryModel> theropistHistoryModelList) {
        this.context = context;
        this.theropistHistoryModelList = theropistHistoryModelList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityTheropistHistoryListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_theropist_history_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        TheropistHistoryModel model = theropistHistoryModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.txtName.setText(checkString(model.getTherpist_name()));
        holder.binding.txtExperience.setText(checkString(model.getExperience() + " year of experience"));

        if (checkString(model.getAppointment_status()).equals("2")) {

            if (checkString(getFormatDate(model.getAppointment_date())).equals("")) {
                holder.binding.txtDate.setText("");
                holder.binding.txtDate.setVisibility(View.GONE);
            } else {
                holder.binding.txtDate.setText("Date : " + checkString(getFormatDate(model.getAppointment_date())));
            }

            if (checkString(getFormatTime(model.getAppointment_date())).equals("")) {
                holder.binding.txtTime.setText("");
                holder.binding.txtTime.setVisibility(View.GONE);
            } else {
                holder.binding.txtTime.setText("Time : " + checkString(getFormatTime(model.getRequest_date())));
            }

            if (holder.binding.txtDate.getText().equals("") && holder.binding.txtDate.getText().equals("")) {
                holder.binding.lnrBottom.setVisibility(View.GONE);
            } else {
                holder.binding.lnrBottom.setVisibility(View.VISIBLE);
            }

        } else {

            if (checkString(model.getAppointment_status()).equals("1")) {
                String status = "Status - Pending";
                status = status.replace("Pending", "<font color='#5D816C'>Pending</font>");
                holder.binding.txtStatus.setText(Html.fromHtml(status));
                holder.binding.txtStatus.setVisibility(View.VISIBLE);
                holder.binding.txtDate.setVisibility(View.GONE);
                holder.binding.txtTime.setVisibility(View.GONE);
            } else if (checkString(model.getAppointment_status()).equals("3")) {
                String status = "Status - Cancelled";
                status = status.replace("Cancelled", "<font color='#FA7325'>Cancelled</font>");
                holder.binding.txtStatus.setText(Html.fromHtml(status));
                holder.binding.txtStatus.setVisibility(View.VISIBLE);
                holder.binding.txtDate.setVisibility(View.GONE);
                holder.binding.txtTime.setVisibility(View.GONE);
            } else {
                holder.binding.lnrBottom.setVisibility(View.GONE);
            }

        }

        holder.binding.imgCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                String details = "Name : " + model.getTherpist_name() +
                        "\nBio : " + model.getBio() +
                        "\nExperience : " + model.getExperience() + " year" +
                        "\nClinic Name : " + model.getClinic_name() +
                        "\nLocation : " + model.getTherapist_location();

                if (!checkString(model.getAppointment_date()).equals("")){
                    details = details + "\nAppointment Date : " + model.getAppointment_date();
                }

                copyText(details);
            }
        });

    }

    @Override
    public int getItemCount() {
        return theropistHistoryModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityTheropistHistoryListBinding binding;

        public viewHolder(@NonNull ActivityTheropistHistoryListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    public void copyText(String text) {
        H.showMessage(context, "Details Copied");
        Object clipboardService = context.getSystemService(CLIPBOARD_SERVICE);
        final ClipboardManager clipboardManager = (ClipboardManager) clipboardService;
        ClipData clipData = ClipData.newPlainText("Details Copied", text);
        clipboardManager.setPrimaryClip(clipData);
    }

}
