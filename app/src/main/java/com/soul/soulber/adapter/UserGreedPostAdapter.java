package com.soul.soulber.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.soul.soulber.BaseActivity.Basefragment.profile.ProfileFragment;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.model.UserPostModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.Collections;
import java.util.List;

public class UserGreedPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Player.EventListener {

    private List<UserPostModel> userPostModelsList;
    private Context context;
    private ProfileFragment profileFragment;

    boolean fromFragment = false;

    public interface onClick {
        void playVideo(String path);
    }

    public interface onClickView {
        void playVideoClick(String path);
    }

    public UserGreedPostAdapter(Context context, List<UserPostModel> userPostModelsList, ProfileFragment profileFragment) {
        this.context = context;
        this.userPostModelsList = userPostModelsList;
        this.profileFragment = profileFragment;
        fromFragment = true;
    }

    public UserGreedPostAdapter(Context context, List<UserPostModel> userPostModelsList) {
        this.context = context;
        this.userPostModelsList = userPostModelsList;
        fromFragment = false;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_greed_post_one, viewGroup, false);
        return new ViewHol_1(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        UserPostModel model = userPostModelsList.get(position);

        ViewHol_1 viewHol_one = (ViewHol_1) viewHolder;

        if (model.getUpload_type().equals("1")) {
            viewHol_one.imgPost.setVisibility(View.GONE);
            viewHol_one.cardPlayVideo.setVisibility(View.GONE);
            viewHol_one.lntNoPost.setVisibility(View.GONE);
            viewHol_one.weView.setVisibility(View.GONE);
            viewHol_one.videoView.setVisibility(View.GONE);

            viewHol_one.txtTextMessage.setText(checkString(model.getMessage()));

            if (!TextUtils.isEmpty(checkString(model.getFont_color()))) {
                try {
                    checkColor(Integer.parseInt(model.getFont_color()), viewHol_one.lnrTextBg);
                } catch (Exception e) {
                    checkColor(1, viewHol_one.lnrTextBg);
                }
            } else {
                checkColor(1, viewHol_one.lnrTextBg);
            }

            if (!TextUtils.isEmpty(checkString(model.getFont_style()))) {
                try {
                    checkFont(Integer.parseInt(model.getFont_style()), viewHol_one.txtTextMessage);
                } catch (Exception e) {
                    checkFont(1, viewHol_one.txtTextMessage);
                }
            } else {
                checkFont(1, viewHol_one.txtTextMessage);
            }

        } else if (model.getUpload_type().equals("2")) {
            viewHol_one.lnrTextBg.setVisibility(View.GONE);
            viewHol_one.cardPlayVideo.setVisibility(View.GONE);
            viewHol_one.lntNoPost.setVisibility(View.GONE);
            viewHol_one.weView.setVisibility(View.GONE);
            viewHol_one.videoView.setVisibility(View.GONE);

            LoadImage.glideString(context, viewHol_one.imgPost, model.getPost_img(), context.getResources().getDrawable(R.drawable.ic_no_image));

        } else if (model.getUpload_type().equals("3")) {
            viewHol_one.imgPost.setVisibility(View.GONE);
            viewHol_one.lnrTextBg.setVisibility(View.GONE);
            viewHol_one.lntNoPost.setVisibility(View.GONE);
            viewHol_one.cardPlayVideo.setVisibility(View.VISIBLE);
            viewHol_one.videoView.setVisibility(View.GONE);
            viewHol_one.weView.setVisibility(View.GONE);

            playVideo(model, viewHol_one.playerView, viewHol_one.pbVideoPlayer, viewHol_one);
//            playVideoView(model.getPost_video(),viewHol_one.videoView);
//            playWebVideo(viewHol_one.weView,model.getPost_video());

        } else {
            viewHol_one.imgPost.setVisibility(View.GONE);
            viewHol_one.lnrTextBg.setVisibility(View.GONE);
            viewHol_one.cardPlayVideo.setVisibility(View.GONE);
            viewHol_one.weView.setVisibility(View.GONE);
            viewHol_one.videoView.setVisibility(View.GONE);
            viewHol_one.lntNoPost.setVisibility(View.VISIBLE);
        }

        model.setFromPost(false);
        model.setVisibleComment(false);
        model.setPageCount(1);

        viewHol_one.imgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewImageDialog(model.getPost_img());
            }
        });

        viewHol_one.lnrTextBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                viewTextDialog(model.getMessage(), model.getFont_color(), model.getFont_style());
            }
        });

        viewHol_one.playerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(model.getPost_video())) {
                    H.showMessage(context, "Unable to play video, try again.");
                } else {
                    if (fromFragment) {
                        ((ProfileFragment) profileFragment).playVideo(model.getPost_video());
                    } else {
                        ((DependentProfileActivity) context).playVideoClick(model.getPost_video());
                    }

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return userPostModelsList.size();
    }

    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(userPostModelsList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(userPostModelsList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    public class ViewHol_1 extends RecyclerView.ViewHolder {
        private ImageView imgPost;
        private LinearLayout lnrTextBg;
        private LinearLayout lntNoPost;
        private CardView cardPlayVideo;
        private VideoView videoView;
        private WebView weView;
        private TextView txtTextMessage;
        private PlayerView playerView;
        private ProgressBar pbVideoPlayer;

        public ViewHol_1(View view) {
            super(view);
            imgPost = view.findViewById(R.id.imgPost);
            lnrTextBg = view.findViewById(R.id.lnrTextBg);
            lntNoPost = view.findViewById(R.id.lntNoPost);
            cardPlayVideo = view.findViewById(R.id.cardPlayVideo);
            videoView = view.findViewById(R.id.videoView);
            weView = view.findViewById(R.id.weView);
            txtTextMessage = view.findViewById(R.id.txtTextMessage);
            playerView = view.findViewById(R.id.playerView);
            pbVideoPlayer = view.findViewById(R.id.pbVideoPlayer);
        }

    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkCount(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private void viewImageDialog(String imagePath) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_image_view);

        PhotoView imageView = dialog.findViewById(R.id.imageView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        LoadImage.glideString(context, imageView, imagePath, context.getResources().getDrawable(R.drawable.ic_no_image));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void viewTextDialog(String textMessage, String colorCode, String fontCode) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_text_view);

        RelativeLayout lnrBg = dialog.findViewById(R.id.lnrBg);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        TextView txtScroll = dialog.findViewById(R.id.txtScroll);

        txtScroll.setText(textMessage);

        if (!TextUtils.isEmpty(checkString(colorCode))) {
            checkColor(Integer.parseInt(colorCode), lnrBg);
        } else {
            checkColor(1, lnrBg);
        }

        if (!TextUtils.isEmpty(checkString(fontCode))) {
            checkFont(Integer.parseInt(fontCode), txtScroll);
        } else {
            checkFont(1, txtScroll);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

    private void checkColor(int value, LinearLayout lnrBackground) {
        try {
            if (value == 1) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg1));
            } else if (value == 2) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg2));
            } else if (value == 3) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg3));
            } else if (value == 4) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg4));
            } else if (value == 5) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg5));
            }
        } catch (Exception e) {
        }
    }

    private void checkColor(int value, RelativeLayout lnrBackground) {
        try {
            if (value == 1) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg1));
            } else if (value == 2) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg2));
            } else if (value == 3) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg3));
            } else if (value == 4) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg4));
            } else if (value == 5) {
                lnrBackground.setBackgroundColor(context.getResources().getColor(R.color.bg5));
            }
        } catch (Exception e) {
        }
    }

    private void checkFont(int value, TextView etxPost) {
        try {
            if (value == 1) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_1);
                etxPost.setTypeface(face);
            } else if (value == 2) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_2);
                etxPost.setTypeface(face);
            } else if (value == 3) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_3);
                etxPost.setTypeface(face);
            } else if (value == 4) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_4);
                etxPost.setTypeface(face);
            } else if (value == 5) {
                Typeface face = ResourcesCompat.getFont(context, R.font.font_5);
                etxPost.setTypeface(face);
            }
        } catch (Exception e) {
        }
    }

    private void playVideo(UserPostModel model, PlayerView playerView, ProgressBar pbVideoPlayer, ViewHol_1 holder) {

        SimpleExoPlayer exoPlayer = new SimpleExoPlayer.Builder(context).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context.getApplicationContext(), Util.getUserAgent(context.getApplicationContext(), context.getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(model.getPost_video()));

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(model.getPost_video()));

        if (model.isPaying()) {
            play(exoPlayer);
        } else {
            pause(exoPlayer);
        }

        exoPlayer.addListener(new ExoPlayer.Listener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == Player.STATE_BUFFERING) {
                    pbVideoPlayer.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                } else if (playbackState == Player.STATE_ENDED) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                if (isPlaying) {
                    if (!model.isPaying()) {
                        for (int i = 0; i < userPostModelsList.size(); i++) {
                            if (i == holder.getAdapterPosition()) {
                                userPostModelsList.get(i).setPaying(true);
                            } else {
                                userPostModelsList.get(i).setPaying(false);
                            }
                        }
                        notifyDataSetChanged();
                    }
                } else {

                }
            }

        });

        playerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
//                pause(exoPlayer);
                playerView.getPlayer().stop();
            }
        });


    }

    private void pause(SimpleExoPlayer playerView) {
        playerView.pause();
    }

    private void play(SimpleExoPlayer playerView) {
        playerView.play();
    }

    private void playWebVideo(WebView webView, String videoPath) {

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setUserAgentString("Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        webSettings.setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        webView.loadUrl(videoPath);

//        toggleWebViewState(webView,true);
    }

    private void toggleWebViewState(WebView webView, boolean pause) {
        try {
            Class.forName("android.webkit.WebView")
                    .getMethod(pause
                            ? "onPause"
                            : "onResume", (Class[]) null)
                    .invoke(webView, (Object[]) null);
        } catch (Exception e) {
        }
    }


    private void playVideoView(String videoPath, VideoView videoView) {
        Uri uri = Uri.parse(videoPath);
        videoView.setVideoURI(uri);
        MediaController mediaController = new MediaController(context);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
    }

}

