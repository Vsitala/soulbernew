package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.soul.soulber.R;

import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class DependentSelectionAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflter;
    private List<DependentModel> dependentList;

    public DependentSelectionAdapter(Context context, List<DependentModel> dependentList) {
        this.context = context;
        this.dependentList = dependentList;
        inflter = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return dependentList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.activity_dependent_list, null);
        TextView txtName = view.findViewById(R.id.tv_dependentname);
        ImageView ivimage=view.findViewById(R.id.iv_dependentimage);
        DependentModel model = dependentList.get(i);
        txtName.setText(checkString(model.getUsername()));
        LoadImage.glideString(context, ivimage, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        if (i==0){
            txtName.setTextColor(context.getResources().getColor(R.color.inactive));
        }else {
            txtName.setTextColor(context.getResources().getColor(R.color.inactive));
        }
        return view;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }
}
