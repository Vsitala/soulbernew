package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivitySearchDependentListBinding;
import com.soul.soulber.fragment.GuardianAddConnectionFragment;
import com.soul.soulber.model.GuardianModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class SearchGuardianFragAdapter extends RecyclerView.Adapter<SearchGuardianFragAdapter.viewHolder> {

    private Context context;
    private List<GuardianModel> guardianModelList;
    private String userID;
    private GuardianAddConnectionFragment fragment;

    public interface onClick {
        void onConnect(GuardianModel model, Button button);

        void onRemove(GuardianModel model, Button button);

        void onAccept(GuardianModel model, Button button);
    }

    public SearchGuardianFragAdapter(Context context, List<GuardianModel> guardianModelList, String userID, GuardianAddConnectionFragment fragmentV) {
        this.context = context;
        this.guardianModelList = guardianModelList;
        this.userID = userID;
        fragment = fragmentV;
    }


    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivitySearchDependentListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_search_dependent_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        GuardianModel model = guardianModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgProfile, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString(model.getName(), holder.binding.txtName));
        holder.binding.txtAddress.setText(checkString("", holder.binding.lnrAddress));
        holder.binding.txtAge.setText(checkString(model.getAge(), holder.binding.lnrAge));
        holder.binding.txtGender.setText(checkString("", holder.binding.lnrGender));
        holder.binding.txtCity.setText(checkString(model.getCity(), holder.binding.lnrCity));

        if (model.getIs_accepted().equals("0")) {
            holder.binding.btnConnect.setText("Requested");
        } else if (model.getIs_accepted().equals("1")) {
            holder.binding.btnConnect.setText("Remove");
        } else if (model.getIs_accepted().equals("3")) {
            holder.binding.btnConnect.setText("Accept");
        } else {
            holder.binding.btnConnect.setText("Connect");
        }

        holder.binding.btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (holder.binding.btnConnect.getText().equals("Connect")) {
                    ((GuardianAddConnectionFragment) fragment).onConnect(model, holder.binding.btnConnect);
                } else if (holder.binding.btnConnect.getText().equals("Remove")) {
                    ((GuardianAddConnectionFragment) fragment).onRemove(model, holder.binding.btnConnect);
                } else if (holder.binding.btnConnect.getText().equals("Accept") || holder.binding.btnConnect.getText().equals("Reject")) {
                    ((GuardianAddConnectionFragment) fragment).onAccept(model, holder.binding.btnConnect);
                } else if (holder.binding.btnConnect.getText().equals("Requested")) {
                    H.showMessage(context, "Connection request is already sent");
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return guardianModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivitySearchDependentListBinding binding;

        public viewHolder(@NonNull ActivitySearchDependentListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
