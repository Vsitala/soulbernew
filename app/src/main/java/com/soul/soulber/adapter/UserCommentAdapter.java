package com.soul.soulber.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityUserCommentListBinding;
import com.soul.soulber.model.UserCommentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.List;

public class UserCommentAdapter extends RecyclerView.Adapter<UserCommentAdapter.viewHolder> {

    private Context context;
    private List<UserCommentModel> commentModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    public interface onClick{
        void onReply(UserCommentModel model,TextView txtReplyCountView);
    }

    public UserCommentAdapter(Context context, List<UserCommentModel> commentModelList) {
        this.context = context;
        this.commentModelList = commentModelList;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityUserCommentListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_user_comment_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        UserCommentModel model = commentModelList.get(position);

        LoadImage.glideString(context,holder.binding.imgUser,model.getProfile_pic(),context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        String status = checkString(model.getUsername()) + ", " + checkString(model.getComment());
        status = status.replace(model.getUsername(), "<font color='#000'>" + model.getUsername()  + "</font>");
        holder.binding.txtComment.setText(Html.fromHtml(status));
        holder.binding.txtTime.setText(checkString(model.getComment_time()));
        holder.binding.txtLikeCount.setText(checkString(model.getLikes()+ " likes"));
        holder.binding.txtReplyCountView.setText(checkString("View " + model.getReplies() + " replies"));

        if (model.getIs_liked_by_me().equals("1")) {
            holder.binding.imgLike.setVisibility(View.VISIBLE);
            holder.binding.imgDisLike.setVisibility(View.GONE);
        } else {
            holder.binding.imgLike.setVisibility(View.GONE);
            holder.binding.imgDisLike.setVisibility(View.VISIBLE);
        }

        if (userID.equals(model.getComment_by())){
            holder.binding.txtDeleteComment.setVisibility(View.VISIBLE);
        }else {
            holder.binding.txtDeleteComment.setVisibility(View.GONE);
        }

        holder.binding.imgLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitCommentDisLike(model, holder.binding.imgLike, holder.binding.imgDisLike);
            }
        });

        holder.binding.imgDisLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitCommentLike(model, holder.binding.imgLike, holder.binding.imgDisLike);
            }
        });

        holder.binding.txtReplyCountView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((UserPostActivity)context).onReply(model,holder.binding.txtReplyCountView);
            }
        });

        holder.binding.txtReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((UserPostActivity)context).onReply(model,holder.binding.txtReplyCountView);
            }
        });

        holder.binding.txtDeleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onDeleteCommentClick(model,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return commentModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityUserCommentListBinding binding;

        public viewHolder(@NonNull ActivityUserCommentListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private void onDeleteCommentClick(UserCommentModel model,int position){
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context,R.style.MyDatePickerStyle);
        builder1.setMessage("Do you want to delete this comment ?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        hitRemoveComment(model,position,dialog);
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


//        Button positiveButton = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
//        Button nigativeButton = alert11.getButton(DialogInterface.BUTTON_NEGATIVE);
//        positiveButton.setTextColor(getResources().getColor(R.color.lightBlue));
//        nigativeButton.setTextColor(getResources().getColor(R.color.lightBlue));

    }

    private void hitRemoveComment(UserCommentModel model,int position,DialogInterface dialogInterface) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, model.getComment_id());

        Api.newApi(context, API.BaseUrl + "Posts/remove_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        dialogInterface.dismiss();
                        commentModelList.remove(position);
                        notifyDataSetChanged();
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemoveComment", token);
    }

    private void hitCommentLike(UserCommentModel modelData, ImageView imageLike, ImageView imgDisLike) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, modelData.getComment_id());

        Api.newApi(context, API.BaseUrl + "Posts/like_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        imageLike.setVisibility(View.VISIBLE);
                        imgDisLike.setVisibility(View.GONE);
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitCommentLike", token);
    }

    private void hitCommentDisLike(UserCommentModel modelData, ImageView imageLike, ImageView imgDisLike) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.comment_id, modelData.getComment_id());

        Api.newApi(context, API.BaseUrl + "Posts/unlike_comment").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        imgDisLike.setVisibility(View.VISIBLE);
                        imageLike.setVisibility(View.GONE);
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitCommentDisLike", token);
    }

}
