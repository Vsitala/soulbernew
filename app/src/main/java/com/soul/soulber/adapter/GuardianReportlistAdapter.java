package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.GuardianTestResusltActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityTestreportItemsBinding;
import com.soul.soulber.model.GuardianReportModel;
import com.soul.soulber.util.Click;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GuardianReportlistAdapter extends RecyclerView.Adapter<GuardianReportlistAdapter.viewHolder> {
    private Context context;
    private ArrayList<GuardianReportModel> panicchildlist;
    private Session session;
    String token;

    public interface onClick {
        void onDownload(GuardianReportModel model);
    }

    public GuardianReportlistAdapter(Context context, ArrayList<GuardianReportModel> panicchildlist) {
        this.context = context;
        this.panicchildlist = panicchildlist;
    }

    @NonNull
    @Override
    public GuardianReportlistAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityTestreportItemsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_testreport_items, parent, false);
        return new GuardianReportlistAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GuardianReportlistAdapter.viewHolder holder, int position) {
        GuardianReportModel model = panicchildlist.get(position);
        String Remark = model.getReport_file();
        String Reporturl = "https://www.soulber.co/dev/uploads/lab/reports/";
        holder.binding.tvReportname.setText(checkString(model.getReport_title(), holder.binding.tvReportname));
        holder.binding.llReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((GuardianTestResusltActivity) context).onDownload(model);
//                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Reporturl + Remark)));
            }
        });


    }

    @Override
    public int getItemCount() {
        return panicchildlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityTestreportItemsBinding binding;

        public viewHolder(@NonNull ActivityTestreportItemsBinding binding) {
            super(binding.getRoot());
            session = new Session(context);
            token = session.getJson(Config.userData).getString(P.user_token);

            this.binding = binding;
        }
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

}
