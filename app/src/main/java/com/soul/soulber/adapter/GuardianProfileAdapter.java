package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.AcitvityGuardianProfileListBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.ArrayList;

public class GuardianProfileAdapter extends RecyclerView.Adapter<GuardianProfileAdapter.viewHolder> {
    private Context context;
    private ArrayList<DependentModel> dependentModelList;

    public GuardianProfileAdapter(Context context, ArrayList<DependentModel> dependentModelList) {
        this.context = context;
        this.dependentModelList = dependentModelList;
    }

    @NonNull
    @Override
    public GuardianProfileAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AcitvityGuardianProfileListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.acitvity_guardian_profile_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GuardianProfileAdapter.viewHolder holder, int position) {
        DependentModel model = dependentModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgProfile, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString(model.getUsername(), holder.binding.txtName));
        holder.binding.txtAddress.setText(checkString("", holder.binding.lnrAddress));
        holder.binding.txtAge.setText(checkString(model.getAge(), holder.binding.lnrAge));
        holder.binding.txtGender.setText(checkString("", holder.binding.lnrGender));
        holder.binding.txtCity.setText(checkString(model.getCity(), holder.binding.lnrCity));

        holder.binding.btnViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(context, DependentProfileActivity.class);
                intent.putExtra(P.id, model.getDependent_id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dependentModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        AcitvityGuardianProfileListBinding binding;

        public viewHolder(@NonNull AcitvityGuardianProfileListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}

