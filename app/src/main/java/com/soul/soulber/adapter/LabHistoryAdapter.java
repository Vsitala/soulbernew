package com.soul.soulber.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.soul.soulber.R;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityLabHistoryListBinding;
import com.soul.soulber.fragment.LabPastFragment;
import com.soul.soulber.fragment.LabUnConformFragment;
import com.soul.soulber.fragment.LabUpcomingFragment;
import com.soul.soulber.fragment.LabsFragment;
import com.soul.soulber.model.LabHistoryModel;
import com.soul.soulber.model.PDFModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class LabHistoryAdapter extends RecyclerView.Adapter<LabHistoryAdapter.viewHolder> {

    private Context context;
    private List<LabHistoryModel> labHistoryModelList;
    private LabPastFragment labPastFragment;
    private LabUpcomingFragment labUpcomingFragment;
    private LabUnConformFragment labUnConformFragment;

    private int value = 0;

    public LabHistoryAdapter(Context context, List<LabHistoryModel> labHistoryModelList, LabUpcomingFragment labUpcomingFragment) {
        this.context = context;
        this.labHistoryModelList = labHistoryModelList;
        this.labUpcomingFragment = labUpcomingFragment;
        value = 1;
    }

    public LabHistoryAdapter(Context context, List<LabHistoryModel> labHistoryModelList, LabPastFragment labPastFragment) {
        this.context = context;
        this.labHistoryModelList = labHistoryModelList;
        this.labPastFragment = labPastFragment;
        value = 2;
    }

    public LabHistoryAdapter(Context context, List<LabHistoryModel> labHistoryModelList, LabUnConformFragment labUnConformFragment) {
        this.context = context;
        this.labHistoryModelList = labHistoryModelList;
        this.labUnConformFragment = labUnConformFragment;
        value = 3;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityLabHistoryListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_lab_history_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        LabHistoryModel model = labHistoryModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString(model.getLab_name()));
        holder.binding.txtAddress.setText(checkString(model.getAddress(), holder.binding.lnrAddress));

        if (checkString(model.getAppointment_status()).equals("2")) {

            if (checkString(getFormatDate(model.getAppointment_date())).equals("")) {
                holder.binding.txtDate.setText("");
                holder.binding.txtDate.setVisibility(View.GONE);
            } else {
                holder.binding.txtDate.setText("Date : " + checkString(getFormatDate(model.getAppointment_date())));
            }

            if (checkString(getFormatTime(model.getAppointment_date())).equals("")) {
                holder.binding.txtTime.setText("");
                holder.binding.txtTime.setVisibility(View.GONE);
            } else {
                holder.binding.txtTime.setText("Time : " + checkString(getFormatTime(model.getRequest_date())));
            }

            if (holder.binding.txtDate.getText().equals("") && holder.binding.txtDate.getText().equals("")) {
                holder.binding.lnrBottom.setVisibility(View.GONE);
            } else {
                holder.binding.lnrBottom.setVisibility(View.VISIBLE);
            }

        } else {

            if (checkString(model.getAppointment_status()).equals("1")) {
                String status = "Status - Pending";
                status = status.replace("Pending", "<font color='#5D816C'>Pending</font>");
                holder.binding.txtStatus.setText(Html.fromHtml(status));
                holder.binding.txtStatus.setVisibility(View.VISIBLE);
                holder.binding.txtDate.setVisibility(View.GONE);
                holder.binding.txtTime.setVisibility(View.GONE);
            } else if (checkString(model.getAppointment_status()).equals("3")) {
                String status = "Status - Cancelled";
                status = status.replace("Cancelled", "<font color='#FA7325'>Cancelled</font>");
                holder.binding.txtStatus.setText(Html.fromHtml(status));
                holder.binding.txtStatus.setVisibility(View.VISIBLE);
                holder.binding.txtDate.setVisibility(View.GONE);
                holder.binding.txtTime.setVisibility(View.GONE);
            } else {
                holder.binding.lnrBottom.setVisibility(View.GONE);
            }

        }

        List<PDFModel> pdfModelList = new ArrayList<>();
        final int[] count = {0};
        if (model.getReports() != null && model.getReports().size() != 0) {
            for (Json json : model.getReports()) {
                PDFModel pdfModel = new PDFModel();
                pdfModel.setReport_title(json.getString(P.report_title));
                pdfModel.setReport_file(model.getLab_reports_images_path() + json.getString(P.report_file));
                if (pdfModelList.size() < 3) {
                    pdfModelList.add(pdfModel);
                }
                count[0]++;
            }
        }

        PDFAdapter adapter = null;
        if (value == 1) {
            adapter = new PDFAdapter(context, pdfModelList, labUpcomingFragment);
        } else if (value == 2) {
            adapter = new PDFAdapter(context, pdfModelList, labPastFragment);
        } else if (value == 3) {
            adapter = new PDFAdapter(context, pdfModelList, labUnConformFragment);
        }
        holder.binding.recyclerPDF.setLayoutManager(new LinearLayoutManager(context));
        holder.binding.recyclerPDF.setHasFixedSize(true);
        holder.binding.recyclerPDF.setAdapter(adapter);

        if (count[0] > 3) {
            holder.binding.txtViewAll.setVisibility(View.VISIBLE);
            holder.binding.txtViewAll.setText("View All");
        } else {
            holder.binding.txtViewAll.setVisibility(View.GONE);
        }

        PDFAdapter finalAdapter = adapter;
        holder.binding.txtViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);

                if (holder.binding.txtViewAll.getText().toString().equals("View All")) {
                    pdfModelList.clear();
                    if (model.getReports() != null && model.getReports().size() != 0) {
                        for (Json json : model.getReports()) {
                            PDFModel pdfModel = new PDFModel();
                            pdfModel.setReport_title(json.getString(P.report_title));
                            pdfModel.setReport_file(model.getLab_reports_images_path() + json.getString(P.report_file));
                            pdfModelList.add(pdfModel);
                        }
                        finalAdapter.notifyDataSetChanged();
                        holder.binding.txtViewAll.setText("View Less");
                    }
                } else if (holder.binding.txtViewAll.getText().toString().equals("View Less")) {
                    pdfModelList.clear();
                    if (model.getReports() != null && model.getReports().size() != 0) {
                        for (Json json : model.getReports()) {
                            PDFModel pdfModel = new PDFModel();
                            pdfModel.setReport_title(json.getString(P.report_title));
                            pdfModel.setReport_file(model.getLab_reports_images_path() + json.getString(P.report_file));
                            if (pdfModelList.size() < 3) {
                                pdfModelList.add(pdfModel);
                            }
                            count[0]++;
                        }
                        finalAdapter.notifyDataSetChanged();
                        holder.binding.txtViewAll.setText("View All");
                    }
                }
            }
        });

        holder.binding.imgCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                String details = "Lab Name : " + model.getLab_name() +
                        "\nBio : " + model.getBio() +
                        "\nAddress : " + model.getAddress();

                if (!checkString(model.getAppointment_date()).equals("")) {
                    details = details + "\nAppointment Date : " + model.getAppointment_date();
                }

                copyText(details);
            }
        });

    }


    @Override
    public int getItemCount() {
        return labHistoryModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityLabHistoryListBinding binding;

        public viewHolder(@NonNull ActivityLabHistoryListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    public void copyText(String text) {
        H.showMessage(context, "Details Copied");
        Object clipboardService = context.getSystemService(CLIPBOARD_SERVICE);
        final ClipboardManager clipboardManager = (ClipboardManager) clipboardService;
        ClipData clipData = ClipData.newPlainText("Details Copied", text);
        clipboardManager.setPrimaryClip(clipData);
    }
}
