package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.soul.soulber.R;
import com.soul.soulber.model.CountryModel;

import java.util.List;

public class CountrySelectionAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflter;
    private List<CountryModel> itemListModels;

    public CountrySelectionAdapter(Context context, List<CountryModel> itemListModelList) {
        this.context = context;
        this.itemListModels = itemListModelList;
        inflter = (LayoutInflater.from(context));
    }


    @Override
    public int getCount() {
        return itemListModels.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.activity_country_list, null);
        TextView txtName = view.findViewById(R.id.txtName);
        CountryModel model = itemListModels.get(i);
        txtName.setText(model.getCity());
        if (i==0){
            txtName.setTextColor(context.getResources().getColor(R.color.inactive));
        }else {
            txtName.setTextColor(context.getResources().getColor(R.color.inactive));
        }
        return view;
    }

}