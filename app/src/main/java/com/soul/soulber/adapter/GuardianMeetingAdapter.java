package com.soul.soulber.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMeetingItemsBinding;
import com.soul.soulber.databinding.ActivityTestresultItemsBinding;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.GuardianMeetingModel;
import com.soul.soulber.model.GuardianReportModel;
import com.soul.soulber.model.GuardianTestResultModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GuardianMeetingAdapter extends RecyclerView.Adapter<GuardianMeetingAdapter.viewHolder> {
    private Context context;
    private ArrayList<GuardianMeetingModel> panicchildlist;
    private Session session;
    String token;
    LinearLayoutManager linearLayout;
    ArrayList<GuardianReportModel> reportlist;
    private GuardianReportlistAdapter adapter;
    private ArrayList<DependentModel> dependentModels;

    public GuardianMeetingAdapter(Context context, ArrayList<GuardianMeetingModel> panicchildlist) {
        this.context = context;
        this.panicchildlist = panicchildlist;
        this.dependentModels=dependentModels;
    }

    @NonNull
    @Override
    public GuardianMeetingAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityMeetingItemsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_meeting_items, parent, false);
        return new GuardianMeetingAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GuardianMeetingAdapter.viewHolder holder, int position) {
        GuardianMeetingModel model = panicchildlist.get(position);
        String Remark = model.getRemark();
        String addresh=checkString(model.getCity())+","+checkString(model.getVenue());
        LoadImage.glideString(context, holder.binding.ivMeethallpic, model.getPrimary_image(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.tvMeethallname.setText(checkString(model.getTopic(), holder.binding.tvMeethallname));
        holder.binding.tvHalladdresh.setText(addresh);
        holder.binding.tvDate.setText("Date : " + checkString(model.getDate()));
        holder.binding.tvTime.setText("Time : " + checkString(model.getTime()));
        holder.binding.tvDependentname.setText(checkString(model.getUsername(),holder.binding.tvDependentname));
        LoadImage.glideString(context, holder.binding.ivDependentpic, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        if(model.getRemark().equals("") || model.getRemark()==null)
        {
            holder.binding.ivRemark.setVisibility(View.GONE);
        }
        else
        {
            holder.binding.ivRemark.setVisibility(View.VISIBLE);
        }
        holder.binding.ivRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                H.showMessage(context,Remark);
                Toast toast= Toast.makeText(context,
                        Remark, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });






    }

    @Override
    public int getItemCount() {
        return panicchildlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityMeetingItemsBinding binding;


        public viewHolder(@NonNull ActivityMeetingItemsBinding binding) {
            super(binding.getRoot());
            session = new Session(context);
            token =session.getJson(Config.userData).getString(P.user_token);
            reportlist= new ArrayList<>();
            adapter= new GuardianReportlistAdapter(context,reportlist);
            linearLayout= new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
            this.binding = binding;
        }
    }
    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }
    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }
    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }
    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

}
