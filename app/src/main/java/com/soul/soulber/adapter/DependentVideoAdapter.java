package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.soul.soulber.BaseActivity.Basefragment.profile.ProfileFragment;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.activity.VideoFullScreenActivity;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentVideoListBinding;
import com.soul.soulber.model.DependentVideoModel;
import com.soul.soulber.util.Click;

import java.util.List;

public class DependentVideoAdapter extends RecyclerView.Adapter<DependentVideoAdapter.viewHolder> implements Player.EventListener {

    private Context context;
    private List<DependentVideoModel> dependentVideoModelList;

    public DependentVideoAdapter(Context context, List<DependentVideoModel> dependentVideoModelList) {
        this.context = context;
        this.dependentVideoModelList = dependentVideoModelList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityDependentVideoListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_dependent_video_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        DependentVideoModel model = dependentVideoModelList.get(position);

        holder.binding.txtTitle.setText(checkString(model.getTitle()));
        holder.binding.txtDate.setText(checkString(model.getDate()));
        playVideo(model, holder.binding.playerView, holder.binding.pbVideoPlayer, holder);

        holder.binding.playerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (TextUtils.isEmpty(model.getVideoPath())) {
                    H.showMessage(context, "Unable to play video, try again.");
                } else {
                    Intent intent = new Intent(context, VideoFullScreenActivity.class);
                    intent.putExtra(P.video, model.getVideoPath());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dependentVideoModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityDependentVideoListBinding binding;

        public viewHolder(@NonNull ActivityDependentVideoListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


    private void playVideo(DependentVideoModel model, PlayerView playerView, ProgressBar pbVideoPlayer, viewHolder holder) {

        SimpleExoPlayer exoPlayer = new SimpleExoPlayer.Builder(context).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(context.getApplicationContext(), Util.getUserAgent(context.getApplicationContext(), context.getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(model.getVideoPath()));

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(model.getVideoPath()));

        pause(exoPlayer);

        exoPlayer.addListener(new ExoPlayer.Listener() {

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                if (playbackState == Player.STATE_BUFFERING) {
                    pbVideoPlayer.setVisibility(View.VISIBLE);
                } else if (playbackState == Player.STATE_READY) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                } else if (playbackState == Player.STATE_ENDED) {
                    pbVideoPlayer.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void onIsPlayingChanged(boolean isPlaying) {
                if (isPlaying) {

                } else {

                }
            }

        });

        playerView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
//                pause(exoPlayer);
                playerView.getPlayer().stop();
            }
        });


    }

    private void pause(SimpleExoPlayer playerView) {
        playerView.pause();
    }

    private void play(SimpleExoPlayer playerView) {
        playerView.play();
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

}
