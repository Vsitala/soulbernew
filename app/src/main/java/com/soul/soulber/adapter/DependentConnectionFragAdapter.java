package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityGuardianConnectionFragListBinding;
import com.soul.soulber.fragment.DependentConnectionFragment;
import com.soul.soulber.model.DependentConnectionFragModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class DependentConnectionFragAdapter extends RecyclerView.Adapter<DependentConnectionFragAdapter.viewHolder> {

    private Context context;
    private List<DependentConnectionFragModel> dependentConnectionFragModelList;

    private DependentConnectionFragment fragment;

    public interface onClick {
        void onConnect(DependentConnectionFragModel model, TextView txtAction);

        void onRemove(DependentConnectionFragModel model, TextView txtAction);
    }

    public DependentConnectionFragAdapter(Context context, List<DependentConnectionFragModel> dependentConnectionFragModelList, DependentConnectionFragment fragment) {
        this.context = context;
        this.dependentConnectionFragModelList = dependentConnectionFragModelList;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityGuardianConnectionFragListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_guardian_connection_frag_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        DependentConnectionFragModel model = dependentConnectionFragModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgNewUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        LoadImage.glideString(context, holder.binding.imgOldUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtNewUserName.setText(checkString(model.getDependent_username(), holder.binding.txtNewUserName));
        holder.binding.txtOldUserName.setText(checkString(model.getDependent_username(), holder.binding.txtOldUserName));
        holder.binding.txtConnectionStatusOne.setText("Requested "+ checkString(model.getConnection_time().trim(), holder.binding.txtConnectionStatusOne));
        holder.binding.txtConnectionStatusTwo.setText("Requested "+ checkString(model.getConnection_time().trim(), holder.binding.txtConnectionStatusTwo));

        if (model.getIs_accepted().equals("0")) {
            holder.binding.txtConnection.setText("Requested");
            holder.binding.txtConnectionStatusOne.setVisibility(View.VISIBLE);
        }

//        holder.binding.txtConnection.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Click.preventTwoClick(v);
//                if (holder.binding.txtConnection.getText().toString().equals("Connect")) {
//                    ((DependentConnectionFragment) fragment).onConnect(model, holder.binding.txtConnection);
//                } else {
//                    H.showMessage(context, "Connection request is already sent");
//                }
//            }
//        });

        holder.binding.txtRemoveConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (holder.binding.txtRemoveConnection.getText().toString().equals("Remove connection")) {
                    ((DependentConnectionFragment) fragment).onRemove(model, holder.binding.txtRemoveConnection);
                }
            }
        });

        if (model.getIs_accepted().equals("1")) {
            holder.binding.lnrNewConnection.setVisibility(View.GONE);
            holder.binding.lnrOldConnection.setVisibility(View.VISIBLE);
        } else {
            holder.binding.lnrNewConnection.setVisibility(View.VISIBLE);
            holder.binding.lnrOldConnection.setVisibility(View.GONE);
        }

        holder.binding.txtNewUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToDependent(model.getDependent_id());
            }
        });

        holder.binding.txtOldUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToDependent(model.getDependent_id());
            }
        });
    }

    private void jumpToDependent(String id) {
        Intent intent = new Intent(context, DependentProfileActivity.class);
        intent.putExtra(P.id, id);
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return dependentConnectionFragModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityGuardianConnectionFragListBinding binding;

        public viewHolder(@NonNull ActivityGuardianConnectionFragListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
