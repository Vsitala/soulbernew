package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Session;
import com.soul.soulber.BaseActivity.Basefragment.home.HomeFragment;
import com.soul.soulber.R;
import com.soul.soulber.activity.ChatDetailsActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityChatListBinding;
import com.soul.soulber.model.ChatListModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.viewHolder> {

    private Context context;
    private List<ChatListModel> chatListModelList;

    private Session session;
    private String usertype_id;
    private String userID;

    private HomeFragment fragment;

    public interface onClick {
        void chatClicked(ChatListModel model);
    }

    public ChatListAdapter(Context context, List<ChatListModel> chatListModelList, HomeFragment fragment) {
        this.context = context;
        this.chatListModelList = chatListModelList;
        this.fragment = fragment;
        session = new Session(context);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityChatListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_chat_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ChatListModel model = chatListModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        String userName = "";
        if (model.getOpponent_user_type_id().equals(Config.DEPENDENT)) {
            userName = model.getUsername();
        } else {
            userName = model.getName();
        }

        holder.binding.textViewName.setText(checkString(userName));

        holder.binding.txtTime.setText("Active " + checkString(model.getLast_activated(), holder.binding.txtTime));
        holder.binding.lnrChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                ((HomeFragment) fragment).chatClicked(model);
                String id = "";
                if (model.getFrom_user_id().equals(userID)) {
                    id = model.getTo_user_id();
                } else {
                    id = model.getFrom_user_id();
                }
                Intent intent = new Intent(context, ChatDetailsActivity.class);
                intent.putExtra(P.username, model.getUsername());
                intent.putExtra(P.id, id);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return chatListModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityChatListBinding binding;

        public viewHolder(@NonNull ActivityChatListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
