package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.Meeting.MeetingDetailsActivity;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMeetingHistoryListDataBinding;
import com.soul.soulber.model.MeetingHistoryModel;
import com.soul.soulber.util.LoadImage;

import org.json.JSONArray;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MeetingHistoryAdapter extends RecyclerView.Adapter<MeetingHistoryAdapter.viewHolder> {

    private Context context;
    private List<MeetingHistoryModel> meetingModelList;
    private LoadingDialog loadingDialog;
    private Session session;
    private String token;

    public MeetingHistoryAdapter(Context context, List<MeetingHistoryModel> meetingModelList) {
        this.context = context;
        this.meetingModelList = meetingModelList;
        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityMeetingHistoryListDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_meeting_history_list_data, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        MeetingHistoryModel model = meetingModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgMeeting, model.getImagePath() + model.getPrimary_image(), context.getResources().getDrawable(R.drawable.ic_no_image));
        JSONArray jsonList = model.getProfile_pic();

        try {
            LoadImage.glideString(context, holder.binding.img1, jsonList.get(0).toString(), context.getResources().getDrawable(R.drawable.ic_baseline_account_circle_24));
        } catch (Exception e) {
            LoadImage.glideString(context, holder.binding.img1, model.getImagePath(), context.getResources().getDrawable(R.drawable.ic_baseline_account_circle_24));
            holder.binding.carIMG1.setVisibility(View.GONE);
        }

        try {
            LoadImage.glideString(context, holder.binding.img2, jsonList.get(1).toString(), context.getResources().getDrawable(R.drawable.ic_baseline_account_circle_24));
        } catch (Exception e) {
            LoadImage.glideString(context, holder.binding.img2, model.getImagePath(), context.getResources().getDrawable(R.drawable.ic_baseline_account_circle_24));
            holder.binding.carIMG2.setVisibility(View.GONE);
        }

        try {
            LoadImage.glideString(context, holder.binding.img3, jsonList.get(2).toString(), context.getResources().getDrawable(R.drawable.ic_baseline_account_circle_24));
        } catch (Exception e) {
            LoadImage.glideString(context, holder.binding.img3, model.getImagePath(), context.getResources().getDrawable(R.drawable.ic_baseline_account_circle_24));
            holder.binding.carIMG3.setVisibility(View.GONE);
        }

        holder.binding.txtTopic.setText(checkString(model.getTopic(), holder.binding.txtTopic));
        holder.binding.txtAddress.setText(checkString(model.getCity() + "," + model.getVenue(), holder.binding.txtAddress));
//        holder.binding.txtDate.setText(checkString(getFormatDate(model.getDate()), holder.binding.txtDate));
        if (checkString(model.getAttendee_count()).equals("")) {
            holder.binding.txtAttendanceCount.setVisibility(View.GONE);
        } else {
            holder.binding.txtAttendanceCount.setText("+ " + checkString(model.getAttendee_count() + " are attending", holder.binding.txtAttendanceCount));
        }

        if (checkString(model.getAttendee_status()).equals("2")) {

            if (checkString(getFormatDate(model.getDate())).equals("")) {
                holder.binding.txtDate.setText("");
                holder.binding.txtDate.setVisibility(View.GONE);
            } else {
                holder.binding.txtDate.setText(checkString(getFormatDate(model.getDate())));
            }

        } else {

            if (checkString(model.getAttendee_status()).equals("1")) {
                String status = "Status - Pending";
                status = status.replace("Pending", "<font color='#5D816C'>Pending</font>");
                holder.binding.txtStatus.setText(Html.fromHtml(status));
                holder.binding.txtStatus.setVisibility(View.VISIBLE);
                holder.binding.txtDate.setVisibility(View.GONE);
            } else if (checkString(model.getAttendee_status()).equals("3")) {
                String status = "Status - Cancelled";
                status = status.replace("Cancelled", "<font color='#FA7325'>Cancelled</font>");
                holder.binding.txtStatus.setText(Html.fromHtml(status));
                holder.binding.txtStatus.setVisibility(View.VISIBLE);
                holder.binding.txtDate.setVisibility(View.GONE);
            }else {
                holder.binding.txtStatus.setVisibility(View.GONE);
                holder.binding.txtDate.setVisibility(View.GONE);
            }

        }

        holder.binding.lnrMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.MEETING_ID = model.getId();
                Intent intent = new Intent(context, MeetingDetailsActivity.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return meetingModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityMeetingHistoryListDataBinding binding;

        public viewHolder(@NonNull ActivityMeetingHistoryListDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM, yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

}
