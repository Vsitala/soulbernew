package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentProfileActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.AcitvityGuardianProfileListBinding;
import com.soul.soulber.databinding.ItemHistorypanicBinding;
import com.soul.soulber.model.ChildExpandableModel;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PanicHistoryAdapter extends RecyclerView.Adapter<PanicHistoryAdapter.viewHolder> {

    private Context context;
    private ArrayList<DependentModel> dependentModelList;
    private ArrayList<ChildExpandableModel> panicchildlist;
    private PanicHistoryChildAdapter childadapter;
    private LinearLayoutManager linearLayoutManager;
    String token;
    private Session session;


    public PanicHistoryAdapter(Context context, ArrayList<DependentModel> dependentModelList) {
        this.context = context;
        this.dependentModelList = dependentModelList;
    }

    @NonNull
    @Override
    public PanicHistoryAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHistorypanicBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_historypanic, parent, false);
        return new PanicHistoryAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PanicHistoryAdapter.viewHolder holder, int position) {
        DependentModel model = dependentModelList.get(position);
        LoadImage.glideString(context, holder.binding.ivUserimage, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.listTitle.setText(checkString(model.getName(), holder.binding.listTitle));
        String userid=model.getDependent_id();
        holder.binding.llpanidependent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if(holder.binding.rvPanicchild.getVisibility()==View.VISIBLE)
                {
                    holder.binding.ivArrow.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrowdown));
                    holder.binding.rvPanicchild.setVisibility(View.GONE);
                    holder.binding.tvNopanichistory.setVisibility(View.GONE);
                }
                else
                    if(holder.binding.rvPanicchild.getVisibility()==View.GONE)
                {
                    holder.binding.ivArrow.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrowup));
                    holder.binding.rvPanicchild.setVisibility(View.VISIBLE);
                    panicchildlist= new ArrayList<>();
                    childadapter= new PanicHistoryChildAdapter(context,panicchildlist);
                    holder.binding.rvPanicchild.setAdapter(childadapter);
                    linearLayoutManager= new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
                    holder.binding.rvPanicchild.setLayoutManager(linearLayoutManager);
                    Api.newApi(context, API.BaseUrl+"Dashboard/view_panic_history?user_id="+userid+"&page=1")
                            .setMethod(Api.GET)
                            .onSuccess(new Api.OnSuccessListener() {
                                @Override
                                public void onSuccess(Json json) {
                                    if(json.getInt(P.status)==1)

                                    {

                                        String profile_pic_path="https://www.soulber.co/dev/uploads/profile_pic/";
                                        Json data = json.getJson(P.data);
                                        JsonList jsonList = data.getJsonList(P.list);
                                        if(jsonList!=null && jsonList.size()!=0)
                                        {

                                            for(Json jvalue : jsonList){
                                                ChildExpandableModel modelnew = new ChildExpandableModel();
                                                modelnew.setName(jvalue.getString(P.name));
                                                modelnew.setProfile_pic(profile_pic_path+jvalue.getString(P.profile_pic));
                                                modelnew.setAdd_date(jvalue.getString(P.add_date));
                                                panicchildlist.add(modelnew);
                                            }
                                            childadapter.notifyDataSetChanged();
                                        }
                                        else  if (panicchildlist.isEmpty())
                                        {
                                            holder.binding.tvNopanichistory.setVisibility(View.VISIBLE);


                                        }

                                    }
                                }
                            })
                            .onError(new Api.OnErrorListener() {
                                @Override
                                public void onError() {

                                }
                            })
                            .run("hitpanicdetails",token);
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return dependentModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ItemHistorypanicBinding binding;

        public viewHolder(@NonNull ItemHistorypanicBinding binding) {
            super(binding.getRoot());
            session = new Session(context);
            token =session.getJson(Config.userData).getString(P.user_token);
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }



}

