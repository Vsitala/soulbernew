package com.soul.soulber.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityTestresultItemsBinding;
import com.soul.soulber.databinding.ActivityTheraphyItemsBinding;
import com.soul.soulber.model.ChildExpandableModel;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.model.GuardianReportModel;
import com.soul.soulber.model.GuardianTestResultModel;
import com.soul.soulber.model.GuardianTheraphyModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class GuardianTestResultAdapter extends RecyclerView.Adapter<GuardianTestResultAdapter.viewHolder> {
    private Context context;
    private ArrayList<GuardianTestResultModel> panicchildlist;
    private Session session;
    String token;
    LinearLayoutManager linearLayout;
    ArrayList<GuardianReportModel> reportlist;
    private GuardianReportlistAdapter adapter;
    private ArrayList<DependentModel> dependentModels;
    String userid;
    String Remark;
    int pos;
    ActivityTestresultItemsBinding binding;


    public GuardianTestResultAdapter(Context context, ArrayList<GuardianTestResultModel> panicchildlist, ArrayList<DependentModel> dependentModels) {
        this.context = context;
        this.panicchildlist = panicchildlist;
        this.dependentModels = dependentModels;
    }

    @NonNull
    @Override
    public GuardianTestResultAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_testresult_items, parent, false);
        return new GuardianTestResultAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GuardianTestResultAdapter.viewHolder holder, int position) {
        pos = position;
        GuardianTestResultModel model = panicchildlist.get(position);
        DependentModel dependent_reportid = dependentModels.get(position);
        Remark = model.getRemark();
        userid = dependent_reportid.getDependent_id();
        LoadImage.glideString(context, holder.binding.ivLappic, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.tvLabname.setText(checkString(model.getLab_name(), holder.binding.tvLabname));
        holder.binding.tvLabaddresh.setText(checkString(model.getAddress(), holder.binding.tvLabaddresh));
        holder.binding.tvDate.setText("Date : " + checkString(getFormatDate(model.getAppointment_date())));
        holder.binding.tvTime.setText("Time : " + checkString(getFormatTime(model.getAppointment_date())));
        holder.binding.tvDependentname.setText(checkString(model.getUsername(), holder.binding.tvDependentname));
        LoadImage.glideString(context, holder.binding.ivDependentpic, model.getProfile_pic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        if (model.getRemark().equals("") || model.getRemark() == null) {
            holder.binding.ivRemark.setVisibility(View.GONE);
        } else {
            holder.binding.ivRemark.setVisibility(View.VISIBLE);
        }
        holder.binding.ivRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                H.showMessage(context,Remark);
                Toast toast = Toast.makeText(context,
                        Remark, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();
            }
        });
        holder.binding.tvViewless.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitpdflist();
                binding.tvViewless.setVisibility(View.GONE);
                binding.tvViewalllist.setVisibility(View.VISIBLE);
            }
        });
        holder.binding.tvViewalllist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                reportlist = new ArrayList<>();
                Api.newApi(context, API.BaseUrl + "lab/guardian_lab_list?dependent=" + userid)
                        .setMethod(Api.GET)
                        .onSuccess(new Api.OnSuccessListener() {
                            @Override
                            public void onSuccess(Json json) {

                                if (json.getInt(P.status) == 1) {
                                    holder.binding.tvViewless.setVisibility(View.VISIBLE);
                                    holder.binding.tvViewalllist.setVisibility(View.GONE);
                                    Json data = json.getJson(P.data);
                                    JsonList jsonList = data.getJsonList(P.labs).get(position).getJsonList(P.reports);
                                    if (jsonList != null && jsonList.size() != 0) {
                                        for (Json jvalue : jsonList) {
                                            holder.binding.rvReportlist.setVisibility(View.VISIBLE);
                                            GuardianReportModel modelnew = new GuardianReportModel();
                                            modelnew.setReport_title(jvalue.getString(P.report_title));
                                            modelnew.setReport_file(jvalue.getString(P.report_file));
                                            reportlist.add(modelnew);
                                        }
                                        adapter = new GuardianReportlistAdapter(context, reportlist);
                                        holder.binding.rvReportlist.setAdapter(adapter);
                                        holder.binding.rvReportlist.setLayoutManager(linearLayout);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (jsonList.isEmpty()) {
                                        holder.binding.rvReportlist.setVisibility(View.GONE);
                                    }
                                }
                            }
                        })
                        .onError(new Api.OnErrorListener() {
                            @Override
                            public void onError() {

                            }
                        })
                        .run("hitreportlistguard", token);
            }
        });
        hitpdflist();

    }

    private void hitpdflist() {
        reportlist = new ArrayList<>();
        Api.newApi(context, API.BaseUrl + "lab/guardian_lab_list?dependent=" + userid)
                .setMethod(Api.GET)
                .onSuccess(new Api.OnSuccessListener() {
                    @Override
                    public void onSuccess(Json json) {
                        if (json.getInt(P.status) == 1) {
                            String lab_image_path = "https://www.soulber.co/dev/uploads/lab/";
                            Json data = json.getJson(P.data);
                            JsonList jsonList = data.getJsonList(P.labs).get(pos).getJsonList(P.reports);
                            if (jsonList != null && jsonList.size() != 0) {
                                for (Json jvalue : jsonList) {
                                    if (reportlist.size() < 3) {
                                        binding.rvReportlist.setVisibility(View.VISIBLE);
                                        GuardianReportModel modelnew = new GuardianReportModel();
                                        modelnew.setReport_title(jvalue.getString(P.report_title));
                                        modelnew.setReport_file(jvalue.getString(P.report_file));
                                        reportlist.add(modelnew);
                                        binding.tvViewalllist.setVisibility(View.GONE);
                                    } else {
                                        binding.tvViewalllist.setVisibility(View.VISIBLE);
                                    }
                                }
                                adapter = new GuardianReportlistAdapter(context, reportlist);
                                binding.rvReportlist.setAdapter(adapter);
                                binding.rvReportlist.setLayoutManager(linearLayout);
                                adapter.notifyDataSetChanged();
                            }
                            if (jsonList.isEmpty()) {
                                binding.rvReportlist.setVisibility(View.GONE);
                            }

                        }
                    }
                })
                .onError(new Api.OnErrorListener() {
                    @Override
                    public void onError() {

                    }
                })
                .run("hitreportlistguard", token);
    }

    @Override
    public int getItemCount() {
        return panicchildlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityTestresultItemsBinding binding;

        public viewHolder(@NonNull ActivityTestresultItemsBinding binding) {
            super(binding.getRoot());
            session = new Session(context);
            token = session.getJson(Config.userData).getString(P.user_token);
            reportlist = new ArrayList<>();
            adapter = new GuardianReportlistAdapter(context, reportlist);
            linearLayout = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            this.binding = binding;
        }
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }


}
