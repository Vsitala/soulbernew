package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.JobDetailsActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityMyJobAppliedListBinding;
import com.soul.soulber.model.MyAppliedJobModel;
import com.soul.soulber.util.ProgressView;

import java.util.List;

public class MyAppliedJobAdapter extends RecyclerView.Adapter<MyAppliedJobAdapter.viewHolder> {

    private Context context;
    private List<MyAppliedJobModel> appliedJobModelList;
    private LoadingDialog loadingDialog;
    private Session session;
    private String token;
    private int fromBookmarkAdd = 1;
    private int fromBookmarkRemove = 2;

    public MyAppliedJobAdapter(Context context, List<MyAppliedJobModel> appliedJobModelList) {
        this.context = context;
        this.appliedJobModelList = appliedJobModelList;
        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityMyJobAppliedListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_my_job_applied_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        MyAppliedJobModel model = appliedJobModelList.get(position);

        holder.binding.txtSkills.setText(checkString(model.getSkills(), holder.binding.txtSkills));
        holder.binding.txtWebLink.setText(checkString(model.getWebsite_link(), holder.binding.txtWebLink));
        holder.binding.txtLocation.setText(checkString(model.getJob_location(), holder.binding.txtLocation));
        holder.binding.txtJobType.setText(checkString(model.getCategory(), holder.binding.txtJobType));
        holder.binding.txtAppliedTime.setText(checkString(model.getApplied()));
        holder.binding.txtStatus.setText(checkString(model.getApplication_status()));
        holder.binding.txtStatusAccepted.setText(checkString(model.getApplication_status()));

        if (!TextUtils.isEmpty(checkString(model.getExperience_to()))) {
            holder.binding.txtExperience.setText(checkString(checkString(model.getExperience_from()) + checkString(" - " + model.getExperience_to() + " year"), holder.binding.txtExperience));
        } else {
            holder.binding.txtExperience.setText(checkString(checkString(model.getExperience_from() + " year"), holder.binding.txtExperience));
        }

        if (model.getIs_bookmarked().equals("1")) {
            holder.binding.imgBookmarkRemove.setVisibility(View.VISIBLE);
            holder.binding.imgBookmarkAdd.setVisibility(View.GONE);
        } else {
            holder.binding.imgBookmarkRemove.setVisibility(View.GONE);
            holder.binding.imgBookmarkAdd.setVisibility(View.VISIBLE);
        }

        if (checkString(model.getApplication_status()).equalsIgnoreCase("Requested") || checkString(model.getApplication_status()).equalsIgnoreCase("Applied")) {
            holder.binding.txtStatus.setTextColor(context.getResources().getColor(R.color.colorLightBlue));
            holder.binding.txtStatusAccepted.setTextColor(context.getResources().getColor(R.color.colorLightBlue));
            holder.binding.txtStatus.setVisibility(View.VISIBLE);
            holder.binding.txtStatusAccepted.setVisibility(View.GONE);
        } else if (checkString(model.getApplication_status()).equalsIgnoreCase("Accepted")) {
            holder.binding.txtStatus.setTextColor(context.getResources().getColor(R.color.colorDarkGreen));
            holder.binding.txtStatusAccepted.setTextColor(context.getResources().getColor(R.color.colorDarkGreen));
            holder.binding.txtStatus.setVisibility(View.GONE);
            holder.binding.txtStatusAccepted.setVisibility(View.VISIBLE);
        } else if (checkString(model.getApplication_status()).equalsIgnoreCase("Rejected") || checkString(model.getApplication_status()).equalsIgnoreCase("Not Selected")) {
            holder.binding.txtStatus.setTextColor(context.getResources().getColor(R.color.grayLight));
            holder.binding.txtStatusAccepted.setTextColor(context.getResources().getColor(R.color.grayLight));
            holder.binding.txtStatus.setVisibility(View.VISIBLE);
            holder.binding.txtStatusAccepted.setVisibility(View.GONE);
        }

        holder.binding.imgBookmarkAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitBookmark(model.getId(), holder.binding.imgBookmarkAdd, holder.binding.imgBookmarkRemove, fromBookmarkAdd);
            }
        });

        holder.binding.imgBookmarkRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitBookmark(model.getId(), holder.binding.imgBookmarkAdd, holder.binding.imgBookmarkRemove, fromBookmarkRemove);
            }
        });

        holder.binding.imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "Job Description - \n" +
                        "Skills : " + model.getSkills() + "\n" +
                        "Website : " + model.getWebsite_link() + "\n" +
                        "Location : " + model.getJob_location() + " " + model.getCategory() + "\n" +
                        "Experience : " + holder.binding.txtExperience.getText().toString() + "\n" +
                        "Status : " + model.getApplied();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, message);
                sendIntent.setType("text/plain");
                context.startActivity(sendIntent);
            }
        });

        holder.binding.lnrJobView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, JobDetailsActivity.class);
                intent.putExtra(Config.JOB_ID, model.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return appliedJobModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityMyJobAppliedListBinding binding;

        public viewHolder(@NonNull ActivityMyJobAppliedListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private void hitBookmark(String job_id, ImageView imgAdd, ImageView imgRemove, int from) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.job_id, job_id);

        Api.newApi(context, API.BaseUrl + "job/bookmark_job").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        if (from == fromBookmarkAdd) {
                            imgAdd.setVisibility(View.GONE);
                            imgRemove.setVisibility(View.VISIBLE);
                        } else if (from == fromBookmarkRemove) {
                            imgAdd.setVisibility(View.VISIBLE);
                            imgRemove.setVisibility(View.GONE);
                        }
                        H.showMessage(context, json.getString(P.msg));

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitBookmark", token);
    }
}
