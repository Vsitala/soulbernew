package com.soul.soulber.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityLabListBinding;
import com.soul.soulber.model.LabModel;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.text.DecimalFormat;
import java.util.List;

public class LabAdapter extends RecyclerView.Adapter<LabAdapter.viewHolder> {

    private Context context;
    private List<LabModel> labModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    public LabAdapter(Context context, List<LabModel> labModelList) {
        this.context = context;
        this.labModelList = labModelList;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityLabListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_lab_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        LabModel model = labModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString(model.getLab_name()));

        String labBio = checkString("Bio:") + " " + checkString(model.getBio());
        labBio = labBio.replace("Bio:", "<font color='#000'>" + "<b>" + "Bio:" + "</b>" + "</font>");
        holder.binding.txtBio.setText(Html.fromHtml(labBio));

        holder.binding.txtCity.setText("City : "+checkString(model.getCity()));
        holder.binding.txtAddress.setText(checkString(model.getAddress()));

        holder.binding.txtBookAppointment.setText(model.getButton_name());

        holder.binding.txtBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkString(model.getBooking_allowed()).equals("1")) {
                    hitAppointmentData(model, holder.binding.txtBookAppointment);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return labModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityLabListBinding binding;

        public viewHolder(@NonNull ActivityLabListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private void hitAppointmentData(LabModel model, TextView textView) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.lab_id, model.getId());
        Api.newApi(context, API.BaseUrl + "lab/appointment_lab").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        textView.setText("Appointment Requested");
                        model.setBooking_allowed("0");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitAppointmentData", token);
    }

}
