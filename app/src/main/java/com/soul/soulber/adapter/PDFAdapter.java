package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityPdfListBinding;
import com.soul.soulber.fragment.LabPastFragment;
import com.soul.soulber.fragment.LabUnConformFragment;
import com.soul.soulber.fragment.LabUpcomingFragment;
import com.soul.soulber.model.LabHistoryModel;
import com.soul.soulber.model.PDFModel;
import com.soul.soulber.util.Click;

import java.util.List;

public class PDFAdapter extends RecyclerView.Adapter<PDFAdapter.viewHolder> {

    private Context context;
    private List<PDFModel> pdfModelList;
    private int value = 0;

    private LabUpcomingFragment labUpcomingFragment;
    private LabPastFragment labPastFragment;
    private LabUnConformFragment labUnConformFragment;

    public interface onClick {
        void onDownload(PDFModel model);
    }

    public PDFAdapter(Context context, List<PDFModel> pdfModelList, LabUpcomingFragment labUpcomingFragment) {
        this.context = context;
        this.pdfModelList = pdfModelList;
        this.labUpcomingFragment = labUpcomingFragment;
        value = 1;
    }

    public PDFAdapter(Context context, List<PDFModel> pdfModelList, LabPastFragment labPastFragment) {
        this.context = context;
        this.pdfModelList = pdfModelList;
        this.labPastFragment = labPastFragment;
        value = 2;
    }

    public PDFAdapter(Context context, List<PDFModel> pdfModelList, LabUnConformFragment labUnConformFragment) {
        this.context = context;
        this.pdfModelList = pdfModelList;
        this.labUnConformFragment = labUnConformFragment;
        value = 3;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityPdfListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_pdf_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        PDFModel model = pdfModelList.get(position);
        holder.binding.txtPDF.setText(checkString(model.getReport_title()));
        holder.binding.txtPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                download(model);
            }
        });
    }

    private void download(PDFModel model) {
        if (value == 1) {
            ((LabUpcomingFragment) labUpcomingFragment).onDownload(model);
        } else if (value == 2) {
            ((LabPastFragment) labPastFragment).onDownload(model);
        } else if (value == 3) {
            ((LabUnConformFragment) labUnConformFragment).onDownload(model);
        }
    }


    @Override
    public int getItemCount() {
        return pdfModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityPdfListBinding binding;

        public viewHolder(@NonNull ActivityPdfListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

}
