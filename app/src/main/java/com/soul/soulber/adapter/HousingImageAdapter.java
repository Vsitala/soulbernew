package com.soul.soulber.adapter;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.github.chrisbanes.photoview.PhotoView;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityHousingImageListBinding;
import com.soul.soulber.model.ImageModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class HousingImageAdapter extends RecyclerView.Adapter<HousingImageAdapter.viewHolder> {

    private Context context;
    private List<ImageModel> imageModelList;
    private int from = 0;

    public HousingImageAdapter(Context context, List<ImageModel> imageModelList, int from) {
        this.context = context;
        this.imageModelList = imageModelList;
        this.from = from;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityHousingImageListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_housing_image_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        ImageModel model = imageModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgHouse1, model.getImage(), context.getResources().getDrawable(R.drawable.ic_no_image));
        holder.binding.imgHouse1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(model.getImage());
            }
        });

        LoadImage.glideString(context, holder.binding.imgHouse2, model.getImage(), context.getResources().getDrawable(R.drawable.ic_no_image));
        holder.binding.imgHouse2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(model.getImage());
            }
        });

        LoadImage.glideString(context, holder.binding.imgHouse0, model.getImage(), context.getResources().getDrawable(R.drawable.ic_no_image));
        holder.binding.imgHouse0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewImageDialog(model.getImage());
            }
        });

        if (from == 1) {
            holder.binding.carLow.setVisibility(View.VISIBLE);
            holder.binding.carHigh.setVisibility(View.GONE);
            holder.binding.carStart.setVisibility(View.GONE);
        } else if (from == 2) {
            holder.binding.carHigh.setVisibility(View.VISIBLE);
            holder.binding.carLow.setVisibility(View.GONE);
            holder.binding.carStart.setVisibility(View.GONE);
        } else if (from == 3) {
            holder.binding.carStart.setVisibility(View.VISIBLE);
            holder.binding.carHigh.setVisibility(View.GONE);
            holder.binding.carLow.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return imageModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityHousingImageListBinding binding;

        public viewHolder(@NonNull ActivityHousingImageListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private void viewImageDialog(String imagePath) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_image_view);

        PhotoView imageView = dialog.findViewById(R.id.imageView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        LoadImage.glideString(context, imageView, imagePath, context.getResources().getDrawable(R.drawable.ic_no_image));

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.white);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);

    }

}
