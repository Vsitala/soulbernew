package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.JobsHouseFriends.JobsFragment;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityJobCategoryListBinding;
import com.soul.soulber.model.JobCategoryModel;

import java.util.List;

public class JobCategoryAdapter extends RecyclerView.Adapter<JobCategoryAdapter.viewHolder> {

    private Context context;
    private List<JobCategoryModel> jobCategoryModelList;
    private int lastPosition = -1;
    private boolean firstTime = false;
    private JobsFragment fragment;

    public interface onClick {
        void onCategoryClick(JobCategoryModel model);
    }

    public JobCategoryAdapter(Context context, List<JobCategoryModel> jobCategoryModelList, JobsFragment fragment) {
        this.context = context;
        this.jobCategoryModelList = jobCategoryModelList;
        this.fragment = fragment;
        firstTime = true;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityJobCategoryListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_job_category_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        JobCategoryModel model = jobCategoryModelList.get(position);

        holder.binding.txtCategory.setText(checkString(model.getCategory(), holder.binding.txtCategory));
        holder.binding.txtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((JobsFragment) fragment).onCategoryClick(model);
                lastPosition = position;
                notifyDataSetChanged();
            }
        });

        if (firstTime) {
            firstTime = false;
            holder.binding.txtCategory.setBackground(context.getResources().getDrawable(R.drawable.color_bg));
            holder.binding.txtCategory.setTextColor(context.getResources().getColor(R.color.white));
            ((JobsFragment) fragment).onCategoryClick(model);
        } else {
            if (lastPosition == position) {
                holder.binding.txtCategory.setBackground(context.getResources().getDrawable(R.drawable.color_bg));
                holder.binding.txtCategory.setTextColor(context.getResources().getColor(R.color.white));
            } else {
                holder.binding.txtCategory.setBackground(context.getResources().getDrawable(R.drawable.gray_bg));
                holder.binding.txtCategory.setTextColor(context.getResources().getColor(R.color.inactive));
            }
        }

    }

    @Override
    public int getItemCount() {
        return jobCategoryModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityJobCategoryListBinding binding;

        public viewHolder(@NonNull ActivityJobCategoryListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
