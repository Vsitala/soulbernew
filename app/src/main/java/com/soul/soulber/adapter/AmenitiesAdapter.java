package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityAmenitiesListBinding;
import com.soul.soulber.databinding.ActivityHousingImageListBinding;
import com.soul.soulber.model.AmenitiesModel;

import java.util.List;

public class AmenitiesAdapter extends RecyclerView.Adapter<AmenitiesAdapter.viewHolder> {

    private Context context;
    private List<AmenitiesModel> amenitiesModelList;

    public AmenitiesAdapter(Context context, List<AmenitiesModel> amenitiesModelList) {
        this.context = context;
        this.amenitiesModelList = amenitiesModelList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityAmenitiesListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_amenities_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        AmenitiesModel model = amenitiesModelList.get(position);

        holder.binding.txtAmenities.setText(checkString(model.getAmenities()));
    }

    @Override
    public int getItemCount() {
        return amenitiesModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityAmenitiesListBinding binding;

        public viewHolder(@NonNull ActivityAmenitiesListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

}
