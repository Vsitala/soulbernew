package com.soul.soulber.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.H;
import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityTheraphyItemsBinding;
import com.soul.soulber.model.GuardianTheraphyModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.content.Context.CLIPBOARD_SERVICE;

public class GuardianTheraphyAdapter extends RecyclerView.Adapter<GuardianTheraphyAdapter.viewHolder> {
    private Context context;
    private ArrayList<GuardianTheraphyModel> panicchildlist;

    public GuardianTheraphyAdapter(Context context, ArrayList<GuardianTheraphyModel> panicchildlist) {
        this.context = context;
        this.panicchildlist = panicchildlist;
    }

    @NonNull
    @Override
    public GuardianTheraphyAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityTheraphyItemsBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_theraphy_items, parent, false);
        return new GuardianTheraphyAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull GuardianTheraphyAdapter.viewHolder holder, int position) {
        GuardianTheraphyModel model = panicchildlist.get(position);
        String Remark = model.getRemark();
        LoadImage.glideString(context, holder.binding.ivTherpahypic, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.tvTherapyname.setText("Dr. " + checkString(model.getTherpist_name(), holder.binding.tvTherapyname));
        holder.binding.tvTheraphyexperie.setText(checkString(model.getExperience(), holder.binding.tvTheraphyexperie));
        holder.binding.tvDate.setText("Date : " + checkString(getFormatDate(model.getAppointment_date())));
        holder.binding.tvTime.setText("Time : " + checkString(getFormatTime(model.getAppointment_date())));
        holder.binding.tvDependentname.setText(checkString(model.getUsername(), holder.binding.tvDependentname));
        LoadImage.glideString(context, holder.binding.ivDependentpic, model.getProfilepic(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        if (model.getRemark().equals("") || model.getRemark() == null) {
            holder.binding.ivRemark.setVisibility(View.GONE);
        } else {
            holder.binding.ivRemark.setVisibility(View.VISIBLE);
        }
        holder.binding.ivRemark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
//                H.showMessage(context,Remark);
                Toast toast = Toast.makeText(context,
                        Remark, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();

                String details = "Therapist Name : " + model.getTherpist_name() +
                        "\nExperience : " + model.getExperience() +
                        "\nAppointment Date : " + checkString(getFormatDate(model.getAppointment_date())) +
                        "\nAppointment time : " + checkString(getFormatTime(model.getAppointment_date())) +
                        "\nRemark : " + model.getRemark();
                copyText(details);
            }
        });

    }

    @Override
    public int getItemCount() {
        return panicchildlist.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityTheraphyItemsBinding binding;

        public viewHolder(@NonNull ActivityTheraphyItemsBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd-MM-yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatTime(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("hh.mm a");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

    public void copyText(String text) {
        H.showMessage(context, "Details Copied");
        Object clipboardService = context.getSystemService(CLIPBOARD_SERVICE);
        final ClipboardManager clipboardManager = (ClipboardManager) clipboardService;
        ClipData clipData = ClipData.newPlainText("Details Copied", text);
        clipboardManager.setPrimaryClip(clipData);
    }

}
