package com.soul.soulber.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.ForumDetailsActivity;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityForumListBinding;
import com.soul.soulber.model.ForumModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ForumAdapter extends RecyclerView.Adapter<ForumAdapter.viewHolder> {

    private Context context;
    private List<ForumModel> forumModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    public ForumAdapter(Context context, List<ForumModel> forumModelList) {
        this.context = context;
        this.forumModelList = forumModelList;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);
        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public ForumAdapter.viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityForumListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_forum_list, parent, false);
        return new ForumAdapter.viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ForumAdapter.viewHolder holder, int position) {
        ForumModel model = forumModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgImage, model.getImage(), context.getResources().getDrawable(R.drawable.ic_no_image));
        holder.binding.txtTitle.setText(checkString(model.getForum_text(), holder.binding.txtTitle));
        holder.binding.txtUpVotes.setText(checkString(model.getUpvotes(), holder.binding.txtUpVotes) + " upvotes");
        holder.binding.txtDownVotes.setText(checkString(model.getDownvotes(), holder.binding.txtDownVotes) + " downvotes");
        holder.binding.txtComment.setText(checkString(model.getComments(), holder.binding.txtComment) + " comments");
        holder.binding.txtDate.setText("Posted on " + checkString(getFormatDate(model.getAdd_date()), holder.binding.txtDate));

        holder.binding.lnrForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(context, ForumDetailsActivity.class);
                intent.putExtra(P.id, model.getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return forumModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityForumListBinding binding;

        public viewHolder(@NonNull ActivityForumListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String getFormatDate(String actualDate) {
        String app_date = "";
        try {
            app_date = actualDate;
            DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
            Date date = (Date) formatter.parse(app_date);
            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM, yyyy");
            String finalString = newFormat.format(date);
            app_date = finalString;
        } catch (Exception e) {
            app_date = actualDate;
        }

        return app_date;
    }

}
