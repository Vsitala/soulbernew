package com.soul.soulber.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.soul.soulber.R;
import com.soul.soulber.databinding.ActivityPartnerListBinding;
import com.soul.soulber.model.PartnerModel;
import com.soul.soulber.util.LoadImage;

import java.util.List;

public class OurPartnersAdapter extends RecyclerView.Adapter<OurPartnersAdapter.viewHolder> {

    private Context context;
    private List<PartnerModel> partnerModelList;


    public OurPartnersAdapter(Context context, List<PartnerModel> partnerModelList) {
        this.context = context;
        this.partnerModelList = partnerModelList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityPartnerListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_partner_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        PartnerModel model = partnerModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgCompany, model.getLogo(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));
        holder.binding.txtName.setText(model.getPartner());
        holder.binding.txtAbout.setText(checkString(model.getAbout_partner(),holder.binding.txtAbout));

    }

    @Override
    public int getItemCount() {
        return partnerModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityPartnerListBinding binding;

        public viewHolder(@NonNull ActivityPartnerListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string, LinearLayout lnr) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            lnr.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

    private String checkString(String string, TextView textView) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            textView.setVisibility(View.GONE);
        } else {
            value = string;
        }
        return value;
    }

}
