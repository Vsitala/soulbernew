package com.soul.soulber.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityAmenitiesListBinding;
import com.soul.soulber.databinding.ActivityTherapistListBinding;
import com.soul.soulber.model.AmenitiesModel;
import com.soul.soulber.model.TherapistModel;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.text.DecimalFormat;
import java.util.List;

public class TherapistAdapter extends RecyclerView.Adapter<TherapistAdapter.viewHolder> {

    private Context context;
    private List<TherapistModel> therapistModelList;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    public TherapistAdapter(Context context, List<TherapistModel> therapistModelList) {
        this.context = context;
        this.therapistModelList = therapistModelList;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ActivityTherapistListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.activity_therapist_list, parent, false);
        return new viewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        TherapistModel model = therapistModelList.get(position);

        LoadImage.glideString(context, holder.binding.imgUser, model.getImage(), context.getResources().getDrawable(R.drawable.ic_baseline_person_24));

        holder.binding.txtName.setText(checkString("Dr. " + model.getName()));
        if (checkString(model.getExperience()).contains("year")) {
            holder.binding.txtExperience.setText(checkString(model.getExperience() + " experience"));
        } else {
            holder.binding.txtExperience.setText(checkString(model.getExperience() + " year experience"));
        }

        String labBio = checkString("Bio:") + " " + checkString(model.getBio());
        labBio = labBio.replace("Bio:", "<font color='#000'>" + "<b>" + "Bio:" + "</b>" + "</font>");
        holder.binding.txtBio.setText(Html.fromHtml(labBio));

        holder.binding.txtCity.setText(checkString(model.getCity()));
        holder.binding.txtClenic.setText(checkString(model.getClinic_name()));
        holder.binding.txtCharges.setText(checkString("Rs." + model.getConsultation_fees() + " consultation fees"));

        if (!TextUtils.isEmpty(checkString(model.getDistance()))) {
            double distance = Double.parseDouble(model.getDistance());
            distance = Double.parseDouble(new DecimalFormat("##.##").format(distance));
            holder.binding.txtDistance.setText(distance + "km");
        } else {
            holder.binding.txtDistance.setText("0.0" + "km");
        }

        if (checkString(model.getIs_connected()).equals("1")) {
            holder.binding.txtConnect.setText("Requested");
        } else {
            holder.binding.txtConnect.setText("Connect");
        }

        holder.binding.txtBookAppointment.setText(model.getButton_name());

        holder.binding.txtConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkString(model.getIs_connected()).equals("1")) {
                    hitConnectionData(model, holder.binding.txtConnect);
                }
            }
        });

        holder.binding.txtBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkString(model.getBooking_allowed()).equals("1")) {
                    hitAppointmentData(model, holder.binding.txtBookAppointment);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return therapistModelList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ActivityTherapistListBinding binding;

        public viewHolder(@NonNull ActivityTherapistListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private void hitConnectionData(TherapistModel model, TextView textView) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.therapist_id, model.getId());
        Api.newApi(context, API.BaseUrl + "therapist/connect_therapist").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        textView.setText("Requested");
                        model.setIs_connected("1");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitConnectionData", token);
    }

    private void hitAppointmentData(TherapistModel model, TextView textView) {

        ProgressView.show(context, loadingDialog);

        Json j = new Json();
        j.addString(P.therapist_id, model.getId());
        Api.newApi(context, API.BaseUrl + "therapist/appointment_therapist").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        textView.setText("Appointment Requested");
                        model.setBooking_allowed("0");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitAppointmentData", token);
    }

}
