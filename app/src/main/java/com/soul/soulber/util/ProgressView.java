package com.soul.soulber.util;

import android.content.Context;

import com.adoisstudio.helper.LoadingDialog;

public class ProgressView {

    public static void show(Context context, LoadingDialog loadingDialog) {
        try {
            if (loadingDialog != null) {
                loadingDialog.show("Please wait...");
            }
        } catch (Exception e) {

        }

    }

    public static void dismiss(LoadingDialog loadingDialog) {
        try {
            if (loadingDialog != null) {
                loadingDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }

}
