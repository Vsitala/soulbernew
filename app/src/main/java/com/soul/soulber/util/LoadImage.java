package com.soul.soulber.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;

public class LoadImage {

    public static void glide(Context context, ImageView imageView, Drawable imagePath,Drawable error){

        Glide.with(context).load(imagePath)
                .error(error)
                .apply(new RequestOptions()
                        .fitCenter()
                        .format(DecodeFormat.PREFER_ARGB_8888)
                        .override(Target.SIZE_ORIGINAL))
                .into(imageView);
    }

    public static void glideString(Context context, ImageView imageView, String imagePath,Drawable error){

        if(!TextUtils.isEmpty(imagePath) && !imagePath.equals("null")){
            Glide.with(context).load(imagePath)
                    .placeholder(error)
                    .error(error)
                    .apply(new RequestOptions()
                            .fitCenter()
                            .format(DecodeFormat.PREFER_ARGB_8888)
                            .override(Target.SIZE_ORIGINAL))
                    .into(imageView);
        }else {
            Glide.with(context).load(error)
                    .apply(new RequestOptions()
                            .fitCenter()
                            .format(DecodeFormat.PREFER_ARGB_8888)
                            .override(Target.SIZE_ORIGINAL))
                    .into(imageView);
        }

    }

    public static void glideString(Context context, PhotoView imageView, String imagePath,Drawable error) {

        try {
            if (!TextUtils.isEmpty(imagePath) && !imagePath.equals("null") && context!=null) {
                Glide.with(context).load(imagePath)
//                    .placeholder(R.drawable.progress_animation)
                        .placeholder(error)
                        .error(error)
                        .apply(new RequestOptions()
                                .fitCenter()
                                .format(DecodeFormat.PREFER_ARGB_8888)
                                .override(Target.SIZE_ORIGINAL))
                        .into(imageView);
            } else {
                Glide.with(context).load(error)
                        .apply(new RequestOptions()
                                .fitCenter()
                                .format(DecodeFormat.PREFER_ARGB_8888)
                                .override(Target.SIZE_ORIGINAL))
                        .into(imageView);
            }
        } catch (Exception e) {

        }

    }



}
