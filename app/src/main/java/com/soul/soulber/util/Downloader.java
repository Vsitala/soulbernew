package com.soul.soulber.util;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;

import com.adoisstudio.helper.H;
import com.soul.soulber.api.Config;

import java.io.File;

public class Downloader {

    public static String FOLDER_PATH = "/Soulber/Download/";

    public static void download(Context context, String fileURL, String title, int flag) {
        checkDirectory(context, fileURL, title, flag);

    }

    public static void checkDirectory(Context context, String fileURL, String title, int flag) {
        try {
            String extension = "";
            if (fileURL.contains(".")) {
                extension = fileURL.substring(fileURL.lastIndexOf("."));
            }
            String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + FOLDER_PATH;
            String fileName = title + extension;
            destination += fileName;
            File direct = new File(destination);
            if (direct.exists()) {
                if (flag == Config.SHARE) {
                    share(context, destination);
                } else if (flag == Config.OPEN) {
                    openFile(context, destination);
                }
            } else {
                startDownload(context, fileURL, title, destination, flag);
            }

        } catch (Exception e) {
            H.showMessage(context, "Something went wrong, try again");
        }

    }

    public static void startDownload(Context context, String fileURL, String title, String destination, int flag) {
        H.showMessage(context, "Downloading....");
        final Uri uri = Uri.parse("file://" + destination);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(fileURL));
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDescription("Downloading....");
        request.setTitle(title);
        request.setDestinationUri(uri);

        final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);
        BroadcastReceiver onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                H.showMessage(ctxt, "Download complete");
                context.unregisterReceiver(this);
                if (flag == Config.SHARE) {
                    share(context, destination);
                } else if (flag == Config.OPEN) {
                    openFile(context, destination);
                }
            }
        };
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public static void share(Context context, String destination) {
        try {

            String extension = "";
            if (destination.contains(".")) {
                extension = destination.substring(destination.lastIndexOf("."));
                extension = extension.replace(".", "");
            }

            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            File fileWithinMyDir = new File(destination);

            if (fileWithinMyDir.exists()) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                intentShareFile.setType("application/*");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + destination));
                intentShareFile.putExtra(Intent.EXTRA_SUBJECT, "Sharing File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File...");
                context.startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }
        } catch (Exception e) {
            H.showMessage(context, "Unable to share application, try again.");
        }
    }

    public static void openFile(Context context, String filepath) {
        try {

            String extension = "";
            if (filepath.contains(".")) {
                extension = filepath.substring(filepath.lastIndexOf("."));
                extension = extension.replace(".", "");
            }

            File file = new File(filepath);
            Uri uri = Uri.fromFile(file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/*");    // or use */*
            context.startActivity(intent);
        } catch (Exception e) {
            H.showMessage(context, "Unable to open application, try again.");
        }
    }

}
