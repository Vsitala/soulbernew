package com.soul.soulber;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.util.ConnectionUtil;
import com.soul.soulber.util.ProgressView;

public class Splashscreen extends AppCompatActivity {

    private Splashscreen activity = this;
    private Session session;
    private LoadingDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        intView();
    }

    private void intView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        if (ConnectionUtil.isOnline(activity)) {
            hitInitData();
        } else {
            noInternetDialog();
        }

    }

    private void noInternetDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        builder1.setTitle("Internet Alert..!");
        builder1.setMessage("No internet connection available, try again.");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void jumpToPage() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (session.getBool(Config.userLogin)) {
                    Intent mainIntent = new Intent(activity, BaseAcivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                    finish();
                } else {
                    Intent mainIntent = new Intent(activity, WelcomeActivity.class);
                    mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                    finish();
                }

            }
        }, 3000);
    }


    public int getCurrentVersionCode() {
        int v = 0;
        try {
            v = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {

        }
        return v;
    }


    private void showUpdateDialog() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        adb.setTitle("Update Alert..!");
        adb.setMessage("New update available, please updated.");
        adb.setCancelable(false);

        adb.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                redirectToPlayStore();
            }
        }).show();
    }

    private void redirectToPlayStore() {

        try {
            Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id=com.soul.soulber");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
            startActivity(marketIntent);
        } catch (ActivityNotFoundException e) {
            H.showMessage(activity, "Something went wrong while updating, try again");
        }

        finish();
    }

    private void hitInitData() {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "common/init")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        int latestVersionCode = data.getInt(P.android_min_version);
                        if (latestVersionCode > getCurrentVersionCode()) {
                            showUpdateDialog();
                        } else {
                            jumpToPage();
                        }
                    } else {

                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitInitData");
    }
}