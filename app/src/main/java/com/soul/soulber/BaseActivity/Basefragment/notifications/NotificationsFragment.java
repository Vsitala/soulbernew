package com.soul.soulber.BaseActivity.Basefragment.notifications;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.NotificationAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentNotificationsBinding;
import com.soul.soulber.model.NotificationModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;


public class NotificationsFragment extends Fragment {

    private FragmentNotificationsBinding binding;
    private Context context;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private List<NotificationModel> notificationModelList;
    private NotificationAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notifications, container, false);
            context = inflater.getContext();

            initView();
        }

        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        notificationModelList = new ArrayList<>();
        adapter = new NotificationAdapter(getActivity(), notificationModelList);
        binding.recyclerNotification.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recyclerNotification.setHasFixedSize(true);
        binding.recyclerNotification.setItemViewCacheSize(notificationModelList.size());
        binding.recyclerNotification.setAdapter(adapter);

        hitNotificationData();

    }

    private void hitNotificationData() {

        ProgressView.show(context, loadingDialog);

        Api.newApi(context, API.BaseUrl + "Notifications/notification_list")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        JsonList jsonList = json.getJsonList(P.data);

                        if (jsonList != null && jsonList.size() != 0) {
                            for (Json jsonValue : jsonList) {
                                NotificationModel model = new NotificationModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setTitle(jsonValue.getString(P.title));
                                model.setDescription(jsonValue.getString(P.description));
                                model.setImage(jsonValue.getString(P.image));
                                model.setAction(jsonValue.getString(P.action));
                                model.setAction_data(jsonValue.getString(P.action_data));
                                model.setUser_type(jsonValue.getString(P.user_type));
                                model.setStatus(jsonValue.getString(P.status));
                                model.setDelete_flag(jsonValue.getString(P.delete_flag));
                                model.setNotification_status(jsonValue.getString(P.notification_status));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setFcm_token(jsonValue.getString(P.fcm_token));

                                notificationModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        H.showMessage(context, json.getString(P.error));
                        checkData();
                    }
                })
                .run("hitNotificationData", token);
    }

    private void checkData() {
        if (notificationModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }
}