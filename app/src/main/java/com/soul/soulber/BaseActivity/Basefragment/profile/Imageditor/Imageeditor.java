package com.soul.soulber.BaseActivity.Basefragment.profile.Imageditor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.soul.soulber.BaseActivity.Basefragment.profile.Imageselector.ImagesActivity;
import com.soul.soulber.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class Imageeditor extends AppCompatActivity {
    private static int VIEW_TYPE;
    static
    Imagefilter processor;
    public static final int Uploadprofilepic = 1;
    public static final int feedspost = 0;
    ImageView onback_editorimage, oneIV, twoIV, threeIV, fourIV, originalIV;
    Bitmap oneBitMap, twoBitMap, threeBitmap, fourBitMap;
    TextView upload;
    String fileUri;
    LinearLayout ll_filters, ll_editcontroller, filtersclick, editclick;
    View filters_view, edit_view;
    SeekBar SeekBar_contrast;
    SeekBar SeekBar_brightness;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageeditor);
        originalIV = findViewById(R.id.idIVOriginalImage);
        processor = new Imagefilter();
        oneIV = findViewById(R.id.idIVOne);
        twoIV = findViewById(R.id.idIVTwo);
        threeIV = findViewById(R.id.idIVThree);
        fourIV = findViewById(R.id.idIVFour);
        upload = findViewById(R.id.done_upload);
        onback_editorimage= findViewById(R.id.onback_editorimage);
        ll_filters = findViewById(R.id.ll_filters);
        ll_editcontroller = findViewById(R.id.ll_editcontroller);
        filters_view = findViewById(R.id.filters_view);
        edit_view = findViewById(R.id.edit_view);
        filtersclick = findViewById(R.id.filtersclick);
        editclick = findViewById(R.id.editclick);
        SeekBar_contrast = findViewById(R.id.seekBarContrast);
        SeekBar_brightness = findViewById(R.id.seekBarBrightness);
        Glide.with(this)
                .asBitmap()
                .load(getIntent().getStringExtra("image"))
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(final Bitmap bitmap, Transition<? super Bitmap> transition) {
                        originalIV.setImageBitmap(bitmap);
                        oneIV.setImageBitmap(oneBitMap);
                        threeBitmap = processor.toSepiaNice(bitmap);
                        threeIV.setImageBitmap(threeBitmap);
                        fourBitMap = processor.balckandwhite(bitmap);
                        fourIV.setImageBitmap(fourBitMap);
                        oneBitMap = processor.tintImage(bitmap, 11);
                        oneIV.setImageBitmap(oneBitMap);
                        twoIV.setImageBitmap(bitmap);
                        oneIV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                oneBitMap = processor.tintImage(bitmap, 20);
                                originalIV.setImageBitmap(oneBitMap);
                            }
                        });

                        twoIV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                originalIV.setImageBitmap(bitmap);

                            }
                        });

                        threeIV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                threeBitmap = processor.toSepiaNice(bitmap);
                                originalIV.setImageBitmap(threeBitmap);
                            }
                        });

                        fourIV.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                fourBitMap = processor.balckandwhite(bitmap);
                                originalIV.setImageBitmap(fourBitMap);
                            }
                        });

                        upload.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                switch (VIEW_TYPE) {
                                    case Uploadprofilepic:
                                        try {
                                            File mydir = new File(Environment.getExternalStorageDirectory(), "Soulbaruploads");
                                            if (!mydir.exists()) {
                                                mydir.mkdirs();
                                                Toast.makeText(getApplicationContext(), "Directory Created", Toast.LENGTH_LONG).show();
                                            }
                                            fileUri = mydir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpeg";
                                            FileOutputStream outputStream = new FileOutputStream(fileUri);
                                            Bitmap bm = ((BitmapDrawable) originalIV.getDrawable()).getBitmap();
                                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                            byte[] byteArray = stream.toByteArray();
                                            try {
                                                Intent intent = new Intent();
                                                intent.putExtra("datas",byteArray);
                                                setResult(24444,intent);
                                                finish();
                                            }catch (Exception e){
                                                Toast.makeText(Imageeditor.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    //    Toast.makeText(getApplicationContext(), "Image Saved" + fileUri, Toast.LENGTH_LONG).show();
                                        break;
                                    case feedspost:
                                        try {
                                            File mydir = new File(Environment.getExternalStorageDirectory(), "Soulbaruploads");
                                            if (!mydir.exists()) {
                                                mydir.mkdirs();
                                                Toast.makeText(getApplicationContext(), "Directory Created", Toast.LENGTH_LONG).show();
                                            }
                                            fileUri = mydir.getAbsolutePath() + File.separator + System.currentTimeMillis() + ".jpg";
                                            FileOutputStream outputStream = new FileOutputStream(fileUri);
                                            Bitmap bm = ((BitmapDrawable) originalIV.getDrawable()).getBitmap();
                                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                                            bm.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                                            try {
                                                Intent intent = new Intent();
                                                intent.putExtra("registered",false);
                                                setResult(2,intent);
                                                finish();
                                            }catch (Exception e){
                                                Toast.makeText(Imageeditor.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        Toast.makeText(getApplicationContext(), "Image Saved" + fileUri, Toast.LENGTH_LONG).show();
                                        break;
                                }


                            }
                        });

                        //  SeekBar_contrast.setMax(50);
                        // SeekBar_brightness.setMax(10);
                        // SeekBar_contrast.setProgress((150));
                        //   SeekBar_brightness.setProgress((20));
                        SeekBar_brightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                                Bitmap bn = processor.changeBitmapContrastBrightness(bitmap, (float) progress / 60f, 1);
//                                originalIV.setImageBitmap(bn);
                                originalIV.setColorFilter(Imagefilter.getContrastBrightnessFilter((float) progress / 60f, 1));

                            }

                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });
                        SeekBar_contrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                originalIV.setColorFilter(Imagefilter.setContrast((float) progress / 150f));
//                                Bitmap bn = processor.changeBitmapContrastBrightness(bitmap, (float) (progress + 20) / 200, 0);
//                                originalIV.setImageBitmap(bn);
                            }


                            @Override
                            public void onStartTrackingTouch(SeekBar seekBar) {
                            }

                            @Override
                            public void onStopTrackingTouch(SeekBar seekBar) {

                            }
                        });


                    }

                    @Override
                    public void onLoadCleared(Drawable placeholder) {


                    }
                });
        filtersclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filters_view.setVisibility(View.VISIBLE);
                ll_filters.setVisibility(View.VISIBLE);
                edit_view.setVisibility(View.GONE);
                ll_editcontroller.setVisibility(View.GONE);

            }
        });
        editclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_filters.setVisibility(View.GONE);
                filters_view.setVisibility(View.GONE);
                edit_view.setVisibility(View.VISIBLE);
                ll_editcontroller.setVisibility(View.VISIBLE);


            }
        });
        onback_editorimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Imageeditor.this, ImagesActivity.class);
                startActivity(intent);
                finish();

            }
        });



    }

    public void imageedito(int type) {
        VIEW_TYPE = type;
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Imageeditor.this, ImagesActivity.class);
        startActivity(intent);
        finish();
    }



}