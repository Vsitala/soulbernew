package com.soul.soulber.BaseActivity.Basefragment.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.R;
import com.soul.soulber.SpannedGridLayoutManagerNew;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.FollowersActivity;
import com.soul.soulber.activity.FollowingActivity;
import com.soul.soulber.activity.GuardianConnectionActivity;
import com.soul.soulber.adapter.UserGreedPostAdapter;
import com.soul.soulber.adapter.UserVerticalPostAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentProfileBinding;
import com.soul.soulber.model.UserPostModel;
import com.soul.soulber.model.Userdetailsmodel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment implements UserGreedPostAdapter.onClick {

    public static final int Post = 1;
    private SlideUp slideUp;
    ArrayList<DependentFeedsModelclass> modlelist;
    LoadingDialog loadingDialog;
    private String token;
    private Session session;
    private String usertype_id;
    private String userID;
    ArrayList<Userdetailsmodel> usersdata;
    View root;
    RelativeLayout slideView;
    FragmentProfileBinding binding;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";

    private TextView txtLogOut;
    private TextView txtBlockUser;
    private TextView txtReportUser;
    private TextView txtMessage;

    private List<UserPostModel> userPostModelList;
    private UserVerticalPostAdapter userPostAdapter;
    private UserGreedPostAdapter userGreedPostAdapter;

    private SpannedGridLayoutManagerNew greedLayout;
    private boolean loading1 = true;
    private boolean loading2 = true;
    int pastVisiblesItems1, visibleItemCount1, totalItemCount1;
    int pastVisiblesItems2, visibleItemCount2, totalItemCount2;
    LinearLayoutManager linearLayoutManagerNew;
    int count;
    int pageCount = 1;

    private String profile_pic = "";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        root = binding.getRoot();

        iniView();
        return root;
    }

    private void iniView() {

        loadingDialog = new LoadingDialog(getContext());
        session = new Session(getContext());

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        txtMessage = root.findViewById(R.id.txtMessage);
        txtReportUser = root.findViewById(R.id.txtReportUser);
        txtBlockUser = root.findViewById(R.id.txtBlockUser);
        txtLogOut = root.findViewById(R.id.txtLogOut);

        txtMessage.setVisibility(View.GONE);
        txtReportUser.setVisibility(View.GONE);
        txtBlockUser.setVisibility(View.GONE);

        slideView = root.findViewById(R.id.slideViewreport);
        slideUp = new SlideUpBuilder(slideView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {
                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        binding.dimprofile.setVisibility(View.VISIBLE);
                        binding.dimprofile.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                slideUp.hide();
                            }
                        });
                        if (visibility == View.GONE) {
                            binding.dimprofile.setVisibility(View.GONE);
                        }
                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .withSlideFromOtherView(root.findViewById(R.id.slideViewreport))
                .build();

        modlelist = new ArrayList<>();
        usersdata = new ArrayList<>();

        binding.ivUsersmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideUp.show();
            }
        });

        binding.imgGreed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.imgGreed.setColorFilter(ContextCompat.getColor(getContext(), R.color.lightgreen));
                    binding.imgList.setColorFilter(ContextCompat.getColor(getContext(), R.color.inactive));
                } catch (Exception e) {
                }
                binding.recyclerHorizontalPost.setVisibility(View.VISIBLE);
                binding.recyclerVerticalPost.setVisibility(View.GONE);
            }
        });


        binding.imgList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    binding.imgList.setColorFilter(ContextCompat.getColor(getContext(), R.color.lightgreen));
                    binding.imgGreed.setColorFilter(ContextCompat.getColor(getContext(), R.color.inactive));
                } catch (Exception e) {
                }
                binding.recyclerVerticalPost.setVisibility(View.VISIBLE);
                binding.recyclerHorizontalPost.setVisibility(View.GONE);
            }
        });

        binding.tvEditprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileEditActivity.class);
                startActivity(intent);
            }
        });

        onClick();
        setListData();

    }

    private void setListData() {

        profile_pic = imageBaseUrl + session.getJson(Config.userProfileData).getString(P.profile_pic);
        userPostModelList = new ArrayList<>();
        userPostAdapter = new UserVerticalPostAdapter(getActivity(), userPostModelList, profile_pic);
        linearLayoutManagerNew = new LinearLayoutManager(getActivity());
//        SnapHelper snapHelper = new PagerSnapHelper();
        binding.recyclerVerticalPost.setLayoutManager(linearLayoutManagerNew);
//        snapHelper.attachToRecyclerView(binding.recyclerVerticalPost);
        binding.recyclerVerticalPost.setHasFixedSize(true);
        binding.recyclerVerticalPost.setItemViewCacheSize(userPostModelList.size());
        binding.recyclerVerticalPost.setAdapter(userPostAdapter);

        greedLayout = new SpannedGridLayoutManagerNew(
                new SpannedGridLayoutManagerNew.GridSpanLookup() {
                    @Override
                    public SpannedGridLayoutManagerNew.SpanInfo getSpanInfo(int position) {

                        if (position % 10 == 0 || position % 10 == 7) {
                            return new SpannedGridLayoutManagerNew.SpanInfo(2, 2);
                        } else {
                            return new SpannedGridLayoutManagerNew.SpanInfo(1, 1);
                        }
                    }
                },
                4,
                1f
        );

        userGreedPostAdapter = new UserGreedPostAdapter(getActivity(), userPostModelList, ProfileFragment.this);
        binding.recyclerHorizontalPost.setLayoutManager(greedLayout);
        binding.recyclerHorizontalPost.setHasFixedSize(true);
        binding.recyclerHorizontalPost.setItemViewCacheSize(userPostModelList.size());
        binding.recyclerHorizontalPost.setAdapter(userGreedPostAdapter);

        hitPostData(pageCount);
        setPagination();
    }

    private void setPagination() {
        binding.recyclerVerticalPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading1 = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount1 = linearLayoutManagerNew.getChildCount();
                totalItemCount1 = linearLayoutManagerNew.getItemCount();
                pastVisiblesItems1 = linearLayoutManagerNew.findFirstVisibleItemPosition();

                if (loading1 && (visibleItemCount1 + pastVisiblesItems1 == totalItemCount1)) {
                    loading1 = false;
                    if (userPostModelList != null && !userPostModelList.isEmpty()) {
                        if (userPostModelList.size() < count) {
                            pageCount++;
                            hitPostData(pageCount);
                        }
                    }
                }
            }
        });


        binding.recyclerHorizontalPost.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading2 = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount2 = greedLayout.getChildCount();
                totalItemCount2 = greedLayout.getItemCount();
                pastVisiblesItems2 = greedLayout.getFirstVisibleItemPosition();

                if (loading2 && (visibleItemCount2 + pastVisiblesItems2 == totalItemCount2)) {
                    loading2 = false;
                    if (userPostModelList != null && !userPostModelList.isEmpty()) {
                        if (userPostModelList.size() < count) {
                            pageCount++;
                            hitPostData(pageCount);
                        }
                    }
                }
            }
        });

    }

    @Override
    public void playVideo(String path) {
        ((BaseAcivity) getActivity()).playerVideoDialog(path);
    }

    private void hitPostData(int pageCount) {

        ProgressView.show(getActivity(), loadingDialog);

        String api = "";
        Json j = new Json();
        j.addString(P.page, pageCount + "");

        j.addString(P.user_id, userID);
        api = "Posts/viewposts_by_userid";

        Api.newApi(getActivity(), API.BaseUrl + api).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String posts_img_path = data.getString(P.posts_img_path);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        String post_video_path = data.getString(P.post_video_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        JsonList posts = data.getJsonList(P.posts);
                        if (posts != null && posts.size() != 0) {
                            for (Json jsonValue : posts) {
                                UserPostModel model = new UserPostModel();
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setName(jsonValue.getString(P.name));
                                model.setPost_id(jsonValue.getString(P.post_id));
                                model.setMessage(jsonValue.getString(P.message));
                                model.setPost_img(posts_img_path + "/" + jsonValue.getString(P.post_img));
                                model.setPost_date(jsonValue.getString(P.post_date));
                                model.setLikes(jsonValue.getString(P.likes));
                                model.setComments(jsonValue.getString(P.comments));
                                model.setIs_liked_by_me(jsonValue.getString(P.is_liked_by_me));
                                model.setProfile_pic(profile_pic_path + "/" + jsonValue.getString(P.profile_pic));
                                model.setUpload_type(jsonValue.getString(P.upload_type));
                                model.setFont_style(jsonValue.getString(P.font_style));
                                model.setFont_color(jsonValue.getString(P.font_color));
                                model.setSubtitle(jsonValue.getString(P.subtitle));
                                model.setPost_video(post_video_path + "/" + jsonValue.getString(P.post_video));
                                model.setPaying(false);
                                userPostModelList.add(model);
                            }
                            userPostAdapter.notifyDataSetChanged();
                            userGreedPostAdapter.notifyDataSetChanged();
                        }
                        checkData();
                    } else {
                        checkData();
                        H.showMessage(getActivity(), json.getString(P.error));
                    }
                })
                .run("hitPostData", token);
    }

    private void checkData() {
        if (userPostModelList.isEmpty()) {
            binding.txtPostError.setVisibility(View.VISIBLE);
        } else {
            binding.txtPostError.setVisibility(View.GONE);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        hitUserDetails(token);
    }

    private void hitUserDetails(String token) {

        ProgressView.show(getActivity(), loadingDialog);

        Json j = new Json();
        j.addString(P.dependent_id, userID);
        Api.newApi(getActivity(), API.BaseUrl + "userprofiles/single_dependent_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json dependent_details = data.getJson(P.dependent_details);
                        setUserData(dependent_details);
                    } else {
                        H.showMessage(getActivity(), json.getString(P.error));
                    }
                })
                .run("hitUserDetails", token);
    }

    private void setUserData(Json json) {

        LoadImage.glideString(getActivity(), binding.imgProfile, imageBaseUrl + json.getString(P.profile_pic), getResources().getDrawable(R.drawable.ic_baseline_person_24));
        binding.txtTitle.setText(checkString(json.getString(P.username)));
        binding.txtUserName.setText(checkString(json.getString(P.name)));
        binding.txtUserBio.setText(checkString(json.getString(P.bio)));

        if (json.has(P.connections)) {
            binding.txtConnection.setText(json.getString(P.connections));
        }
        if (json.has(P.followers)) {
            binding.txtFollowers.setText(json.getString(P.followers));
        }
        if (json.has(P.following)) {
            binding.txtFollowing.setText(json.getString(P.following));
        }

    }

    private void onClick() {

        binding.lnrConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (usertype_id.equals(Config.DEPENDENT)) {
                    Intent intentDependent = new Intent(getContext(), DependentConnectionActivity.class);
                    startActivity(intentDependent);
                } else if (usertype_id.equals(Config.GUARDIAN)) {
                    Intent intentGuardian = new Intent(getContext(), GuardianConnectionActivity.class);
                    startActivity(intentGuardian);
                }
            }
        });

        binding.lnrFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getActivity(), FollowersActivity.class);
                startActivity(intent);
            }
        });

        binding.lnrFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getActivity(), FollowingActivity.class);
                startActivity(intent);
            }
        });

        txtLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                logOutUser();
            }
        });
    }

    private void logOutUser() {
        session.clear();
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

}