package com.soul.soulber.BaseActivity;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.android.volley.VolleyError;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.soul.soulber.BaseActivity.Basefragment.dashboard.DashboardFragment;
import com.soul.soulber.BaseActivity.Basefragment.home.HomeFragment;
import com.soul.soulber.BaseActivity.Basefragment.notifications.NotificationsFragment;
import com.soul.soulber.BaseActivity.Basefragment.profile.ProfileFragment;
import com.soul.soulber.JobsHouseFriends.Housings.HousingDetailsActivity;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.Meeting.MeetingDetailsActivity;
import com.soul.soulber.R;
import com.soul.soulber.activity.CreateImagePostActivity;
import com.soul.soulber.activity.CreateTextPostActivity;
import com.soul.soulber.activity.CreateVideoPostActivity;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.FollowersActivity;
import com.soul.soulber.activity.ForumDetailsActivity;
import com.soul.soulber.activity.GuardianConnectionActivity;
import com.soul.soulber.activity.JobDetailsActivity;
import com.soul.soulber.activity.LabAndTherapistActivity;
import com.soul.soulber.activity.PanicHistoryActivity;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.fragment.GuardianDashboardFragment;
import com.soul.soulber.fragment.GuardianProfileFragment;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class BaseAcivity extends AppCompatActivity implements Player.EventListener {

    private BaseAcivity activity = this;
    BottomNavigationView navView;
    boolean doubleBackToExitPressedOnce = false;

    private Session session;
    private LoadingDialog loadingDialog;
    private String usertype_id;
    private String token;

    private static final int REQUEST_IMAGE = 19;
    private static final int REQUEST_VIDEO = 12;
    private static final int READ_WRIRE = 111;
    private String imgStringName = "";

    private RelativeLayout lnrUploadView;
    private LinearLayout lnrTextUpload;
    private LinearLayout lnrImageUpload;
    private LinearLayout lnrVideoUpload;
    private ImageView imgUser;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";

    private int clickFor = 0;
    private int clickImage = 1;
    private int clickVideo = 2;

    private ActivityResultLauncher<Intent> startForResult;

    private SimpleExoPlayer exoPlayer;
    private PlayerView playerView;
    private ProgressBar pbVideoPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_acivity);

        navView = findViewById(R.id.nav_view);

        session = new Session(activity);
        loadingDialog = new LoadingDialog(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);

        lnrUploadView = findViewById(R.id.lnrUploadView);
        lnrTextUpload = findViewById(R.id.lnrTextUpload);
        lnrImageUpload = findViewById(R.id.lnrImageUpload);
        lnrVideoUpload = findViewById(R.id.lnrVideoUpload);
        imgUser = findViewById(R.id.imgUser);

        getAccess();

        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_chat, R.id.navigation_dashboard, R.id.navigation_notifications, R.id.navigation_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        loaddasboardfragment();
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                FragmentManager fragmentManager = null;
                FragmentTransaction fragmentTransaction;
                Fragment fragment = null;
                int id = menuItem.getItemId();
                //  menuView.findViewById(R.id.navigation_gallry).setVisibility(View.GONE);


                switch (id) {

                    case R.id.navigation_profile:
                        hideUploadVisibility();
                        if (navView.getMenu().findItem(R.id.navigation_gallry).isVisible()) {
                            if (usertype_id.equals(Config.GUARDIAN)) {
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.nav_host_fragment, new GuardianProfileFragment());
                                fragmentTransaction.commit();
                                break;
                            } else if (usertype_id.equals(Config.DEPENDENT)) {
                                fragmentManager = getSupportFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.nav_host_fragment, new ProfileFragment());
                                fragmentTransaction.commit();
                                break;
                            }

                        }

                        if (usertype_id.equals(Config.DEPENDENT)) {
                            navView.getMenu().clear();
                            navView.inflateMenu(R.menu.bottom_nav_menu);
                            navView.getMenu()
                                    .findItem(R.id.navigation_gallry)
                                    .setVisible(true);
                            BottomNavigationMenuView menuView = (BottomNavigationMenuView) navView.getChildAt(0);
                            for (int i = 2; i < 3; i++) {
                                final View iconView = menuView.getChildAt(i).findViewById(com.google.android.material.R.id.navigation_bar_item_icon_view);
                                final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
                                final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                                // set your height here
                                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, displayMetrics);
                                // set your width here
                                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 35, displayMetrics);
                                iconView.setLayoutParams(layoutParams);
                            }
                        }

                        if (usertype_id.equals(Config.GUARDIAN)) {
                            fragmentManager = getSupportFragmentManager();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.nav_host_fragment, new GuardianProfileFragment());
                            fragmentTransaction.commit();
                            break;
                        } else if (usertype_id.equals(Config.DEPENDENT)) {
                            fragmentManager = getSupportFragmentManager();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.nav_host_fragment, new ProfileFragment());
                            fragmentTransaction.commit();
                            break;
                        }
                        break;
                    case R.id.navigation_dashboard:
                        hideUploadVisibility();
                        if (navView.getMenu().findItem(R.id.navigation_gallry).isVisible()) {
                            navView.getMenu()
                                    .findItem(R.id.navigation_gallry)
                                    .setVisible(false);
                            navView.getMenu().clear(); //clear old inflated items.
                            navView.inflateMenu(R.menu.bottom_nav_menu);
                        }
                        if (usertype_id.equals(Config.GUARDIAN)) {
                            fragmentManager = getSupportFragmentManager();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.nav_host_fragment, new GuardianDashboardFragment());
                            fragmentTransaction.commit();
                            break;
                        } else if (usertype_id.equals(Config.DEPENDENT)) {
                            fragmentManager = getSupportFragmentManager();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.nav_host_fragment, new DashboardFragment());
                            fragmentTransaction.commit();
                        }
                        break;

                    case R.id.navigation_chat:
                        hideUploadVisibility();
                        if (navView.getMenu().findItem(R.id.navigation_gallry).isVisible()) {
                            navView.getMenu()
                                    .findItem(R.id.navigation_gallry)
                                    .setVisible(false);
                            navView.getMenu().clear(); //clear old inflated items.
                            navView.inflateMenu(R.menu.bottom_nav_menu);

                        }
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.nav_host_fragment, new HomeFragment());
                        fragmentTransaction.commit();


                        break;
                    case R.id.navigation_notifications:
                        hideUploadVisibility();
                        if (navView.getMenu().findItem(R.id.navigation_gallry).isVisible()) {
                            navView.getMenu()
                                    .findItem(R.id.navigation_gallry)
                                    .setVisible(false);
                            navView.getMenu().clear(); //clear old inflated items.
                            navView.inflateMenu(R.menu.bottom_nav_menu);
                        }
                        fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.nav_host_fragment, new NotificationsFragment());
                        fragmentTransaction.commit();


                        break;
                    case R.id.navigation_gallry:
//                        Intent intent = new Intent(getBaseContext(), ImagesActivity.class);
//                        Imageeditor imageeditor= new Imageeditor();
//                        imageeditor.imageedito(0);
//                        startActivity(intent);
//                        getPermission();
                        checkUploadVisibility();
                        break;

                }
                return true;
            }
        });

//        NavigationUI.setupWithNavController(navView, navController);

        if (session.getBool(Config.userLogin)) {
            hitUsersData(token);
            onClick();
            activityTrimResult();
            getFirebaseTokenAndDeviceID();
            getNotificationAction();
        } else {
            jumpToLogin();
        }
    }

    private void getNotificationAction() {

        String action = getIntent().getStringExtra(P.action);
        String action_data = getIntent().getStringExtra(P.action_data);

        Log.e("TAG", "getNotificationAction: " + action + " " + action_data);

        if (!TextUtils.isEmpty(checkString(action)) && !TextUtils.isEmpty(checkString(action_data))) {
            if (action.equalsIgnoreCase(Config.NOTIFICATION_CONNECTION_REQUEST)) {
                //connection activity
                loadConnectionView();
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_REQUEST_APPROVED)) {
                //connection activity
                loadConnectionView();
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_REQUEST_ACCEPTED)) {
                //connection activity
                loadConnectionView();
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_NEW_FOLLOWER)) {
                // followers activity
                loadFollowersView();
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_NEW_POST)) {
                // user post activity
                Config.IS_NOTIFICATION_POST = true;
                Config.NOTIFICATION_POST_ID = action_data;
                loadPostView();
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_PANIC)) {
                // user panic data
                if (usertype_id.equals(Config.GUARDIAN)) {
                    loadPanicHistory();
                }
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_NEW_FORUM)) {
                // forum post activity
                loadForumView(action_data);
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_JOB_APPLICATION)) {
                // applied job activity
                loadAppliedJob(action_data);
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_MEETING_ACTION)) {
                // meeting appointment
                loadMeeting(action_data);
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_HOUSE_BOOKING_ACTION)) {
                // housing booking
                loadHousingBooking(action_data);
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_THERAPIST_APPOINTMENT)) {
                // therapist appointment activity
                loadTherapist();
            } else if (action.equalsIgnoreCase(Config.NOTIFICATION_LAB_APPOINTMENT)) {
                // lab appointment activity
                loadLab();
            }
        }
    }

    private void loadConnectionView() {
        if (usertype_id.equals(Config.DEPENDENT)) {
            Intent intentDependent = new Intent(activity, DependentConnectionActivity.class);
            startActivity(intentDependent);
        } else if (usertype_id.equals(Config.GUARDIAN)) {
            Intent intentGuardian = new Intent(activity, GuardianConnectionActivity.class);
            startActivity(intentGuardian);
        }
    }

    private void loadFollowersView() {
        Intent intent = new Intent(activity, FollowersActivity.class);
        startActivity(intent);
    }

    private void loadPostView() {
        Intent intent = new Intent(activity, UserPostActivity.class);
        startActivity(intent);
    }

    private void loadForumView(String id) {
        Intent intent = new Intent(activity, ForumDetailsActivity.class);
        intent.putExtra(P.id, id);
        startActivity(intent);
    }

    private void loadPanicHistory() {
        Intent intent = new Intent(activity, PanicHistoryActivity.class);
        startActivity(intent);
    }

    private void loadAppliedJob(String id) {
        Intent intent = new Intent(activity, JobDetailsActivity.class);
        intent.putExtra(Config.JOB_ID, id);
        startActivity(intent);
    }

    private void loadHousingBooking(String id) {
        Config.HOUSING_ID = id;
        Intent intent = new Intent(activity, HousingDetailsActivity.class);
        startActivity(intent);
    }

    private void loadMeeting(String id) {
        Config.MEETING_ID = id;
        Intent intent = new Intent(activity, MeetingDetailsActivity.class);
        startActivity(intent);
    }

    private void loadTherapist() {
        Config.FOR_THERAPIST = true;
        Intent intent = new Intent(activity, LabAndTherapistActivity.class);
        startActivity(intent);
    }

    private void loadLab() {
        Config.FOR_THERAPIST = false;
        Intent intent = new Intent(activity, LabAndTherapistActivity.class);
        startActivity(intent);
    }


    private void jumpToLogin() {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void loaddasboardfragment() {
        if (usertype_id.equals(Config.GUARDIAN)) {
            GuardianDashboardFragment fragment = new GuardianDashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.nav_host_fragment, fragment, "");
            fragmentTransaction.commit();
        } else if (usertype_id.equals(Config.DEPENDENT)) {
            DashboardFragment fragment = new DashboardFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.nav_host_fragment, fragment, "");
            fragmentTransaction.commit();
        }
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }


    private void hitUsersData(String token) {
        ProgressView.show(activity, loadingDialog);

        String apiUrl = "";
        if (usertype_id.equals(Config.DEPENDENT)) {
            apiUrl = "dependent/profile/my_details";
        } else if (usertype_id.equals(Config.GUARDIAN)) {
            apiUrl = "guardian/profile/my_details";
        }

        Api.newApi(activity, API.BaseUrl + apiUrl)
                .setMethod(Api.GET)
                .onError(() -> {
                    VolleyError volleyError = new VolleyError();
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "" + volleyError);
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);

                        if (usertype_id.equals(Config.DEPENDENT)) {
                            Json dependent_details = data.getJson(P.dependent_details);
                            session.addJson(Config.userProfileData, dependent_details);
                        } else if (usertype_id.equals(Config.GUARDIAN)) {
                            Json guardian_details = data.getJson(P.guardian_details);
                            session.addJson(Config.userProfileData, guardian_details);
                        }

                        try {
                            Json jsonProfile = session.getJson(Config.userProfileData);
                            LoadImage.glideString(activity, imgUser, imageBaseUrl + jsonProfile.getString(P.profile_pic), getResources().getDrawable(R.drawable.ic_baseline_person_24));
                        } catch (Exception e) {
                        }

                    }

                })
                .run("hitUsersData", token);
    }

    private void checkUploadVisibility() {

        if (lnrUploadView.getVisibility() == View.VISIBLE) {
            lnrUploadView.setVisibility(View.GONE);
        } else if (lnrUploadView.getVisibility() == View.GONE) {
            lnrUploadView.setVisibility(View.VISIBLE);
        }
    }

    private void hideUploadVisibility() {

        if (lnrUploadView.getVisibility() == View.VISIBLE) {
            lnrUploadView.setVisibility(View.GONE);
            return;
        }
    }


    private void onClick() {

        lnrUploadView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkUploadVisibility();
            }
        });

        lnrTextUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkUploadVisibility();
                Intent intent = new Intent(activity, CreateTextPostActivity.class);
                startActivity(intent);
            }
        });

        lnrImageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkUploadVisibility();
                clickFor = clickImage;
                getPermission();
            }
        });

        lnrVideoUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkUploadVisibility();
                clickFor = clickVideo;
                getPermission();
            }
        });

    }

    private void getAccess() {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
        }
    }

    private void getPermission() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                },
                READ_WRIRE);
    }

    private void jumpToSetting() {
        H.showMessage(activity, "Please allow permission from setting.");
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            activity.startActivity(intent);
        } catch (Exception e) {
        }
    }

    private void openImageGallery() {
        try {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, REQUEST_IMAGE);
        } catch (Exception e) {
            H.showMessage(activity, "Unable to get image, try again.");
        }
    }

    private void openVideoGallery() {
        try {
//            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
////            intent.setType("image/* video/*");
//            intent.setType("video/*");
//            startActivityForResult(intent, REQUEST_VIDEO);

//            Intent intent = new Intent();
//            intent.setType("video/*");
//            intent.setAction(Intent.ACTION_GET_CONTENT);
//            startActivityForResult(Intent.createChooser(intent, "Select a Video "), REQUEST_GALLARY);


            if (Build.VERSION.SDK_INT <= 19) {
                Intent i = new Intent();
                i.setType("video/mp4");
                i.setAction(Intent.ACTION_GET_CONTENT);
                i.addCategory(Intent.CATEGORY_OPENABLE);
//                i.putExtra(android.provider.MediaStore.EXTRA_VIDEO_QUALITY,-5);
                startActivityForResult(i, REQUEST_VIDEO);
            } else if (Build.VERSION.SDK_INT > 19) {
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/mp4");
//                intent.putExtra(android.provider.REQUEST_VIDEO.EXTRA_VIDEO_QUALITY,-5);
                startActivityForResult(intent, REQUEST_VIDEO);
            }

        } catch (Exception e) {
            H.showMessage(activity, "Unable to get video, try again.");
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRIRE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (clickFor == clickImage) {
                        openImageGallery();
                    } else if (clickFor == clickVideo) {
                        openVideoGallery();
                    }
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    jumpToSetting();
                } else {
                    getPermission();
                }
                return;
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    try {
                        Uri selectedImage = data.getData();
                        setImageData(selectedImage);
                    } catch (Exception e) {
                        Log.e("TAG", "onActivityResult21212: " + e.getMessage());
                        H.showMessage(activity, "Unable to get image, try again.");
                    }
                }
                break;
            case REQUEST_VIDEO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    try {
                        Uri selectedVideo = data.getData();
                        String videoPath = getRealPathFromURI(selectedVideo);
                        Log.e("TAG", "onActivityResult21212: " + videoPath);
                        if (selectedVideo != null) {
//                            trimVideo(selectedVideo);
                            int size6 = 6291456;
                            int size10 = 10485760;
                            if (getFileSize(selectedVideo) < size10) {
                                setVideoData(selectedVideo);
                            } else {
                                sizeAlert();
                            }
                        } else {
                            H.showMessage(activity, "Unable to get video, try again. 4");
                        }

                    } catch (Exception e) {
                        Log.e("TAG", "onActivityResult21212: " + e.getMessage());
                        H.showMessage(activity, "Unable to get video, try again.");
                    }
                }
                break;
        }
    }


    private void sizeAlert() {
        new AlertDialog.Builder(activity)
                .setTitle("Alert")
                .setMessage("Video size should be less than 10 Mb.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private long getFileSize(Uri fileUri) {
        Cursor returnCursor = getContentResolver().
                query(fileUri, null, null, null, null);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();

        return returnCursor.getLong(sizeIndex);
    }


    private void trimVideo(Uri selectedVideo) {

//        TrimVideo.activity(String.valueOf(selectedVideo))
//                .setHideSeekBar(true)
//                .setTrimType(TrimType.MIN_MAX_DURATION)
//                .setMinToMax(10, 30)
//                .start(this,startForResult);

    }

    private void activityTrimResult() {
        startForResult = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK &&
                            result.getData() != null) {
                        Uri selectedVideo = Uri.parse("");
//                        Uri selectedVideo = Uri.parse(TrimVideo.getTrimmedVideoPath(result.getData()));
//                        String videoPath = getRealPathFromURI(selectedVideo);
                        if (selectedVideo != null) {
                            Uri selectedVideoFinal = null;
                            if (selectedVideo.getScheme() == null) {
                                selectedVideoFinal = Uri.fromFile(new File(selectedVideo.getPath()));
                            } else {
                                selectedVideoFinal = selectedVideo;
                            }
                            setVideoData(selectedVideoFinal);
                        } else {
                            H.showMessage(activity, "Unable to get video, try again. 1");
                        }

                    } else {
                        H.showMessage(activity, "Unable to get video, try again. 2");
                    }
                });
    }


    private void setVideoData(Uri selectedVideo) {

        try {
            InputStream inStream = getContentResolver().openInputStream(selectedVideo);
            byte[] originalBytes = getBytes(inStream);
            String base64Video = Base64.encodeToString(originalBytes, Base64.DEFAULT);
            Config.POST_VIDEO_URI = selectedVideo;
            Config.POST_VIDEO_BASE64 = base64Video;
            Intent intent = new Intent(activity, CreateVideoPostActivity.class);
            startActivity(intent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            H.showMessage(activity, "Unable to get video, try again.3");
        } catch (IOException e) {
            e.printStackTrace();
            H.showMessage(activity, "Unable to get video, try again.4");
        }
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    private void setImageData(Uri uri) {
        String imgBase64 = "";
        try {
            final InputStream imageStream = getContentResolver().openInputStream(uri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            imgBase64 = encodeImage(selectedImage);
            Config.POST_BASE_64 = imgBase64;
            Config.POST_BITMAP = selectedImage;
            Intent intent = new Intent(activity, CreateImagePostActivity.class);
            startActivity(intent);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            H.showMessage(activity, "Unable to get image, try again.");
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
        cursor.close();

        return path;
    }


    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }


    public void playerVideoDialog(String url) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_player_view);

        pbVideoPlayer = dialog.findViewById(R.id.pbVideoPlayer);
        playerView = dialog.findViewById(R.id.playerView);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);

        playVideo(url);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (exoPlayer != null) {
                    exoPlayer.stop(true);
                }
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

    }


    private void playVideo(String videoPath) {
        exoPlayer = new SimpleExoPlayer.Builder(activity).build();
        playerView.setPlayer(exoPlayer);
        playerView.setResizeMode(AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT);
        exoPlayer.setPlayWhenReady(true);
        exoPlayer.addListener(this);

        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(), getApplicationContext().getString(R.string.app_name)));

        MediaSource videoSource = new ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource(Uri.parse(videoPath));

        exoPlayer.prepare(videoSource);
        exoPlayer.setMediaItem(MediaItem.fromUri(videoPath));
        exoPlayer.play();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (exoPlayer != null) {
            exoPlayer.stop(true);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (exoPlayer != null) {
            exoPlayer.pause();
        }
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

        if (playbackState == Player.STATE_BUFFERING) {
            pbVideoPlayer.setVisibility(View.VISIBLE);

        } else if (playbackState == Player.STATE_READY || playbackState == Player.STATE_ENDED) {
            pbVideoPlayer.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            Json jsonProfile = session.getJson(Config.userProfileData);
            LoadImage.glideString(activity, imgUser, imageBaseUrl + jsonProfile.getString(P.profile_pic), getResources().getDrawable(R.drawable.ic_baseline_person_24));
        } catch (Exception e) {
        }
    }

    @Override
    public void onBackPressed() {
        hideUploadVisibility();
        onBackPress();
    }

    private void onBackPress() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        H.showMessage(activity, "Please click BACK again to exit");
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private void getFirebaseTokenAndDeviceID() {
        String androidID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        new Session(activity).addString(P.device_id, androidID);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            new Session(activity).addString(P.fcm_token, newToken);
            Log.e("TAG", "getFirebaseToken: " + newToken);
        });
    }



}