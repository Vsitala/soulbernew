package com.soul.soulber.BaseActivity.Basefragment.home;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.ChatListAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentChatBinding;
import com.soul.soulber.model.ChatListModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements ChatListAdapter.onClick{

    private Context context;
    private FragmentChatBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private List<ChatListModel> chatListModelList;
    private ChatListAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
            context = inflater.getContext();
            initView();
        }
        return binding.getRoot();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        chatListModelList = new ArrayList<>();
        adapter = new ChatListAdapter(context, chatListModelList,HomeFragment.this);
        linearLayoutManager = new LinearLayoutManager(context);
        binding.recyclerChat.setLayoutManager(linearLayoutManager);
        binding.recyclerChat.setHasFixedSize(true);
        binding.recyclerChat.setNestedScrollingEnabled(false);
        binding.recyclerChat.setAdapter(adapter);

        binding.etxSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence newText, int start, int before, int count) {
                if (!TextUtils.isEmpty(newText)) {
                    String text = newText.toString();
                    List<ChatListModel> list = new ArrayList<ChatListModel>();
                    for (ChatListModel model : chatListModelList) {
                        if (model.getName().toLowerCase().contains(text.toLowerCase()) || model.getName().toLowerCase().contains(text.toLowerCase())) {
                            list.add(model);
                        }
                    }
                    adapter = new ChatListAdapter(context, list,HomeFragment.this);
                    binding.recyclerChat.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } else {
                    adapter = new ChatListAdapter(context, chatListModelList,HomeFragment.this);
                    binding.recyclerChat.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        hitChatListData(pageCount);
        setPagination();

    }


    @Override
    public void chatClicked(ChatListModel model) {
        binding.etxSearch.setText("");
    }

    private void setPagination() {
        binding.recyclerChat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (chatListModelList != null && !chatListModelList.isEmpty()) {
                        if (chatListModelList.size() < count) {
                            pageCount++;
                            hitChatListData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitChatListData(int pageCount) {

        ProgressView.show(context, loadingDialog);

        Api.newApi(context, API.BaseUrl + "Userprofiles/get_all_chats?page=" + pageCount + "&per_page=100")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            chatListModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList chats = data.getJsonList(P.chats);
                        if (chats != null && chats.size() != 0) {
                            for (Json jsonValue : chats) {

                                ChatListModel model = new ChatListModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setLast_message(jsonValue.getString(P.last_message));
                                model.setFrom_user_id(jsonValue.getString(P.from_user_id));
                                model.setTo_user_id(jsonValue.getString(P.to_user_id));
                                model.setLast_updated(jsonValue.getString(P.last_updated));
                                model.setName(jsonValue.getString(P.name));
                                model.setUsername(jsonValue.getString(P.username));
                                model.setProfile_pic(jsonValue.getString(P.profile_pic));
                                model.setLast_activated(jsonValue.getString(P.last_activated));
                                model.setSession_user_type_id(jsonValue.getString(P.session_user_type_id));
                                model.setOpponent_user_type_id(jsonValue.getString(P.opponent_user_type_id));

                                chatListModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                        }

                        checkData();

                    } else {
                        H.showMessage(context, json.getString(P.error));
                        checkData();
                    }
                })
                .run("hitChatListData", token);
    }

    private void checkData() {
        if (chatListModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

}