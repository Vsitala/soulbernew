package com.soul.soulber.BaseActivity.Basefragment.dashboard;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.Meeting.MeetingLocationActivity;
import com.soul.soulber.R;
import com.soul.soulber.activity.ComingSoonActivity;
import com.soul.soulber.activity.DependentConnectionActivity;
import com.soul.soulber.activity.DependentPanicHistoryActivity;
import com.soul.soulber.activity.ForumActivity;
import com.soul.soulber.activity.GuardianConnectionActivity;
import com.soul.soulber.activity.LabAndTherapistActivity;
import com.soul.soulber.activity.UserPostActivity;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.TextlayoutBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

public class DashboardFragment extends Fragment {
    LinearLayout ll_search, connection_updates;
    private DashboardViewModel dashboardViewModel;
    TextlayoutBinding dashboardbinging;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    long down, up;

    @SuppressLint("ClickableViewAccessibility")
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardbinging = DataBindingUtil.inflate(inflater, R.layout.textlayout, container, false);
        View root = dashboardbinging.getRoot();

        loadingDialog = new LoadingDialog(getActivity());
        session = new Session(getActivity());

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        dashboardbinging.llSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), Maindashjobhouse.class);
//                startActivity(intent);
                Intent intent = new Intent(getContext(), ComingSoonActivity.class);
                startActivity(intent);
            }
        });

        dashboardbinging.connectionUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), Updates.class);
                Config.FOR_MY_POST = false;
                Intent intent = new Intent(getContext(), UserPostActivity.class);
                startActivity(intent);
            }
        });
        dashboardbinging.llMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MeetingLocationActivity.class);
                startActivity(intent);
            }
        });
        dashboardbinging.llTherapist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getContext(), TherapistLocationActivity.class);
//                startActivity(intent);
                Intent intent = new Intent(getContext(), LabAndTherapistActivity.class);
                startActivity(intent);
            }
        });
        dashboardbinging.llForums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ForumActivity.class);
                startActivity(intent);
            }
        });
        dashboardbinging.llPanic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent panicintent = new Intent(getContext(), DependentPanicHistoryActivity.class);
                startActivity(panicintent);
            }
        });

//        dashboardbinging.llPanic.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                switch (motionEvent.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        down = System.currentTimeMillis();
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        up = System.currentTimeMillis();
//                        if (up - down > 3000) {
//                            hitPanicAction(token);
//                        } else {
//                            H.showMessage(getActivity(), "Please press and hold for 3 second.");
//                        }
//                        return true;
//                }
//                return false;
//            }
//        });

        dashboardbinging.llUsersConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (usertype_id.equals(Config.DEPENDENT)) {
                    Intent intentDependent = new Intent(getContext(), DependentConnectionActivity.class);
                    startActivity(intentDependent);
                } else if (usertype_id.equals(Config.GUARDIAN)) {
                    Intent intentGuardian = new Intent(getContext(), GuardianConnectionActivity.class);
                    startActivity(intentGuardian);
                }
            }
        });

        dashboardbinging.lnrReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComingSoonActivity.class);
                startActivity(intent);
            }
        });

        return root;
    }

    private void hitPanicAction(String token) {

        ProgressView.show(getActivity(), loadingDialog);
        Json j = new Json();
//        j.addString("", "");

        Api.newApi(getActivity(), API.BaseUrl + "Dashboard/hit_panic")
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(getActivity(), json.getString(P.msg));
                    } else {
                        H.showMessage(getActivity(), json.getString(P.error));
                    }
                })
                .run("hitPanicAction", token);
    }

    private void hitDashboard(String token) {

        ProgressView.show(getActivity(), loadingDialog);

        Api.newApi(getActivity(), API.BaseUrl + "Dashboard/dashboard")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        Json my_details = data.getJson(P.my_details);

                        dashboardbinging.txtUserName.setText("Welcome " + my_details.getString(P.name));
                        dashboardbinging.txtConnection.setText(my_details.getString(P.connections));

                    } else {
                        H.showMessage(getActivity(), json.getString(P.msg));
                    }
                })
                .run("hitDashboard", token);
    }

    @Override
    public void onResume() {
        super.onResume();
        hitDashboard(token);
    }

}