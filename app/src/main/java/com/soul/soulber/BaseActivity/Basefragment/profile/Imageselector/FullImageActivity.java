package com.soul.soulber.BaseActivity.Basefragment.profile.Imageselector;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.soul.soulber.R;

public class FullImageActivity extends AppCompatActivity {

    ImageView myImage, back;
    Bitmap myBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);
        myImage = findViewById(R.id.image);
        back = findViewById(R.id.back);
        Glide.with(this)
                .asBitmap()
                .load(getIntent().getStringExtra("image"))
                .into(new BitmapImageViewTarget(myImage) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        // Do bitmap magic here
                        super.setResource(resource);
                        myImage.setImageBitmap(myBitmap);
                    }
                });
    //        Intent intent = getIntent();

//        File image = new File(intent.getStringExtra("image"));
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(),bmOptions);
//        myBitmap = Bitmap.createScaledBitmap(myBitmap,myImage.getWidth(),myImage.getHeight(),true);
  //    myBitmap = BitmapFactory.decodeFile(image.getAbsolutePath());

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0,0);
            }
        });
    }
}