package com.soul.soulber.BaseActivity.Basefragment.profile;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.android.volley.VolleyError;
import com.mancj.slideup.SlideUp;
import com.mancj.slideup.SlideUpBuilder;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.BaseActivity.Basefragment.profile.Imageditor.Imageeditor;
import com.soul.soulber.BaseActivity.Basefragment.profile.Imageselector.ImagesActivity;
import com.soul.soulber.R;
import com.soul.soulber.WelcomeActivity;
import com.soul.soulber.adapter.CountrySelectionAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityProfileEditBinding;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.model.Userdetailsmodel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.LoadImage;
import com.soul.soulber.util.ProgressView;
import com.soul.soulber.util.Validation;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ProfileEditActivity extends AppCompatActivity {

    private ActivityProfileEditBinding binding;
    private ProfileEditActivity activity = this;

    private LoadingDialog loadingDialog;
    private Session session;

    private SlideUp slideUp;
    private RelativeLayout slideView;

    private View slidedown;
    private TextView txtNewPic;
    private TextView txtRemovePic;

    private String token;
    private String usertype_id;

    private String imageBaseUrl = "https://dev.digiinterface.com/2021/soulber/uploads/profile_pic/";
    private List<CountryModel> countryCodeList;

    private static final int REQUEST_GALLARY = 19;
    private static final int READ_WRIRE = 111;
    private String imgStringName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile__edit);
        initView();
    }

    private void initView() {

        getAccess();

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);

        slideView = findViewById(R.id.slideView);
        slidedown = findViewById(R.id.tv_slidedown);
        txtNewPic = findViewById(R.id.txtNewPic);
        txtRemovePic = findViewById(R.id.txtRemovePic);

        countryCodeList = new ArrayList<>();

        setupSlider();
        setCountryCodeData();
        onSelectionClick();
        onClick();
        setUserData();

    }

    private void getAccess(){
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }catch (Exception e){
        }
    }

    private void setupSlider(){
        slideUp = new SlideUpBuilder(slideView)
                .withListeners(new SlideUp.Listener.Events() {
                    @Override
                    public void onSlide(float percent) {

                    }

                    @Override
                    public void onVisibilityChanged(int visibility) {
                        binding.dim.setVisibility(View.VISIBLE);

                        binding.dim.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                slideUp.hide();
                            }
                        });
                        if (visibility == View.GONE) {
                            binding.dim.setVisibility(View.GONE);
                        }


                    }
                })
                .withStartGravity(Gravity.BOTTOM)
                .withLoggingEnabled(true)
                .withGesturesEnabled(true)
                .withStartState(SlideUp.State.HIDDEN)
                .withSlideFromOtherView(findViewById(R.id.slideView))
                .build();
    }

    private void setCountryCodeData(){

        countryCodeList.add(new CountryModel("","+112"));
        countryCodeList.add(new CountryModel("","+191"));
        countryCodeList.add(new CountryModel("","+234"));
        countryCodeList.add(new CountryModel("","+123"));
        CountrySelectionAdapter adapter = new CountrySelectionAdapter(activity,countryCodeList);
        binding.spinnerCountryCode1.setAdapter(adapter);
        binding.spinnerCountryCode2.setAdapter(adapter);
        binding.spinnerCountryCode3.setAdapter(adapter);

    }
    
    private void onSelectionClick(){

        binding.spinnerCountryCode1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = countryCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerCountryCode2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = countryCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerCountryCode3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = countryCodeList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void onClick(){

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });

        binding.etxDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                setDate(binding.etxDate);
            }
        });

        binding.txtContactView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.layoutContact.getVisibility() == View.VISIBLE) {
                    binding.layoutContact.setVisibility(View.GONE);
                } else if (binding.layoutContact.getVisibility() == View.GONE) {
                    binding.layoutContact.setVisibility(View.VISIBLE);
                }

            }
        });

        binding.txtEmergencyOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.layoutEmergencyOne.getVisibility() == View.VISIBLE) {
                    binding.layoutEmergencyOne.setVisibility(View.GONE);
                } else if (binding.layoutEmergencyOne.getVisibility() == View.GONE) {
                    binding.layoutEmergencyOne.setVisibility(View.VISIBLE);
                }

            }
        });

        binding.txtEmergencyTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.layoutEmergencyTwo.getVisibility() == View.VISIBLE) {
                    binding.layoutEmergencyTwo.setVisibility(View.GONE);
                } else if (binding.layoutEmergencyTwo.getVisibility() == View.GONE){
                    binding.layoutEmergencyTwo.setVisibility(View.VISIBLE);
                }

            }
        });

        binding.txtChangePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                slideUp.show();
            }
        });

        slidedown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                slideUp.hide();
            }
        });


        binding.btnSaveDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                if (usertype_id.equals(Config.DEPENDENT)){
                    checkDependentValidation();
                }else if (usertype_id.equals(Config.GUARDIAN)){
                   checkGuardianValidation();
                }
            }
        });

        txtNewPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                getPermission();
            }
        });

        txtRemovePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                hitRemoveProfilePic("");
            }
        });
    }

    private void setUserData() {

        Json json = session.getJson(Config.userProfileData);

        LoadImage.glideString(activity,binding.imgProfile,imageBaseUrl+json.getString(P.profile_pic),getResources().getDrawable(R.drawable.ic_baseline_person_24));

        binding.txtTitle.setText(checkString(json.getString(P.username)));

        if (json.getString(P.is_pic_private).equals("1")) {
            binding.radioYes.setChecked(true);
        } else {
            binding.radioNo.setChecked(true);
        }

        binding.etxName.setText(checkString(json.getString(P.name)));
        binding.etxUserName.setText(checkString(json.getString(P.username)));
        binding.etxBio.setText(checkString(json.getString(P.bio)));
        if (json.has(P.birth_date)){
            binding.etxDate.setText(checkString(json.getString(P.birth_date)));
        }

        binding.etxContactNumber.setText(checkString(json.getString(P.phone)));
        binding.etxContactEmail.setText(checkString(json.getString(P.email)));

        binding.etxEmrContact1.setText(checkString(json.getString(P.contact1)));
        binding.etxEmrEmail1.setText(checkString(json.getString(P.email1)));

        binding.etxEmrContact2.setText(checkString(json.getString(P.contact2)));
        binding.etxEmrEmail2.setText(checkString(json.getString(P.email2)));

        hideDetails();
    }

    private void hideDetails(){

        if (usertype_id.equals(Config.GUARDIAN)){
            binding.inputBio.setVisibility(View.GONE);
            binding.inputDate.setVisibility(View.GONE);
            binding.txtEmergencyOne.setVisibility(View.GONE);
            binding.layoutEmergencyOne.setVisibility(View.GONE);
            binding.txtEmergencyTwo.setVisibility(View.GONE);
            binding.layoutEmergencyTwo.setVisibility(View.GONE);
        }

    }

    private void checkDependentValidation(){

        if (TextUtils.isEmpty(binding.etxName.getText().toString().trim())) {
            binding.etxName.setError("Enter your name");
            binding.etxName.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxUserName.getText().toString().trim())) {
            binding.etxUserName.setError("Enter username");
            binding.etxUserName.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxDate.getText().toString().trim())) {
            binding.etxDate.setError("Enter Date you became sober");
            binding.etxDate.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxContactNumber.getText().toString().trim())) {
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactNumber.setError("Enter contact number");
            binding.etxContactNumber.requestFocus();
        }else if(binding.etxContactNumber.getText().toString().trim().length()!=10){
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactNumber.setError("Enter valid number");
            binding.etxContactNumber.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxContactEmail.getText().toString().trim())) {
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactEmail.setError("Enter contact email");
            binding.etxContactEmail.requestFocus();
        }else if (!Validation.validEmail(binding.etxContactEmail.getText().toString().trim())){
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactEmail.setError("Enter valid email");
            binding.etxContactEmail.requestFocus();
        } else if (TextUtils.isEmpty(binding.etxEmrContact1.getText().toString().trim())) {
            binding.layoutEmergencyOne.setVisibility(View.VISIBLE);
            binding.etxEmrContact1.setError("Enter any emergency number");
            binding.etxEmrContact1.requestFocus();
        }else if(binding.etxEmrContact1.getText().toString().trim().length()!=10){
            binding.layoutEmergencyOne.setVisibility(View.VISIBLE);
            binding.etxEmrContact1.setError("Enter valid number");
            binding.etxEmrContact1.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxEmrEmail1.getText().toString().trim())) {
            binding.layoutEmergencyOne.setVisibility(View.VISIBLE);
            binding.etxEmrEmail1.setError("Enter any emergency email");
            binding.etxEmrEmail1.requestFocus();
        }else if (!Validation.validEmail(binding.etxEmrEmail1.getText().toString().trim())){
            binding.layoutEmergencyOne.setVisibility(View.VISIBLE);
            binding.etxEmrEmail1.setError("Enter valid email");
            binding.etxEmrEmail1.requestFocus();
        }else {

            String isPrivate = "";
            if (binding.radioYes.isChecked()){
                isPrivate = "1";
            }else {
                isPrivate = "0";
            }

            Json j = new Json();
            j.addString(P.name, binding.etxName.getText().toString().trim());
            j.addString(P.username, binding.etxUserName.getText().toString().trim());
            j.addString(P.email, binding.etxContactEmail.getText().toString().trim());
            j.addString(P.contact, binding.etxContactNumber.getText().toString().trim());
            j.addString(P.email1, binding.etxEmrEmail1.getText().toString().trim());
            j.addString(P.contact1, binding.etxEmrContact1.getText().toString().trim());
            j.addString(P.email2, binding.etxEmrEmail2.getText().toString().trim());
            j.addString(P.contact2, binding.etxEmrContact2.getText().toString().trim());
            j.addString(P.birth_date, binding.etxDate.getText().toString().trim());
            j.addString(P.city_id, "2");
            j.addString(P.is_pic_private, isPrivate);
            j.addString(P.bio, binding.etxBio.getText().toString().trim());

            hitEditProfileDetails(j);
        }

    }

    private void checkGuardianValidation(){

        if (TextUtils.isEmpty(binding.etxName.getText().toString().trim())) {
            binding.etxName.setError("Enter your name");
            binding.etxName.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxUserName.getText().toString().trim())) {
            binding.etxUserName.setError("Enter username");
            binding.etxUserName.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxContactNumber.getText().toString().trim())) {
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactNumber.setError("Enter contact number");
            binding.etxContactNumber.requestFocus();
        }else if(binding.etxContactNumber.getText().toString().trim().length()!=10){
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactNumber.setError("Enter valid number");
            binding.etxContactNumber.requestFocus();
        }else if (TextUtils.isEmpty(binding.etxContactEmail.getText().toString().trim())) {
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactEmail.setError("Enter contact email");
            binding.etxContactEmail.requestFocus();
        }else if (!Validation.validEmail(binding.etxContactEmail.getText().toString().trim())){
            binding.layoutContact.setVisibility(View.VISIBLE);
            binding.etxContactEmail.setError("Enter valid email");
            binding.etxContactEmail.requestFocus();
        } else {

            String isPrivate = "";
            if (binding.radioYes.isChecked()){
                isPrivate = "1";
            }else {
                isPrivate = "0";
            }

            Json j = new Json();
            j.addString(P.name, binding.etxName.getText().toString().trim());
            j.addString(P.username, binding.etxUserName.getText().toString().trim());
            j.addString(P.email, binding.etxContactEmail.getText().toString().trim());
            j.addString(P.contact, binding.etxContactNumber.getText().toString().trim());
            j.addString(P.is_pic_private, isPrivate);

            hitEditProfileDetails(j);

        }

    }

    private void getPermission() {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                READ_WRIRE);
    }

    private void jumpToSetting() {
        H.showMessage(activity, "Please allow permission from setting.");
        try {
            Intent intent = new Intent();
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
            intent.setData(uri);
            activity.startActivity(intent);
        } catch (Exception e) {
        }
    }

    private void openGallery() {
        try {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, REQUEST_GALLARY);
        } catch (Exception e) {
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRIRE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openGallery();
                } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    jumpToSetting();
                } else {
                    getPermission();
                }
                return;
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_GALLARY:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    try {
                        Uri selectedImage = data.getData();
                        Log.e("TAG", "onActivityResult2q131313 "+selectedImage);
                        setImageData(selectedImage);
                    } catch (Exception e) {
                        Log.e("TAG", "onActivityResult21212: "+ e.getMessage() );
                    }
                }
                break;
        }
    }

    private void setImageData(Uri uri) {
        String imgBase64 = "";
        try {
            final InputStream imageStream = getContentResolver().openInputStream(uri);
            final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            binding.imgProfile.setImageBitmap(selectedImage);
            imgBase64 = encodeImage(selectedImage);
            hitUploadProfilePic(imgBase64);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            H.showMessage(activity, "Unable to get image, try again.");
        }
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }


    private void setDate(EditText etxDate) {

        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog mDatePickerDialog = new DatePickerDialog(activity, R.style.MyDatePickerStyle, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
                final Date date = newDate.getTime();
                String fdate = sd.format(date);
                etxDate.setText(fdate);
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
//        mDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        mDatePickerDialog.show();
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    private void hitUploadProfilePic(String imageBase64) {

        if (slideUp!=null && slideUp.isVisible()){
            slideUp.hide();
        }
        ProgressView.show(activity,loadingDialog);
        Json j = new Json();
        j.addString(P.image, "data:image/jpeg;base64," + imageBase64);
        new Api(activity, API.BaseUrl + "Common/upload_base64image").addJson(j)
                .setMethod(Api.POST)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        imgStringName = data.getString(P.filename);
                        hitUpdateProfilePic(imgStringName);
                    }else {
                        H.showMessage(activity,json.getString(P.msg));
                    }
                })
                .run("hitUploadProfilePic", token);
    }

    private void hitUpdateProfilePic(String fileName) {
        ProgressView.show(activity,loadingDialog);
        Json j = new Json();
        j.addString(P.filename, fileName);
        new Api(activity, API.BaseUrl + "Common/update_profile_pic").addJson(j)
                .setMethod(Api.POST)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    hitUsersData(json.getString(P.msg),0);
                })
                .run("hitUpdateProfilePic", token);
    }

    private void hitRemoveProfilePic(String fileName) {
        ProgressView.show(activity,loadingDialog);
        Json j = new Json();
        j.addString(P.filename, fileName);
        new Api(activity, API.BaseUrl + "Common/update_profile_pic").addJson(j)
                .setMethod(Api.POST)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    slideUp.hide();
                    LoadImage.glideString(activity, binding.imgProfile, "", getResources().getDrawable(R.drawable.ic_defualtprofile));
                    hitUsersData(json.getString(P.msg),0);
                })
                .run("hitRemoveProfilePic", token);
    }


    private void hitEditProfileDetails(Json jsonData) {
        ProgressView.show(activity, loadingDialog);

        String apiUrl = "";

        if (usertype_id.equals(Config.DEPENDENT)){
            apiUrl = "dependent/profile/edit_profile";
        }else if (usertype_id.equals(Config.GUARDIAN)){
            apiUrl = "guardian/profile/edit_profile";
        }

        Api.newApi(activity, API.BaseUrl + apiUrl).addJson(jsonData)
                .setMethod(Api.POST)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if (json.getInt(P.status) == 1) {
                        hitUsersData(json.getString(P.msg),1);
                    } else {
                        H.showMessage(activity,json.getString(P.msg));
                    }

                })
                .run("hitEditProfileDetails", token);

    }

    private void hitUsersData(String message, int fromWhere) {
        ProgressView.show(activity,loadingDialog);

        String apiUrl = "";
        if (usertype_id.equals(Config.DEPENDENT)){
            apiUrl = "dependent/profile/my_details";
        }else if (usertype_id.equals(Config.GUARDIAN)){
            apiUrl = "guardian/profile/my_details";
        }

        Api.newApi(activity,API.BaseUrl+ apiUrl)
                .setMethod(Api.GET)
                .onError(() -> {
                    VolleyError volleyError = new VolleyError();
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity,""+volleyError);
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if(json.getInt(P.status)==1)
                    {
                        Json data = json.getJson(P.data);

                        if (usertype_id.equals(Config.DEPENDENT)){
                            Json dependent_details = data.getJson(P.dependent_details);
                            session.addJson(Config.userProfileData,dependent_details);
                        }else if (usertype_id.equals(Config.GUARDIAN)){
                            Json guardian_details = data.getJson(P.guardian_details);
                            session.addJson(Config.userProfileData,guardian_details);
                        }

                        H.showMessage(activity,message);
                        if (fromWhere==1){
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            },1500);
                        }

                    }

                })
                .run("hitUsersData",token);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}