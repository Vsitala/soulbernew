package com.soul.soulber.BaseActivity.Basefragment.profile;

import android.view.View;
import android.widget.ImageView;

import androidx.lifecycle.ViewModel;

import com.soul.soulber.R;

public class ProfileViewModel extends ViewModel {

    private ImageView mText;

    public ProfileViewModel(View itemview) {
      super();
        mText =itemview.findViewById(R.id.image_upload);

    }

    public ImageView getmText() {
        return mText;
    }
}