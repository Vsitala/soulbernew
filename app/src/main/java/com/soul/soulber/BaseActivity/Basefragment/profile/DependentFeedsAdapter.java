package com.soul.soulber.BaseActivity.Basefragment.profile;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.soul.soulber.R;
import com.soul.soulber.Updates.Updates;

import java.util.ArrayList;
import java.util.Collections;

public class DependentFeedsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<DependentFeedsModelclass> feedslist;
    private Context context;
    public static final int ITEM_TYPE_GRID = 0;
    public static final int ITEM_TYPE_CARD_LIST = 1;
    private int VIEW_TYPE;

    public DependentFeedsAdapter(ArrayList<DependentFeedsModelclass> feedslist, Context context) {
        this.feedslist = feedslist;
        this.context = context;
    }

    public void setVIEW_TYPE(int viewType) {
        VIEW_TYPE = viewType;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = null;
        switch (VIEW_TYPE) {
            case ITEM_TYPE_GRID:
                view = LayoutInflater.from(context).inflate(R.layout.uploaditems, viewGroup, false);
                return new ViewHol(view);
            case ITEM_TYPE_CARD_LIST:

                view = LayoutInflater.from(context).inflate(R.layout.secondlayout, viewGroup, false);
                return new Linearholder(view);
        }

        return new ViewHol(view);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (VIEW_TYPE == ITEM_TYPE_GRID) {
            if (viewHolder instanceof ViewHol) {
                ViewHol viewHoldernew = (ViewHol) viewHolder;
                Glide
                        .with(context)
                        .load(feedslist.get(position).getImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(viewHoldernew.image_upload);
                viewHoldernew.image_upload.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, Updates.class);
                        context.startActivity(intent);
                    }
                });
            }
        } else if (VIEW_TYPE == ITEM_TYPE_CARD_LIST) {

            if (viewHolder instanceof Linearholder) {
                Linearholder linearViewHolder = (Linearholder) viewHolder;
                Glide.with(context)
                        .load(feedslist.get(position).getImage())
                        .into(linearViewHolder.image_linear);
                linearViewHolder.image_linear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Toast.makeText(context, "" + position, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(context, Updates.class);
                        context.startActivity(intent);
                    }
                });
            }
        }


    }

    @Override
    public int getItemCount() {
        return feedslist.size();
    }

    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(feedslist, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(feedslist, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    public class ViewHol extends RecyclerView.ViewHolder {
        private ImageView image_upload;

        public ViewHol(View view) {
            super(view);
            image_upload = (ImageView) view.findViewById(R.id.image_upload);
        }

    }

    public class Linearholder extends RecyclerView.ViewHolder {
        private ImageView image_linear;

        public Linearholder(View itemView) {
            super(itemView);
            image_linear = (ImageView) itemView.findViewById(R.id.image_uploadnew);
        }
    }


}

