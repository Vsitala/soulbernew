package com.soul.soulber.Dependent;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.android.volley.VolleyError;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.R;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityDependentsearchBinding;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;
import com.soul.soulber.util.Validation;

public class DependentSearchActivity extends AppCompatActivity {

    private DependentSearchActivity activity = this;
    private ActivityDependentsearchBinding binding;

    private Session session;
    private LoadingDialog loadingDialog;
    private String usertype_id;
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dependentsearch);

        initView();

    }

    private void initView() {

        session = new Session(activity);
        loadingDialog = new LoadingDialog(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);

        hitUsersData(token);;

        onClick();
    }

    private void onClick() {

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                onBackPressed();
            }
        });

        binding.btnNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                jumpToMain();
            }
        });

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                checkLoginValidation();
            }
        });

    }

    private void checkLoginValidation() {
        if (TextUtils.isEmpty(binding.etxEmail.getText().toString().trim())) {
            binding.etxEmail.setError("Enter email id");
            binding.etxEmail.requestFocus();
        } else if (!Validation.validEmail(binding.etxEmail.getText().toString().trim())) {
            binding.etxEmail.setError("Enter valid email id");
            binding.etxEmail.requestFocus();
        } else {
            Intent intent = new Intent(activity, DependentResultActivity.class);
            intent.putExtra(Config.dependentEmail, binding.etxEmail.getText().toString().trim());
            startActivity(intent);
        }
    }

    private void jumpToMain(){
        Intent intent = new Intent(activity, BaseAcivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void hitUsersData(String token) {
        ProgressView.show(activity,loadingDialog);
        Api.newApi(activity, API.BaseUrl+ "dependent/profile/my_details")
                .setMethod(Api.GET)
                .onError(() -> {
                    VolleyError volleyError = new VolleyError();
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity,""+volleyError);
                })
                .onSuccess(json -> {
                    ProgressView.dismiss(loadingDialog);
                    if(json.getInt(P.status)==1)
                    {
                        Json data = json.getJson(P.data);
                        Json dependent_details = data.getJson(P.dependent_details);
                        session.addJson(Config.userProfileData,dependent_details);
                    }

                })
                .run("hitUsersData",token);
    }

    @Override
    public void onBackPressed() {
        jumpToMain();
    }
}