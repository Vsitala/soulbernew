package com.soul.soulber.Dependent;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.R;
import com.soul.soulber.adapter.SearchDependentAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityResultDependentBinding;
import com.soul.soulber.model.DependentConnectionModel;
import com.soul.soulber.model.DependentModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class DependentResultActivity extends AppCompatActivity implements SearchDependentAdapter.onClick {

    private DependentResultActivity activity = this;
    private ActivityResultDependentBinding binding;
    private LoadingDialog loadingDialog;
    private Session session;

    private SearchDependentAdapter adapter;
    private List<DependentModel> dependentModelList;

    private String usertype_id;
    private String token;
    private String dependentEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_result_dependent);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        dependentEmail = getIntent().getStringExtra(Config.dependentEmail);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        dependentModelList = new ArrayList<>();
        adapter = new SearchDependentAdapter(activity, dependentModelList, userID, true);
        binding.recyclerSearchDependent.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerSearchDependent.setHasFixedSize(true);
        binding.recyclerSearchDependent.setNestedScrollingEnabled(true);
        binding.recyclerSearchDependent.setAdapter(adapter);

        hitSearchDependent(dependentEmail, token);
        onClick();

    }

    @Override
    public void onConnect(DependentModel model, Button button) {
        hitConnect(model.getDependent_id(), token, button);
    }

    @Override
    public void onRemove(DependentModel model, Button button) {
        hitRemove(model.getConnection_id(), token, button);
    }

    @Override
    public void onAccept(DependentModel model, Button button) {
        hitAction(model.getConnection_id(), token, button);
    }

    private void onClick() {

        binding.imgBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(DependentResultActivity.this, DependentSearchActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void hitSearchDependent(String email, String token) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.name, "");
        j.addString(P.email, email);

        Api.newApi(activity, API.BaseUrl + "userprofiles/search_depenedent").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        Json dependentlist = data.getJson(P.dependentlist);
                        JsonList connectable = dependentlist.getJsonList(P.connectable);
                        JsonList newList = dependentlist.getJsonList(P.newList);

                        if (connectable != null && connectable.size() != 0) {
                            for (Json jsonData : connectable) {

                                DependentModel model = new DependentModel();
                                model.setDependent_id(jsonData.getString(P.dependent_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                dependentModelList.add(model);
                            }
                        }


                        if (newList != null && newList.size() != 0) {
                            for (Json jsonData : newList) {

                                DependentModel model = new DependentModel();
                                model.setDependent_id(jsonData.getString(P.dependent_id));
                                model.setName(jsonData.getString(P.name));
                                model.setUsername(jsonData.getString(P.username));
                                model.setEmail(jsonData.getString(P.email));
                                model.setPhone(jsonData.getString(P.phone));
                                model.setEmail1(jsonData.getString(P.email1));
                                model.setContact1(jsonData.getString(P.contact1));
                                model.setEmail2(jsonData.getString(P.email2));
                                model.setContact2(jsonData.getString(P.contact2));
                                model.setUser_type_id(jsonData.getString(P.user_type_id));
                                model.setUser_type_name(jsonData.getString(P.user_type_name));
                                model.setCity_id(jsonData.getString(P.city_id));
                                model.setCity(jsonData.getString(P.city));
                                model.setAdd_date(jsonData.getString(P.add_date));
                                model.setIs_pic_private(jsonData.getString(P.is_pic_private));
                                model.setProfile_pic(jsonData.getString(P.profile_pic));
                                model.setBio(jsonData.getString(P.bio));
                                model.setIs_accepted(jsonData.getString(P.is_accepted));
                                model.setAge(jsonData.getString(P.Age));
                                model.setConnection_id(jsonData.getString(P.connection_id));

                                dependentModelList.add(model);
                            }
                        }

                        adapter.notifyDataSetChanged();

                        if (dependentModelList.isEmpty()) {
                            binding.txtError.setVisibility(View.VISIBLE);
                            binding.txtMessage.setText("Showing 0 result for " + email);
                        } else {
                            binding.txtError.setVisibility(View.GONE);
                            binding.txtMessage.setText("Showing " + dependentModelList.size() + " result for " + email);
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.msg));
                    }
                })
                .run("hitSearchDependent", token);
    }


    private void hitConnect(String id, String token, Button button) {

        ProgressView.show(activity, loadingDialog);
        String apiUrl = "";
        Json j = new Json();
        if (usertype_id.equals(Config.DEPENDENT)) {
            apiUrl = "dependent/connection/connect_dependent";
            j.addString(P.to_dependent_id, id);
        } else if (usertype_id.equals(Config.GUARDIAN)) {
            apiUrl = "guardian/connection/connect_dependent";
            j.addString(P.dependent_id, id);
        }

        Api.newApi(activity, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        button.setText("Requested");
                        jumpToMain();
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }


    private void jumpToMain(){
        Intent intent = new Intent(activity, BaseAcivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();
    }

    private void hitRemove(String id, String token, Button button) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        String apiUrl = "";

        if (usertype_id.equals(Config.DEPENDENT)) {
            apiUrl = "dependent/connection/remove_dependent_connection";
            j.addString(P.connection_id, id);
        } else if (usertype_id.equals(Config.GUARDIAN)) {
            apiUrl = "guardian/connection/remove_dependent_connection";
            j.addString(P.connection_id, id);
        }

        Api.newApi(activity, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        button.setText("Connect");
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }

    private void hitAction(String id, String token, Button txtAction) {

        ProgressView.show(activity, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, id);

        String apiUrl = "";
        if (usertype_id.equals(Config.DEPENDENT)) {
            apiUrl = "";
            j.addString(P.connection_id, id);
        } else if (usertype_id.equals(Config.GUARDIAN)) {
            apiUrl = "";
            j.addString(P.connection_id, id);
        }

        if (txtAction.getText().toString().equals("Accept")) {
            j.addString(P.action_id, "1");
        } else if (txtAction.getText().toString().equals("Reject")) {
            j.addString(P.action_id, "2");
        }

        Api.newApi(activity, API.BaseUrl + apiUrl).addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(activity, json.getString(P.msg));
                        if (txtAction.getText().toString().equals("Accept")) {
                            txtAction.setText("Reject");
                        } else if (txtAction.getText().toString().equals("Reject")) {
                            txtAction.setText("Accept");
                        }
                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("onAction", token);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}