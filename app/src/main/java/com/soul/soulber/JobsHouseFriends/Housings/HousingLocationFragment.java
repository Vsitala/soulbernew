package com.soul.soulber.JobsHouseFriends.Housings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.JobsHouseFriends.Maindashjobhouse;
import com.soul.soulber.R;
import com.soul.soulber.adapter.HousingRequestedAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FragmentHousingLocationBinding;
import com.soul.soulber.model.HousingRequestedModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class HousingLocationFragment extends Fragment {
    FragmentHousingLocationBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private List<HousingRequestedModel> housingModelList;
    private HousingRequestedAdapter adapter;


    public static HousingLocationFragment newInstance() {
        HousingLocationFragment fragment = new HousingLocationFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_housing_location, viewGroup, false);
        View root = binding.getRoot();
        initView();
        return root;
    }

    private void initView() {

        clearSearch();

        loadingDialog = new LoadingDialog(getActivity());
        session = new Session(getActivity());

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        housingModelList = new ArrayList<>();
        adapter = new HousingRequestedAdapter(getActivity(), housingModelList);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerHousing.setLayoutManager(linearLayoutManager);
        binding.recyclerHousing.setItemViewCacheSize(housingModelList.size());
        binding.recyclerHousing.setHasFixedSize(true);
        binding.recyclerHousing.setAdapter(adapter);

        onClick();
        hitHousingData(pageCount);
        setPagination();

    }

    private void clearSearch() {
        Config.IS_SEARCH = false;
        Config.SEARCH_ADD = "";
        Config.SEARCH_LOGN = "";
        Config.SEARCH_LAT = "";
    }

    public void setSearch() {
        if (Config.IS_SEARCH) {
            Config.IS_SEARCH = false;
            Config.HOUSING_LAT = Config.SEARCH_LAT;
            Config.HOUSING_LOGN = Config.SEARCH_LOGN;
            Config.HOUSING_SEARCH_ADD = Config.SEARCH_ADD;
            binding.txtLocation.setText(Config.SEARCH_ADD);
        }
    }

    private void onClick() {

        binding.txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), SearchLocationActivity.class);
//                startActivity(intent);
                ((Maindashjobhouse) getActivity()).onGoogleSearchCalled(2, binding.txtLocation);
            }
        });

        binding.btnSearchHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.txtLocation.getText().toString().equals("")) {
                    H.showMessage(getActivity(), "Please select location");
                } else {
                    Intent intent = new Intent(getActivity(), HousingResultsActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    private void setPagination() {
        binding.recyclerHousing.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (housingModelList != null && !housingModelList.isEmpty()) {
                        if (housingModelList.size() < count) {
                            pageCount++;
                            hitHousingData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitHousingData(int pageCount) {

        ProgressView.show(getActivity(), loadingDialog);
        Api.newApi(getActivity(), API.BaseUrl + "housing/dependent_housing_booking_list?page=" + pageCount + "&per_page=20")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    Json bookingsJson = json.getJson(P.bookings);
                    if (bookingsJson.getInt(P.status) == 1) {
                        Json data = bookingsJson.getJson(P.data);
                        String housing_images_path = data.getString(P.housing_images_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            housingModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList bookings = data.getJsonList(P.bookings);
                        if (bookings != null && bookings.size() != 0) {
                            for (Json jsonValue : bookings) {
                                HousingRequestedModel model = new HousingRequestedModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setHouse_id(jsonValue.getString(P.house_id));
                                model.setUser_id(jsonValue.getString(P.user_id));
                                model.setRequest_date(jsonValue.getString(P.request_date));
                                model.setApprove_date(jsonValue.getString(P.approve_date));
                                model.setRemark(jsonValue.getString(P.remark));
                                model.setStatus(jsonValue.getString(P.status));
                                model.setIs_latest(jsonValue.getString(P.is_latest));
                                model.setBooking_status(jsonValue.getString(P.booking_status));
                                model.setAgent_name(jsonValue.getString(P.agent_name));
                                model.setAgent_contact(jsonValue.getString(P.agent_contact));
                                model.setBedrooms(jsonValue.getString(P.bedrooms));
                                model.setBathrooms(jsonValue.getString(P.bathrooms));
                                model.setPrice_range(jsonValue.getString(P.price_range));
                                model.setCity(jsonValue.getString(P.city));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setAddress(jsonValue.getString(P.address));
                                model.setAvailability(jsonValue.getString(P.availability));
                                model.setPets_friendly(jsonValue.getString(P.pets_friendly));
                                model.setTotal_images(jsonValue.getString(P.total_images));
                                model.setApplied(jsonValue.getString(P.applied));
                                model.setImagePath(housing_images_path);
                                model.setImages(jsonValue.getJsonArray(P.images));

                                housingModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                        }

                        checkData();

                    } else {
                        H.showMessage(getActivity(), json.getString(P.error));
                        checkData();
                    }
                })
                .run("hitHousingData", token);
    }

    private void checkData() {
        if (housingModelList.isEmpty()) {
            binding.txtError.setVisibility(View.VISIBLE);
        } else {
            binding.txtError.setVisibility(View.GONE);
        }
    }

}
