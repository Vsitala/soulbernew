package com.soul.soulber.JobsHouseFriends.Housings;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.CountrySelectionAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityHousingFilterBinding;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class HousingFilterActivity extends AppCompatActivity {

    private HousingFilterActivity activity = this;
    private ActivityHousingFilterBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private List<CountryModel> bedroomList;
    private CountrySelectionAdapter bedroomAdapter;

    private List<CountryModel> bathroomList;
    private CountrySelectionAdapter bathroomAdapter;

    private List<CountryModel> priceList;
    private CountrySelectionAdapter priceAdapter;

    private List<CountryModel> amenitiesList;
    private CountrySelectionAdapter amenitiesAdapter;

    private List<CountryModel> petsList;
    private CountrySelectionAdapter petsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_housing_filter);
        initView();
    }


    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        bedroomList = new ArrayList<>();
        bedroomAdapter = new CountrySelectionAdapter(activity, bedroomList);
        binding.spinnerBedroom.setAdapter(bedroomAdapter);

        bathroomList = new ArrayList<>();
        bathroomAdapter = new CountrySelectionAdapter(activity, bathroomList);
        binding.spinnerBathroom.setAdapter(bathroomAdapter);

        priceList = new ArrayList<>();
        priceAdapter = new CountrySelectionAdapter(activity, priceList);
        binding.spinnerPriceRange.setAdapter(priceAdapter);

        amenitiesList = new ArrayList<>();
        amenitiesAdapter = new CountrySelectionAdapter(activity, amenitiesList);
        binding.spinnerAmenities.setAdapter(amenitiesAdapter);

        petsList = new ArrayList<>();
        petsAdapter = new CountrySelectionAdapter(activity, petsList);
        binding.spinnerPetsFriendly.setAdapter(petsAdapter);

        binding.spinnerBedroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = bedroomList.get(position);
                if (!model.getCity_id().equals("")) {
                    if (binding.txtBedroom.getVisibility() == View.GONE) {
                        binding.txtBedroom.setVisibility(View.VISIBLE);
                    }
                    if (!binding.txtBedroom.getText().toString().contains(model.getCity())) {
                        if (binding.txtBedroom.getText().toString().equals("")) {
                            binding.txtBedroom.setText(model.getCity());
                            Config.HOUSING_BEDROOM = model.getCity_id();
                            Config.HOUSING_BEDROOM_TEXT = model.getCity();
                        } else {
                            binding.txtBedroom.setText(binding.txtBedroom.getText().toString() + "," + model.getCity());
                            Config.HOUSING_BEDROOM = Config.HOUSING_BEDROOM + "," + model.getCity_id();
                            Config.HOUSING_BEDROOM_TEXT = Config.HOUSING_BEDROOM_TEXT + "," + model.getCity();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerBathroom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = bathroomList.get(position);
                if (!model.getCity_id().equals("")) {
                    if (binding.txtBathroom.getVisibility() == View.GONE) {
                        binding.txtBathroom.setVisibility(View.VISIBLE);
                    }
                    if (!binding.txtBathroom.getText().toString().contains(model.getCity())) {
                        if (binding.txtBathroom.getText().toString().equals("")) {
                            binding.txtBathroom.setText(model.getCity());
                            Config.HOUSING_BATHROOM = model.getCity_id();
                            Config.HOUSING_BATHROOM_TEXT = model.getCity();
                        } else {
                            binding.txtBathroom.setText(binding.txtBathroom.getText().toString() + "," + model.getCity());
                            Config.HOUSING_BATHROOM = Config.HOUSING_BATHROOM + "," + model.getCity_id();
                            Config.HOUSING_BATHROOM_TEXT = Config.HOUSING_BATHROOM_TEXT + "," + model.getCity();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerPriceRange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = priceList.get(position);
                if (!model.getCity_id().equals("")) {
                    if (binding.txtPriceRange.getVisibility() == View.GONE) {
                        binding.txtPriceRange.setVisibility(View.VISIBLE);
                    }
                    if (!binding.txtPriceRange.getText().toString().contains(model.getCity())) {
                        if (binding.txtPriceRange.getText().toString().equals("")) {
                            binding.txtPriceRange.setText(model.getCity());
                            Config.HOUSING_PRICE_RANCE = model.getCity_id();
                            Config.HOUSING_PRICE_RANCE_TEXT = model.getCity();
                        } else {
                            binding.txtPriceRange.setText(binding.txtPriceRange.getText().toString() + "," + model.getCity());
                            Config.HOUSING_PRICE_RANCE = Config.HOUSING_PRICE_RANCE + "," + model.getCity_id();
                            Config.HOUSING_PRICE_RANCE_TEXT = Config.HOUSING_PRICE_RANCE_TEXT + "," + model.getCity();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerAmenities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = amenitiesList.get(position);
                if (!model.getCity_id().equals("")) {
                    if (binding.txtAmenities.getVisibility() == View.GONE) {
                        binding.txtAmenities.setVisibility(View.VISIBLE);
                    }
                    if (!binding.txtAmenities.getText().toString().contains(model.getCity())) {
                        if (binding.txtAmenities.getText().toString().equals("")) {
                            binding.txtAmenities.setText(model.getCity());
                            Config.HOUSING_AMENITIES = model.getCity_id();
                            Config.HOUSING_AMENITIES_TEXT = model.getCity();
                        } else {
                            binding.txtAmenities.setText(binding.txtAmenities.getText().toString() + "," + model.getCity());
                            Config.HOUSING_AMENITIES = Config.HOUSING_AMENITIES + "," + model.getCity_id();
                            Config.HOUSING_AMENITIES_TEXT = Config.HOUSING_AMENITIES_TEXT + "," + model.getCity();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        binding.spinnerPetsFriendly.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryModel model = petsList.get(position);
                if (!model.getCity_id().equals("")) {
                    Config.HOUSING_PET_FRIENDLY = model.getCity_id();
                    Config.HOUSING_PET_FRIENDLY_TEXT = model.getCity();
                } else {
                    Config.HOUSING_PET_FRIENDLY = "";
                    Config.HOUSING_PET_FRIENDLY_TEXT = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Config.HOUSING_PET_FRIENDLY = "";
                Config.HOUSING_PET_FRIENDLY_TEXT = "";
            }
        });

        onClick();
        hitCommonData();
    }

    private void setData() {
        if (!Config.HOUSING_BEDROOM_TEXT.equals("")) {
            binding.txtBedroom.setVisibility(View.VISIBLE);
            binding.txtBedroom.setText(Config.HOUSING_BEDROOM_TEXT);
        }

        if (!Config.HOUSING_BATHROOM_TEXT.equals("")) {
            binding.txtBathroom.setVisibility(View.VISIBLE);
            binding.txtBathroom.setText(Config.HOUSING_BATHROOM_TEXT);
        }

        if (!Config.HOUSING_PRICE_RANCE_TEXT.equals("")) {
            binding.txtPriceRange.setVisibility(View.VISIBLE);
            binding.txtPriceRange.setText(Config.HOUSING_PRICE_RANCE_TEXT);
        }

        if (!Config.HOUSING_AMENITIES_TEXT.equals("")) {
            binding.txtAmenities.setVisibility(View.VISIBLE);
            binding.txtAmenities.setText(Config.HOUSING_AMENITIES_TEXT);
        }

        if (Config.HOUSING_PET_FRIENDLY.equals("1")) {
            binding.spinnerPetsFriendly.setSelection(1);
        } else if (Config.HOUSING_PET_FRIENDLY.equals("0")) {
            binding.spinnerPetsFriendly.setSelection(2);
        }else {
            binding.spinnerPetsFriendly.setSelection(0);
        }
    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.txtBedroom.equals("") ||
                        binding.txtBathroom.equals("") ||
                        binding.txtPriceRange.equals("") ||
                        binding.txtAmenities.equals("") ||
                        Config.HOUSING_PET_FRIENDLY.equals("")) {
                    H.showMessage(activity, "Please select filter value");
                } else {
                    Config.IS_HOUSING_FILTER = true;
                    finish();
                }
            }
        });

    }

    private void hitCommonData() {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "Common/all_common_data")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    Json bedroomJSON = json.getJson(P.apartment_size);
                    Json bedroomData = bedroomJSON.getJson(P.data);
                    JsonList bedrooms = bedroomData.getJsonList(P.bedrooms);
                    if (bedrooms != null && bedrooms.size() != 0) {
                        for (Json jsonValue : bedrooms) {
                            CountryModel model = new CountryModel();
                            model.setCity_id(jsonValue.getString(P.id));
                            model.setCity(jsonValue.getString(P.bedroom));
                            if (bedroomList.isEmpty()) {
                                bedroomList.add(new CountryModel("", "Select Bedrooms"));
                            }
                            bedroomList.add(model);
                        }
                        bedroomAdapter.notifyDataSetChanged();
                        binding.spinnerBedroom.setSelection(0);
                    }

                    Json bathroomsJSON = json.getJson(P.bathrooms);
                    Json bathroomsData = bathroomsJSON.getJson(P.data);
                    JsonList bathrooms = bathroomsData.getJsonList(P.bathrooms);
                    if (bathrooms != null && bathrooms.size() != 0) {
                        for (Json jsonValue : bathrooms) {
                            CountryModel model = new CountryModel();
                            model.setCity_id(jsonValue.getString(P.id));
                            model.setCity(jsonValue.getString(P.bathroom));
                            if (bathroomList.isEmpty()) {
                                bathroomList.add(new CountryModel("", "Select Bathrooms"));
                            }
                            bathroomList.add(model);
                        }
                        bathroomAdapter.notifyDataSetChanged();
                        binding.spinnerBathroom.setSelection(0);
                    }

                    Json house_priceJSON = json.getJson(P.house_price);
                    Json house_priceData = house_priceJSON.getJson(P.data);
                    JsonList prices = house_priceData.getJsonList(P.prices);
                    if (prices != null && prices.size() != 0) {
                        for (Json jsonValue : prices) {
                            CountryModel model = new CountryModel();
                            model.setCity_id(jsonValue.getString(P.id));
                            model.setCity(jsonValue.getString(P.from_price) + "-" + jsonValue.getString(P.to_price));
                            if (priceList.isEmpty()) {
                                priceList.add(new CountryModel("", "Select Price Range"));
                            }
                            priceList.add(model);
                        }
                        priceAdapter.notifyDataSetChanged();
                        binding.spinnerPriceRange.setSelection(0);
                    }

                    Json house_amenitiesJSON = json.getJson(P.house_amenities);
                    Json house_amenitiesData = house_amenitiesJSON.getJson(P.data);
                    JsonList amenitites = house_amenitiesData.getJsonList(P.amenitites);
                    if (amenitites != null && amenitites.size() != 0) {
                        for (Json jsonValue : amenitites) {
                            CountryModel model = new CountryModel();
                            model.setCity_id(jsonValue.getString(P.id));
                            model.setCity(jsonValue.getString(P.amenity));
                            if (amenitiesList.isEmpty()) {
                                amenitiesList.add(new CountryModel("", "Select Amenities"));
                            }
                            amenitiesList.add(model);
                        }
                        amenitiesAdapter.notifyDataSetChanged();
                        binding.spinnerAmenities.setSelection(0);
                    }

                    if (petsList.isEmpty()) {
                        petsList.add(new CountryModel("", "Select Pets Friendly"));
                        petsList.add(new CountryModel("1", "Yes"));
                        petsList.add(new CountryModel("0", "No"));
                        petsAdapter.notifyDataSetChanged();
                        binding.spinnerPetsFriendly.setSelection(0);
                    }

                    setData();

                })
                .run("hitCommonData", token);
    }
}