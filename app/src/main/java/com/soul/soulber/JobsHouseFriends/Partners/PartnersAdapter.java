package com.soul.soulber.JobsHouseFriends.Partners;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.soul.soulber.R;

import java.util.ArrayList;


    public class PartnersAdapter extends RecyclerView.Adapter<PartnersAdapter.ViewHolder> {
        private ArrayList<Parternsmodleclass> companylist;
        private Context context;
        private int selectedItem;
        public PartnersAdapter(Context context,ArrayList<Parternsmodleclass> product) {
            this.companylist = product;
            this.context = context;
            selectedItem=0;
        }

        @Override
        public PartnersAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.company_item, viewGroup, false);
            return new PartnersAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(PartnersAdapter.ViewHolder viewHolder, int position) {

            viewHolder.tv_companyname.setText(companylist.get(position).getCompany_name());


            Glide
                    .with(context)

                    .load(companylist.get(position).getCompany_image())
                    //cropping center image
                    .into(viewHolder.iv_companybrand);





//        if (selectedItem == position) {
//            viewHolder.addtofavorate.setImageResource(R.drawable.heart);
//
//        }
//        else
//        {
//            viewHolder.addtofavorate.setBackgroundColor(Color.TRANSPARENT);
//        }
//        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int previousItem = selectedItem;
//                selectedItem = position;
//
//               notifyItemChanged(previousItem);
//               notifyItemChanged(position);
//           }
//       });



        }

        @Override
        public int getItemCount() {
            return companylist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

            private ImageView iv_companybrand;
            private TextView tv_companyname;

            public ViewHolder(View view) {
                super(view);
                iv_companybrand=view.findViewById(R.id.iv_companybrand);

                tv_companyname =  view.findViewById(R.id.tv_companyname);

            }

        }

    }







