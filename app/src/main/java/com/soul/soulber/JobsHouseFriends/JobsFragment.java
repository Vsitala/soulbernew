package com.soul.soulber.JobsHouseFriends;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.JobsHouseFriends.Partners.Ourpartners;
import com.soul.soulber.R;
import com.soul.soulber.activity.CreateResumeActivity;
import com.soul.soulber.activity.JobAppliedListActivity;
import com.soul.soulber.activity.MyJobAppliedListActivity;
import com.soul.soulber.adapter.CategoryJobListAdapter;
import com.soul.soulber.adapter.JobCategoryAdapter;
import com.soul.soulber.adapter.OurPartnersHorizontalAdapter;
import com.soul.soulber.adapter.SkillsSearchAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.JobslayoutBinding;
import com.soul.soulber.model.AppliedJobModel;
import com.soul.soulber.model.CountryModel;
import com.soul.soulber.model.JobCategoryModel;
import com.soul.soulber.model.PartnerModel;
import com.soul.soulber.util.Click;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class JobsFragment extends Fragment implements JobCategoryAdapter.onClick {

    JobslayoutBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private List<PartnerModel> partnerModelList;
    private OurPartnersHorizontalAdapter adapter;

    private List<JobCategoryModel> jobCategoryModelList;
    private JobCategoryAdapter categoryAdapter;

    private List<AppliedJobModel> appliedJobModelList;
    private CategoryJobListAdapter appliedJobAdapter;

    private String selectedCategoryId = "";
    private String selectedCategoryName = "";

    private List<CountryModel> skillsSearchList;
    private SkillsSearchAdapter skillsSearchAdapter;

    private String skillId = "";
    private String locationId = "";


    public static JobsFragment newInstance() {
        JobsFragment fragment = new JobsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.jobslayout, viewGroup, false);
        View root = binding.getRoot();

        initView();

        return root;
    }


    private void clearSearch() {
        Config.IS_SEARCH = false;
        Config.SEARCH_ADD = "";
        Config.SEARCH_LOGN = "";
        Config.SEARCH_LAT = "";
    }

    public void setSearch() {
        if (Config.IS_SEARCH) {
            Config.IS_SEARCH = false;
            Config.CATEGORY_LAT = Config.SEARCH_LAT;
            Config.CATEGORY_LOGN = Config.SEARCH_LOGN;
            binding.txtLocation.setText(Config.SEARCH_ADD);
        }
    }

    private void initView() {

        loadingDialog = new LoadingDialog(getActivity());
        session = new Session(getActivity());

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        binding.etxSkillSearch.setThreshold(1);
        skillsSearchList = new ArrayList<>();

        Config.FROM_CATEGORY = false;
        Config.FROM_TYPE = false;
        clearSearch();

        partnerModelList = new ArrayList<>();
        adapter = new OurPartnersHorizontalAdapter(getActivity(), partnerModelList);
        binding.recyclerOurPartner.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        binding.recyclerOurPartner.setHasFixedSize(true);
        binding.recyclerOurPartner.setNestedScrollingEnabled(false);
        binding.recyclerOurPartner.setAdapter(adapter);

        jobCategoryModelList = new ArrayList<>();
        categoryAdapter = new JobCategoryAdapter(getActivity(), jobCategoryModelList, JobsFragment.this);
        binding.recyclerTrendingJobsCategory.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        binding.recyclerTrendingJobsCategory.setHasFixedSize(true);
        binding.recyclerTrendingJobsCategory.setNestedScrollingEnabled(false);
        binding.recyclerTrendingJobsCategory.setAdapter(categoryAdapter);

        appliedJobModelList = new ArrayList<>();
        appliedJobAdapter = new CategoryJobListAdapter(getActivity(), appliedJobModelList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerJobsList.setLayoutManager(linearLayoutManager);
        binding.recyclerJobsList.setItemViewCacheSize(appliedJobModelList.size());
        binding.recyclerJobsList.setHasFixedSize(true);
        binding.recyclerJobsList.setNestedScrollingEnabled(false);
        binding.recyclerJobsList.setAdapter(appliedJobAdapter);

        onClick();
    }

    private void onClick() {

        binding.lnrSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(binding.txtLocation.getText().toString().trim())) {
                    H.showMessage(getActivity(), "Please select location");
                } else if (skillId.equals("")) {
                    H.showMessage(getActivity(), "Please select skills");
                } else {
                    Config.FROM_CATEGORY = false;
                    Config.FROM_TYPE = true;
                    Config.CATEGORY_LOCATION = locationId;
                    Config.CATEGORY_SKILLS = skillId;
                    Config.FROM_APPLIED_JOB = false;
                    Intent intent = new Intent(getActivity(), JobAppliedListActivity.class);
                    startActivity(intent);
                }
            }
        });

        binding.txtLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getActivity(), SearchLocationActivity.class);
//                startActivity(intent);
                ((Maindashjobhouse) getActivity()).onGoogleSearchCalled(1, binding.txtLocation);
            }
        });

        binding.etxSkillSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
                CountryModel countryModel = (CountryModel) adapterView.getItemAtPosition(pos);
                binding.etxSkillSearch.setText("");
                if (binding.txtSkills.getVisibility() == View.GONE) {
                    binding.txtSkills.setVisibility(View.VISIBLE);
                }
                if (!binding.txtSkills.getText().toString().contains(countryModel.getCity())) {
                    if (binding.txtSkills.getText().toString().equals("")) {
                        binding.txtSkills.setText(countryModel.getCity());
                        skillId = countryModel.getCity_id();
                    } else {
                        binding.txtSkills.setText(binding.txtSkills.getText().toString() + "," + countryModel.getCity());
                        skillId = skillId + "," + countryModel.getCity_id();
                    }
                }
            }
        });

        binding.llPartners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Ourpartners.class);
                startActivity(intent);

            }
        });

        binding.txtViewAllPartners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Ourpartners.class);
                startActivity(intent);
            }
        });

        binding.lnrAppliedJobList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.FROM_CATEGORY = false;
                Config.FROM_TYPE = false;
                Config.FROM_APPLIED_JOB = true;
                Intent intent = new Intent(getActivity(), MyJobAppliedListActivity.class);
                startActivity(intent);
            }
        });

        binding.txtViewAllCategoryJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.FROM_CATEGORY = true;
                Config.FROM_TYPE = false;
                Config.FROM_APPLIED_JOB = false;
                Config.CATEGORY_ID = selectedCategoryId;
                Config.CATEGORY_NAME = selectedCategoryName;
                Intent intent = new Intent(getActivity(), JobAppliedListActivity.class);
                startActivity(intent);
            }
        });


        binding.lnrBuildResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Click.preventTwoClick(v);
                Intent intent = new Intent(getActivity(), CreateResumeActivity.class);
                startActivity(intent);
            }
        });

        hitCommonData();
        hitPartnersData(1);

    }

    private void hitCommonData() {

        ProgressView.show(getActivity(), loadingDialog);

        Api.newApi(getActivity(), API.BaseUrl + "Common/all_common_data")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                    checkCategoryData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    Json job_skillsObject = json.getJson(P.job_skills);
                    Json dataJonSkill = job_skillsObject.getJson(P.data);
                    JsonList job_skills_list = dataJonSkill.getJsonList(P.job_skills);
                    if (job_skills_list != null && job_skills_list.size() != 0) {
                        for (Json jsonValue : job_skills_list) {
                            CountryModel model = new CountryModel();
                            model.setCity_id(jsonValue.getString(P.id));
                            model.setCity(jsonValue.getString(P.skill));
                            skillsSearchList.add(model);
                        }

                        skillsSearchAdapter = new SkillsSearchAdapter(getActivity(), R.layout.activity_auto_list, skillsSearchList, JobsFragment.this);
                        binding.etxSkillSearch.setAdapter(skillsSearchAdapter);
                    }

                    Json job_categoriesObjecr = json.getJson(P.job_categories);
                    Json dataJobCategory = job_categoriesObjecr.getJson(P.data);
                    JsonList job_categories_list = dataJobCategory.getJsonList(P.job_categories);

                    String category_image_path = dataJobCategory.getString(P.category_image_path);

                    if (job_categories_list != null && job_categories_list.size() != 0) {
                        jobCategoryModelList.add(new JobCategoryModel("", "All", ""));
                        for (Json jsonValue : job_categories_list) {
                            JobCategoryModel model = new JobCategoryModel();
                            model.setId(jsonValue.getString(P.id));
                            model.setCategory(jsonValue.getString(P.category));
                            model.setCat_img(category_image_path + jsonValue.getString(P.cat_img));
                            jobCategoryModelList.add(model);
                        }
                        categoryAdapter.notifyDataSetChanged();
                    }
                    checkCategoryData();


                })
                .run("hitCommonData", token);
    }


    @Override
    public void onCategoryClick(JobCategoryModel model) {
        selectedCategoryId = model.getId();
        selectedCategoryName = model.getCategory();
        hitAppliedJobData(1, selectedCategoryId);
    }


    private void hitPartnersData(int pageCount) {

        ProgressView.show(getActivity(), loadingDialog);

        Api.newApi(getActivity(), API.BaseUrl + "job/partner_list?page=" + pageCount + "&per_page=20")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String partner_logo_path = data.getString(P.partner_logo_path);
                        int num_rows = data.getInt(P.num_rows);

                        JsonList partners = data.getJsonList(P.partners);
                        if (partners != null && partners.size() != 0) {
                            for (Json jsonValue : partners) {
                                PartnerModel model = new PartnerModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setPartner(jsonValue.getString(P.partner));
                                model.setLogo(partner_logo_path + jsonValue.getString(P.logo));
                                model.setAbout_partner(jsonValue.getString(P.about_partner));
                                if (partnerModelList.size() < 4) {
                                    partnerModelList.add(model);
                                }
                            }
                            adapter.notifyDataSetChanged();
                        }

                    } else {
                        H.showMessage(getActivity(), json.getString(P.error));
                    }
                })
                .run("hitPartnersData", token);
    }

    private void checkCategoryData() {
        if (jobCategoryModelList.isEmpty()) {
            binding.lnrTendingJob.setVisibility(View.GONE);
        } else {
            binding.lnrTendingJob.setVisibility(View.VISIBLE);
        }
    }

    private void hitAppliedJobData(int pageCount, String categoryId) {

        ProgressView.show(getActivity(), loadingDialog);

        Api.newApi(getActivity(), API.BaseUrl + "job/job_list?page=" + pageCount + "&per_page=20&category=" + categoryId)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(getActivity(), "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String partner_logo_path = data.getString(P.partner_logo_path);
                        int num_rows = data.getInt(P.num_rows);

                        if (pageCount == 1) {
                            appliedJobModelList.clear();
                            appliedJobAdapter.notifyDataSetChanged();
                        }

                        JsonList jobs = data.getJsonList(P.jobs);
                        if (jobs != null && jobs.size() != 0) {
                            for (Json jsonValue : jobs) {
                                AppliedJobModel model = new AppliedJobModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setCategory_id(jsonValue.getString(P.category_id));
                                model.setPartner_id(jsonValue.getString(P.partner_id));
                                model.setTitle(jsonValue.getString(P.title));
                                model.setAbout_company(jsonValue.getString(P.about_company));
                                model.setWebsite_link(jsonValue.getString(P.website_link));
                                model.setEmployment(jsonValue.getString(P.employment));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setAddress(jsonValue.getString(P.address));
                                model.setCity(jsonValue.getString(P.city));
                                model.setSalary_from(jsonValue.getString(P.salary_from));
                                model.setSalary_to(jsonValue.getString(P.salary_to));
                                model.setExperience_from(jsonValue.getString(P.experience_from));
                                model.setExperience_to(jsonValue.getString(P.experience_to));
                                model.setWho_can_apply(jsonValue.getString(P.who_can_apply));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setUpdate_date(jsonValue.getString(P.update_date));
                                model.setDelete_date(jsonValue.getString(P.delete_date));
                                model.setStatus(jsonValue.getString(P.status));
                                model.setDeleteflag(jsonValue.getString(P.deleteflag));
                                model.setJob_location(jsonValue.getString(P.job_location));
                                model.setSkills(jsonValue.getString(P.skills));
                                model.setIs_bookmarked(jsonValue.getString(P.is_bookmarked));
                                model.setPartner(jsonValue.getString(P.partner));
                                model.setPartner_location(jsonValue.getString(P.partner_location));
                                model.setPartner_logo(jsonValue.getString(P.partner_logo));
                                model.setCategory(jsonValue.getString(P.category));
                                if (appliedJobModelList.size() < 2) {
                                    appliedJobModelList.add(model);
                                }
                            }
                            appliedJobAdapter.notifyDataSetChanged();

                        }
                        checkData();
                    } else {
                        H.showMessage(getActivity(), json.getString(P.error));
                        checkData();
                    }
                })
                .run("hitAppliedJobData", token);
    }

    private void checkData() {
        if (appliedJobModelList.isEmpty()) {
            binding.txtJobListError.setVisibility(View.VISIBLE);
        } else {
            binding.txtJobListError.setVisibility(View.GONE);
        }
    }

}