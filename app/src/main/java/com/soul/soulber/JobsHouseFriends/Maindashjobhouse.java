package com.soul.soulber.JobsHouseFriends;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adoisstudio.helper.H;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.tabs.TabLayout;

import com.soul.soulber.JobsHouseFriends.Housings.HousingLocationFragment;
import com.soul.soulber.R;
import com.soul.soulber.api.Config;

import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

public class Maindashjobhouse extends AppCompatActivity {

    private Maindashjobhouse activity = this;

    SharedPreferences sharedPref;
    TabLayout tabLayout;
    ImageView onback_jobshousefriends;
    private int[] tabIcons = {
            R.drawable.jobsdrawable,
            R.drawable.housingdrawable,
            R.drawable.frindsdrawable
    };

    private ViewPager viewPager;

    private JobsFragment jobsFragment = JobsFragment.newInstance();
    private HousingLocationFragment housingLocationFragment = HousingLocationFragment.newInstance();
    private FriendsFragment friendsFragment = FriendsFragment.newInstance();

    final int AUTOCOMPLETE_REQUEST_CODE = 1000;

    int fromJOB = 1;
    int fromHOUSE = 2;
    int fromWHERE = 0;
    TextView txtLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maindashjobhouse);

        initializeKey();

        onback_jobshousefriends = findViewById(R.id.onback_jobshousefriends);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(viewPager);
        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        View view1 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((ImageView) view1.findViewById(R.id.tabImageView)).setImageResource(R.drawable.greenjob);
        ((TextView) view1.findViewById(R.id.tab)).setTextColor(getResources().getColor(R.color.lightgreen));
        ((TextView) view1.findViewById(R.id.tab)).setText(getResources().getString(R.string.job));


        View view2 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((ImageView) view2.findViewById(R.id.tabImageView)).setImageResource(R.drawable.housingdrawable);
        ((TextView) view2.findViewById(R.id.tab)).setText(getResources().getString(R.string.house));


        View view3 = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        ((ImageView) view3.findViewById(R.id.tabImageView)).setImageResource(R.drawable.frindsdrawable);
        ((TextView) view3.findViewById(R.id.tab)).setText(getResources().getString(R.string.friend));
        tabLayout.getTabAt(0).setCustomView(view1);
        tabLayout.getTabAt(1).setCustomView(view2);
        tabLayout.getTabAt(2).setCustomView(view3);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.tab)).setTextColor(getResources().getColor(R.color.lightgreen));
                if (tab.getPosition() == 0) {
                    ((ImageView) view.findViewById(R.id.tabImageView)).setImageResource(R.drawable.greenjob);
                } else if (tab.getPosition() == 1) {
                    ((ImageView) view.findViewById(R.id.tabImageView)).setImageResource(R.drawable.greenhouse);
                } else {

                    ((ImageView) view.findViewById(R.id.tabImageView)).setImageResource(R.drawable.greenfriends);

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View view = tab.getCustomView();
                ((TextView) view.findViewById(R.id.tab)).setTextColor(getResources().getColor(R.color.unselectedtab));
                if (tab.getPosition() == 0) {
                    ((ImageView) view.findViewById(R.id.tabImageView)).setImageResource(R.drawable.jobsdrawable);
                } else if (tab.getPosition() == 1) {
                    ((ImageView) view.findViewById(R.id.tabImageView)).setImageResource(R.drawable.housingdrawable);
                } else {

                    ((ImageView) view.findViewById(R.id.tabImageView)).setImageResource(R.drawable.frindsdrawable);

                }

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {


            }
        });


        onback_jobshousefriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }


    private void setupViewPager(ViewPager viewPager) {
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFrag(jobsFragment, "Jobs");
        adapter.addFrag(housingLocationFragment, "Housing");
        adapter.addFrag(friendsFragment, "Friends");
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (tabLayout.getSelectedTabPosition() == 0) {
//            jobsFragment.setSearch();
//        } else if (tabLayout.getSelectedTabPosition() == 1) {
//            housingLocationFragment.setSearch();
//        } else if (tabLayout.getSelectedTabPosition() == 2) {
//        }
    }

    private void initializeKey() {
        if (!Places.isInitialized()) {
            Places.initialize(activity, Config.API_KEY);
        }
        PlacesClient placesClient = Places.createClient(activity);
    }

    public void onGoogleSearchCalled(int fromCame, TextView textView) {
        fromWHERE = fromCame;
        txtLocation = textView;
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(
//                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("NG") //NIGERIA
//                .build(this);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry(Config.COUNTRY) //NIGERIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AUTOCOMPLETE_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    String id = place.getId();
                    String address = place.getAddress();
                    String name = place.getName();
                    LatLng latlong = place.getLatLng();
                    double latitude = latlong.latitude;
                    double longitude = latlong.longitude;

                    if (fromWHERE == fromJOB) {
                        Config.CATEGORY_LAT = latitude + "";
                        Config.CATEGORY_LOGN = longitude + "";
                    } else if (fromWHERE == fromHOUSE) {
                        Config.HOUSING_LAT = latitude + "";
                        Config.HOUSING_LOGN = longitude + "";
                        Config.HOUSING_SEARCH_ADD = address;
                    }

                    txtLocation.setText(address);

                    Log.e(TAG, "latitude: " + latitude + "");
                    Log.e(TAG, "longitude: " + longitude + "");
                    Log.e(TAG, "address: " + address.trim() + "");

                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
//                    H.showMessage(activity, "Unable to get address, try again.");
                    H.showMessage(activity, status + "");
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;
        }
    }
}