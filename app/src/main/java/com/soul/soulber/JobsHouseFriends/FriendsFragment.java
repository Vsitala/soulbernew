package com.soul.soulber.JobsHouseFriends;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.activity.DependentAddConnectionActivity;
import com.soul.soulber.adapter.DependentConnectionAdapter;
import com.soul.soulber.adapter.GuardianConnectionAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.FriendslayoutBinding;
import com.soul.soulber.model.DependentConnectionModel;
import com.soul.soulber.model.GuardianConnectionModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class FriendsFragment extends Fragment implements GuardianConnectionAdapter.onClick, DependentConnectionAdapter.onClick {

    private Context context;
    private FriendslayoutBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;

    private GuardianConnectionAdapter guardianConnectionAdapter;
    private List<GuardianConnectionModel> guardianConnectionModelList;

    private DependentConnectionAdapter dependentConnectionAdapter;
    private List<DependentConnectionModel> dependentConnectionModelList;


    public static FriendsFragment newInstance() {
        FriendsFragment fragment = new FriendsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        if (binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.friendslayout, container, false);
            context = inflater.getContext();
            initView();
        }
        return binding.getRoot();
    }

    private void initView() {

        Config.CONNECTION_APPLY = false;

        loadingDialog = new LoadingDialog(context);
        session = new Session(context);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        String userID = session.getJson(Config.userProfileData).getString(P.id);

        guardianConnectionModelList = new ArrayList<>();
        guardianConnectionAdapter = new GuardianConnectionAdapter(context, guardianConnectionModelList, FriendsFragment.this);
        binding.recyclerGuardianConnection.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerGuardianConnection.setHasFixedSize(true);
        binding.recyclerGuardianConnection.setNestedScrollingEnabled(false);
        binding.recyclerGuardianConnection.setAdapter(guardianConnectionAdapter);

        dependentConnectionModelList = new ArrayList<>();
        dependentConnectionAdapter = new DependentConnectionAdapter(context, dependentConnectionModelList, userID, FriendsFragment.this);
        binding.recyclerDependentConnection.setLayoutManager(new LinearLayoutManager(context));
        binding.recyclerDependentConnection.setHasFixedSize(true);
        binding.recyclerDependentConnection.setNestedScrollingEnabled(false);
        binding.recyclerDependentConnection.setAdapter(dependentConnectionAdapter);

        hitMyConnection(token);
        onClick();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Config.CONNECTION_APPLY) {
            Config.CONNECTION_APPLY = false;
            hitMyConnection(token);
        }

        if (Config.IS_BLOCK_UNBLOCK) {
            Config.IS_BLOCK_UNBLOCK = false;
            hitMyConnection(token);
        }
    }

    private void clearData() {
        guardianConnectionModelList.clear();
        guardianConnectionAdapter.notifyDataSetChanged();
        dependentConnectionModelList.clear();
        dependentConnectionAdapter.notifyDataSetChanged();
    }

    private void onClick() {

        binding.txtAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DependentAddConnectionActivity.class);
                startActivity(intent);
            }
        });
    }

    //GUARDIAN
    @Override
    public void onAction(GuardianConnectionModel model, TextView txtAction) {
        hitAction(model, token, txtAction);
    }

    //DEPENDENT
    @Override
    public void onConnect(DependentConnectionModel model, TextView txtConnect) {
        hitConnect(model, token, txtConnect);
    }

    @Override
    public void onIgnore(DependentConnectionModel model, TextView txtIgnore) {
        hitIgnore(model, token, txtIgnore);
    }

    @Override
    public void onRemove(DependentConnectionModel model, TextView button) {
        hitRemove(model, token, button);
    }

    private void hitMyConnection(String token) {

        ProgressView.show(context, loadingDialog);
        Api.newApi(context, API.BaseUrl + "dependent/connection/my_connections")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    clearData();

                    if (json.getInt(P.status) == 1) {

                        Json data = json.getJson(P.data);
                        String profile_pic_path = data.getString(P.profile_pic_path);
                        JsonList guardians_connection = data.getJsonList(P.guardians_connection);
                        JsonList dependent_connection = data.getJsonList(P.dependent_connection);

                        if (guardians_connection != null && guardians_connection.size() != 0) {
                            for (Json jsonValue : guardians_connection) {
                                GuardianConnectionModel model = new GuardianConnectionModel();
                                model.setConnection_id(jsonValue.getString(P.connection_id));
                                model.setGuardian_id(jsonValue.getString(P.guardian_id));
                                model.setGuardian_name(jsonValue.getString(P.guardian_name));
                                model.setIs_accepted(jsonValue.getString(P.is_accepted));
                                model.setProfile_pic(profile_pic_path + jsonValue.getString(P.profile_pic));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setCity(jsonValue.getString(P.city));
                                model.setAge(jsonValue.getString(P.Age));
                                model.setConnection_time(jsonValue.getString(P.connection_time));
                                guardianConnectionModelList.add(model);
                            }
                            guardianConnectionAdapter.notifyDataSetChanged();
                        }

                        if (dependent_connection != null && dependent_connection.size() != 0) {
                            for (Json jsonValue : dependent_connection) {
                                DependentConnectionModel model = new DependentConnectionModel();
                                model.setConnection_id(jsonValue.getString(P.connection_id));
                                model.setFrom_dependent_id(jsonValue.getString(P.from_dependent_id));
                                model.setFrom_dependent_name(jsonValue.getString(P.from_dependent_name));
                                model.setTo_dependent_id(jsonValue.getString(P.to_dependent_id));
                                model.setTo_dependent_name(jsonValue.getString(P.to_dependent_name));
                                model.setIs_accepted(jsonValue.getString(P.is_accepted));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setCity(jsonValue.getString(P.city));
                                model.setAge(jsonValue.getString(P.Age));
                                model.setConnection_time(jsonValue.getString(P.connection_time));
                                model.setFrom_username(jsonValue.getString(P.from_username));
                                model.setTo_dependent_username(jsonValue.getString(P.to_dependent_username));
                                try {
                                    model.setProfile_pic(profile_pic_path + jsonValue.getString(P.profile_pic));
                                } catch (Exception e) {
                                    model.setProfile_pic(profile_pic_path);
                                }
                                dependentConnectionModelList.add(model);
                            }
                            dependentConnectionAdapter.notifyDataSetChanged();
                        }

                        if (dependentConnectionModelList.isEmpty()) {
                            binding.txtDependentConnection.setVisibility(View.GONE);
                            binding.recyclerDependentConnection.setVisibility(View.GONE);
                        }

                        if (guardianConnectionModelList.isEmpty()) {
                            binding.txtGuardianConnection.setVisibility(View.GONE);
                            binding.recyclerGuardianConnection.setVisibility(View.GONE);
                        }

                        if (guardianConnectionModelList.isEmpty() && dependentConnectionModelList.isEmpty()) {
                            binding.txtError.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtError.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitMyConnection", token);
    }

    private void hitConnect(DependentConnectionModel model, String token, TextView txtConnect) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.to_dependent_id, model.getTo_dependent_id());

        Api.newApi(context, API.BaseUrl + "dependent/connection/connect_dependent").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        txtConnect.setText("Requested");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitConnect", token);
    }

    private void hitIgnore(DependentConnectionModel model, String token, TextView txtIgnore) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.from_dependent_id, model.getFrom_dependent_id());
        j.addString(P.connection_id, model.getConnection_id());

        if (txtIgnore.getText().toString().equals("Ignore")) {
            j.addString(P.action_id, "2");
        } else if (txtIgnore.getText().toString().equals("Accept")) {
            j.addString(P.action_id, "1");
        }

        Api.newApi(context, API.BaseUrl + "dependent/connection/dependent_connection_action").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        if (txtIgnore.getText().toString().equals("Ignore")) {
                            txtIgnore.setText("Accept");
                        } else if (txtIgnore.getText().toString().equals("Accept")) {
                            txtIgnore.setText("Ignore");
                        }
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitIgnore", token);
    }

    private void hitRemove(DependentConnectionModel model, String token, TextView textView) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, model.getConnection_id());

        Api.newApi(context, API.BaseUrl + "dependent/connection/remove_dependent_connection").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        textView.setText("Remove Connection Requested");
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("hitRemove", token);
    }


    private void hitAction(GuardianConnectionModel model, String token, TextView txtAction) {

        ProgressView.show(context, loadingDialog);
        Json j = new Json();
        j.addString(P.connection_id, model.getConnection_id());

        if (txtAction.getText().toString().equals("Accept")) {
            j.addString(P.action_id, "1");
        } else if (txtAction.getText().toString().equals("Reject")) {
            j.addString(P.action_id, "2");
        }

        Api.newApi(context, API.BaseUrl + "dependent/connection/guardidan_connection_action").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(context, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        H.showMessage(context, json.getString(P.msg));
                        if (txtAction.getText().toString().equals("Accept")) {
                            txtAction.setText("Reject");
                        } else if (txtAction.getText().toString().equals("Reject")) {
                            txtAction.setText("Accept");
                        }
                    } else {
                        H.showMessage(context, json.getString(P.error));
                    }
                })
                .run("onAction", token);
    }

}
