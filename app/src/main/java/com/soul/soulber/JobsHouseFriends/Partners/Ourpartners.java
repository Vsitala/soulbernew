package com.soul.soulber.JobsHouseFriends.Partners;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.OurPartnersAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityOurpartnersBinding;
import com.soul.soulber.model.PartnerModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;

public class Ourpartners extends AppCompatActivity {

    private Ourpartners activity = this;
    private ActivityOurpartnersBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    GridLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private List<PartnerModel> partnerModelList;
    private OurPartnersAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ourpartners);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_ourpartners);
        initView();
    }

    private void initView(){

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        partnerModelList = new ArrayList<>();
        adapter =  new OurPartnersAdapter(activity,partnerModelList);
        linearLayoutManager = new GridLayoutManager(activity,2);
        binding.recyclerPartners.setLayoutManager(linearLayoutManager);
        binding.recyclerPartners.setItemViewCacheSize(partnerModelList.size());
        binding.recyclerPartners.setHasFixedSize(true);
        binding.recyclerPartners.setAdapter(adapter);

        hitPartnersData(pageCount);
        setPagination();
        onClick();
    }


    private void onClick(){
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setPagination(){
        binding.recyclerPartners.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)){
                    loading = false;
                    if (partnerModelList!=null && !partnerModelList.isEmpty()){
                        Log.e("TAG", "onScrolled: "+ count + "  " + partnerModelList.size() );
                        if (partnerModelList.size()<count){
                            pageCount++;
                            hitPartnersData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitPartnersData(int pageCount) {

        ProgressView.show(activity, loadingDialog);

        Api.newApi(activity, API.BaseUrl + "job/partner_list?page="+pageCount+"&per_page=20")
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String partner_logo_path = data.getString(P.partner_logo_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        }catch (Exception e){
                            count = 0;
                        }

                        JsonList partners = data.getJsonList(P.partners);
                        if (partners!=null && partners.size()!=0){
                            for (Json jsonValue : partners){
                                PartnerModel model = new PartnerModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setPartner(jsonValue.getString(P.partner));
                                model.setLogo(partner_logo_path+jsonValue.getString(P.logo));
                                model.setAbout_partner(jsonValue.getString(P.about_partner));
                                partnerModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitPartnersData", token);
    }
}