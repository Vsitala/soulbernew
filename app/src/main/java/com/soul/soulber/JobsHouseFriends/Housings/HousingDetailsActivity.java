package com.soul.soulber.JobsHouseFriends.Housings;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.AmenitiesAdapter;
import com.soul.soulber.adapter.HousingImageAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityHousingdetailsBinding;
import com.soul.soulber.model.AmenitiesModel;
import com.soul.soulber.model.ImageModel;
import com.soul.soulber.util.ProgressView;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class HousingDetailsActivity extends AppCompatActivity {

    private HousingDetailsActivity activity = this;
    private ActivityHousingdetailsBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private String contact = "";
    private String latitude = "";
    private String longitude = "";
    private String address = "";
    private String availability = "";
    private String is_booked = "";

    private HousingImageAdapter housingImageAdapter;
    private List<ImageModel> imageModels;

    private List<AmenitiesModel> amenitiesModelList;
    private AmenitiesAdapter amenitiesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_housingdetails);
        initView();
    }

    private void initView() {

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        String housingID = Config.HOUSING_ID;

        imageModels = new ArrayList<>();
        housingImageAdapter = new HousingImageAdapter(activity, imageModels, 2);
        binding.recyclerHousingImages.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
        binding.recyclerHousingImages.setHasFixedSize(true);
        binding.recyclerHousingImages.setItemViewCacheSize(imageModels.size());
        binding.recyclerHousingImages.setAdapter(housingImageAdapter);

        amenitiesModelList = new ArrayList<>();
        amenitiesAdapter = new AmenitiesAdapter(activity, amenitiesModelList);
        binding.recyclerAmenities.setLayoutManager(new GridLayoutManager(activity, 2));
        binding.recyclerAmenities.setHasFixedSize(true);
        binding.recyclerAmenities.setItemViewCacheSize(amenitiesModelList.size());
        binding.recyclerAmenities.setAdapter(amenitiesAdapter);

        onClick(housingID);
        hitHousingDetails(housingID);
    }

    private void onClick(String housingID) {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (TextUtils.isEmpty(checkString(contact))) {
                        H.showMessage(activity, "No contact available, try after some time");
                    } else {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + contact + ""));
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    H.showMessage(activity, "Something went wrong, try after some time");
                }
            }
        });

        binding.imgWhatsppCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (TextUtils.isEmpty(checkString(contact))) {
                        H.showMessage(activity, "No contact available, try after some time");
                    } else {

//                        Uri uri = Uri.parse("smsto:" + model.getAgent_contact());
//                        Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
//                        intent.setPackage("com.whatsapp");
//                        context.startActivity(Intent.createChooser(intent, ""));

                        openWhatsAppConversation(contact, "");
                    }
                } catch (Exception e) {
                    H.showMessage(activity, "Something went wrong, try after some time");
                }
            }
        });


        binding.txtLocateOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

//                    Uri mapUri = Uri.parse("geo:0,0?q=lat,lng(label)");
//                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
//                    mapIntent.setPackage("com.google.android.apps.maps");
//                    startActivity(mapIntent);

                    Uri mapUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    startActivity(mapIntent);

                } catch (Exception e) {
                    H.showMessage(activity, "Something went wrong, try again");
                }
            }
        });

        binding.btnBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_booked.equals("1")) {
                    hitHousingStatus(housingID);
                }
            }
        });
    }

    private void hitHousingDetails(String house_id) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.house_id, house_id);
        Api.newApi(activity, API.BaseUrl + "housing/house_details").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);

                        String house_images_path = checkString(data.getString(P.house_images_path));
                        String total_images = checkString(data.getString(P.total_images));
                        String amenities = checkString(data.getString(P.amenities));
                        contact = checkString(data.getString(P.agent_contact));
                        latitude = checkString(data.getString(P.latitude));
                        address = checkString(data.getString(P.address));
                        longitude = checkString(data.getString(P.longitude));
                        availability = checkString(data.getString(P.availability));
                        binding.txtType.setText(checkString(data.getString(P.bedrooms) + "BHK"));
                        binding.txtPrice.setText(checkString(data.getString(P.price_range)));
                        binding.txtPhotoCount.setText(checkString(total_images + " photos"));
                        binding.ttxAgentName.setText(checkString(data.getString(P.agent_name)));
                        binding.txtCityName.setText(checkString(data.getString(P.house_location) + " City"));
                        binding.txtAddress.setText(checkString(data.getString(P.address)));
                        binding.btnBookNow.setText(checkString(data.getString(P.button_name)));

                        is_booked = data.getString(P.booking_allowed);

                        if (availability.equals("1")) {
                            binding.txtMessage.setVisibility(View.VISIBLE);
                        } else {
                            binding.txtMessage.setVisibility(View.GONE);
                        }

                        if (TextUtils.isEmpty(checkString(address))) {
                            binding.txtLocateOnMap.setVisibility(View.GONE);
                        }

                        JSONArray jsonArray = data.getJsonArray(P.images);
                        try {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                String image = house_images_path + jsonArray.getString(i);
                                ImageModel imageModel = new ImageModel();
                                imageModel.setImage(image);
                                imageModels.add(imageModel);
                            }
                            housingImageAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            binding.recyclerHousingImages.setVisibility(View.GONE);
                        }

                        if (!amenities.equals("")) {
                            String[] items = amenities.split(",");
                            for (String item : items) {
                                AmenitiesModel model = new AmenitiesModel();
                                model.setAmenities(item);
                                amenitiesModelList.add(model);
                            }
                            amenitiesAdapter.notifyDataSetChanged();
                        } else {
                            binding.txtAmenitiesTitle.setVisibility(View.GONE);
                            binding.recyclerAmenities.setVisibility(View.GONE);
                        }

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitHousingDetails", token);
    }

    private void hitHousingStatus(String house_id) {

        ProgressView.show(activity, loadingDialog);

        Json j = new Json();
        j.addString(P.house_id, house_id);
        Api.newApi(activity, API.BaseUrl + "housing/book_house").addJson(j)
                .setMethod(Api.POST)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {

                        binding.btnBookNow.setText("Requested");
                        is_booked = "0";

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                    }
                })
                .run("hitHousingStatus", token);
    }

    private String checkString(String string) {
        String value = "";
        if (string == null || string.equals("") || string.equals("null")) {
            value = "";
        } else {
            value = string;
        }
        return value;
    }

    public void openWhatsAppConversation(String number, String message) {

        try {
            number = number.replace(" ", "").replace("+", "");

            Intent sendIntent = new Intent("android.intent.action.MAIN");

            sendIntent.setType("text/plain");
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");

            startActivity(sendIntent);
        } catch (Exception e) {
            H.showMessage(activity, "Unable to open whatsapp conversation");
        }

    }

}