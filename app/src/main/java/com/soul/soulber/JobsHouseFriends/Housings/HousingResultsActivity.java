package com.soul.soulber.JobsHouseFriends.Housings;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.adoisstudio.helper.Api;
import com.adoisstudio.helper.H;
import com.adoisstudio.helper.Json;
import com.adoisstudio.helper.JsonList;
import com.adoisstudio.helper.LoadingDialog;
import com.adoisstudio.helper.Session;
import com.soul.soulber.R;
import com.soul.soulber.adapter.HousingAdapter;
import com.soul.soulber.api.API;
import com.soul.soulber.api.Config;
import com.soul.soulber.api.P;
import com.soul.soulber.databinding.ActivityHousingResultBinding;
import com.soul.soulber.model.HousingModel;
import com.soul.soulber.util.ProgressView;

import java.util.ArrayList;
import java.util.List;


public class HousingResultsActivity extends AppCompatActivity {

    private HousingResultsActivity activity = this;
    private ActivityHousingResultBinding binding;

    private LoadingDialog loadingDialog;
    private Session session;
    private String usertype_id;
    private String token;
    private String userID;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager linearLayoutManager;
    int count;
    int pageCount = 1;

    private List<HousingModel> housingModelList;
    private HousingAdapter adapter;

    String locationId = "";
    String locationName = "";
    String searchLat = "";
    String searchLogn = "";
    String searchAdd = "";

    String bedrooms = "";
    String bathrooms = "";
    String pets_friendly = "";
    String price_range = "";
    String amenities = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_housing_result);

        initView();
    }

    private void initView() {

        Config.IS_HOUSING_FILTER = false;
        Config.HOUSING_BEDROOM = "";
        Config.HOUSING_BATHROOM = "";
        Config.HOUSING_PET_FRIENDLY = "";
        Config.HOUSING_PRICE_RANCE = "";
        Config.HOUSING_AMENITIES = "";

        Config.HOUSING_BEDROOM_TEXT = "";
        Config.HOUSING_BATHROOM_TEXT = "";
        Config.HOUSING_PET_FRIENDLY_TEXT = "";
        Config.HOUSING_PRICE_RANCE_TEXT = "";
        Config.HOUSING_AMENITIES_TEXT = "";

        loadingDialog = new LoadingDialog(activity);
        session = new Session(activity);

        token = session.getJson(Config.userData).getString(P.user_token);
        usertype_id = session.getJson(Config.userData).getString(P.usertype_id);
        userID = session.getJson(Config.userProfileData).getString(P.id);

        locationId = Config.HOUSING_LOCATION_ID;
        locationName = Config.HOUSING_LOCATION;
        searchLat = Config.HOUSING_LAT;
        searchLogn = Config.HOUSING_LOGN;
        searchAdd = Config.HOUSING_SEARCH_ADD;

        binding.txtCity.setText("List of apartments in " + searchAdd);

        housingModelList = new ArrayList<>();
        adapter = new HousingAdapter(activity, housingModelList);
        linearLayoutManager = new LinearLayoutManager(activity);
        binding.recyclerHousing.setLayoutManager(linearLayoutManager);
        binding.recyclerHousing.setItemViewCacheSize(housingModelList.size());
        binding.recyclerHousing.setHasFixedSize(true);
        binding.recyclerHousing.setAdapter(adapter);

        onClick();
        hitHousingData(pageCount);
        setPagination();

    }

    private void onClick() {

        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, HousingFilterActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Config.IS_HOUSING_FILTER) {
            Config.IS_HOUSING_FILTER = false;
            bedrooms = Config.HOUSING_BEDROOM;
            bathrooms = Config.HOUSING_BATHROOM;
            pets_friendly = Config.HOUSING_PET_FRIENDLY;
            price_range = Config.HOUSING_PRICE_RANCE;
            amenities = Config.HOUSING_AMENITIES;
            pageCount = 1;
            hitHousingData(pageCount);
        }
    }

    private void setPagination() {
        binding.recyclerHousing.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    loading = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                if (loading && (visibleItemCount + pastVisiblesItems == totalItemCount)) {
                    loading = false;
                    if (housingModelList != null && !housingModelList.isEmpty()) {
                        if (housingModelList.size() < count) {
                            pageCount++;
                            hitHousingData(pageCount);
                        }
                    }
                }
            }
        });
    }

    private void hitHousingData(int pageCount) {

        ProgressView.show(activity, loadingDialog);
        Api.newApi(activity, API.BaseUrl + "housing/housing_list?page=" + pageCount + "&per_page=20" +
                "&bedrooms=" + bedrooms + "&bathrooms=" + bathrooms + "&pets_friendly=" + pets_friendly + "&price_range=" + price_range + "&amenities=" + amenities +
                "&lt=" + searchLat + "&lng=" + searchLogn)
                .setMethod(Api.GET)
//                .onHeaderRequest(App::getHeaders)
                .onError(() -> {
                    ProgressView.dismiss(loadingDialog);
                    H.showMessage(activity, "On error is called");
                    checkData();
                })
                .onSuccess(json ->
                {
                    ProgressView.dismiss(loadingDialog);

                    if (json.getInt(P.status) == 1) {
                        Json data = json.getJson(P.data);
                        String house_images_path = data.getString(P.house_images_path);
                        int num_rows = data.getInt(P.num_rows);
                        try {
                            count = num_rows;
                        } catch (Exception e) {
                            count = 0;
                        }

                        if (pageCount == 1) {
                            housingModelList.clear();
                            adapter.notifyDataSetChanged();
                        }

                        JsonList houses = data.getJsonList(P.houses);
                        if (houses != null && houses.size() != 0) {
                            for (Json jsonValue : houses) {
                                HousingModel model = new HousingModel();
                                model.setId(jsonValue.getString(P.id));
                                model.setAgent_name(jsonValue.getString(P.agent_name));
                                model.setAgent_contact(jsonValue.getString(P.agent_contact));
                                model.setBedrooms(jsonValue.getString(P.bedrooms));
                                model.setBathrooms(jsonValue.getString(P.bathrooms));
                                model.setPrice_range(jsonValue.getString(P.price_range));
                                model.setCity(jsonValue.getString(P.city));
                                model.setLatitude(jsonValue.getString(P.latitude));
                                model.setLongitude(jsonValue.getString(P.longitude));
                                model.setAddress(jsonValue.getString(P.address));
                                model.setAvailability(jsonValue.getString(P.availability));
                                model.setPets_friendly(jsonValue.getString(P.pets_friendly));
                                model.setStatus(jsonValue.getString(P.status));
                                model.setDeleted(jsonValue.getString(P.deleted));
                                model.setAdd_date(jsonValue.getString(P.add_date));
                                model.setUpdate_date(jsonValue.getString(P.update_date));
                                model.setDelete_date(jsonValue.getString(P.delete_date));
                                model.setHouse_location(jsonValue.getString(P.house_location));
                                model.setAmenities(jsonValue.getString(P.amenities));
                                model.setTotal_images(jsonValue.getString(P.total_images));
                                model.setImagePath(house_images_path);
                                model.setImages(jsonValue.getJsonArray(P.images));
                                housingModelList.add(model);
                            }
                            adapter.notifyDataSetChanged();

                            if (housingModelList.size() == 0) {
                                binding.txtCount.setText("Apartments 0");
                            } else {
                                binding.txtCount.setText("Apartments 1-" + housingModelList.size() + " of " + count);
                            }
                        }

                        checkData();

                    } else {
                        H.showMessage(activity, json.getString(P.error));
                        checkData();
                    }
                })
                .run("hitHousingData", token);
    }

    private void checkData() {
        if (housingModelList.isEmpty()) {
            binding.txtJobListError.setVisibility(View.VISIBLE);
        } else {
            binding.txtJobListError.setVisibility(View.GONE);
        }
    }

}