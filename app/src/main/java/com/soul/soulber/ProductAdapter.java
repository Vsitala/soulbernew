package com.soul.soulber;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;


public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private ArrayList<Productmodle> product;
    private Context context;
    private int selectedItem;
    public ProductAdapter(Context context,ArrayList<Productmodle> product) {
        this.product = product;
        this.context = context;
        selectedItem=0;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.productcard_view, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder viewHolder, int position) {

        viewHolder.productprice.setText(product.get(position).getName());


        Glide
                .with(context)

                .load(product.get(position).getImage())
                //cropping center image
                .into(viewHolder.img_android);





//        if (selectedItem == position) {
//            viewHolder.addtofavorate.setImageResource(R.drawable.heart);
//
//        }
//        else
//        {
//            viewHolder.addtofavorate.setBackgroundColor(Color.TRANSPARENT);
//        }
//        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int previousItem = selectedItem;
//                selectedItem = position;
//
//               notifyItemChanged(previousItem);
//               notifyItemChanged(position);
//           }
//       });



    }

    @Override
    public int getItemCount() {
        return product.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView img_android;
        private TextView productprice;

        public ViewHolder(View view) {
            super(view);
            productprice=view.findViewById(R.id.productprice);

            img_android =  view.findViewById(R.id.productimage);

        }

    }

}
