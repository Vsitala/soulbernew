package com.soul.soulber.Updates;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.soul.soulber.R;

import java.util.ArrayList;


public class Updateadapter extends RecyclerView.Adapter<Updateadapter.ViewHolder> {
    private ArrayList<Update_modle> userlist;
    ArrayList<Commentsmodleclass> commenlist;
    private Context context;
    private int selectedItem;
    private int row_index = -1;
    Boolean clicked = true;
    boolean doubleClick = false;
    Commentsadapter commentsadapter;
    String parentcomment[] = {"John Doe Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt utlabore et", "John Doe Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt utlabore et"};

    public Updateadapter(Context context, ArrayList<Update_modle> userlist) {
        this.userlist = userlist;
        this.context = context;
        selectedItem = 0;
    }

    @Override
    public Updateadapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.updates_item, viewGroup, false);
        return new Updateadapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Updateadapter.ViewHolder viewHolder, int position) {
        viewHolder.discription.setText(userlist.get(position).getName());


        Glide
                .with(context)
                .load(userlist.get(position).getImage())
                //cropping center image
                .into(viewHolder.uploadedimage);
        Glide
                .with(context)

                .load(userlist.get(position).getShareimageprofile())
                //cropping center image
                .into(viewHolder.shareprofile);
        viewHolder.uploadedimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Handler handler = new Handler();
                Runnable r = new Runnable() {
                    @Override
                    public void run() {

                        doubleClick = false;
                    }
                };

                if (doubleClick) {
                    Toast.makeText(context, "Liked", Toast.LENGTH_SHORT).show();
                    viewHolder.liked.setImageResource(R.drawable.likecount);
                    //your logic for double click action
                    doubleClick = false;

                } else {
                    doubleClick = true;
                    handler.postDelayed(r, 500);
                }


            }
        });
        viewHolder.postcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.rv_comments.setVisibility(View.VISIBLE);
                for (int i = 0; i < 1; i++) {
                    Commentsmodleclass commentsmodleclass = new Commentsmodleclass();
                    commentsmodleclass.setComment(parentcomment[i]);
                    commenlist.add(commentsmodleclass);
                }
                commentsadapter = new Commentsadapter(context, commenlist);
                viewHolder.rv_comments.setAdapter(commentsadapter);
                commentsadapter.notifyDataSetChanged();
//                commenlist.add( viewHolder.commenttext.getText().toString());
            }
        });
        viewHolder.comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.commentlayout.setVisibility(View.VISIBLE);
                commenlist = new ArrayList<>();
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                viewHolder.rv_comments.setLayoutManager(linearLayoutManager);
            }
        });
        viewHolder.liked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clicked) {
                    clicked = false;
                    viewHolder.liked.setImageResource(R.drawable.likecount);
                    Toast.makeText(context, "liked", Toast.LENGTH_SHORT).show();
                } else {
                    clicked = true;
                    viewHolder.liked.setImageResource(R.drawable.like_heart);
                    Toast.makeText(context, "unliked", Toast.LENGTH_SHORT).show();
                }


//                    if (userlist.get(viewHolder.getAdapterPosition())
//                            .getisliked.equals("true")) {
//                        //update unlike drawable
//                        userlist.get(viewHolder.getAdapterPosition()).setGetisliked(false);
//                        notifyItemChanged(viewHolder.getAdapterPosition(), "preunlike");
//                    } else {
//                        //update like drawable
//                        userlist.get(viewHolder.getAdapterPosition()).setGetisliked(true);
//                        notifyItemChanged(viewHolder.getAdapterPosition(), "prelike");
//                    }
//                    //make network request
//                    updateLike(viewHolder.getAdapterPosition());
            }
        });


        viewHolder.shareprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, userlist.get(position).getImage());
                sendIntent.setType("image/png");
                Intent shareIntent = Intent.createChooser(sendIntent, null);
                shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(shareIntent);


            }
        });

    }

    private void updateLike(int adapterPosition) {


    }

    @Override
    public int getItemCount() {
        return userlist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView uploadedimage, shareprofile, direct, comment, liked;
        private TextView discription, postcomment;
        private EditText commenttext;
        private RecyclerView rv_comments;
        private LinearLayout commentlayout;

        public ViewHolder(View view) {
            super(view);
            postcomment = view.findViewById(R.id.postcomment);
            commenttext = view.findViewById(R.id.commenttext);
            discription = view.findViewById(R.id.discription);
            uploadedimage = view.findViewById(R.id.uploadedimage);
            shareprofile = view.findViewById(R.id.shareprofile);
            comment = view.findViewById(R.id.comment);
            commentlayout = view.findViewById(R.id.commentlayout);
            liked = view.findViewById(R.id.liked);
            rv_comments = view.findViewById(R.id.rv_comments);
        }

    }

}

