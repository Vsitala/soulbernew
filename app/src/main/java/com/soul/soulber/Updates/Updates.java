package com.soul.soulber.Updates;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.R;

import java.util.ArrayList;

public class Updates extends AppCompatActivity {
    RecyclerView rv_updates;
    ArrayList<Update_modle> uselist;
    Updateadapter updateadapter;
    ImageView onback_updates;
    int shareimage[]={R.drawable.sharelink};

    String discription[] = {"In nature, nothing is perfect and everything is perfect. Trees can be contorted, bent in weird ways, and they’re still beautiful",
            "Trailblazin’ through the weekend",
            "If we surrendered to earth’s intelligence we could rise up rooted, like trees.” – Rainer Maria Rilke",
            "The difference between “hill” and “hell” isn’t a fine line",
            "Fashion is temporary, but nature never goes out of style",
            "Nature is the closest place to Heaven on Earth",
            "iamdude In nature, nothing is perfect and everything is perfect. Trees can be contorted, bent in weird ways, and they’re still beautiful",
            "Trailblazin’ through the weekend",
            "If we surrendered to earth’s intelligence we could rise up rooted, like trees.” – Rainer Maria Rilke",
            "The difference between “hill” and “hell” isn’t a fine line",
            "Fashion is temporary, but nature never goes out of style",
            "Nature is the closest place to Heaven on Earth"};
    int image[] = {R.drawable.updates_pic2, R.drawable.updates_pic1,R.drawable.updates_pic2, R.drawable.updates_pic1, R.drawable.updates_pic2, R.drawable.updates_pic1,
            R.drawable.updates_pic2, R.drawable.updates_pic1,R.drawable.updates_pic2, R.drawable.updates_pic1};
    String name="iamdude";

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updates);
        rv_updates = findViewById(R.id.rv_updates);
        onback_updates=findViewById(R.id.onback_updates);
//        Intent intent = getIntent();
//        postion= intent.getStringExtra("pos");
        LinearLayoutManager layoutmanger = new LinearLayoutManager(Updates.this,LinearLayoutManager.VERTICAL,false);
        rv_updates.setLayoutManager(layoutmanger);
        uselist = new ArrayList<>();

        for (int i = 0; i < image.length; i++) {
            Update_modle update_modlei = new Update_modle();
            update_modlei.setName(name+discription[i]);
            update_modlei.setImage(image[i]);
            update_modlei.setShareimageprofile(shareimage[0]);
            uselist.add(update_modlei);
        }
        updateadapter = new Updateadapter(Updates.this, uselist);
        rv_updates.setAdapter(updateadapter);
        onback_updates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Updates.this, BaseAcivity.class));
                finish();
            }
        });


    }
}