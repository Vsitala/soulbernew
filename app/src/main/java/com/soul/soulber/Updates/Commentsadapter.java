package com.soul.soulber.Updates;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.soul.soulber.ProductAdapter;
import com.soul.soulber.Productmodle;
import com.soul.soulber.R;

import java.util.ArrayList;

    public class Commentsadapter extends RecyclerView.Adapter<Commentsadapter.ViewHolder> {
        private ArrayList<Commentsmodleclass> commentlist;
        private Context context;
        private int selectedItem;
        public Commentsadapter(Context context,ArrayList<Commentsmodleclass> commentlist) {
            this.commentlist = commentlist;
            this.context = context;
            selectedItem=0;
        }

        @Override
        public Commentsadapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_commentparent, viewGroup, false);
            return new Commentsadapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(Commentsadapter.ViewHolder viewHolder, int position) {
            viewHolder.commentsonpost.setText(commentlist.get(position).getComment());

//            Glide
//                    .with(context)
//
//                    .load(commentlist.get(position).getImage())
//                    //cropping center image
//                    .into(viewHolder.img_android);
        }

        @Override
        public int getItemCount() {
            return commentlist.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{

//            private ImageView img_android;
            private TextView commentsonpost;

            public ViewHolder(View view) {
                super(view);
                commentsonpost=view.findViewById(R.id.commentsonpost);

//                img_android =  view.findViewById(R.id.productimage);

            }

        }

    }


