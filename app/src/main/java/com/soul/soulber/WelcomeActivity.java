package com.soul.soulber;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.soul.soulber.BaseActivity.BaseAcivity;
import com.soul.soulber.Login_Registration.MainActivity;
import com.soul.soulber.databinding.ActivityWelcomeBinding;

import java.util.ArrayList;

public class WelcomeActivity extends AppCompatActivity {

    private ActivityWelcomeBinding binding;
    private WelcomeActivity activity = this;

    private RecyclerView recycleproduct;
    private PagerAdapter myViewPagerAdapter;
    private int[] layouts;
    private View view;
    private ArrayList<Productmodle> productlist;
    private int productimage[] = {R.drawable.check};
    private String productname[] = {"You will become better in stress-coping mechanisms",
            "You will have a better mental health",
            "You will have a better physical health",
            "You will have a better social life", "You will have more success"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);

        productlist = new ArrayList<>();
        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2
        };

        // adding bottom dots
//        addBottomDots(0);

        // making notification bar transparent
//        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        binding.viewPager.setAdapter(myViewPagerAdapter);
        binding.viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        binding.layoutDots.setViewPager(binding.viewPager);

        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page if true launch MainActivity
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    binding.viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });
    }

//    private void addBottomDots(int currentPage) {
//        dots = new TextView[layouts.length];
//
//        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
//        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);
//
//        dotsLayout.removeAllViews();
//        for (int i = 0; i < dots.length; i++) {
//            dots[i] = new TextView(this);
//            dots[i].setText(Html.fromHtml("?"));
//            dots[i].setTextSize(35);
//            dots[i].setTextColor(colorsInactive[currentPage]);
//            dotsLayout.addView(dots[i]);
//        }
//
//        if (dots.length > 0)
//            dots[currentPage].setTextColor(colorsActive[currentPage]);
//    }

    private int getItem(int i) {
        return binding.viewPager.getCurrentItem() + i;
    }

    private void launchHomeScreen() {
//        prefManager.setFirstTimeLaunch(false);
//        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
//        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_left);
//        finish();

        Intent intent = new Intent(activity, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(activity, R.anim.enter_from_right, R.anim.exit_out_left);
        startActivity(intent, options.toBundle());
        finish();

    }


    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
//            addBottomDots(position);


            if (position == layouts.length - 1) {
//                tex=view.findViewById(R.id.productprice);
//                tex.setTextColor(Color.BLACK);

                binding.btnNext.setText(getString(R.string.nextOne));
//                dotsLayout.setSelectedIndicatorColors(R.color.icon_disabled);

            } else {
                // still pages are left
                binding.btnNext.setText(getString(R.string.nextOne));
            }

        }

        @Override
        public void onPageScrolled(int arg0, float positionOffset, int positionOffsetPixels) {
            int x = (int) ((binding.viewPager.getWidth() * arg0 + positionOffsetPixels) * computeFactor());
            binding.scrollView.scrollTo(x, 0);
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

        private float computeFactor() {
            return (binding.imgBackground.getWidth() - binding.viewPager.getWidth()) /
                    (float) (binding.viewPager.getWidth() * (binding.viewPager.getAdapter().getCount() - 1));
        }
    };

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            if (position == 1) {

                recycleproduct = view.findViewById(R.id.recycleproduct);
                recycleproduct.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                recycleproduct.setNestedScrollingEnabled(false);

                for (int i = 0; i < productname.length; i++) {
                    Productmodle product = new Productmodle();
                    product.setName(productname[i]);
                    product.setImage(productimage[0]);
                    productlist.add(product);
                }

                ProductAdapter proadapter = new ProductAdapter(WelcomeActivity.this, productlist);
                recycleproduct.setAdapter(proadapter);
            }

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}

