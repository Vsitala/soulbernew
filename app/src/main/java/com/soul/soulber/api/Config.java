package com.soul.soulber.api;

import android.graphics.Bitmap;
import android.net.Uri;

public class Config {

    public static String COUNTRY = "US";

    //    public static String API_KEY = "AIzaSyCGx_Fl0yKDXwvqekOgYiU78Th3MAWzOxE";
    public static String API_KEY = "AIzaSyCIaXvF71jFZy1Q1ZX-YkBDKDhcDKNsVs0";

    public static String userData = "data";
    public static String userProfileData = "userProfileData";
    public static String userLogin = "userLogin";
    public static String dependentEmail = "dependentEmail";
    public static String registerAmount = "registerAmount";
    public static String registerUserType = "registerUserType";

    public static String DEPENDENT = "1";
    public static String GUARDIAN = "2";

    public static String Unfollow = "Unfollow";
    public static String Follow = "Follow";

    public static String UPLOAD_TYPE_1 = "1";
    public static String UPLOAD_TYPE_2 = "2";
    public static String UPLOAD_TYPE_3 = "3";

    public static String FONT_COLOR_1 = "1";
    public static String FONT_COLOR_2 = "2";
    public static String FONT_COLOR_3 = "3";
    public static String FONT_COLOR_4 = "4";
    public static String FONT_COLOR_5 = "5";

    public static String FONT_STYLE_1 = "1";
    public static String FONT_STYLE_2 = "2";
    public static String FONT_STYLE_3 = "3";
    public static String FONT_STYLE_4 = "4";
    public static String FONT_STYLE_5 = "5";

    public static String POST_BASE_64 = "";
    public static Bitmap POST_BITMAP;
    public static Uri POST_VIDEO_URI;
    public static String POST_VIDEO_BASE64;
    public static boolean FOR_MY_POST = false;

    public static String JOB_ID = "";
    public static boolean UPDATED_BOOKMARK = false;

    public static String CATEGORY_ID = "";
    public static String CATEGORY_NAME = "";
    public static String CATEGORY_LOCATION = "";
    public static String CATEGORY_SKILLS = "";
    public static String CATEGORY_LAT = "";
    public static String CATEGORY_LOGN = "";
    public static boolean FROM_CATEGORY = false;
    public static boolean FROM_TYPE = false;

    public static String MEETING_LOCATION = "";
    public static boolean IS_MEETING_FILTER = false;
    public static String MEETING_FILTER_ID = "";
    public static String MEETING_ID = "";
    public static String MEETING_IMAGE_PATH = "";
    public static String MEETING_LAT = "";
    public static String MEETING_LOGN = "";
    public static String MEETING_ADD = "";

    public static String HOUSING_ID = "";
    public static String HOUSING_IMAGE_PATH = "";
    public static String HOUSING_IMAGE_COUNT = "";
    public static String HOUSING_LOCATION_ID = "";
    public static String HOUSING_LOCATION = "";
    public static String HOUSING_LAT = "";
    public static String HOUSING_LOGN = "";
    public static String HOUSING_SEARCH_ADD = "";
    public static boolean IS_HOUSING_FILTER = false;
    public static String HOUSING_BEDROOM = "";
    public static String HOUSING_BATHROOM = "";
    public static String HOUSING_PET_FRIENDLY = "";
    public static String HOUSING_PRICE_RANCE = "";
    public static String HOUSING_AMENITIES = "";

    public static String HOUSING_BEDROOM_TEXT = "";
    public static String HOUSING_BATHROOM_TEXT = "";
    public static String HOUSING_PET_FRIENDLY_TEXT = "";
    public static String HOUSING_PRICE_RANCE_TEXT = "";
    public static String HOUSING_AMENITIES_TEXT = "";

    public static String THERAPIST_LAT = "";
    public static String THERAPIST_LOGN = "";
    public static String THERAPIST_ADD = "";

    public static String SEARCH_LAT = "";
    public static String SEARCH_LOGN = "";
    public static String SEARCH_ADD = "";
    public static boolean IS_SEARCH = false;

    public static boolean IS_NOTIFICATION_POST = false;
    public static String NOTIFICATION_POST_ID = "";
    public static String NOTIFICATION_CONNECTION_REQUEST = "connection request";
    public static String NOTIFICATION_REQUEST_APPROVED = "Request approved";
    public static String NOTIFICATION_REQUEST_ACCEPTED = "Request accepted";
    public static String NOTIFICATION_NEW_FOLLOWER = "New follower";
    public static String NOTIFICATION_NEW_POST = "new post";
    public static String NOTIFICATION_PANIC = "panic";
    public static String NOTIFICATION_NEW_FORUM = "new forum";
    public static String NOTIFICATION_JOB_APPLICATION = "Job application";
    public static String NOTIFICATION_MEETING_ACTION = "Meeting action";
    public static String NOTIFICATION_HOUSE_BOOKING_ACTION = "House booking action";
    public static String NOTIFICATION_THERAPIST_APPOINTMENT = "Therapist appointment";
    public static String NOTIFICATION_LAB_APPOINTMENT = "Lab appointment";

    public static String LAB_LAT = "";
    public static String LAB_LONG = "";
    public static String LAB_ADD = "";

    public static boolean CONNECTION_APPLY = false;
    public static boolean IS_BLOCK_UNBLOCK = false;

    public static int OPEN = 1;
    public static int SHARE = 2;

    public static boolean FORUM_ACTION = false;
    public static boolean FROM_APPLIED_JOB = false;

    public static boolean FOR_THERAPIST = false;

}
